{
    "id": "7b0d0ef5-24d2-4fb5-9516-9534664e077b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "drag",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "fef197e1-8da0-44e0-9e79-c2e49bfb6e71",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "20983f81-1141-4195-842e-b45b1c9bc597",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "79fbb180-ef1f-4dee-a967-e7ba7c20bcc8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9ec68e5a-0072-4801-b237-1f69f89555dd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "fd38386d-e5bf-4d21-9e74-f3315ce5be35",
    "visible": true
}