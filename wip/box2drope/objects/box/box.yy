{
    "id": "026d7bd4-201d-4db4-8e6c-0244a1f7e781",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "box",
    "eventList": [
        {
            "id": "03b4babe-d6f5-4943-acea-bfcff9bf13ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "026d7bd4-201d-4db4-8e6c-0244a1f7e781"
        },
        {
            "id": "e999dd08-88f1-4c22-ba15-532e5354396d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "026d7bd4-201d-4db4-8e6c-0244a1f7e781"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 2,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "c26b05ab-6af9-451e-acc9-71d6891f6a8a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 5
        },
        {
            "id": "01fccb19-1f29-4b71-911e-d6c3e7b6c91c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 5
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "dd6905c9-1757-44db-b503-87ac969b7b9b",
    "visible": true
}