{
    "id": "e993e37e-b3f2-452a-91a7-a3724ec5e8b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "rope",
    "eventList": [
        {
            "id": "93c516a3-14b4-4382-979f-1cf36e3bd224",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e993e37e-b3f2-452a-91a7-a3724ec5e8b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "3f57b4ea-eec0-47e1-9edc-23f556873ca1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "5011512b-7b73-4013-b159-dea8c35f0ae4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 1,
            "y": 0
        },
        {
            "id": "31f80066-339a-4fb5-ac0c-bca32e840122",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 1,
            "y": 1
        },
        {
            "id": "9e5d1582-dc16-46b5-ae4a-2ed1fc80011e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 1
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "3decf5e5-fff2-4a53-bdae-44badbd81be3",
    "visible": true
}