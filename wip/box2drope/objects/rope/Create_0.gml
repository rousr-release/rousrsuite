///@desc 
Links = ds_list_create();

var offsety = y+1;
var offsettest = y + 1;

var s_size = sprite_get_height(sprite_rope_link);
var s_half = (s_size / 2);

var link = instance_create_depth(x, y+1, depth, rope_link);
var jnt = physics_joint_revolute_create(id, link, x, y, 0, 0, false, 0, 0, 0, 0);

//var jnt = physics_joint_rope_create(id, link, x, y, link.x, link.y + 2, 1, false);
//physics_joint_set_value(jnt, phy_joint_damping_ratio, 1);
//physics_joint_set_value(jnt, phy_joint_frequency, 30);

ds_list_add(Links, link);

repeat(15) {
  offsety += s_size;
  var lastLink = link;
  link = instance_create_depth(x, offsety, depth, rope_link);

  jnt = physics_joint_revolute_create(link, lastLink, lastLink.x, lastLink.y + s_size, 0, 0, false, 0, 0, 0, 0);
  //jnt = physics_joint_rope_create(lastLink, link, lastLink.x, lastLink.y + s_size - 2, link.x, link.y+2, 1, false);
//  physics_joint_set_value(jnt, phy_joint_damping_ratio, 1);
//  physics_joint_set_value(jnt, phy_joint_frequency, 30);
  
  ds_list_add(Links, link);
}
offsety += sprite_get_height(sprite_rope_link);
lastLink = link;
link = instance_create_depth(x, offsety, depth, box);
jnt = physics_joint_revolute_create(link, lastLink, lastLink.x, lastLink.y + s_size, 0, 0, false, 0, 0, 0, 0);
//physics_joint_rope_create(lastLink, link, lastLink.x, lastLink.y + s_size - 1, link.x, link.y + 1, 1, false);
//physics_joint_set_value(jnt, phy_joint_damping_ratio, 1);
//physics_joint_set_value(jnt, phy_joint_frequency, 30);


var flag = phy_debug_render_aabb | phy_debug_render_collision_pairs | phy_debug_render_obb;
physics_world_draw_debug(flag);