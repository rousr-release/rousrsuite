{
    "id": "88dda661-ec36-40b3-a9de-f55f639d7c47",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "rope_link",
    "eventList": [
        {
            "id": "2e17a86c-8cd3-447b-9eae-2d73adabb707",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "88dda661-ec36-40b3-a9de-f55f639d7c47"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 50,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "67e5bcdf-7c66-49bd-a0eb-556b5c8cedeb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "3a1e837c-1bf4-4296-b4ad-ae8f7cfc19d7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 1,
            "y": 0
        },
        {
            "id": "58d51e71-ba64-46c7-9822-6736d5d7abb9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 1,
            "y": 5
        },
        {
            "id": "a2d0dba0-9b88-4c59-b159-f1d93b09354a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 5
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "46e632fb-e0bd-437a-9073-e83f53553987",
    "visible": true
}