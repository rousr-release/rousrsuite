{
    "id": "fd38386d-e5bf-4d21-9e74-f3315ce5be35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pixel_alpha",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "21484ac1-d219-4901-96be-fee3d3ee8278",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd38386d-e5bf-4d21-9e74-f3315ce5be35",
            "compositeImage": {
                "id": "e8c88b2d-fc1c-4545-98a6-d8d271494852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21484ac1-d219-4901-96be-fee3d3ee8278",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a60813-6891-43aa-b899-3213c5b77db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21484ac1-d219-4901-96be-fee3d3ee8278",
                    "LayerId": "38f31e5d-d33d-4caf-bbeb-c29c108ab48a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "38f31e5d-d33d-4caf-bbeb-c29c108ab48a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd38386d-e5bf-4d21-9e74-f3315ce5be35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}