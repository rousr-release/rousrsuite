{
    "id": "46e632fb-e0bd-437a-9073-e83f53553987",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_rope_link",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9f445c63-d87f-4bc4-ba25-d1fcea910195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46e632fb-e0bd-437a-9073-e83f53553987",
            "compositeImage": {
                "id": "c46aaad1-fc4a-4f84-b27a-c878a50f73df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f445c63-d87f-4bc4-ba25-d1fcea910195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceb7ed6a-49d7-40f8-bed0-89ebbac19824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f445c63-d87f-4bc4-ba25-d1fcea910195",
                    "LayerId": "de76999f-7e2d-4d55-ad80-08dc34380b38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "de76999f-7e2d-4d55-ad80-08dc34380b38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46e632fb-e0bd-437a-9073-e83f53553987",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}