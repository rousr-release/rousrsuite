{
    "id": "dd6905c9-1757-44db-b503-87ac969b7b9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6da26320-a527-45db-a046-c804cbccb2ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd6905c9-1757-44db-b503-87ac969b7b9b",
            "compositeImage": {
                "id": "73805d56-8b36-49f6-b51f-7c308bf2e84d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6da26320-a527-45db-a046-c804cbccb2ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee7d6576-3021-4d5d-aab4-5a50681df8ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6da26320-a527-45db-a046-c804cbccb2ba",
                    "LayerId": "320aced8-569e-4423-a6b1-26c8efddeddd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "320aced8-569e-4423-a6b1-26c8efddeddd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd6905c9-1757-44db-b503-87ac969b7b9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}