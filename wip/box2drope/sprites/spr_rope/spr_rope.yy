{
    "id": "3decf5e5-fff2-4a53-bdae-44badbd81be3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2429b60f-24ba-4d6b-ae33-b1d72ff8494f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3decf5e5-fff2-4a53-bdae-44badbd81be3",
            "compositeImage": {
                "id": "19659436-91ef-4e04-811e-a59d45175f2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2429b60f-24ba-4d6b-ae33-b1d72ff8494f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf1d239-977b-4880-8121-44f1d8fc4a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2429b60f-24ba-4d6b-ae33-b1d72ff8494f",
                    "LayerId": "bd0de5c8-9122-4483-be8a-bf2787275d3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "bd0de5c8-9122-4483-be8a-bf2787275d3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3decf5e5-fff2-4a53-bdae-44badbd81be3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}