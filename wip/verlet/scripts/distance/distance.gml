///@func distance(_p1, _p2)
///@desc return distance between two points
///@param {array} _p1   point 1
///@param {array} _p2   point 2
///@returns {real} actual distance between two points
gml_pragma("forceinline");
var dsq = distanceSq(argument0, argument1);
return sqrt(dsq); 