///@func draw_line_fast(pixel_sprite, x1, y1, x2, y2, color, alpha, width)
///@param {real} pixel   a sprite resource that is 1 pixel and white with an origin at 0,0
///@param {real} x1
///@param {real} y1
///@param {real} x2
///@param {real} y2
///@param {real} color
///@param {real} alpha
///@param {real} widtuh
var _pixel=argument[0],
    _x1=argument[1]+1,
    _y1=argument[2]+1,
    _x2=argument[3]+1,
    _y2=argument[4]+1,
    _color=argument[5],
    _alpha=argument[6],
    _width=argument[7],
    _dir = point_direction(_x1, _y1, _x2, _y2),
    _len = point_distance(_x1, _y1, _x2, _y2);

draw_sprite_ext(_pixel, 0, 
                _x1+lengthdir_x(_width/2,_dir+90), 
                _y1+lengthdir_y(_width/2,_dir+90), 
                _len, _width, _dir, _color, _alpha);

