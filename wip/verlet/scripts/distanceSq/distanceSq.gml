///@func distanceSq(_p1, _p2)
///@desc return distance between two points
///@param {array} _p1   point 1
///@param {array} _p2   point 2
///@returns {real} squared distance between two points
gml_pragma("forceinline");
var _p1 = is_real(argument0) ? points_get(argument0) : argument0, 
    _p2 = is_real(argument1) ? points_get(argument1) : argument1;

var p1x = _p1[0], p1y = _p1[1],
    p2x = _p2[0], p2y = _p2[1];

var dx = p2x - p1x,
		dy = p2y - p1y;

return (dx * dx + dy * dy);