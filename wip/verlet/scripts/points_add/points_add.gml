///@func points_add(_x, _y, [_oldx], [_oldy], [_velocity])
///@pdesc add a new point and return its index
///@params {real} _x           x position of the point
///@params {real} _y           y positionof the point
///@params {real} [_oldx]      last x, or fill with x 
///@params {real} [_oldy]      last y, or fill with y
///@params {real} [_velocity]  velocity to use (or 0) NOTE: if given 3 params, 3rd is velocity.
///@params {bool} [_pinned]    pinned (or false)
///@returns {real} point_index 
var _x = argument[0],
    _y = argument[1],
    _velocity = 0,
    _oldx = argument_count >= 4 ? argument[2] : argument[0],
    _oldy = argument_count >= 4 ? argument[3] : argument[1],
    _pinned = argument_count > 5 ? argument[5] : false;  


if (argument_count == 3) _velocity = argument[2];
else if (argument_count == 5) _velocity = argument[4];
var _index = undefined;

with (Points) {
  var pt = [ _x, _y, _oldx, _oldy, _velocity, _pinned ];
  if (ds_stack_empty(_Points_Free))
    _index = _Max_Point++;
  else
    _index = ds_stack_pop(_Points_Free);

  _Points[@ _index] = pt;
  ds_list_add(_Points_Used, _index);
  _Num_Used++;
}

return _index;