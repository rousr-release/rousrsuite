///draw_rectangle_fast(pixel_sprite,x1, y1, x2, y2, color, alpha, outline)
// Argument 1 should be a sprite resource that is 1 pixel and white with an origin at 0,0
var _pixel=argument[0],
    _x1=argument[1],
    _y1=argument[2],
    _x2=argument[3],
    _y2=argument[4],
    _color=argument[5],
    _alpha=argument[6],
    _outline=argument[7];
    
if(_outline)
{ //Outline
  //top
  draw_sprite_ext(_pixel,0, _x1+1, _y1, _x2-_x1-2, 1, 0, _color,_alpha);
  //bottom 
  draw_sprite_ext(_pixel,0, _x1, _y2-1, _x2-_x1, 1, 0, _color,_alpha);
  //left 
  draw_sprite_ext(_pixel,0, _x1, _y1, 1, _y2-_y1-1, 0, _color,_alpha);
  //rirght
  draw_sprite_ext(_pixel,0, _x2-1,_y1, 1, _y2-_y1-1, 0, _color,_alpha);
}
else
{ //Filled
  draw_sprite_ext(_pixel,0,_x1,_y1,_x2-_x1,_y2-_y1,0,_color,_alpha);
}
