///@func points_get(_point_index)
///@desc returns array represetning the opint at _point_index;
///@param {real} _point_index   the point we want an array of
///@returns {array} Array with { x, y, oldx, oldy, velocity } 
var _point_index = argument0;
var _point = undefined;

with (Points) {
  _point = _Points[@ _point_index];
}

return _point;