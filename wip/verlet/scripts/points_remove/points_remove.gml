///@func points_remove(_point_index)
///@desc frees an index, also clears the array params to undefined to signal its been 'freed'
///@param {real} _point_index   the point we want an array of
var _point_index = argument0;

with (Points) {
  ds_stack_push(_Points_Free, _point_index);
  _Points[@ _point_index] = undefined;
  
  var point_list_index = ds_list_find_index(_Points_Used, _point_index);
  if (point_list_index >= 0) {
    ds_list_delete(_Points_Used, point_list_index);
    _Num_Used--;
  }
}
