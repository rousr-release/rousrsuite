///@desc 

// Constrain
for(var i = 0; i < _Num_Used; ++i) {
	var pindex = _Points_Used[| i];
  var p = _Points[ pindex];

  if(!p[EPoint.Pinned]) {
		var vx = (p[EPoint.x] - p[EPoint.oldx]) * Friction,
			  vy = (p[EPoint.y] - p[EPoint.oldy]) * Friction;

		if(p[EPoint.x] > Width) {
			p[@ EPoint.x] = Width;
			p[@ EPoint.oldx] = p[EPoint.x] + vx * Bounce;
		} else if(p[EPoint.x] < 0) {
			p[@ EPoint.x]    = 0;
			p[@ EPoint.oldx] = p[EPoint.x] + vx * Bounce;
		}
    
		if(p[EPoint.y] > Height) {
			p[@ EPoint.y]    = Height;
			p[@ EPoint.oldy] = p[EPoint.y] + vy * Bounce;
		}	else if(p[EPoint.y] < 0) {
			p[@ EPoint.y]    = 0;
			p[@ EPoint.oldy] = p[EPoint.y] + vy * Bounce;
		}
	}
}