///@desc Points - Step

// Integrate
for(var i = 0; i < _Num_Used; ++i) {
	var pindex = _Points_Used[| i];
  var p = _Points[ pindex];
  
	if(!p[EPoint.Pinned]) {
    var px = p[0], py = p[1],
        ox = p[2], oy = p[3]
		var vx = (px - ox) * Friction,
			  vy = (py - oy) * Friction;

		p[@ EPoint.OldX] = px;
		p[@ EPoint.OldY] = py;
		p[@ EPoint.X] += vx;
		p[@ EPoint.Y] += vy;
		p[@ EPoint.Y] += Gravity;
	}
}