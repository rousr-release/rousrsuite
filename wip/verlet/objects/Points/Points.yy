{
    "id": "143997ce-49c3-4e3b-bba4-03f81c96c4d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Points",
    "eventList": [
        {
            "id": "daf42e57-103b-4c54-8a9e-f2c3f1a5d2ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "143997ce-49c3-4e3b-bba4-03f81c96c4d8"
        },
        {
            "id": "9b40510e-ee84-4397-81e3-809c6a365bdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "143997ce-49c3-4e3b-bba4-03f81c96c4d8"
        },
        {
            "id": "f9815a1f-af2c-4a3e-8380-ab7b1012526b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "143997ce-49c3-4e3b-bba4-03f81c96c4d8"
        },
        {
            "id": "82bddd57-d1ad-42e0-8fb2-70967d7d833d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "143997ce-49c3-4e3b-bba4-03f81c96c4d8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}