///@desc 

enum EPoint {
  X = 0,
  Y,
  OldX,
  OldY,
  
  Velocity,
  Pinned,
  
  Num,
  
  x = EPoint.X,
  y = EPoint.Y,
  oldx = EPoint.OldX, Oldx = EPoint.OldX,
  oldy = EPoint.OldY, Oldy = EPoint.OldY
};

_Points = array_create(0);
_Max_Point = 0;
_Points_Free = ds_stack_create();
_Points_Used = ds_list_create();
_Num_Used = 0;

Width  = 640;
Height = 360;
Friction = 0.999;
Gravity = 0.5;
Bounce = 0.9;
Angle = 0;
Spd = 0.1;