///@desc Rope - Create
enum EStick {
  P0 = 0,
  P1,
  Length,
  Hidden,
  
  Num
};

RopePoints = [
  points_add(250, 100, 100 + random(50) - 25, 100 + random(50) - 25),
  points_add(300, 100),
  points_add(300, 150),
  points_add(250, 150),
  points_add(250, 50),
  points_add(300, 75),
];

Sticks = ds_list_create();
ds_list_add(Sticks, [ RopePoints[0], RopePoints[1], distance(RopePoints[0], RopePoints[1]), false ]);
ds_list_add(Sticks, [ RopePoints[1], RopePoints[2], distance(RopePoints[1], RopePoints[2]), false ]);
ds_list_add(Sticks, [	RopePoints[2], RopePoints[3], distance(RopePoints[2], RopePoints[3]), false ]);
ds_list_add(Sticks, [ RopePoints[3], RopePoints[0], distance(RopePoints[3], RopePoints[0]), false ]);
ds_list_add(Sticks, [ RopePoints[0], RopePoints[2], distance(RopePoints[0], RopePoints[2]),	true  ]);
ds_list_add(Sticks, [ Engine.Point,  RopePoints[4],	distance(Engine.Point,  RopePoints[4]), false	]);
ds_list_add(Sticks, [ RopePoints[4], RopePoints[5], distance(RopePoints[4], RopePoints[5]), false ]);
BoxPoint = ds_list_size(Sticks);
ds_list_add(Sticks, [ RopePoints[5], RopePoints[0], distance(RopePoints[5], RopePoints[0]), false ]);

Num_sticks = ds_list_size(Sticks);