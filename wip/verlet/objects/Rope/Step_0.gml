///@desc Rope - Step

if (BoxPoint != -1 && keyboard_check_pressed(ord("D"))) {
  ds_list_delete(Sticks, BoxPoint);
  BoxPoint = -1;
  Num_sticks--;
}

for (var i = 0; i < Num_sticks; ++i) {
  var s = Sticks[| i];
  
  var p0index = s[EStick.P0], p1index = s[EStick.P1];
  var p0 = points_get(p0index), p1 = points_get(p1index);

  var dx = p1[EPoint.X] - p0[EPoint.X],
      dy = p1[EPoint.Y] - p0[EPoint.Y];

  var dist = sqrt(dx * dx + dy * dy);

  var difference = s[EStick.Length] - dist;
  var percent = difference / dist / 2;
      
  var offsetX = dx * percent,
      offsetY = dy * percent;

  if(!p0[ EPoint.Pinned]) {
    p0[@ EPoint.X] -= offsetX;
  	p0[@ EPoint.Y] -= offsetY;
  }
  
  if(!p1[ EPoint.Pinned]) {
  	p1[@ EPoint.X] += offsetX;
  	p1[@ EPoint.Y] += offsetY;
  }
}