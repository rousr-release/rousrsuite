{
    "id": "e57d9a51-d4d1-4b43-b801-ee88267cc6a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Rope",
    "eventList": [
        {
            "id": "c2d57389-9971-4d86-aacb-40ea595596fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e57d9a51-d4d1-4b43-b801-ee88267cc6a1"
        },
        {
            "id": "ad7be245-d823-4e20-806b-35f6da92b976",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e57d9a51-d4d1-4b43-b801-ee88267cc6a1"
        },
        {
            "id": "2668a65e-90c0-4b40-93a8-f43046f575c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e57d9a51-d4d1-4b43-b801-ee88267cc6a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}