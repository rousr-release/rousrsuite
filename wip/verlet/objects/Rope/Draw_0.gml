///@desc Rope - Draw

for(var i = 0; i < Num_sticks; i++) {
	var s = Sticks[| i];
	if(!s[EStick.Hidden]) {
    var p0 = points_get(s[EStick.P0]);
    var p1 = points_get(s[EStick.P1]);
    
    draw_line_fast(Pixel, p0[EPoint.x], p0[EPoint.Y], p1[EPoint.x], p1[EPoint.y], c_black, 1.0, 1.0);
    show_debug_message("Drew line from: " + string(p0[EPoint.x]) + ", " + string(p0[EPoint.Y]) + " to " + string(p1[EPoint.x]) + "," + string(p1[EPoint.y]));
	}
}