{
    "id": "b9b9b95c-fa01-49d9-917a-654da94ea83c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Pixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b70b3a26-dd69-4a31-b959-c35f45f54dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9b9b95c-fa01-49d9-917a-654da94ea83c",
            "compositeImage": {
                "id": "5b56101a-5bca-4e3b-ab3f-2f93e4576b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b70b3a26-dd69-4a31-b959-c35f45f54dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed1e0155-d73b-4be6-8032-cc20a27a5985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b70b3a26-dd69-4a31-b959-c35f45f54dea",
                    "LayerId": "0c4df1cf-3337-4929-9f41-32537b5f1f77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "0c4df1cf-3337-4929-9f41-32537b5f1f77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9b9b95c-fa01-49d9-917a-654da94ea83c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}