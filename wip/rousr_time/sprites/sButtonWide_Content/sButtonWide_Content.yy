{
    "id": "eff29bc9-4f21-4617-b722-bb8a3210a527",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide_Content",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "600612d5-9727-4b75-916e-65314f29d9e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "compositeImage": {
                "id": "585c7295-9e52-4ed9-8123-bc0de11e9de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "600612d5-9727-4b75-916e-65314f29d9e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8106141-e630-4644-9414-71520213a9bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "600612d5-9727-4b75-916e-65314f29d9e2",
                    "LayerId": "3baf89da-2f40-46a0-aed5-30cbc20c2128"
                }
            ]
        },
        {
            "id": "0abecac1-10d4-4989-a1b4-4725edc6d743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "compositeImage": {
                "id": "63b9d8b3-2497-4e5b-9606-268d670b423f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0abecac1-10d4-4989-a1b4-4725edc6d743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d8cef68-ac94-4a38-b87f-a81655cae764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0abecac1-10d4-4989-a1b4-4725edc6d743",
                    "LayerId": "3baf89da-2f40-46a0-aed5-30cbc20c2128"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "3baf89da-2f40-46a0-aed5-30cbc20c2128",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}