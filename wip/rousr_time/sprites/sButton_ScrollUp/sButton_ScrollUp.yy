{
    "id": "9461ec09-42de-47dd-a186-c3f65c5c8fd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton_ScrollUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6774c87c-715d-4a0f-b24a-27c55b875d16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9461ec09-42de-47dd-a186-c3f65c5c8fd2",
            "compositeImage": {
                "id": "88def4ba-1209-41a6-8602-5fe47c5d93ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6774c87c-715d-4a0f-b24a-27c55b875d16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fb2e1e0-fd2e-4234-9b24-c118160dc66b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6774c87c-715d-4a0f-b24a-27c55b875d16",
                    "LayerId": "ed6840c0-b0da-439c-89f9-dff4eafd6e32"
                }
            ]
        },
        {
            "id": "35ec2093-43b5-4539-9914-a426ade468be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9461ec09-42de-47dd-a186-c3f65c5c8fd2",
            "compositeImage": {
                "id": "9f191f81-1a40-4fd7-baf4-e392d6876c53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ec2093-43b5-4539-9914-a426ade468be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe7260df-9d1c-4d0f-a7a2-dab56a6568d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ec2093-43b5-4539-9914-a426ade468be",
                    "LayerId": "ed6840c0-b0da-439c-89f9-dff4eafd6e32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ed6840c0-b0da-439c-89f9-dff4eafd6e32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9461ec09-42de-47dd-a186-c3f65c5c8fd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}