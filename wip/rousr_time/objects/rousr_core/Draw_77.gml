/// @description Draw the application Surface
if (!__displayDirty) {
  draw_surface(application_surface, 0, 0);
} else {
  draw_clear_alpha(ClearColor, 1);
}