if (!ensureSingleton("__closeButton"))
  return;

event_inherited();
clickAction = quitGame;
anchorInstance_topRight();