{
    "id": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TitleBarButtonClose",
    "eventList": [
        {
            "id": "4a1d66cd-c54d-4ef3-89ef-be0b16de81ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 5,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "a1c0ee40-1052-4f9e-8b3d-850bfcba04c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 10,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "bb4ea058-4a8c-4c2a-b789-7b6fddba0ddb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "1d8808e0-ddf1-45a5-a9ca-8b72eefeb47b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
    "visible": true
}