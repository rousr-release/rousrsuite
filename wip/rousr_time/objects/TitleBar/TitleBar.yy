{
    "id": "4617493a-9a65-476f-8119-a6740e12df53",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TitleBar",
    "eventList": [
        {
            "id": "d0c7c4cc-97d0-47cc-9439-3a1211ce92d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        },
        {
            "id": "013ef8b9-a1e2-4f32-8a2d-0405fefe6135",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        },
        {
            "id": "e2300949-c256-4580-817a-070a51de2edf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        },
        {
            "id": "df2cc381-3d10-4d57-8b3d-c3ca193cd21a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        },
        {
            "id": "26ccddf9-f179-48c0-bb75-5d794ad37107",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "da321195-6230-4870-b017-c16bac16ebc5",
    "visible": true
}