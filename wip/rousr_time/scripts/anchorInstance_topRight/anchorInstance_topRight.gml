///anchorInstance_bottomLeft
///@desc snap an instance to the bottom left of the screen
///with(instance)
///@param (opt)padRight
///@param (opt)padTop
var _padRight = argument_count > 0 ? argument[0] : 0;
var _padTop = argument_count > 1 ? argument[1] : 0;
var _addList = argument_count > 2 ? argument[2] : true;

x = babyJams_gameWidth() - (anchorInstance_getWidth(id) + _padRight);
y = _padTop;

if (_addList)
  anchorInstance_addList(id, anchorInstance_topRight, [ _padRight, _padTop, false ]);