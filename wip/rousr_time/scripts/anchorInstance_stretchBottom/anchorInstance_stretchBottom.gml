///anchorInstance_stretchBottom
///@desc stretch this object acrosst the Bottom
///with(instance)
///@param (opt)padLeftOrSides
///@param (opt)padBottom
///@param (opt)padRight
///@param (opt)addToList
var _padLeft   = argument_count > 0 ? argument[0] : 0;
var _padBottom = argument_count > 1 ? argument[1] : 0;
var _padRight  = argument_count > 2 ? argument[2] : _padLeft;
var _addToList = argument_count > 3 ? argument[3] : true;

var sw = babyJams_gameWidth() - (_padLeft + _padRight);

with(id) {
  x = _padLeft;
  y = babyJams_gameHeight() - _padBottom - sprite_height;
  
  var scaleW = sw / sprite_get_width(sprite_index);
  image_xscale = scaleW;
  
  if (_addToList) {
    var updateParams = [ _padLeft, _padBottom, _padRight, false ];
    anchorInstance_addList(id, anchorInstance_stretchBottom, updateParams);
  }
}