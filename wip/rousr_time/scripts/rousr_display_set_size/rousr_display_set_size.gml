///@desc set the window / room / camera size
///@param width
///@param height
var _width = argument0;
var _height = argument1;

var camera = view_camera[0];

var scaleX = 3;//view_get_xport(0) / camera_get_view_width(camera);
var scaleY = 3;//view_get_yport(0) / camera_get_view_height(camera);

var scaledW = _width / scaleX;
var scaledH = _height / scaleY;

var oldWidth = window_get_width();
var oldHeight = window_get_height();
var oldX = window_get_x();
var oldY = window_get_y();
var windowX = oldX + (oldWidth - _width) * 0.5;
var windowY = oldY + (oldHeight - _height) * 0.5;

room_width  = scaledW;
room_height = scaledH;

window_set_size(_width, _height);
window_set_position(windowX, windowY);

view_set_wport(0, _width);
view_set_hport(0, _height);

camera_set_view_size(camera, scaledW, scaledH);
camera_set_view_pos(camera,0,0);

surface_resize(application_surface, scaledW, scaledH);