///@description rousr_execute - call a function with variadic argument support
///@param {Real}  _script_index - script to execute
///@param {Array} _params       - parameters to call, in an array
///@param {Real}  [paramCount]  - if you've already got the count ready, pass it, or else it's computed.
///@returns {*} return value from script call
var _func = argument[0];
var _params = argument[1];
var _paramCount = argument_count > 2 ? argument[2] : array_length_1d(_params);

var ret = undefined;
switch (_paramCount) {
	case 0:  ret = script_execute(_func); break;
	case 1:  ret = script_execute(_func, _params[0]); break;
	case 2:  ret = script_execute(_func, _params[0], _params[1]); break;
	case 3:  ret = script_execute(_func, _params[0], _params[1], _params[2]); break;
	case 4:  ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3]); break;
	case 5:  ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4]); break;
	case 6:  ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5]); break;
	case 7:  ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6]); break;
	case 8:  ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7]); break;
	case 9:  ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8]); break;
	case 10: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9]); break;
	case 11: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10]); break;
	case 12: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11]); break;
	case 13: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12]); break;
	case 14: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13]); break;
	case 15: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14]); break;
  case 16: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15]); break;
  case 17: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16]); break;
  case 18: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17]); break;
  case 19: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18]); break;
  case 20: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19]); break;
  case 21: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20]); break;
  case 22: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21]); break;
  case 23: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22]); break;
  case 24: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23]); break;
  case 25: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23], _params[24]); break;
  case 26: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23], _params[24], _params[25]); break;
  case 27: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23], _params[24], _params[25], _params[26]); break;
  case 28: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23], _params[24], _params[25], _params[26], _params[27]); break;
  case 29: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23], _params[24], _params[25], _params[26], _params[27], _params[28]); break;
  case 30: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23], _params[24], _params[25], _params[26], _params[27], _params[28], _params[29]); break;
  case 31: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23], _params[24], _params[25], _params[26], _params[27], _params[28], _params[29], _params[30]); break;
  case 32: ret = script_execute(_func, _params[0], _params[1], _params[2], _params[3], _params[4], _params[5], _params[6], _params[7], _params[8], _params[9], _params[10], _params[11], _params[12], _params[13], _params[14], _params[15], _params[16], _params[17], _params[18], _params[19], _params[20], _params[21], _params[22], _params[23], _params[24], _params[25], _params[26], _params[27], _params[28], _params[29], _params[30], _params[31]); break;
};

return ret;