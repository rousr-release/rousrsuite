///@func contains(_array, _val, [_array_length)
///@desc returns true if the array contains the val
///@param {array}        _array    array of vals
///@param {real|string}  _val      val to find
///@param {real} [_array_length]   length of _array
///@return {bool} _array contains tile
var _array = argument[0],
    _val   = argument[1];

if (is_array(_array)) {    
  var _len = argument_count > 2 ? argument[2] : array_length_1d(_array);
  for (var i = 0; i < _len; ++i) 
    if (_array[i] == _val)
      return true;
} else { 
  var _len = argument_count > 2 ? argument[2] : ds_list_size(_array);
  for (var i = 0; i < _len; ++i) 
    if (_array[| i] == _val)
      return true;
}    

return false;