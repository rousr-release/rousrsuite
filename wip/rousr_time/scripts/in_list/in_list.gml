///@desc return true if the passed position is already on the list
///@param _list
///@param _args...
gml_pragma("forceinline");
var numArgs = argument_count;
if (numArgs < 2)
	return undefined;
	
var _list = argument[0];
var args = array_create(numArgs - 1);
for (var i = 1; i < numArgs; ++i) 
	args[i-1] = argument[i];

return in_list_arg_array(args, _list);