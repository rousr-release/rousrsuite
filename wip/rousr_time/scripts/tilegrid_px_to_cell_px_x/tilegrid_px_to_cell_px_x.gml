///@description return pixel x of a cell at pixel position
///@param table
///@param pixelX
///@param pixelY
var _table = argument0;
var _pxX   = argument1;
var _pxY   = argument2;

var mapId = _table.TileMapId;
var tw    = _table.TileWidth;
var ox    = _table.TileMapX;

var _x = tilemap_get_cell_x_at_pixel(mapId, _pxX, _pxY);
		
return ox + (_x * tw)