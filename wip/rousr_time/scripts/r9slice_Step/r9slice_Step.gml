2///@desc update a frame object
///expected to be called with(frameObject)
if (!visible)
  return;
  
var sizeDirty = false, spriteDirty = false;

// mark dirty if the frame size changed
if (image_xscale != FrameWidth || image_yscale != FrameHeight ||
    FrameWidth != __frameLastWidth || FrameHeight != __frameLastHeight ) {
  
  if (FrameWidth  == __frameLastWidth)  FrameWidth = image_xscale;
  else image_xscale = FrameWidth;
  
  if (FrameHeight == __frameLastHeight) FrameHeight = image_yscale
  else image_yscale = FrameHeight;
  
  sizeDirty = true;
}

// mark dirty if the sprite changed
if (FrameSprite != sprite_index || FrameSprite != __frameLastSprite) {
  if (FrameSprite == __frameLastSprite) FrameSprite = sprite_index;
  else sprite_index = FrameSprite;
  
  FrameSpriteWidth  = sprite_get_width(FrameSprite);
  FrameSpriteHeight = sprite_get_height(FrameSprite);
  
  spriteDirty = true;
}

if (sizeDirty || spriteDirty) {
  FrameWidth  = max((FrameSpriteWidth  * 2) + 1, FrameWidth);
  FrameHeight = max((FrameSpriteHeight * 2) + 1, FrameHeight);
  
  __frameCenterWidth  = (FrameWidth  - (2 * FrameSpriteWidth)) / FrameSpriteWidth;
  __frameCenterHeight = (FrameHeight - (2 * FrameSpriteHeight)) / FrameSpriteHeight;  
}

__frameDirty |= sizeDirty || spriteDirty;
__frameLastWidth  = FrameWidth;
__frameLastHeight = FrameHeight;
__frameLastSprite = FrameSprite;
