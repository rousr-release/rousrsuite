///_babyJams_addHook
///@desc add a hook, formatted for the hook system
///@param hookFunction
///@param paramsArray
///@param hooksList
var _hook = argument0;
var _params = argument1;
var _hooks = argument2;

var hook = [ _hook, _params ];
ds_list_add(_hooks, hook);