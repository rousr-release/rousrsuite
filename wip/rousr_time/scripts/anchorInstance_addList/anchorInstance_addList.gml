///anchorInstance_addList
///@desc Add an instance that has been anchored to the list to track for resizes
///@param instance
///@param params
var _id     = argument0;
var _scr    = argument1;
var _params = argument2;

var anchorList = global.___anchorList
ds_list_add(anchorList, [ _id, _scr, _params] );
return ds_list_size(anchorList) - 1;