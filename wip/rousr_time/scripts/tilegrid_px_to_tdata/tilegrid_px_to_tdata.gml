/// @description return tdata of a grid cell based on pixel position
/// @param pixelX
/// @param pixelY
var _table = argument0;
var _pxX   = argument1;
var _pxY   = argument2;

var mapId = _table.TileMapId;

var _x = tilemap_get_cell_x_at_pixel(mapId, _pxX, _pxY);
var _y = tilemap_get_cell_y_at_pixel(mapId, _pxX, _pxY);
			
return tilemap_get(mapId, _x, _y);