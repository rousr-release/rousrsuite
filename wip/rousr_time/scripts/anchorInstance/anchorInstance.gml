///@desc anchor an instance
///@param instance
///@param anchor

enum AnchorInstance {
  BottomLeft,
  Bottom,
  BottomRight,
  Left,
  Center,
  Right,
  TopLeft,
  Top,
  TopRight
}

var _id = argument0;
var _anchor = argument1;
var param1 = argument_count > 2 ? argument[2] : 0;
var param2 = argument_count > 3 ? argument[3] : 0;
var param3 = argument_count > 4 ? argument[4] : 0;
var param4 = argument_count > 5 ? argument[5] : 0;
  
with (_id) {
  
  switch(_anchor) {
    case AnchorInstance.BottomLeft:  anchorInstance_bottomLeft(param1, param2); break;
    case AnchorInstance.Bottom:      break;
    case AnchorInstance.BottomRight: break;
    case AnchorInstance.Left:        break;
    case AnchorInstance.Center:      break;
    case AnchorInstance.Right:       break;
    case AnchorInstance.TopLeft:     break;
    case AnchorInstance.Top:         break;
    case AnchorInstance.TopRight:    break;
    default:break;
  }
}