///@desc clean up the hooks system
if (variable_global_exists("___createHooks") && !is_undefined(global.___createHooks))
  ds_list_destroy(global.___createHooks);
ds_list_destroy(global.___beginStepHooks);
ds_list_destroy(global.___stepHooks);
ds_list_destroy(global.___cleanupHooks);
ds_list_destroy(global.___preDrawHooks);
ds_list_destroy(global.___postDrawHooks);
ds_list_destroy(global.___windowResizeHooks);

global.___createHooks    = undefined;
global.___beginStepHooks = undefined;
global.___stepHooks      = undefined;
global.___cleanupHooks   = undefined;
global.___preDrawHooks   = undefined;
global.___postDrawHooks  = undefined;
global.___windowResizeHooks = undefined; 