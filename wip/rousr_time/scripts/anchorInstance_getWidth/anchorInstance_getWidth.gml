///anchorInstance_getWidth
///@desc helper fucntion to best guess width
///@param instance
var _id = argument0;
var width = 0;

if (variable_instance_exists(_id, "width")) {
  width = variable_instance_get(_id, "width");
} else {
  with(id) {
    if (mask_index == -1) 
      width = sprite_width;
    else
      width = bbox_right - bbox_left;
  }
}

return width;