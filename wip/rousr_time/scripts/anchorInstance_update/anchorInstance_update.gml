///anchorInstance_update
///@desc reposition all anchored objects in the room
var anchorList = global.___anchorList;
var numAnchors = ds_list_size(anchorList);
for (var i = 0; i < numAnchors; ++i) {
  var anchorInst = anchorList[|i];
  var inst = anchorInst[0];
  var scr = anchorInst[1];
  var params = anchorInst[2];
  with (inst) {
    babyJams_execute(scr, params);
  }
}