/// @description DisplayManager - Create
LastWindowWidth = window_get_width();
LastWindowHeight = window_get_height();

LastAppSurfaceID = -1;
__displayInit = false;
__displayDirty = true;
__displayDirtyHooked = false;

layer_background_blend(layer_background_get_id(layer_get_id("Background")), ClearColor);
babyJams_addStepHook(_displayManager_step, id);