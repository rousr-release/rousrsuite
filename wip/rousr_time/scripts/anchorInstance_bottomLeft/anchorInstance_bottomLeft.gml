///anchorInstance_bottomLeft
///@desc snap an instance to the bottom left of the screen
///with(instance)
///@param (opt)padLeft
///@param (opt)padBottom
var _padLeft = argument_count > 0 ? argument[0] : 0;
var _padBottom = argument_count > 1 ? argument[1] : 0;
var _addList = argument_count > 2 ? argument[2] : true;

x = _padLeft;
y = babyJams_gameHeight()- (anchorInstance_getHeight(id) + _padBottom);

if (_addList)
  anchorInstance_addList(id, anchorInstance_bottomLeft, [ _padLeft, _padBottom, false ]);