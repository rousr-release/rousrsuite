///@desc vfx_fadeRoom
///@param _fadeDuration
///@param _fadeHold;

FadeStart = -1;
FadeDuration = argument0;
FadeHold = argument1;
FadeEase = argument2;
babyJams_addPreDrawHook(vfx_fadeRoomIn, id);
babyJams_addPostDrawHook(vfx_fadeRoomInPost, id);