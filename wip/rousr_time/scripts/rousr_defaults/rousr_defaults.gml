///@desc default values - can be override by making a "rousr_defines" script
ScreenWidth = 1280;
ScreenHeight = 720;

EnableAnchors        = true;
EnableDeltaTime      = true;
EnableDisplayManager = true;

// Todo: Color prefs
ClearColor = colorHex($000000);