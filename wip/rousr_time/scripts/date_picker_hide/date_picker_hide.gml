///@desc Hide datePicker and its fields
var _datePicker = global._datePicker;

with(_datePicker) {
  for(var i = 0; i < DatePicker.FieldCount; ++i)
    instance_deactivate_object(InputFields[i]);

  visible = false;
  instance_deactivate_object(id);
}