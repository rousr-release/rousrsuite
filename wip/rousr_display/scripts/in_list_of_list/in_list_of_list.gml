///@desc __lm_inListOfList - Find args in list of list
///@param list    - list
///@param args... - list of arguments
var numArgs = argument_count;
if (numArgs < 2)
	return undefined;

var _lists = argument[0];
var listCount = array_length_1d(_lists);
	
var args   = array_create(numArgs - 1);
for (var i = 1; i < numArgs; ++i)
	args[i-1] = argument[i];

var found = false;
for (var listIndex = 0; listIndex < listCount && !found; ++listIndex) {
	var list = _lists[listIndex];
	found = in_list_arg_array(args, list);
}

return found;