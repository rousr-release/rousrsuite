/// @desc Begin Drag - call from the leftdown event
/// @desc Manually drag window
if (!visible) {
  if (__dragger_dragState != DragState.None) dragger_reset();
	return;
}
	
switch (__dragger_dragState) {
	case DragState.None: 
    if (iDragger.dragging != noone || !mouse_check_button_pressed(mb_left)|| (__dragger_onCheck != noone && !script_execute(__dragger_onCheck)))
			return;
		
		__dragger_startX     = x;
		__dragger_startY     = y;
		__dragger_startDepth = depth;
		
    __dragger_hasMoved = false;
		__dragger_dragStartX = mouse_x;
		__dragger_dragStartY = mouse_y;
		
		__dragger_lastMouseX = mouse_x;
		__dragger_lastMouseY = mouse_y;
      
    __dragger_dragState = DragState.Begin;
		break;
	
	case DragState.Begin:
    if (__dragger_lastMouseX != mouse_x || __dragger_lastMouseY != mouse_y)
      __dragger_hasMoved = true;
      
    if (iDragger.dragging != noone) {
      if (iDragger.dragging != id) __dragger_dragState = DragState.None;
    } else if (point_distance(__dragger_startX, __dragger_startY, mouse_x, mouse_y) > __dragger_slack && __dragger_hasMoved) {
			__dragger_dragState = DragState.Dragging;
			iDragger.dragging = id;
		
      depth = !is_undefined(__dragger_dragDepth) ? __dragger_dragDepth : (depth + __dragger_depthOffset);
			
			__dragger_lastMouseX = __dragger_dragStartX;
			__dragger_lastMouseY = __dragger_dragStartY;
      
      if (__dragger_onBegin != noone) script_execute(__dragger_onBegin);
		}
		break;
		
	default: break;
}