///@description return cell y of a cell at pixel position
///for consitency with the other table functions
///@param table
///@param pixelX
///@param pixelY
var _table = argument0;
var _pxX   = argument1;
var _pxY   = argument2;

var mapId = _table.TileMapId;

return tilemap_get_cell_y_at_pixel(mapId, _pxX, _pxY);