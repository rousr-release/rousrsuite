///@desc input_registerAction - register an action type with the input system
///@param _actionKey         - an real or string that represents the action. this key is bound to.
///@param _actionFunction    - a script to callback _actionScript(_action, _type)  _type = pressed, released, down, down and pressed come in the same frame.
var _actionKey = argument0;
var _actionFunction = argument1;
if (!variable_global_exists("__input") || global.__input == undefined) {
	show_debug_message("input - warning: input system not initialized, action: " + string(_actionKey) + " not bound.");
	return;
}

var input = global.__input;

var actionMap  = input[EInput.Actions]
var actionList = actionMap[? _actionKey];
if (actionList == undefined) {
	actionList = [ [ false, false ] ];
	actionMap[? _actionKey] = actionList;
}

var numActionFunctions = array_length_1d(actionList);
actionList[@numActionFunctions] = _actionFunction;