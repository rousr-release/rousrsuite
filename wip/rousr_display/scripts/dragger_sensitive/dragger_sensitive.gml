///@desc Make it a sensitive dragger
///@param sensitive
///@param (opt)newSlackValue 
var sensitive = argument[0] ? 0 : DraggerDefaultSlack;

__dragger_hasMoved = false;
__dragger_slack = argument_count > 1 ? argument[1] : sensitive;