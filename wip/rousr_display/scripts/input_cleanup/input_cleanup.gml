///@desc input_cleanup - clean up input data
if (!variable_global_exists("__input") || global.__input == undefined)
	return;

var inputData = global.__input;
ds_map_destroy(inputData[EInput.Actions]);
var  bindingMaps = inputData[EInput.Binds];
for (var i = 0; i < EInputType.Num; ++i) 
	ds_map_destroy(bindingMaps[i]);
	
global.__input = undefined;