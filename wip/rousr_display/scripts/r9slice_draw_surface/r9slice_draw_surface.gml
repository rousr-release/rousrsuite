///@desc Simply draw the frame surface with a surface frame.
///called `with(frameObject)`
///@remarks only needs to manually called if optionally disabled in frame_draw
///@param __opt_x
///@param __opt_y
gml_pragma("forceinline");
if (FrameSurface == undefined)
  return;

var _x = argument_count > 1 ? argument[0] : x;
var _y = argument_count > 1 ? argument[1] : y; 
draw_surface(FrameSurface, _x, _y);