///@desc finish updating the display update.
var _displayManager = argument0;
with(_displayManager) {
  __displayDirty = false;

  if (surface_exists(application_surface)) {  
    var surfaceW = surface_get_width(application_surface);
    var surfaceH = surface_get_height(application_surface);
  
    __displayDirty = (surfaceW != LastSurfaceW || surfaceH != LastSurfaceH || surfaceW != ScreenWidth|| surfaceH != ScreenHeight);
    if (__displayDirty && surfaceW != ScreenWidth && surfaceH != ScreenHeight) {
      surface_resize(application_surface, ScreenWidth, ScreenHeight);
      LastAppSurfaceID = application_surface;
    }
  
    LastSurfaceW = surfaceW;
    LastSurfaceH = surfaceH;
  } else {
    __displayDirty = true;
  }
  
  var viewW = view_wport[0];
  var viewH = view_hport[0];

  __displayDirty |= !(viewW == ScreenWidth && viewH == ScreenHeight);

  var cw = camera_get_view_width(view_camera[0]);
  var ch = camera_get_view_height(view_camera[0]);
  
  __displayDirty |= !(cw == ViewWidth && ch == ViewHeight);
  __displayDirty |= application_surface == LastAppSurfaceID;

  if (!__displayDirty) {
    __displayDirtyHooked = false;
    return true;
  }
}

return false;