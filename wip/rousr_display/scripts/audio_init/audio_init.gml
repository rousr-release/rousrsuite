///@desc audio_init - initialize audio
enum EMusic {
	Main,
	Intro,
	
	Num
}

MusicTracks = [ 
	[ shenanijam1, 12.0, 120.0 ],	// main theme
	[ undefined,    0.0,   0.0 ], // intro theme
];

CurrentTrack = undefined;
MusicPlaying = false;
MusicTime    = 0.0;
SoundId      = undefined;
IntroPlayed  = false;

audio_play(0);