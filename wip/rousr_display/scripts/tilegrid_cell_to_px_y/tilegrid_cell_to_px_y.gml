///tilegrid_cell_to_px_y
///@description return pixel x of cell tl-corner 
///@param table
///@param cellX
///@param cellY
var _table = argument0;
var _cx    = argument1;
var _cy    = argument2;

var mapId = _table.TileMapId;
var th    = _table.TileHeight;
var oy    = _table.TileMapY + _table.OffsetY;

return oy + (_cy * th)