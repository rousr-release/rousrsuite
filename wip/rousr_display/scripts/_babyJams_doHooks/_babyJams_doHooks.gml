///_babyjams_doHooks
///@desc helper function to execute hooks
///@param hookList
///@param (opt=false)removeOnReturn
var _hooks = argument[0];
var _removeOnReturn = argument_count > 1 ? argument[1] : false;

var removeHooks = noone;
var removeCount = 0;

if (_removeOnReturn) removeHooks = [ ];

var listSize = ds_list_size(_hooks);
for (var hookIndex = 0; hookIndex < listSize; ++hookIndex) {
  var hook = _hooks[|hookIndex];
  if (babyJams_execute(hook[0], hook[1]) == true && _removeOnReturn) {
    removeHooks[removeCount] = hookIndex;
    removeCount++;
  } 
}

if (_removeOnReturn) {
  for (var removeIndex = 0; removeIndex < removeCount; ++removeIndex)
    ds_list_delete(_hooks, removeHooks[removeIndex]);
}

return _hooks;