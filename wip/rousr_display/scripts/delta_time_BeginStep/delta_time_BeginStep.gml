///@desc delta Time step function
var _bj = argument0;
with (_bj) {
  global.frameDt   = gameTimer - _deltaTimeLastTime;
  global.gameDt    = global.frameDt * global.gameSpeedScale;
  
  global.frameTime += global.frameDt;
  global.gameTime  += global.gameDt;
  
  _deltaTimeLastTime = gameTimer;
}

return false;