///@func rousr_ds_safe_destroy(_type, _ds_id)
///@desc check if `_ds_id` is a valid `_type` and destroy it if it is.
///@param {Real} _type - type of data structure, i.e., `ds_type_map`, `ds_type_list`
///@param {Real} _ds_id - id for the datastructure
///@returns {Boolean} true on success
gml_pragma("forceinline");

var _type  = argument0;
var _ds_id = argument1;

if (is_real(_ds_id)) {
  switch(_type) {
    case ds_type_grid:     ds_grid_destroy(_ds_id);     break;
    case ds_type_list:     ds_list_destroy(_ds_id);     break;
    case ds_type_map:      ds_map_destroy(_ds_id);      break;
    case ds_type_priority: ds_priority_destroy(_ds_id); break;
    case ds_type_queue:    ds_queue_destroy(_ds_id);    break;
    case ds_type_stack:    ds_stack_destroy(_ds_id);    break;
    default: return false;;
  }
  return true;
}

return false;