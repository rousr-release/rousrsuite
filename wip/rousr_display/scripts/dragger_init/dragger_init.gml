///@desc call *with* an object you'd like to make draggable
///@param (opt=noone)onDragComplete(bool)
///@param (opt=noone)onDragCheck
///@param (opt=noone)onDragBegin
///@param (opt=Default)draggerSlack
__dragger_dragState =  DragState.None;
__dragger_startX      = -1;
__dragger_startY      = -1;
__dragger_startDepth  = depth;

__dragger_dragStartX  = -1;
__dragger_dragStartY  = -1;
__dragger_dragDepth   = undefined;
__dragger_depthOffset = -5;

__dragger_onComplete  = argument_count > 0 ? argument[0] : noone;
__dragger_onCheck     = argument_count > 1 ? argument[1] : noone;
__dragger_onBegin     = argument_count > 2 ? argument[2] : noone;
__dragger_slack       = argument_count > 3 ? argument[3] : DraggerDefaultSlack;


