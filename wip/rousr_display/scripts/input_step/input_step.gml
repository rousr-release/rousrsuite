///@desc input_step - check binds and call callbacks
if (!variable_global_exists("__input") || global.__input == undefined) {
	if (!variable_global_exists("__input_notInit")) {
		show_debug_message("input - warning: input system not initialized, input not checked: " + string(_inputButton));
		global.__input_notInit = true;
	}
	return;
}

var inputData = global.__input;
var inputActions = inputData[EInput.Actions];
var inputBinds   = inputData[EInput.Binds];

// reset the actions
var actionKey = ds_map_find_first(inputActions);
while (actionKey != undefined) {
	var actionList = inputActions[? actionKey];
	var numActionList = array_length_1d(actionList);
	if (numActionList > 0) {
		var actionState = actionList[0];
		actionState[@EInputAction.WasActive] = actionState[EInputAction.Active];
		actionState[@EInputAction.Active]    = false;
	}
	
	actionKey = ds_map_find_next(inputActions, actionKey);
}

// keys first
var keyBinds = inputBinds[EInputType.Key];
var queuedActions = [ ];
	
var keyCheck = ds_map_find_first(keyBinds);
var numQueuedActions = 0;
while (keyCheck != undefined) {
	var down     = keyboard_check(keyCheck);
	var pressed  = keyboard_check_pressed(keyCheck);
	var released = keyboard_check_released(keyCheck);
	
	var binds = keyBinds[? keyCheck];
	var numBinds = array_length_1d(binds);

	for (var i = 0; i < numBinds; ++i) {
		var bind = binds[i];
		var bindEvent  = bind[0];
		var bindAction = bind[1];
		
		if (down     && (bindEvent == EInputEvent.Down))     { if (!__inList(queuedActions, bindAction)) queuedActions[numQueuedActions++] = bindAction; }
		if (pressed  && (bindEvent == EInputEvent.Pressed))  { if (!__inList(queuedActions, bindAction)) queuedActions[numQueuedActions++] = bindAction; }
		if (released && (bindEvent == EInputEvent.Released)) { if (!__inList(queuedActions, bindAction)) queuedActions[numQueuedActions++] = bindAction; }
	}
	
	keyCheck = ds_map_find_next(keyBinds, keyCheck);
}

for (var i = 0; i < numQueuedActions; ++i) {
	var actionKey = queuedActions[i];
	var actionList = inputActions[? actionKey];
	var numCallbacks = array_length_1d(actionList);
	if (numCallbacks > 0) {
		var actionState = actionList[0];
		actionState[@EInputAction.Active] = true;
		
		for (var callbackIndex = numCallbacks - 1; callbackIndex > 0; --callbackIndex) {
			var callback = actionList[callbackIndex];
			if (script_execute(callback, actionKey, actionState[EInputAction.WasActive]))
				break;
		}
	}
}