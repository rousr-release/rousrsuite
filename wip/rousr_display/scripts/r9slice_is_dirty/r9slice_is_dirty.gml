///@desc helper to return frame dirty
gml_pragma("forceinline");
return __frameDirty;