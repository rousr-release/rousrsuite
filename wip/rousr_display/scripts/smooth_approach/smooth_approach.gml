///https://twitter.com/2DArray/status/873431750235623425
///
//// initialize:
//showinfo=false
//// raw position:
//x=64
//y=64
//// smoothed positions:
//primex=x
//primey=y
//smoothx=x
//smoothy=y
//// tuning parameters:
//maxspeed=4
//stiff=.1

//// enable mouse listener:
//poke(0x5f2d, 1)

//function _update()

//	// toggle info:
//	if (btnp(4)) then
//		showinfo=not showinfo
//	end

//	// get mouse position:
//	x=stat(32)
//	y=stat(33)
	
//	// prime-position approaches
//	// the target at linear speed:
//	local dx=x-primex
//	local dy=y-primey
//	local dist=sqrt(dx*dx+dy*dy)
//	if (dist>maxspeed) then
//		dx=dx/dist*maxspeed
//		dy=dy/dist*maxspeed
//		primex+=dx
//		primey+=dy
//	else
//		primex=x
//		primey=y
//	end
	
//	// smooth-position eases
//	// toward prime-position:
	
//	dx=primex-smoothx
//	dy=primey-smoothy
	
//	if (abs(dx)>.5) then
//		smoothx+=dx*stiff
//	else
//		smoothx=primex
//	end
//	if (abs(dy)>.5) then
//		smoothy+=dy*stiff
//	else
//		smoothy=primey
//	end
//end

//function _draw()
//	cls()
	
//	if (showinfo==true) then
	
//		print("target: raw mouse position\nprime: moves at linear speed\nsmooth: eases toward prime",1,1,5)
	
//		line(x,y,primex,primey,13)
//		line(primex,primey,
//		     smoothx,smoothy,6)
//	end
	
//	spr(002,x-8,y-8,2,2)
//	if (showinfo==true) then
//		print("target",x-32,y-3,13)
//		spr(004,primex-8,primey-8,2,2)
//		print("prime x/y",
//		      primex+9,
//		      primey-3,6)
//	end
//	spr(001,smoothx-4,smoothy-4)
//	if (showinfo==true) then
//		print("smooth x/y",
//		      smoothx-15,
//		      smoothy-12,8)
//	end
	
//	print("\142 - toggle info",1,120,5)
//end