///@desc reset the dragger to where it was before dragging
x = __dragger_startX;
y = __dragger_startY;
depth = __dragger_startDepth;

if (__dragger_dragState != DragState.None)
  script_execute(__dragger_onComplete, false);

__dragger_dragState = DragState.None;
