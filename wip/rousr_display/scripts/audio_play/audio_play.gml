///@desc audio_play - play a music track
///@param _trackIndex
var _track = argument0;
if (_track >= EMusic.Num || _track < 0)
	return;
	
var track = MusicTracks[_track];
if (track[0] == undefined)
	return;
	
SoundId = audio_play_sound(track[0], 0, true);
CurrentTrack = _track;
MusicPlaying = true;
IntroPlayed  = false;