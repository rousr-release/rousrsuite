{
    "id": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "rousr_core",
    "eventList": [
        {
            "id": "acb933ed-9492-43f6-9302-4b318c05bf22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "653369cb-e011-4217-bd28-ab0a9485dc1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "a58d5087-115c-46ee-a217-18f0f890a4d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "7ac2c552-614f-4dcc-a215-1b953e6409db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "2e68e452-8f73-4c77-9538-1ab485a4b878",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "42e68dae-542e-47e9-b81d-81c9ac48e34b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "4b522234-08c7-4683-8de9-b11036c2092e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "7dd192df-1deb-40cd-90c0-cabe455b8277",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "091ed4fe-ee28-4624-88d6-aa1a822e9ed1",
    "visible": true
}