/// @description babyjams - CleanUp
if (global.___babyjams != id)
  return;

// Do hooks last
_babyJams_doHooks(babyJams_cleanUpHooks());

_babyJams_hooksCleanUp();