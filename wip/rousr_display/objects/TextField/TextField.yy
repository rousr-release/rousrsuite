{
    "id": "67c7dce5-6d32-42bb-9fa1-b69185db1144",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TextField",
    "eventList": [
        {
            "id": "d4a83fd7-cc9e-4a51-a927-7999bb4be257",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "67c7dce5-6d32-42bb-9fa1-b69185db1144"
        },
        {
            "id": "faea538d-d971-4286-99a3-0a08cfca17e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "67c7dce5-6d32-42bb-9fa1-b69185db1144"
        },
        {
            "id": "e4ff7db3-f8ed-4b0b-a04f-12366317b9c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "67c7dce5-6d32-42bb-9fa1-b69185db1144"
        },
        {
            "id": "40f158f2-c469-4e99-be7b-e5f20d192d89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "67c7dce5-6d32-42bb-9fa1-b69185db1144"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "869e5be6-f7dc-467c-8dd9-5be8587f42b2",
    "visible": false
}