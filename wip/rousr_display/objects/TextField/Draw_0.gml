/// @description Draw a textfield
draw_self();
var col = draw_get_color();
{
  draw_set_color(global.fieldFg);
  var tx = x;
  switch(Alignment) {
    case TextAlign.Right:
      tx += (FieldW - string_width(Text)) - 1;
      break;
    
    case TextAlign.Left:
    default: break;
  }
  
  draw_text(tx, y+1, Text);
}
draw_set_color(col);