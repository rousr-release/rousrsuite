{
    "id": "20c287e7-5aee-41e7-93f2-491448840bcc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ButtonWide",
    "eventList": [
        {
            "id": "a1871da9-6f50-44ba-9566-4ca01a628a9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "20c287e7-5aee-41e7-93f2-491448840bcc"
        },
        {
            "id": "e2cb3d75-6533-4e4d-a3b8-ec03e3c9b3ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20c287e7-5aee-41e7-93f2-491448840bcc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
    "visible": true
}