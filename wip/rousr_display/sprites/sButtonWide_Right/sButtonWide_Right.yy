{
    "id": "856df45f-8eb9-4161-b3fb-7e0307098624",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide_Right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ee56155-9e65-4806-a86c-b2bafe349c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856df45f-8eb9-4161-b3fb-7e0307098624",
            "compositeImage": {
                "id": "847c045a-c9a2-4da3-bb38-d993aece5dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee56155-9e65-4806-a86c-b2bafe349c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16efc06e-8f6d-488d-946c-1a7b8a799015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee56155-9e65-4806-a86c-b2bafe349c35",
                    "LayerId": "35347237-386f-41ed-afaa-405e142555c3"
                }
            ]
        },
        {
            "id": "a6085f5b-82aa-41ba-b681-9740e45cc912",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856df45f-8eb9-4161-b3fb-7e0307098624",
            "compositeImage": {
                "id": "24e185d0-7872-433b-87d4-58cda6eb88e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6085f5b-82aa-41ba-b681-9740e45cc912",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebdd9da7-23e2-409a-aaa6-f1420a4e7eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6085f5b-82aa-41ba-b681-9740e45cc912",
                    "LayerId": "35347237-386f-41ed-afaa-405e142555c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "35347237-386f-41ed-afaa-405e142555c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "856df45f-8eb9-4161-b3fb-7e0307098624",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}