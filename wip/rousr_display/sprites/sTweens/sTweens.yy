{
    "id": "2e7d478e-90e4-4ad8-87d0-2566b1da2161",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTweens",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aa6743b-2585-41c9-a9dd-374b5b66fc49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e7d478e-90e4-4ad8-87d0-2566b1da2161",
            "compositeImage": {
                "id": "de9e7e9c-19ba-4f6c-979c-256683950fbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa6743b-2585-41c9-a9dd-374b5b66fc49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1c9baf-0fc6-4af1-9f7b-656e01711c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa6743b-2585-41c9-a9dd-374b5b66fc49",
                    "LayerId": "eafc4b5d-8450-4060-8bf0-f188be9e78d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "eafc4b5d-8450-4060-8bf0-f188be9e78d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e7d478e-90e4-4ad8-87d0-2566b1da2161",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}