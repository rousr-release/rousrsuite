{
    "id": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleBar_CloseButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb61c826-6356-4b7f-bafd-f3a3e230b3b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
            "compositeImage": {
                "id": "0da4276b-2907-4ba1-9b60-09eaf2c8205d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb61c826-6356-4b7f-bafd-f3a3e230b3b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7737ee77-d516-49c9-954e-e36b8406c969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb61c826-6356-4b7f-bafd-f3a3e230b3b8",
                    "LayerId": "255e2da9-858b-46f4-b065-a04b44959063"
                }
            ]
        },
        {
            "id": "5a620c05-5cd7-4dd8-bb27-4ba610caebcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
            "compositeImage": {
                "id": "76f50bee-f8d6-43db-abe6-d8c03aa2e462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a620c05-5cd7-4dd8-bb27-4ba610caebcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c91a5d7e-d082-4d70-bc60-ec22a6c32468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a620c05-5cd7-4dd8-bb27-4ba610caebcb",
                    "LayerId": "255e2da9-858b-46f4-b065-a04b44959063"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "255e2da9-858b-46f4-b065-a04b44959063",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}