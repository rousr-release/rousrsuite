{
    "id": "1be453cc-1512-4869-9d82-96d44ce20eca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sScrollArea_Tab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "924eb5f3-7706-430c-9155-8684f20a8323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be453cc-1512-4869-9d82-96d44ce20eca",
            "compositeImage": {
                "id": "a95d8311-c61c-4e3f-afde-9be884d733c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "924eb5f3-7706-430c-9155-8684f20a8323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a522b250-1831-45e3-ad9c-8f68ba755889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "924eb5f3-7706-430c-9155-8684f20a8323",
                    "LayerId": "df56e16d-29e4-47cf-8df1-aa04408578b4"
                }
            ]
        },
        {
            "id": "3db8609b-f843-40e5-be67-1752ed20b97f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be453cc-1512-4869-9d82-96d44ce20eca",
            "compositeImage": {
                "id": "5e89bf9e-ade4-4e0e-a098-976bf66a41d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db8609b-f843-40e5-be67-1752ed20b97f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceb8276c-fd3d-4c79-81c0-e4a408202e8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db8609b-f843-40e5-be67-1752ed20b97f",
                    "LayerId": "df56e16d-29e4-47cf-8df1-aa04408578b4"
                }
            ]
        },
        {
            "id": "09dc34ef-5fd5-40ab-903a-9bd4825e2bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be453cc-1512-4869-9d82-96d44ce20eca",
            "compositeImage": {
                "id": "48b46e61-d13f-4c25-bacb-11574d3b1a82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09dc34ef-5fd5-40ab-903a-9bd4825e2bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d369fbe9-09b0-4e4d-978b-24aec5bbae75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09dc34ef-5fd5-40ab-903a-9bd4825e2bcf",
                    "LayerId": "df56e16d-29e4-47cf-8df1-aa04408578b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "df56e16d-29e4-47cf-8df1-aa04408578b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1be453cc-1512-4869-9d82-96d44ce20eca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}