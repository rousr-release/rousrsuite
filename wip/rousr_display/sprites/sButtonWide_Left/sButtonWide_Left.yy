{
    "id": "01882bc4-fa43-46ea-ab24-0ae3f0033423",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide_Left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "122e79f9-c6fe-4456-ad23-874ed93061a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01882bc4-fa43-46ea-ab24-0ae3f0033423",
            "compositeImage": {
                "id": "1593bcb2-32e3-4d2d-a3df-e20984b4e73f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "122e79f9-c6fe-4456-ad23-874ed93061a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41118360-bb77-47f7-b6f3-0efc252ecfc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "122e79f9-c6fe-4456-ad23-874ed93061a7",
                    "LayerId": "5d0bdd11-b3f8-4037-993f-152ef47c49ad"
                }
            ]
        },
        {
            "id": "b218fc78-9041-4759-b296-74290d6aeecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01882bc4-fa43-46ea-ab24-0ae3f0033423",
            "compositeImage": {
                "id": "8bf6c1cc-2cdd-42ec-95d0-b26a718436d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b218fc78-9041-4759-b296-74290d6aeecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d39a0a52-f1c0-43d1-bb2b-fa9649fd5b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b218fc78-9041-4759-b296-74290d6aeecf",
                    "LayerId": "5d0bdd11-b3f8-4037-993f-152ef47c49ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "5d0bdd11-b3f8-4037-993f-152ef47c49ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01882bc4-fa43-46ea-ab24-0ae3f0033423",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}