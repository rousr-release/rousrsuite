{
    "id": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GenericDraw",
    "eventList": [
        {
            "id": "f97a8d24-dc89-4505-9881-c58a63537cbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
        },
        {
            "id": "9e171a8e-2c1a-48dc-81fd-c87aee87aba5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
        },
        {
            "id": "9223f10c-c450-464c-91f3-f987062140e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
        },
        {
            "id": "8268a1fb-01c4-4d87-bcba-b0c170f0bb7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
        },
        {
            "id": "f0a67477-97ca-4426-9cec-c85d22ebda41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}