{
    "id": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Generic",
    "eventList": [
        {
            "id": "6eaf9631-d3b4-48d5-8d59-362237027122",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827"
        },
        {
            "id": "13f06f5c-b4bf-42e0-be0b-456e43269ccd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827"
        },
        {
            "id": "a5253191-c3cc-420a-8a2d-c0db2cafa603",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827"
        },
        {
            "id": "e0fb66ed-b0ae-463c-bc8c-fdc005228141",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}