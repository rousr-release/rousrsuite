{
    "id": "66d08ca0-1b2e-4365-9377-763d44741f48",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GenericStepDrawGUI",
    "eventList": [
        {
            "id": "c2f8bdc5-a76f-4a1b-9bf3-2046d04d255a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "66d08ca0-1b2e-4365-9377-763d44741f48"
        },
        {
            "id": "68eca9dc-c450-4d2f-8742-ee5b83fee983",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "66d08ca0-1b2e-4365-9377-763d44741f48"
        },
        {
            "id": "846efdbc-269b-40e2-84b4-127e0fe8c8c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "66d08ca0-1b2e-4365-9377-763d44741f48"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "afe43b15-1c62-40da-845b-629bb89e450e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}