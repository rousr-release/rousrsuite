{
    "id": "16b9a859-5919-4978-a493-48f68fe6180c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GenericStep",
    "eventList": [
        {
            "id": "b904e8ea-4b1a-45f8-b6ab-364a5a6fbbf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "16b9a859-5919-4978-a493-48f68fe6180c"
        },
        {
            "id": "c7216908-c36a-4dc6-a450-def61e8a8393",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "16b9a859-5919-4978-a493-48f68fe6180c"
        },
        {
            "id": "ce89d9a1-2df7-4a43-8e9e-1db0e611fe01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "16b9a859-5919-4978-a493-48f68fe6180c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}