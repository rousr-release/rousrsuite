{
    "id": "c6b2b3c8-bdea-491a-9dbb-063ac14668fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GenericStepDraw",
    "eventList": [
        {
            "id": "d988885c-5117-430a-86ee-9a740605b8ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c6b2b3c8-bdea-491a-9dbb-063ac14668fd"
        },
        {
            "id": "cfd487a6-0c82-4e6a-a38d-efc2048fdc34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6b2b3c8-bdea-491a-9dbb-063ac14668fd"
        },
        {
            "id": "b9953c70-3911-4a2b-b393-85dca716f547",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c6b2b3c8-bdea-491a-9dbb-063ac14668fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}