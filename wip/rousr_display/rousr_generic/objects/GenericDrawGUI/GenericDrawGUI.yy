{
    "id": "afe43b15-1c62-40da-845b-629bb89e450e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GenericDrawGUI",
    "eventList": [
        {
            "id": "6c9a7a99-331f-43df-9e1b-409c594bfac6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        },
        {
            "id": "8afe9653-afba-4b23-a877-62c868f5aea7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        },
        {
            "id": "b3619cc1-6333-4417-8971-6716e1c20375",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        },
        {
            "id": "9a39cf19-4ad6-4679-8984-ca7f59d1c329",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        },
        {
            "id": "e78eb8d1-8e65-4313-b7ea-3a8b80c609ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}