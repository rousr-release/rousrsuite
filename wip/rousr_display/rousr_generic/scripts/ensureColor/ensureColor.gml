///@desc cache the current color based on palette ben
///@param penIndex
var _color = argument0;
if (_color != global._setColor) {
  draw_set_color(_color);
  global._setColor = _color;
}