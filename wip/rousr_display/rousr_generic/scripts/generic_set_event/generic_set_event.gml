///@desc generic_event_set - set the callback for a generic event
///@param genericObjectId
///@param EGenericHook
///@param eventCallback
var _id       = argument0;
var _index    = argument1;
var _callback = argument2;

with(_id) EventHooks[_index] = _callback != undefined && _callback != noone ? _callback : dummy_function;