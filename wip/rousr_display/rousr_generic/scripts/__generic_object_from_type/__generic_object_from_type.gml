///@desc __generic_object_from_type - return what a thing is
///@arg _type - EGenericType
gml_pragma("forceinline");

var type = Generic; 
switch (argument0) {
  case EGenericType.Step:        type = GenericStep;  break;
  case EGenericType.Draw:        type = GenericDraw;  break;
	case EGenericType.StepDraw:    type = GenericStepDraw;  break;
  case EGenericType.DrawGUI:     type = GenericDrawGUI;  break;
  case EGenericType.StepDrawGUI: type = GenericStepDrawGUI;  break;

  default: break;
};

return type;