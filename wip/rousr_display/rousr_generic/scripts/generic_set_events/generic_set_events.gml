///@param generic_events_set - set the callback for a bunch of generic events... 
///@param genericObjectId
///@param EGenericHooks.event
///@param script
///@param ...
var _id       = argument[0];
var numArgs   = argument_count - 1;

with(_id) for (var i = 0; i < numArgs; i+=2) {
	var _index = argument[i], _callback = argument[i+1];
	EventHooks[_index] = _callback != undefined && _callback != noone ? _callback : dummy_function;
}