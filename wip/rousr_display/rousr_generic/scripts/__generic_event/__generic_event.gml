///@desc __generic_event - helper function to call the event function only if they exist.
///@arg _event - event type
gml_pragma("forceinline");

var cb = EventHooks[argument0];
if (cb != dummy_function) script_execute(cb);