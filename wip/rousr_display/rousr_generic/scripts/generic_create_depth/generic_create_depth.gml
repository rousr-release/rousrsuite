///@desc generic_create_depth - create a generic object
///@param x
///@param y
///@param depth
///@param (opt)create script
///@param (opt)generic_type
var _x      = argument[0], 
    _y      = argument[1], 
    _depth  = argument[2];
var _create = argument_count > 3 ? argument[3] : undefined;
var _type   = argument_count > 4 ? argument[4] : EGenericType.Empty;

with (instance_create_depth(_x, _y, _depth, __generic_object_from_type(_type))) {
	if (_create != undefined && _create != noone) 
    script_execute(_create);
	return id;
}

return noone;