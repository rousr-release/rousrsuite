///@desc _generic_ - definitions and docs for Generic Object
  

///define constants and enums for the Generic object system
enum EGenericHook {
	Create = 0,
	Destroy,
	CleanUp,
	
	BeginStep,
	Step,
	EndStep,
	
	BeginDraw,
	Draw,
	EndDraw,
	
  BeginDrawGUI,
  DrawGUI,
  EndDrawGUI,
	
  PreDraw,
  PostDraw,
  
	Num,
}

enum EGenericType {
	Empty,
	Step,
	
  Draw,
	StepDraw,
  
  DrawGUI,
  StepDrawGUI,
  
	Num
};

