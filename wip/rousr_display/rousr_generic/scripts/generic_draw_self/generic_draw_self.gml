///@desc generic_draw_self - function that just calls draw_self, can be used with generics
gml_pragma("forceinline");
draw_self();