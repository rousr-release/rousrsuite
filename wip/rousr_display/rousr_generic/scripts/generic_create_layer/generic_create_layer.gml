///@desc generic_create_layer - create a generic object
///@param x
///@param y
///@param layerOrName
///@param (opt)create script
///@param (opt)generic_type
var _x           = argument[0], 
    _y           = argument[1], 
    _layerOrName = argument[2];
var _create = argument_count > 3 ? argument[3] : undefined;
var _type   = argument_count > 4 ? argument[4] : EGenericType.Empty;

with (instance_create_layer(_x, _y, _layerOrName, __generic_object_from_type(_type))) {
	if (_create != undefined && _create != noone)
		script_execute(_create);
	return id;
}

return noone;