{
    "id": "6eba836d-b7f7-41f8-8c50-a7a147b6a0f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "the_intrinsic_value_of_breath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "72fdf1bd-6b78-4b0e-bf91-4e060746637f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6eba836d-b7f7-41f8-8c50-a7a147b6a0f0",
            "compositeImage": {
                "id": "f6399512-a7dc-429f-90c9-5d24abe94586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72fdf1bd-6b78-4b0e-bf91-4e060746637f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bfba124-20da-42e1-a9c3-ad386f756cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72fdf1bd-6b78-4b0e-bf91-4e060746637f",
                    "LayerId": "663e2d53-44a2-4bb5-b850-d1696f1f40d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "663e2d53-44a2-4bb5-b850-d1696f1f40d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6eba836d-b7f7-41f8-8c50-a7a147b6a0f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}