///@desc 

if (Bloom_State == EBloom_Presets.Num) {
  draw_surface(application_surface, 0, 0);
  return;
}

var bloom_params = Bloom_Settings[Bloom_State];

var render_width  = Screen_Width; //* 0.5;
var render_height = Screen_Height; // * 0.5;

for (var i = 0; i < Num_Targets; ++i) {
  if (!surface_exists(Render_Target[i]))
    Render_Target[i] = surface_create(render_width, render_height);
}

/// This is where it all happens. Grabs a scene that has already been rendered,
/// and uses postprocess magic to add a glowing bloom effect over the top of it.

// Pass 1: draw the scene into rendertarget 1, using a
// shader that extracts only the brightest parts of the image.
surface_set_target(Render_Target[0]);
shader_set(bloomExtract);
shader_set_uniform_f(_Uniforms[EBloom_Uniforms.Threshold], bloom_params[EBloom_Setting.Threshold]);
draw_surface_stretched(application_surface, 0, 0, render_width, render_height);
shader_reset();
surface_reset_target();

// Pass 2: draw from rendertarget 1 into rendertarget 2,
// using a shader to apply a horizontal gaussian blur filter.
var blur = gaussian_set_params(1 / render_width, 0, bloom_params[EBloom_Setting.Blur]);

gpu_set_tex_filter(true);
surface_set_target(Render_Target[1])
shader_set(gaussianBlur);
shader_set_uniform_f_array(_Uniforms[EBloom_Uniforms.SampleOffsetsX], blur[0]);
shader_set_uniform_f_array(_Uniforms[EBloom_Uniforms.SampleOffsetsY], blur[1]);
shader_set_uniform_f_array(_Uniforms[EBloom_Uniforms.Weights],  blur[2]);
draw_surface(Render_Target[0], 0, 0);
shader_reset();
surface_reset_target();

// Pass 3: draw from rendertarget 2 back into rendertarget 1,
// using a shader to apply a vertical gaussian blur filter.
blur = gaussian_set_params(0, 1 / render_height, bloom_params[EBloom_Setting.Blur]);

surface_set_target(Render_Target[0])
shader_set(gaussianBlur);
shader_set_uniform_f_array(_Uniforms[EBloom_Uniforms.SampleOffsetsX], blur[0]);
shader_set_uniform_f_array(_Uniforms[EBloom_Uniforms.SampleOffsetsY], blur[1]);
shader_set_uniform_f_array(_Uniforms[EBloom_Uniforms.Weights],  blur[2]);
draw_surface(Render_Target[1], 0, 0);
shader_reset();
surface_reset_target();

// Pass 4: draw both rendertarget 1 and the original scene
// image back into the main backbuffer, using a shader that
// combines them to produce the final bloomed result.
var surface_texture = surface_get_texture(Render_Target[0]);

shader_set(bloomCombine);
shader_set_uniform_f(_Uniforms[EBloom_Uniforms.BloomIntensity],  bloom_params[EBloom_Setting.BloomIntensity]);
shader_set_uniform_f(_Uniforms[EBloom_Uniforms.BaseIntensity],   bloom_params[EBloom_Setting.BaseIntensity]);
shader_set_uniform_f(_Uniforms[EBloom_Uniforms.BloomSaturation], bloom_params[EBloom_Setting.BloomSat]);
shader_set_uniform_f(_Uniforms[EBloom_Uniforms.BaseSaturation],  bloom_params[EBloom_Setting.BaseSat]);

texture_set_stage(_Uniforms[EBloom_Uniforms.BloomSampler], surface_texture);
draw_surface(application_surface, 0, 0);
shader_reset();

gpu_set_tex_filter(false);