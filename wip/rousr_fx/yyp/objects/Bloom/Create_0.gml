///@desc 

enum EBloom_Setting {
  Threshold = 0,

  Blur,

  BloomIntensity,
  BaseIntensity,
  BloomSat,
  BaseSat,    
};

enum EBloom_Presets {
  Default = 0,
  Soft,
  Desaturated,
  Saturated,
  Blurry,
  Subtle,
  
  Num
};

enum EBloom_Uniforms {
  // Extract
  Threshold,

  // Gaussian Blur
  SampleOffsetsX,
  SampleOffsetsY,
  Weights,
  
  // Combine
  BloomIntensity,
  BaseIntensity,
  BloomSaturation,
  BaseSaturation,
  BloomSampler,
  
  Num
};


Screen_Width = 640;
Screen_Height = 360;

Num_Targets = 2;
Render_Target = array_create(Num_Targets, -1);

_Uniforms = array_create(EBloom_Uniforms.Num);

// Extract
_Uniforms[EBloom_Uniforms.Threshold]      =shader_get_uniform(bloomExtract, "BloomThreshold");

  // Gaussian Blur
_Uniforms[EBloom_Uniforms.SampleOffsetsX] = shader_get_uniform(gaussianBlur, "SampleOffsetsX");
_Uniforms[EBloom_Uniforms.SampleOffsetsY] = shader_get_uniform(gaussianBlur, "SampleOffsetsY");
_Uniforms[EBloom_Uniforms.Weights]        = shader_get_uniform(gaussianBlur, "SampleWeights");
  
// Combine
_Uniforms[EBloom_Uniforms.BloomIntensity]  = shader_get_uniform(bloomCombine, "BloomIntensity");
_Uniforms[EBloom_Uniforms.BaseIntensity]   = shader_get_uniform(bloomCombine, "BaseIntensity");
_Uniforms[EBloom_Uniforms.BloomSaturation] = shader_get_uniform(bloomCombine, "BloomSaturation");
_Uniforms[EBloom_Uniforms.BaseSaturation]  = shader_get_uniform(bloomCombine, "BaseSaturation");
_Uniforms[EBloom_Uniforms.BloomSampler]    = shader_get_sampler_index(bloomCombine, "BloomSampler");

Bloom_Settings = array_create(EBloom_Presets.Num);
Bloom_Settings[EBloom_Presets.Default]     = [ 0.25,  4,   1.25, 1,   1,   1];
Bloom_Settings[EBloom_Presets.Soft]        = [ 0,     3,   1,    1,   1,   1];
Bloom_Settings[EBloom_Presets.Desaturated] = [ 0.5,   8,   2,    1,   0,   1];
Bloom_Settings[EBloom_Presets.Saturated]   = [ 0.25,  4,   2,    1,   2,   0];
Bloom_Settings[EBloom_Presets.Blurry]      = [ 0,     2,   1,    0.1, 1,   1];
Bloom_Settings[EBloom_Presets.Subtle]      = [ 0.5,   2,   1,    1,   1,   1];

Bloom_State = EBloom_Presets.Default;

global.Blur_params = ds_map_create();

var bloom_params = Bloom_Settings[Bloom_State];
var halfWidth = Screen_Width * 0.5;
var halfHeight = Screen_Height * 0.5;

gaussian_set_params(1 / halfWidth, 0,   bloom_params[EBloom_Setting.Blur]);
gaussian_set_params(0, 1 / halfHeight,  bloom_params[EBloom_Setting.Blur]);

application_surface_draw_enable(false);