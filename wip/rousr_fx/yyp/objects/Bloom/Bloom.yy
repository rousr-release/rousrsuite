{
    "id": "1c699b67-2065-4e1c-b68d-177b0df54bf4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Bloom",
    "eventList": [
        {
            "id": "5157c5a8-7a02-4a61-b49d-1e685a1aabe1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        },
        {
            "id": "9548e083-565a-49e1-a837-e5806b09b1f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        },
        {
            "id": "6839d39a-9157-461d-91c7-d7567703cc9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        },
        {
            "id": "762a3e5a-9a23-42d3-8fee-123f1432738a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        },
        {
            "id": "eb62d49f-2af3-458d-a4f8-c651e5edfbfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1c699b67-2065-4e1c-b68d-177b0df54bf4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6eba836d-b7f7-41f8-8c50-a7a147b6a0f0",
    "visible": true
}