///@func rousr_data_grid_destroy(_data_grid)
///@desc destroy a rousr data grid,
///@param {Array} _data_grid - rousr_data_grid to free
gml_pragma("forceinline");
var _data_grid = argument0;
if (!is_real(_data_grid) || _data_grid == undefined)
  return;
  
var stack_id = _data_grid[ERousrDataGrid.AvailableItems];
var grid_id  = _data_grid[ERousrDataGrid.Grid]
if (stack_id != undefined) ds_stack_destroy(stack_id);
if (grid_id != undefined)  ds_grid_destroy(grid_id);

_data_grid[@ ERousrDataGrid.TotalItems]     = -1;
_data_grid[@ ERousrDataGrid.Grid]           = undefined;
_data_grid[@ ERousrDataGrid.AvailableItems] = undefined;
_data_grid[@ ERousrDataGrid.NumItems]       = undefined;
_data_grid[@ ERousrDataGrid.NumFields]      = undefined;
_data_grid[@ ERousrDataGrid.Strict]         = undefined;

