///@func rousr_data_grid_new_item(_rousr_data_grid)
///@desc allocate/return an index for a new item. not necessarily sequential
///@param {Array] _rousr_data_grid  rousr_data_grid to add the item to
///@returns {Real} item index
var _data_grid = argument0;

