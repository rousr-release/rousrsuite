///@func rousr_data_grid_definitions()
///@desc enums and macros for data grids
enum ERousrDataGrid {
  Grid = 0,
  
  AvailableItems,
  UsedItems,   
  
  TotalItems,
  
  NumFields,
  NumItems,
  
  Strict, 
  Num,
  
  // Synonyms
  Cols   = ERousrDataGrid.NumFields,
  Rows   = ERousrDataGrid.NumItems,
};

#macro __rousr_data_grid_DefaultWidth  (1)
#macro __rousr_data_grid_DefaultHeight (1)