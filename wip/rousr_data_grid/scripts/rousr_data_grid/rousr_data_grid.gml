///@func rousr_data_grid(_rousr_data_grid, [_item_index], [_field_index], [_new_value])
///@desc depending on the parameters passed, this function returns various different things
///@param {Array} _rousr_data_grid - the `rousr_data_grid` to operate on, if only param, returns the ds_grid this `rousr_data_grid` wraps
///@param {Real}  [_item_index]    - the index of an item to look up or set, if `_item_index` is passed, `_field_index` must also be passed, or its ignored
///@param {Real}  [_field_index]   - the field of an item to look up or set, if `_new_value` isn't passed, then this returns the value of `_field_index` of `_item_index`
///@param {*}     [_new_value]     - the new value to set `_field_index` of `_item_index` 
///@returns {Real|*|true} if only `_rousr_data_grid` then `grid_id`, if '_item_index' and '_field_index', it's `value`, if `_new_value` true on set, `undefined` on error
gml_pragma("forceinline");

var _rousr_data_grid = argument[0];
var grid = _rousr_data_grid[ERousrDataGrid.Grid];

if (argument_count < 3)
  return grid;

var _item_index  = argument[1],
    _field_index = argument[2];

if (argument_count == 3)
  return grid[# _item_index, _field_index];

if (argument_count > 3) {
  var _new_value = argument[3];
  grid[# _item_index, _field_index] = _new_value;
  return true;
} 

return undefined; 