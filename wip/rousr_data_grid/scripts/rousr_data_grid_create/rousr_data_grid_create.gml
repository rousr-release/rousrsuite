///@func rousr_data_grid_create([_numItems], [_numFields], [_strict=false]);            
///@desc create a `rousr_data_grid`                                    
///@param {Real}    _numItems       - default number of items in the data grid 
///@param {Real}    _numFields      - default number of fields per item        
///@param {Boolean} [_strict=false] - if strict is false, the grid will resize out of bounds writes
///@returns {Array} `rousr_data_grid` object or undefined

///@extended
// A data grid uses a combination of `ds_grid` and `ds_stack` to manage large databases of items
// the way data is stored in the actual `ds_grid` is as follows:
//
// rousr_data_grid = [   //item      //item     //item     //item
//                     [ field, ], [ field ], [ field ], [ field ] ]   
//                     [ field, ], [ field ], [ field ], [ field ] ]   
// ]   
// var itemField = rousr_data_grid[# itemIndex, fieldIndex]; 
///@extended
gml_pragma("forceinline");
 
var _numItems  = argument_count > 0 ? argument[0] : __rousr_data_grid_DefaultWidth;
var _numFields = argument_count > 1 ? argument[1] : __rousr_data_grid_DefaultHeight;
var _strict    = argument_count > 2 ? argument[2] : false;

var data_grid = array_create(ERousrDataGrid.Num);
var stack_id = ds_stack_create();
var grid_id  = ds_grid_create(_numItems, _numFields);

// todo: implement pushing new items
data_grid[@ ERousrDataGrid.AvailableItems] = stack_id;
data_grid[@ ERousrDataGrid.UsedItems]      = [ [ ], 0 ];
data_grid[@ ERousrDataGrid.TotalItems]     = 0;

data_grid[@ ERousrDataGrid.Grid]        = grid_id;
data_grid[@ ERousrDataGrid.NumItems]    = _numItems;
data_grid[@ ERousrDataGrid.NumFields]   = _numFields; 
data_grid[@ ERousrDataGrid.Strict]      = _strict;

return data_grid;