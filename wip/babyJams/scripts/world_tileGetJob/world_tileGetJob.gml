///@desc return whether a specific work type is assigned to a tile
var _workID   = argument0;

var worldTile = world_tile(argument1, argument2);
var workList = worldTile[@EWorldTile.WorkList];
if (workList != undefined) {
  var numWork = ds_list_size(workList);
  for (var workIndex = 0; workIndex < numWork; ++workIndex) {
    var job = jobs_get(workList[| workIndex]);
    if (job[EJob.WorkID] == _workID)
      return job;
  }
}

return undefined;