///@desc helper functino to resize a frame
///@param _w
///@param _h
var _w = argument0, _h = argument1;
FrameWidth = _w;
FrameHeight = _h;
__frameDirty = true;