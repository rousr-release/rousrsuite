///@desc return true if the passed position is already on the list
///@param _list
///@param _args...
gml_pragma("force-inline");
var numArgs = argument_count;
if (numArgs < 2)
	return undefined;
	
var _list = argument[0];
var args = array_create(numArgs - 1);
for (var i = 1; i < numArgs; ++i) 
	args[i-1] = argument[i];

return __inListArgArray(args, _list);