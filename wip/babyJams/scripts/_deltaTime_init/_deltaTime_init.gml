///@desc deltaTime init
#macro gameTimer (get_timer()/1000000)

global.frameDt = 0;
global.gameDt = 0;
global.gameSpeedScale = 1.0;
global.frameTime = gameTimer;
global.gameTime = gameTimer;
_deltaTimeLastTime = gameTimer;

babyJams_addBeginStepHook(_deltaTime_beginStep, id);