///@desc create default set of animation data
///@param _Opt_spriteIndex
var _spriteIndex = argument_count > 0 ? argument[0] : noone;
var anim = array_create(EAnim.Num);
anim[EAnim.SpriteIndex] = _spriteIndex;
anim[EAnim.Loop] = true;
anim[EAnim.StartFrame] = 0;
anim[EAnim.FinalFrame] = _spriteIndex >= 0 ? sprite_get_number(_spriteIndex) - 1 : 0;
anim[EAnim.RunningSpeed] = 1;
anim[EAnim.FrameCount] = anim[EAnim.FinalFrame] + 1;
return anim;