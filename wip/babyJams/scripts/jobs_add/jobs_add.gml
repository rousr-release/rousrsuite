///@desc add a new job to be searched for
var _workID = argument0;
var job = undefined;
with (global.__jobs) {
  var jm = __jobsMap;
  var jobList = jm[? _workID];
  if (jobList == undefined) {
    jobList = ds_list_create();
    jm[? _workID] = jobList;
  }
  
  var jobIndex = ds_stack_empty(__jobsFree) ? __numJobs : ds_stack_pop(__jobsFree);
  if (jobIndex == __numJobs) {
    __jobsPool[@jobIndex] = array_create(EJob.Num);
    __numJobs++;
  }
  
  job = __jobsPool[jobIndex];
  job[@EJob.Active]   = true;
  job[@EJob.JobIndex] = jobIndex;
  job[@EJob.WorkID]   = _workID;
  job[@EJob.Finished] = false;
  job[@EJob.CellX]    = undefined;
  job[@EJob.CellY]    = undefined;
  job[@EJob.Priority] = 0;
}

return job;