///@desc __lm_find - internal helper function to call a script on each found tilemarker
///@param _chunkId
///@param _findFunction
///@param _markerType
var _chunkId = argument[0];
var _findFunction = argument[1];
var _markerType = argument_count > 2 ? argument[2] : undefined;

with(oGame) {
	var chunk = LevelChunks[_chunkId];
	
	var removables = chunk[ELevelChunk.Removables];
	var numRemovables = 0;
	
	var cols = chunk[ELevelChunk.Cols];
	var rows = chunk[ELevelChunk.Rows];
	
	var markerTileMap = chunk[ELevelChunk.TileMap_Markers];
	
	var minX = 0;
	// parsing from left to right/top to bottom, find the left most blocks we can find
	for (var row = 0; row < rows; ++row) {
		for (var col = 0; col < cols; ++col) {
			var tileData = tilemap_get(markerTileMap, col, row);
			if (_markerType == undefined || tile_get_index(tileData) == _markerType) 
				script_execute(_findFunction, chunk, col, row, tileData);
		}
	}
}