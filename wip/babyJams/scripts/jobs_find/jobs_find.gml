///@desc generate a job from whats around
///@param _entityID
var _entityID = argument0;

var ex = 0;
var ey = 0
var cellX = 0;
var cellY = 0;

with (_entityID) {
  ex = x;
  ey = y;
  
  cellX = ex / World.TileWidth;
  cellY = ey / World.TileHeight;
}

var jobIndex = undefined;
with (global.__jobs) {
  var job = undefined;
  var jobPriority = -1;
  
  // find all available jobs around me
  var searchJobs = world_findJobs(_entityID, cellX, cellY);
  var numSearchJobs = array_length_1d(searchJobs);

  // see if any of them fit my filter
  for (var i = 0; i < numSearchJobs; ++i) {
    var searchJob = jobs_get(searchJobs[i]);
    if (searchJob[EJob.Finished])
      continue;
      
    var priority = searchJob[EJob.Priority];
    if (priority < jobPriority)
      continue;
      
    if (priority == jobPriority && random(2) < 1)
      continue;
    
    jobIndex = searchJobs[i];
    jobPriority = priority;
    job = searchJob;
  }
}

// return the new job
return jobIndex;