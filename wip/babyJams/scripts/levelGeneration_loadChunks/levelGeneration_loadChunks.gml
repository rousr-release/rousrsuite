///@desc levelGeneration_loadChunks - load chunks and find the connectors / removables
with(oGame) {
	var numChunks = 0;
	for (var chunkIndex = 0; true; ++chunkIndex) {
		var levelLayerName       = "Level_"      + string(chunkIndex + 1);
		var markerLayerName      = "Markers_"    + string(chunkIndex + 1);
		var collisionLayerName   = "Collision_"  + string(chunkIndex + 1);
		var foregroundLayerName  = "Foreground_" + string(chunkIndex + 1);
		var midLayerName         = "Mid_"        + string(chunkIndex + 1);
		var bgLayerName          = "Background_" + string(chunkIndex + 1);
		var fogLayerName         = "Fog_"        + string(chunkIndex + 1);
		
		var levelLayerId      = layer_get_id(levelLayerName);
		var markerLayerId     = layer_get_id(markerLayerName);
		var collisionLayerId  = layer_get_id(collisionLayerName);
		var foregroundLayerId = layer_get_id(foregroundLayerName);
		var midLayerId        = layer_get_id(midLayerName);
		var bgLayerId         = layer_get_id(bgLayerName);       
		var fogLayerId        = layer_get_id(fogLayerName);
		
		if (levelLayerId == -1 || markerLayerId == -1 || collisionLayerId == -1)
			break;
	
		if (foregroundLayerId == -1 || midLayerId == -1 || bgLayerId == -1 || fogLayerId == -1) {
			show_debug_message("levelGeneration_loadChunks - warning: Chunk #" + string(chunkIndex + 1) + " is not setup properly. Skipping.");
			continue;
		}
		
		var levelTileMapId      = layer_tilemap_get_id(levelLayerId);
		var markerTileMapId     = layer_tilemap_get_id(markerLayerId);
		var collisionTileMapId  = layer_tilemap_get_id(collisionLayerId);
		var foregroundTileMapId = layer_tilemap_get_id(foregroundLayerId);
		var midTileMapId        = layer_tilemap_get_id(midLayerId);
		var bgTileMapId         = layer_tilemap_get_id(bgLayerId);       
				
		var mapWidth = tilemap_get_width(levelTileMapId), mapHeight = tilemap_get_height(levelTileMapId);
		
		for (var r = 0; r < mapHeight; ++r) {
			for (var c = 0; c < mapWidth; ++c) {
				var tileIndex = tile_get_index(tilemap_get(markerTileMapId, c, r));;
				if (tileIndex != ELevelMarker.BottomRight) 
					continue;
				
				mapHeight = r+1;
				mapWidth  = c+1;
				break;
			}
		}		
		
		var fogSprites = [ ];
		var numFog = 0;
		
		with (oFog) {
			if (layer == fogLayerId)
				fogSprites[numFog++] = id;
		}
		
		LevelChunks[@numChunks++] = [ 
			markerLayerId,   collisionLayerId,   foregroundLayerId,   levelLayerId,   midLayerId,   bgLayerId,   fogLayerId,
			markerTileMapId, collisionTileMapId, foregroundTileMapId, levelTileMapId, midTileMapId, bgTileMapId,
			
			fogSprites,
			
			mapWidth, mapHeight,
			[ ], // Connectors
			[ ], // ConnectorData - Separate so the listInList functions work
			[ ], // Removables
			[ ], // PlayerStarts
			[ ], // Spawners
			[ ]  // SpawnerData
		];		
	
		levelMarkers_findStarts(numChunks - 1);
		levelMarkers_findSpawners(numChunks - 1);
		levelMarkers_findConnectors(numChunks - 1);
		levelMarkers_findRemovables(numChunks - 1);
		
		layer_set_visible(markerLayerId, false);
		layer_set_visible(levelLayerId, false);
		layer_set_visible(collisionLayerId, false);
		layer_set_visible(foregroundLayerId, false);
		layer_set_visible(midLayerId, false);
		layer_set_visible(bgLayerId, false);       
		layer_set_visible(fogLayerId, false);
	}	
	
	NumLevelChunks = array_length_1d(LevelChunks);
}

if (DebugMode) __debug_levelGeneration_loadChunks();