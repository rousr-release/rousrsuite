///@desc return a job for the entity, handle book keeping
///@param _entityID
var _entityID = argument0;
var job = undefined;

var ji = _entityID.__jobIndex;

with (global.__jobs) {
  var jobPool = __jobsPool;
    
  if (ji >= 0 && ji < __numJobs)
    job = jobPool[ji];
    
  if (job != undefined) {
    if (!job[EJob.Active])
      job = undefined;
    else if (job[@EJob.Finished]) {
      jobs_finish(job);
      job = undefined;
    }  
  }
    
  if (job == undefined) {
    ji = jobs_find(id);
    if (ji != undefined)
      job = jobs_get(ji); 
  }    
}

_entityID.__jobIndex = ji;
return job;