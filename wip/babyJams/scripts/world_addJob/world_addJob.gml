///@desc add job to the world 
///@param _workID
///@param _optCellX
///@param _optCellY
var _workID = argument[0];
var _x      = argument_count > 1 ? argument[1] : undefined;
var _y      = argument_count > 2 ? argument[2] : undefined;

_x = _x == -1 ? undefined : _x;
_y = _y == -1 ? undefined : _y;

var job = undefined;
with (global.__jobs) {
  job = jobs_add(_workID);
  if (job != undefined) {
    job[@EJob.CellX] = _x;
    job[@EJob.CellY] = _y;
  
    var worldTile = world_tile(_x, _y);
    if (worldTile != undefined) {
      var workList = worldTile[EWorldTile.WorkList];
      if (workList == undefined || workList == -1) {
        workList = ds_list_create();
        worldTile[@EWorldTile.WorkList] = workList;
      
        with (World)
          ds_list_add(__worldLists, workList);
      }
    
      ds_list_add(workList, job[EJob.JobIndex]);
    } else {
      ds_list_add(UniversalJobs, job[EJob.JobIndex]);
    }
  } 
}

return job;