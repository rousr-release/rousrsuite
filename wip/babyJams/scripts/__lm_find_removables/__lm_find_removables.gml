///@desc __lm_find_removable - what to do with a removable!
///@param _col
///@param _row
///@param _tileData
var _chunk = argument0;
var _col = argument1;
var _row = argument2;
var _tileData = argument3;

var removables = _chunk[ELevelChunk.Removables];
var numRemovables = array_length_1d(removables);
if (__inListOfList(removables, _col, _row)) 
	return false;

// walk from here and find any other removable tiles
var tileMap = _chunk[ELevelChunk.TileMap_Markers];
var maxCol = _chunk[ELevelChunk.Cols];
var maxRow = _chunk[ELevelChunk.Rows];

var visitList = [ [_col, _row] ];
var removable = [ ];
var numRemovable = 0;
var numVisitors = 1;

while (numVisitors > 0) {
	var visit = visitList[--numVisitors];

	var col = visit[0];
	var row = visit[1];
	var visitData = tilemap_get(tileMap, col, row);
	
	if (__inListOfList(removables, col, row))
		continue;
	
	if (__inList(removable, col, row))
		continue;
	
	if (tile_get_index(visitData) != ELevelMarker.Removable) 
		continue;
		
	removable[numRemovable++] = visit;
	
	for (var c = -1; c <= 1; ++c) 
		for (var r = -1; r <= 1; ++r)
			if (col + c >= 0 && col + c < maxCol && row + r >= 0 && row + r < maxRow && abs(c) != abs(r)) visitList[numVisitors++] = [ col+c, row+r ];	
}

removables[@numRemovables] = removable;