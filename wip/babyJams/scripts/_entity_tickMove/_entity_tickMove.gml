///@desc called on an entity, updates the movement from SpeedX / SpeedY
var newx = x;
var newy = y;

var tileHeight = 0;
var tileWidth = 0;
var tileMap = noone;
  
var fall = true;
var cellX = -1, cellY = -1;

with (World) {
  tileHeight = TileHeight; tileWidth = TileWidth; tileMap = TileMapId;
}
  
if (SpeedX != 0 || SpeedY != 0) {
  // Check Movement
  var fx = x;
  var fy = y;
  
  var targetX = (fx + (SpeedX * global.gameDt));
  var targetY = (fy + (SpeedY * global.gameDt))
  
  var xdt = abs(targetX - fx);
  var ydt = abs(targetY - fy);
  
  // Move each pixel towards our goal to make sure we don't jump through anything
  var xstep = 1, ystep = 1;
  if (ydt > xdt) xstep = xdt/ydt;
  else           ystep = ydt/xdt;
  
  xstep *= sign(SpeedX);
  ystep *= sign(SpeedY);
  
  // check at least one
  if (ystep == 0) {
    targetY = fy;
    ystep = 1;
  }
  
  if (xstep == 0) {
    targetX = fx;
    xstep = 1;
  }
  
  var xx = fx, yy = fy;
  var solidTile = false;
  cellX = -1; cellY = -1;
  while (true) {
  
    var cx = floor(xx / tileWidth);
    var cy = floor(yy / tileHeight);

    if (cellX != cx || cellY != cy) {
      if (cellY >= 0) {
        if (world_tileIsSolid(cx, cy))
          break;
      }
    }  

    newx = xx;
    newy = yy;
    cellX = cx; cellY = cy;
    
    if (abs(xx) == abs(targetX) && abs(yy) == abs(targetY))
      break;
      
    var prevX = sign(targetX - xx);
    var prevY = sign(targetY - yy);
      
    xx += xstep;
    yy += ystep;
    
    xx = prevX != sign(targetX - xx) ? targetX : xx;
    yy = prevY != sign(targetY - yy) ? targetY : yy;
  }
}

cellX = floor(newx / tileWidth);
cellY = floor((newy + 1) / tileHeight);

if (cellY >= 0) {
  fall = !world_tileIsSolid(cellX, cellY);
}
  
x = newx;
y = newy;

if (fall) {
  SpeedY += global.FallAccel * global.gameDt;
  if (SpeedY > global.FallMaxSpeed) 
    SpeedY = global.FallMaxSpeed;
    
  if (!_entity_animIs(EEntityAnim.Fall))
    _entity_playAnim(EEntityAnim.Fall);
} else { 
  y = floor(y);
  SpeedY = 0;
  if (_entity_animIs(EEntityAnim.Fall)) 
    _entity_playAnim(EEntityAnim.Idle);
}

if (SpeedX > 0 && !_entity_isPlaying() && !_entity_animIs(EEntityAnim.Walk))
  _entity_playAnim(EEntityAnim.Walk);