///@desc __lm_find_removable - what to do with a removable!
///@param _col
///@param _row
///@param _tileData
var _chunk = argument0;
var _col = argument1;
var _row = argument2;
var _tileData = argument3;

var playerStarts = _chunk[ELevelChunk.PlayerStarts];
var numStarts = array_length_1d(playerStarts);
if (__inList(playerStarts, _col, _row)) 
	return false;

playerStarts[@numStarts] = [ _col, _row ];