///@desc check if the anim is looping
if (floor(image_index) == __entityAnim[EAnim.FinalFrame] && !__entityAnim[EAnim.Loop]) {
  image_speed = 0;
  image_index = floor(image_index);
  __entityAnimPlaying = false;
}