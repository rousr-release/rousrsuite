///@desc Error wrapper
///@param system
///@param text
var _sys = argument0;
var _txt = argument1;


_txt = "[ERROR] " + _sys + " :: " + _txt;

// Potentially show error dialog
show_debug_message(_txt);