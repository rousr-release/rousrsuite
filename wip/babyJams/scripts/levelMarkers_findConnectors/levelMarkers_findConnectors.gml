///@desc levelMarkers_findConnectors - Find the connector tile strips in the chunk
///@param _chunkId
__lm_find(argument0, __lm_find_connectors, ELevelMarker.Connector);