///@desc initialize the jobs system
///@param numDefinitions
var _numWork = argument0;

work_defines();
global.__jobs = id;

__numWork = _numWork;
__workDefs = array_create(_numWork);
for (var i = 0; i < _numWork; ++i)
  __workDefs[@i] = work_createDefault();  

__jobsFree = ds_stack_create();
__numJobs = 0;
__jobsPool = [ ];
__jobsMap = ds_map_create();