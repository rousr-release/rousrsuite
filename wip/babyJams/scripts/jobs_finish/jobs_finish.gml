///@desc finish an active job
///@param _jobOrIndex
var _job = argument0;

if (is_real(_job)) {
  _job = jobs_get(_job);
}

if (!is_array(_job))
  return;

with (global.__jobs) {
  var ji   = _job[@EJob.JobIndex];  
  var wi   = _job[@EJob.WorkID];
  var work = work_getDefinition(wi);
  var loop = work[@EWork.Loop];
  
  if (loop != EWorkConstant.Infinite) {
    loop--;
    loop = max(0, loop);
  }
  
  if (loop == 0) {
    _job[@EJob.Active] = false;
    _job[@EJob.WorkID] = undefined;
    ds_stack_push(__jobsFree, ji);
    
    // remove it from the job map
    var jm = __jobMap;
    var jl = jm[? wi];
    if (jl != undefined) {
      var jli = ds_list_find_index(jl, ji);
      if (jli >= 0)
        ds_list_delete(jl, jli);
    }
  } else {
    _job[@EJob.Finished] = false;
  }
}
