///@desc TileProperties (temp for now)
///@param _cellX
///@param _cellY
var _cellX = argument0;
var _cellY = argument1;
var isSolid = false;

with (World) {
  var worldTile = world_tile(_cellX, _cellY);
  if (worldTile != undefined)
    isSolid = worldTile[@EWorldTile.Flags] & EWorldTileFlags.Solid != 0;
}

return isSolid;

var _tileIndex = argument0;
return (_tileIndex != 0);