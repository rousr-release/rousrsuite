///@desc initialize / set an anim index
///@param _animIndex
///@param _animDataOrSpriteIndex
///@param _animOpts
var _animIndex = argument[0];
var _animData  = is_real(argument[1]) ? anim_createDefault(argument[1]) : argument[1];
var _animOpts  = argument_count > 2 ? argument[2] : undefined;

__entityAnims[_animIndex] = _animData;
 
// Pass along any optional options parameter arguments.
if (is_array(_animOpts)) {
  var numOpts = array_length_1d(_animOpts);
  for (var i = 0; i < numOpts; ++i) {
    var opt = _animOpts[i];
    _animData[@opt[0]] = opt[1];
  } 
}
 
return _animData;