///@desc find work for a specific entity
///_entityID
///_cellX
///_cellY

// Keeps in mind distances

var _entityID = argument0;
var _cellX = argument1;
var _cellY = argument2;

var searchJobs = [ ];

with (World) {
  var numSearchJobs = 0;
  
  // Search the map for jobs we can do

  var numUniversalJobs = ds_list_size(UniversalJobs);
  for (var i = 0; i < numUniversalJobs; ++i)
    searchJobs[@numSearchJobs + i] = UniversalJobs[| i];
}

return searchJobs;