///@desc initialize the bare variables needed to be a "table"
///a table as in a "gaming table" i realized way later that table, as a data container, will confuse this greatly.
///@param layerName
///@param mapWidth
///@param mapHeight
///@param (opt)viewWidth
///@param (opt)viewHeight
///@param (opt)mapX
///@param (opt)mapY
var _layerName = argument[0];
var _mapWidth = argument[1];
var _mapHeight = argument[2];
var _viewW = argument_count > 3 ? argument[3] : 0;
var _viewH = argument_count > 4 ? argument[4] : 0;
var _mapX  = argument_count > 5 ? argument[5] : 0; //tilemap_get_x(TileMapId);
var _mapY  = argument_count > 6 ? argument[6] : 0; //tilemap_get_y(TileMapId);

TileMapId = layer_tilemap_get_id(layer_get_id(_layerName));

TileWidth  = tilemap_get_tile_width(TileMapId);
TileHeight = tilemap_get_tile_height(TileMapId);

MapWidth  = _mapWidth; // tilemap_get_width(TileMapId);
MapHeight = _mapHeight; // tilemap_get_height(TileMapId);		

ViewWidth  = _viewW;
ViewHeight = _viewH;

TileMapX = _mapX;
TileMapY = _mapY;

OffsetX = 0;
OffsetY = 0;		