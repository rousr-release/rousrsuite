///babyJams_onWindowResize
///@desc add a callback for when the window / view is resized
var _cb = argument[0];
var _params = argument_count > 1 ? argument[1] : [ ];
_babyJams_addHook(_cb, _params, global.___windowResizeHooks);