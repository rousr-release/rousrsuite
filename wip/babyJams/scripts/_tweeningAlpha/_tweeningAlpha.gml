/// @desc Tweening Helper function for tweening an object's alpha
// expects to be called from object tweening.
/// @param tween
var tween = argument0;

var easing = tween[Tween.Easing];

var sa = tween[Tween.start_alpha];
var da = tween[Tween.end_alpha];

var startTime = tween[Tween.StartTime];
var duration = tween[Tween.Duration];

var currentTime = gameTimer - startTime;
if (currentTime > duration) currentTime = duration;

var targetId = tween[Tween.TargetId];

var swap = false;
if (sa > da) {
  swap = sa;
  sa = da;
  da = swap;
  swap = true;
} 

with (targetId) {
	if (sa != da) {
  	image_alpha = script_execute(easing, currentTime, sa, da, duration);
    if (swap)
      image_alpha = 1.0 - image_alpha;
  }
}

return (currentTime == duration);