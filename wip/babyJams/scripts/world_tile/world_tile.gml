///@desc get a world tile from a cell x/y
///@param _cellX
///@param _cellY
var _cellX = argument0;
var _cellY = argument1;

var tile = undefined;
with(World) {
  if (_cellX != undefined && _cellX >= 0 && _cellX < MapCols &&
      _cellY != undefined && _cellY >= 0 && _cellY < MapRows) {
    return Map[_cellX, _cellY];
  }
}

return tile;