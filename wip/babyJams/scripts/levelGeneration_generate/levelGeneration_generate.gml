///@desc levelGeneration_generate - Kick off the level generator
var LevelChunkIndex = 0;
var ChunkEdge       = 1;
var ChunkConnector  = 2;

with(oGame) { 
	var chunkPicks = ds_list_create();
	
	// seed the random list
	for (var i = 0; i < NumLevelChunks; ++i) {
		for (var j = 0; j < 2; ++j)
			ds_list_add(chunkPicks, i);
	}
	
	// shuffle and draw
	ds_list_shuffle(chunkPicks);
	var count = ds_list_size(chunkPicks);
	
	var level = [];
	var levelChunkNum = 0;
	
	var openConnectors = ds_list_create();
	var numOpen = 0;
	
	var pickIndex = 0;
	while (!ds_list_empty(chunkPicks)) {
		var chunk = LevelChunks[chunkPicks[|pickIndex]];
		var connectors = chunk[ELevelChunk.Connectors];
		var connectorData = chunk[ELevelChunk.ConnectorData];
		var numConnectors = array_length_1d(connectors)
		
		if (levelChunkNum == 0) {
			// place the chunk in the middle
			var chunkW = chunk[ELevelChunk.Cols];
			var chunkH = chunk[ELevelChunk.Rows];
		
			level[@levelChunkNum++] = [ 0, 0, chunk ]
			for (var connectorIndex = 0; connectorIndex < numConnectors; ++connectorIndex) {
				var data = connectorData[connectorIndex];
				ds_list_add(openConnectors, [ levelChunkNum - 1, data[EConnectorData.Edge], connectors[connectorIndex] ]);
				numOpen++;
			}
			
			ds_list_delete(chunkPicks, pickIndex);
			pickIndex = 0;
		} else {
			// find any connector our connectors match with
			var matches = [ ];
			var numMatches = 0;
			
			for (var connectorIndex = 0; connectorIndex < numConnectors; ++connectorIndex) {
				var connector = connectors[connectorIndex];
				var data      = connectorData[connectorIndex];
				var edge      = data[EConnectorData.Edge];
				
				var chunkW = chunk[ELevelChunk.Cols], chunkH = chunk[ELevelChunk.Rows];
				var connDim   = __lg_connectorDimensions(connector);
				
				for (var openIndex = 0; openIndex < numOpen; ++openIndex) {
					var open          = openConnectors[|openIndex];
					var openEdge      = open[ChunkEdge];
					var openConnector = open[ChunkConnector];
					var levelChunk    = level[open[LevelChunkIndex]];
					var openChunk     = levelChunk[2];
					
					var opposite = false;
					opposite |= edge == EEdge.Left   && openEdge == EEdge.Right;
					opposite |= edge == EEdge.Top    && openEdge == EEdge.Bottom;
					opposite |= edge == EEdge.Right  && openEdge == EEdge.Left;
					opposite |= edge == EEdge.Bottom && openEdge == EEdge.Top;
					
					// narrow down matches to only connectors "opposite" of the one we're checking
					if (!opposite)
						continue;
						
					// found an opposite, can it fit though?
		
					// Get the top-most left-most corner tile of the connector
					var openX = levelChunk[0], openY = levelChunk[1], openW = openChunk[ELevelChunk.Cols], openH = openChunk[ELevelChunk.Rows];
					var openDim = __lg_connectorDimensions(openConnector);
					
					var ox = openDim[@EDim.X];
					var oy = openDim[@EDim.Y];
					
					// get top left corner that puts this chunk at
					var top, left;
					switch (edge) {
						case EEdge.Left:   left = openX + openW;  break;
						case EEdge.Top:    top  = openY + openH;  break;
						case EEdge.Right:  left = openX - chunkW; break;
						case EEdge.Bottom: top  = openY - chunkH; break;
					}
					
					// offset it by the amuont the connector is inset in the chunk
					if (edge == EEdge.Left || edge == EEdge.Right) top  = openY + (oy - connDim[EDim.Y]);	
					else                                           left = openX + (ox - connDim[EDim.X]);
					
					var right = left+chunkW, bottom = top+chunkH;
					
					// check for collisions with other chunks
					var collision = false;
					for (var lIndex = 0; lIndex < levelChunkNum && !collision; ++lIndex) {
						var lchunk = level[lIndex];
						var cdata = lchunk[2];
						var lw = cdata[ELevelChunk.Cols], lh = cdata[ELevelChunk.Rows];
						
						var lleft = lchunk[0], ltop = lchunk[1];
						var lright = lleft+lw, lbottom= ltop+lh;
						
			
						collision = (left < lright &&  right > lleft && top < lbottom && bottom < ltop);
					}
					
					if (!collision)
						matches[numMatches++] = [ left, top, connectorIndex, openIndex ];
				}
			}
		
		
			// If there's no matches, then go to the next pick index		
			if (numMatches == 0) {
				pickIndex++;
				if (pickIndex >= ds_list_size(chunkPicks)) {
					show_debug_message("No valid chunks found to place.")
					break;
				}
			} else {
			
				// pick a random match
				var match = matches[irandom(numMatches - 1)];
				var matchConnectorIndex = match[2];
			
				// Add our unattached, unblocked connectors to the list
				for (var connectorIndex = 0; connectorIndex < numConnectors; ++connectorIndex) {
					if (connectorIndex == matchConnectorIndex)
						continue;
						
					var data = connectorData[connectorIndex];
					ds_list_add(openConnectors, [ levelChunkNum, data[EConnectorData.Edge], connectors[connectorIndex] ]);
					numOpen++;
				}
		
				level[@levelChunkNum++] = [ match[0], match[1], chunk ];
				ds_list_delete(chunkPicks, pickIndex);
				ds_list_delete(openConnectors, match[3]);
				numOpen--;
				pickIndex = 0;
			}
		}
	}
	
	ds_list_destroy(chunkPicks);
	ds_list_destroy(openConnectors);
	
	if (DebugMode) {
		show_debug_message("Level Generated: ");
		show_debug_message("  Placed " + string(levelChunkNum) + " LevelChunks.")
		for (var i = 0; i < levelChunkNum; ++i) {
			var levelChunk = level[i];
			var chunk = levelChunk[2];
			show_debug_message("    Chunk #" + string(i + 1) + " placed at (" + string(levelChunk[0])           + ", " + string(levelChunk[1]) + 
			                                                 ") Dimensions: " + string(chunk[ELevelChunk.Cols]) + "x"  + string(chunk[ELevelChunk.Rows]));
		}	
	}
	
	var top = undefined, left = undefined;
	
	// first find the top left corner so we can offset all the maps by this amount
	for (var i = 0; i < levelChunkNum; ++i) {
		var levelChunk = level[i];
		var xx = levelChunk[0];
		var yy = levelChunk[1];
		
		if (top == undefined) top = yy;
		else top = min(top, yy);
		
		if (left == undefined) left = xx;
		else left = min(left, xx);
	}
	
	var offsetX = -1 * left, offsetY = -1 * top;
	
	// Now generate the new tile map, removing removables at random
	for (var i = 0; i < levelChunkNum; ++i) {
		var levelChunk = level[i];
		var chunk = levelChunk[2];
			
		levelChunk[@0] += offsetX;
		levelChunk[@1] += offsetY;
		
		var chunkOffsetX          = levelChunk[0];
		var chunkOffsetY          = levelChunk[1];
		var chunkW                = chunk[ELevelChunk.Cols];
		var chunkH                = chunk[ELevelChunk.Rows];
		var removables            = chunk[ELevelChunk.Removables];
		var numRemovables         = array_length_1d(removables);
		
		for (var xx = 0; xx < chunkW; ++xx) {
			for (var yy = 0; yy < chunkH; ++yy) {
				for (var tileMapIndex = ELevelChunk.TileMapBegin + 1; tileMapIndex <= ELevelChunk.TileMapEnd; ++tileMapIndex) {
					var tileMap = chunk[tileMapIndex];
					var levelTileMap = LevelTileMaps[tileMapIndex - (ELevelChunk.TileMapBegin + 1)]; // Skip markers
					var tileData = tilemap_get(tileMap, xx, yy);
					tilemap_set(levelTileMap, tileData, xx + chunkOffsetX, yy + chunkOffsetY);
				}
			}
		}
	
		// check for removables
		for (var removableIndex = 0; removableIndex < numRemovables; ++removableIndex) {
			if (irandom(1)) {
				var removable = removables[removableIndex];
				var numTiles = array_length_1d(removable);
				for (var tileIndex = 0; tileIndex < numTiles; ++tileIndex) {
					var tile     = removable[tileIndex];
					var tx = tile[0] + chunkOffsetX,
					    ty = tile[1] + chunkOffsetY;
					
					var tileData = tile_set_index(tilemap_get(LevelTileMaps[ELevelTileMap.Level], tx, ty), 0);
					tilemap_set(LevelTileMaps[ELevelTileMap.Level], tileData, tx, ty);
					
					tileData = tile_set_index(tilemap_get(LevelTileMaps[ELevelTileMap.Collision], tx, ty), 0);
					tilemap_set(LevelTileMaps[ELevelTileMap.Collision], tileData, tx, ty);
				}
			}
		} 
	}			
	
	// Find the player start position
	var startChunkIndex = irandom(levelChunkNum - 1);  // randomly pick the chunk to start in
	var startLevelChunk = level[startChunkIndex];	
	var startChunk = startLevelChunk[2];
	
	var startPositions = startChunk[ELevelChunk.PlayerStarts];
	var numStartPositions = array_length_1d(startPositions);

	var startIndex = irandom(numStartPositions - 1); // randomly pick a position
	var startPos = startPositions[startIndex];
	
	var tw = game_getTileWidth();  tw =	128;
	var th = game_getTileHeight(); th = 128;
	
	// Multiple the tile position position by the tile size
	var pos = [ tw * (startPos[0] + startLevelChunk[0]), tw * (startPos[1] + startLevelChunk[1]) ];
	
	// Character anchor bottom center, so account for it
	pos[Pt.X] += tw * 0.5;
	pos[Pt.Y] += th - 1;
	
	var player = instance_create_layer(pos[Pt.X], pos[Pt.Y], "GameLayer", oPlayer);

	if (DebugMode)
		show_debug_message("Start Chunk: " + string(startChunkIndex + 1) 
		                + " (" + string(startLevelChunk[0] * tw) + ", " + string(startLevelChunk[1] * tw) + ") - Position: " 
		                + string(startPos[0]) + ", " + string(startPos[1]));
}