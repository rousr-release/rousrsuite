///@desc return whether or not the aniamtion is playing
///@param _opt_whichAnim
gml_pragma("forceinline");
if (argument_count == 0)
  return __entityAnimPlaying;
  
var _animIndex = argument[0];
return _animIndex == __entityAnimIndex && __entityAnimPlaying;