///@desc get a work definition
var _workID = argument0;
var def = undefined;
with(global.__jobs) {
  for ( ; __numWork <= _workID; ++__numWork)
    __workDefs[@__numWork] = work_createDefault();
  def = __workDefs[_workID];
}

return def;