///@desc toggle edit on a textfield (memberfunction)
///@param submitIfFinished
var submit = argument0;
var ta = textField_getActive();
if (ta != noone && ta != id) {
  with(ta) { 
    textField_toggleEdit(SubmitOnBlur); 
  }
}

IsEditing = !IsEditing;

if (IsEditing) {
  keyboard_string = "";
  global.__bjui_textFieldActive = id;
} else {
  global.__bjui_textFieldActive = noone;
  if (submit && OnSubmit != noone)
    script_execute(OnSubmit, id);
    
  if (HideOnBlur)
    instance_deactivate_object(id);
}