///@desc world systems init
var _mapId = argument0;

TileMapId = _mapId;
TileHeight = 10;
TileWidth = 10;

MapCols = tilemap_get_width(_mapId);
MapRows = tilemap_get_height(_mapId);

show_debug_message("Created World: " + string(MapCols) + "x" + string(MapRows));

// generate world grid
Map = [ ];
for (var yy = 0; yy < MapRows; ++yy) {
  for (var xx = 0; xx < MapCols; ++xx) {
    var newWorldTile = array_create(EWorldTile.Num);
    newWorldTile[@EWorldTile.Flags] = EWorldTileFlags.None;
    newWorldTile[@EWorldTile.CellX] = xx;
    newWorldTile[@EWorldTile.CellY] = yy;
    newWorldTile[@EWorldTile.WorkList] = undefined;
    
    var tileData = tilemap_get(_mapId, xx, yy);
    var tileIndex = tile_get_index(tileData);
    
    newWorldTile[@EWorldTile.TileIndex] = tileIndex
    if (tileIndex >= 1)
      newWorldTile[@EWorldTile.Flags] |= EWorldTileFlags.Solid;
    
    Map[@ xx, yy] = newWorldTile;
  }
}

__worldLists = ds_list_create();
UniversalJobs = ds_list_create();