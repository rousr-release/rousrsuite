///@desc actor_resolveMovement - move the actor according to what's happened this step.


//Apply gravity
VelocityY += game_getGravity();

if (QueueJump && MultiJumps > 0) {
	if ((IsInAir && Jumped) || !IsInAir) {
		VelocityY = -JumpVelocity;
		MultiJumps--;
		Jumped = true;
	}
}
QueueJump = false;

//Classic GM platforming collisions (thanks to net8floz's work in the BT demo)
if (VelocityX != 0) {
	var svx = sign(VelocityX);
	
	if (actor_tileCollision(id, x + VelocityX, y)) {
		while (!actor_tileCollision(id, x + svx, y)) 
			x += svx;
		
		VelocityX = 0;
	}	
	
	x += VelocityX;
}

var wasInAir = IsInAir;
if (VelocityY != 0) {
	var svy = sign(VelocityY);
	
	IsInAir = true;
	
	mask_index = Anims[EAnimation.Fall] != undefined ? Anims[EAnimation.Fall] : -1;
	if (actor_tileCollision(id, x, y + VelocityY)) {
		while (!actor_tileCollision(id, x, y + svy))
			y += svy;
		
		IsInAir = wasInAir && VelocityY < 0;
		VelocityY = 0;
	}
	mask_index = -1;
	y += VelocityY;
}

// HACKS NOTHING TO SEE HERE.
if (IsInAir && !wasInAir && abs(VelocityY) < 1.0)
	IsInAir = false;
	
var moving = VelocityX != 0;

if (IsInAir)  { if (Anims[EAnimation.Fall] != undefined) sprite_index = Anims[EAnimation.Fall]; } 
else {
	MultiJumps = MaxJumps;
  Jumped = false;
	
	if (moving) { if (Anims[EAnimation.Run] != undefined)  sprite_index = Anims[EAnimation.Run];  } 
	else        { if (Anims[EAnimation.Idle] != undefined) sprite_index = Anims[EAnimation.Idle]; }
}

if (x < xprevious && Facing != EFacing.Left) {
	image_xscale = DefaultFacing == EFacing.Left ? 1.0 : -1.0;
	Facing = EFacing.Left;
} else if (x > xprevious && Facing != EFacing.Right) { 
	image_xscale = DefaultFacing == EFacing.Right ? 1.0 : -1.0;
	Facing = EFacing.Right;
}
VelocityX = 0;

x = floor(x);
y = floor(y);