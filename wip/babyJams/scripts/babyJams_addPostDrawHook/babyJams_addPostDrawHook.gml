///babyJams_addPostDrawHook
///@desc Add a step hook
///@param scriptFunction
///@param (opt)arg1...arg9
gml_pragma("forceinline");

var c = argument_count;
var params = array_create(c - 1);
for (var i = 1; i < c; ++i) {
  params[i - 1] = argument[i];
}

return _babyJams_addHook(argument[0], params, babyJams_postDrawHooks());