///@desc animation system defines

enum EAnim {
  SpriteIndex,
  Loop,
  StartFrame,
  FinalFrame,
  RunningSpeed,
  FrameCount,
  
  Num
};