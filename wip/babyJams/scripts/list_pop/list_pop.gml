///@desc pop an element and return it
///@param id
///@param index
var _id       = argument0;
var _popIndex = argument1;

var val = _id[| _popIndex];
ds_list_delete(_id, _popIndex);

return val;