///@desc __lm_find_connector - what to do with a connector!
///@param _col
///@param _row
///@param _tileData
var _chunk = argument0;
var _col = argument1;
var _row = argument2;
var _tileData = argument3;

var connectors = _chunk[ELevelChunk.Connectors];
var numConnectors = array_length_1d(connectors);
if (__inListOfList(connectors, _col, _row)) 
	return false;

var connectorData = _chunk[ELevelChunk.ConnectorData];

// walk from here and find any other removable tiles
var tileMap = _chunk[ELevelChunk.TileMap_Markers];
var maxCol = _chunk[ELevelChunk.Cols];
var maxRow = _chunk[ELevelChunk.Rows];

var visitList = [ [_col, _row] ];
var connector = [ ];
var connectorSeal = [ ];
var numConnector = 0;
var numVisitors = 1;
var numSeal = 0;

var horz = false;
var vert = false;
var edge = EEdge.None;

while (numVisitors > 0) {
	var visit = visitList[--numVisitors];

	var col = visit[0];
	var row = visit[1];
	var visitData = tilemap_get(tileMap, col, row);
	
	if (__inListOfList(connectors, col, row))
		continue;
	
	if (__inList(connector, col, row))
		continue;
	
	var visitTileIndex = tile_get_index(visitData);
	if (visitTileIndex != ELevelMarker.Connector && visitTileIndex != ELevelMarker.ConnectorSeal)
		continue;

	var bothNot = !horz && !vert;
	
	if (visitTileIndex == ELevelMarker.Connector) {
		connector[numConnector++] = visit;

		if (numConnector == 2) {
			var c1 = connector[0];
			var c2 = connector[1];
		
			horz = c1[0] != c2[0];
			vert = c1[1] != c2[1];
		
			if (horz && vert) {
				show_debug_message("Sanity check: this can't happen.");
				return;
			}
			bothNot = false;
			
			if (horz) edge = (c1[1] == 0) ? EEdge.Top : EEdge.Bottom;
			else      edge = (c1[0] == 0) ? EEdge.Left : EEdge.Right;
		}
	
		if (numConnector < 2) {
			for (var c = col - 1; c <= col + 1; ++c) {
				for (var r = row - 1; r <= row + 1; ++r) {
					if (abs(c) != abs(r)) visitList[numVisitors++] = [ c, r ];	
				}
			}
			continue;		
		} 
	} else {
		if (__inList(connectorSeal, col, row))
			continue;
			
		connectorSeal[numSeal++] = visit;
			
		if (bothNot) {
			horz = true;
			vert = true;
		}
	}

	if (horz) { visitList[numVisitors++] = [ col - 1, row ]; visitList[numVisitors++] = [ col + 1, row ]; }
	if (vert) { visitList[numVisitors++] = [ col, row - 1 ]; visitList[numVisitors++] = [ col, row  + 1]; }	
	
	if (bothNot) {
		horz = false;
		vert = false;
	}
}

connectors[@numConnectors] = connector;
connectorData[@numConnectors] = [ edge, connectorSeal ];