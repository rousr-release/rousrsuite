///@desc remove work from the world
///@param _workID
///@param _OptCellX
///@param _OptCellY
///Note: without cells, removes ALL work of workID
var _workID = argument[0];
var _cellX = argument_count > 1 ? argument[1] : undefined;
var _cellY = argument_count > 2 ? argument[2] : undefined;

with (global.__jobs) {
  var workList = __workMap[? _workID];
  if (workList == undefined) // no work of that type
    return;
    
  // just remove all of it
  if (_cellX == undefined || _cellY == undefined) {
    
  }
}