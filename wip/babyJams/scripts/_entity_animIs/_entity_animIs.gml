///@desc return what the animation playing is
///@param _opt_anim
gml_pragma("forceinline");
if (argument_count == 0)
  return __entityAnimIndex;
  
var _anim = argument[0];
return _anim == __entityAnimIndex;