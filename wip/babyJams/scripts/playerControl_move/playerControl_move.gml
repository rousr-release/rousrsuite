///@desc playerControl_move : Move the player
///@param _action    - what action was pressed
///@param _wasActive - was the action active the previous step
var _action = argument0;
var _wasActive = argument1;

with (oPlayer) {
	switch (_action) {
		case EPlayerControl.Move_Left:  VelocityX += MoveSpeed * global.dt; break;
		case EPlayerControl.Move_Right: VelocityX -= MoveSpeed * global.dt; break;
	}
}