///@desc entity system defines
global.FallAccel = 300;
global.FallMaxSpeed = 200;

enum EEntityAnim {
  Idle,
  
  // Movement
  Walk,
  Fall,
  
  // Actions
  DigBegin,
  Dig,
  
  Num,
};