///@desc __lm_find_removable - what to do with a removable!
///@param _col
///@param _row
///@param _tileData
var _chunk = argument0;
var _col = argument1;
var _row = argument2;
var _tileData = argument3;

var tileIndex = tile_get_index(_tileData);
if (tileIndex >= ELevelMarker.SpawnerBegin && tileIndex <= ELevelMarker.SpawnerEnd) {
	var spawners = _chunk[ELevelChunk.Spawners];
	var spawnerData = _chunk[ELevelChunk.SpawnerData];
	var numSpawners = array_length_1d(spawners);
	if (__inList(spawners, _col, _row)) 
		return false;

	spawners[@numSpawners] = [ _col, _row ];
	spawnerData[@numSpawners] = tileIndex - ELevelMarker.SpawnerBegin;
}