///@desc play an animation on an entity
///@param _animIndex
///@param _opt_ContinueFrame
var _animIndex     = argument[0];
var _continueFrame = argument_count > 1 ? argument[1] : false;

var toPlay = __entityAnims[_animIndex];
if (toPlay == noone) {
  if (_entity_animIs(DefaultAnim))
    return;
    
  _animIndex = DefaultAnim;
  toPlay = __entityAnims[_animIndex];
}

__entityAnim      = toPlay;
__entityAnimIndex = _animIndex;


sprite_index        = __entityAnim[EAnim.SpriteIndex];
image_speed         = __entityAnim[EAnim.RunningSpeed];
__entityAnimPlaying = __entityAnim[EAnim.FrameCount] > 1;

if (!_continueFrame)
  image_index = 0;