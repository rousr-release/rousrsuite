///@desc step for job system for entity
var job = worker_getJob(id);
if (job == undefined)
  return;
    
// Reset Job on job complete
var work = work_getDefinition(job[EJob.WorkID]);
job[@EJob.Finished] = script_execute(work[EWork.Step], id, job);