///datePicker_show
///@desc show the date picker at the given position
///      position adjusts to screen edges
///@param x
///@param y
///@param(opt) dateTime
var _x = argument[0];
var _y = argument[1];
var _date = argument_count > 2 ? argument[2] : date_current_datetime();
var _onEdit = argument_count >3 ? argument[3] : noone;

var datePicker = global._datePicker;
instance_activate_object(datePicker);
datePicker.visible = true;
datePicker.Date = _date;
datePicker.Dirty = true;
datePicker.Expanded = DatePicker.None;
datePicker.x = _x;
datePicker.y = _y;
datePicker.OnEdit = _onEdit;