///@desc cache the current font
///@param _font
var _font = argument0;
if (_font != noone && _font != undefined && _font != global._setFont) {
  draw_set_font(_font);
  global._setFont = _font;
}