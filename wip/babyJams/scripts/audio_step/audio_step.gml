///@desc audio_step - update the track
if (!MusicPlaying) 
	return;
	
var time = audio_sound_get_track_position(SoundId);
var track = MusicTracks[CurrentTrack];

if (!IntroPlayed) {
	if (time >= track[1]) {
		IntroPlayed = true;
	} 
} else {
	if (time >= track[2]) {
		audio_sound_set_track_position(SoundId, track[1]);
	}
}
