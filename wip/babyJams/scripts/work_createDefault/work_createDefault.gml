///@desc return a default initialized work array
var _work = array_create(EWork.Num, undefined);
_work[EWork.WorkID] = -1;
_work[EWork.Flags] = EWorkFlags.None;
_work[EWork.Loop] = 0;
_work[EWork.WorkerCapacity] = EWorkConstant.Infinite;
_work[EWork.DefaultPriority] = 0;
  
return _work;