///@desc game definitions
TileHeight = 10;
TileWidth  = 10;

WorldCols = 0;
WorldRows = 0;

enum EWorldTileFlags {
  None  = 1<<0,
  Solid = 1<<1,
}

enum EWorldTile {
  CellX,
  CellY,
  TileIndex,
  
  Flags,
  WorkList,
  
  Num
} 