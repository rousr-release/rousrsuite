///@desc Define the jobs
enum EWorkConstant {
  Infinite = -1
};

enum EJob {
  JobIndex, 
  Active, 
  
  WorkID, 
  CellX,
  CellY,
  Finished,
  Priority,
  
  WorkerList,
  
  Num
};

#macro WorkIDBegin   (0)
#macro WorkTypeBegin (0)

enum EWorkFlags {
  None       = 1>>0,
  NoLocation = 1>>1,
};

enum EWork {
  WorkID,
  Type,
  
  Step,
  Loop, 
  WorkerCapacity,
  Flags,
    
  DefaultPriority,
  
  Num,
};