///@desc return a job
///@param _jobIndex
var _jobIndex = argument0;
var job = undefined;
with(global.__jobs) {
  var jp = __jobsPool;
  if (_jobIndex >= 0 && _jobIndex < __numJobs) {
    job = jp[_jobIndex];
    if (!job[EJob.Active])
      job = undefined;
  }
}
return job;