///@desc __debug_levelGeneration_loadChunks - print debug information after loading chunks
gml_pragma("force-inline");
with(oGame) {
		show_debug_message("LevelChunks: " + string(NumLevelChunks));

	for (var chunkIndex = 0; chunkIndex < NumLevelChunks; ++chunkIndex) {
		var chunk = LevelChunks[chunkIndex];
		var removables = chunk[ELevelChunk.Removables];
		var numRemovables = array_length_1d(removables);		
	
		var connectors = chunk[ELevelChunk.Connectors];
		var numConnectors = array_length_1d(connectors);		
		
		var starts = chunk[ELevelChunk.PlayerStarts];
		var numStarts = array_length_1d(starts);
			
		var spawners = chunk[ELevelChunk.Spawners];
		var numSpawners = array_length_1d(spawners);
		
		show_debug_message("  Chunk #" + string(chunkIndex + 1) + ": " + string(chunk[ELevelChunk.Cols]) + "x" + string(chunk[ELevelChunk.Rows]));
		if (numRemovables > 0) {
			show_debug_message("    Removables: " + string(numRemovables));
			for (var i = 0; i < numRemovables; ++i) {
				var removable = removables[i];
				show_debug_message("      #" + string(i + 1) + ": " + string(array_length_1d(removable)) + " squares, left-most corner: " + string(removable[0]));
			}
		}
			
		if (numConnectors > 0) { 
			show_debug_message("    Connectors: " + string(numConnectors));
			for (var i = 0; i < numConnectors; ++i) {
				var connector = connectors[i];
				show_debug_message("      #" + string(i + 1) + ": " + string(array_length_1d(connector)) + " squares, starting with: " + string(connector[0]));
			}
		}
			
		if (numStarts > 0) {
			show_debug_message("    Player Starts: " + string(numStarts));
			for (var i = 0; i < numStarts; ++i) {
				var start = starts[i];
				show_debug_message("      #" + string(i + 1) + ": (" + string(start[0]) + ", " + string(start[1]) + ")");
			}
		}
			
		if (numSpawners > 0) {
			show_debug_message("    Spawners: " + string(numSpawners));
			for (var i = 0; i < numSpawners; ++i) {
				var spawner = spawners[i];
				show_debug_message("      #" + string(i + 1) + ": (" + string(spawner[0]) + ", " + string(spawner[1]) + ")");
			}
		}
	}	
}