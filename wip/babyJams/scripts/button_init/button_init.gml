///@desc helper function for children to init common button stuff (all optional)
///@param _onClickCallback
///@param _canClickCallback

clickAction = argument_count > 0 ? argument[0] : clickAction;
clickCheck  = argument_count > 1 ? argument[1] : clickCheck;