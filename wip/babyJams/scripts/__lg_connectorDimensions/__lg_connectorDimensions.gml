///@desc return the connector's dimensions
///@param connector
gml_pragma("force-inline");

var _connector = argument0;
var numTiles = array_length_1d(_connector);

var cx = undefined;
var cy = undefined;
var mx = undefined, my = undefined;
var cw = 0;
var ch = 0;
					
for (var tIndex = 0; tIndex < numTiles; ++tIndex) {
	var t = _connector[tIndex];
						
	if (cx == undefined) cx = t[0];
	else cx = min(t[0], cx);
						
	if (mx == undefined) mx = t[0];
	else mx = max(t[0], mx);

	if (cy == undefined) cy = t[1];
	else cy = min(t[1], cy);
						
	if (my == undefined) my = t[1];
	else my = max(t[1], my);
}

cw = (mx - cx) + 1;
ch = (my - cy) + 1;

return [ cx, cy, cw, ch ];