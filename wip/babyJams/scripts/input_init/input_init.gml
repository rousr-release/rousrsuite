///@desc input_init - initialize the input data
if (variable_global_exists("__input") && global.__input != undefined)
	return;

enum EInput {
	Actions = 0,
	Binds,
	
	Num
};

enum EInputType {
	Key,
	GamePad,
	Mouse,
	
	Num
};

enum EInputEvent {
	Down,
	Pressed,
	Released,
	
	Num
};

enum EInputAction {
	WasActive,
	Active,
	
	Num
}

var inputData = [ ];
global.__input = inputData;

inputData[@EInput.Actions] = ds_map_create();
inputData[@EInput.Binds]   = array_create(EInputType.Num);
var  bindingMaps = inputData[EInput.Binds];
for (var i = 0; i < EInputType.Num; ++i) 
	bindingMaps[@i] = ds_map_create();