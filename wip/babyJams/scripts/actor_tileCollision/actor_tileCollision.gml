///@desc actor_tileCollision
///@param _actor
///@param _x
///@param _y

var _actor = argument0;
var _x     = argument1;
var _y     = argument2;

var collisionTileMap = undefined;
with (oGame)
	collisionTileMap = LevelTileMaps[ELevelTileMap.Collision];

// Found variable
var found = false;

with (_actor) {	

	var l = _x + (bbox_left - x);
	var t = _y + (bbox_top - y);
	var r =  l + (bbox_right  - bbox_left);
	var b =  t + (bbox_bottom - bbox_top);
	
	var c = l + ((r - l) * 0.5);
	var m = t + ((b - t) * 0.5);
	
	// TODO: check if the sprite spans 3 cells or not before doing a 9 point check.
	var pts = [
		[ l, t ], [ c, t], [ r, t], 
		[ l, m ], [ c, m], [ r, m],
		[ l, b ], [ c, b], [ r, b]
	];
	
	var numPoints = array_length_1d(pts);
	for (var i = 0; i < numPoints && !found; ++i) {
		var pt = pts[i];
		var tileData = tilemap_get_at_pixel(collisionTileMap, pt[Pt.x], pt[Pt.y]);
		found |= (tile_get_index(tileData) != 0);
	}
}

// return found
return found;
