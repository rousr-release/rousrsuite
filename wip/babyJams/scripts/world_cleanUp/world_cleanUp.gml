///@desc clean up any world memory / data structures
var listSize = ds_list_size(__worldLists);
while (listSize > 0) {
  ds_list_destroy(__worldLists[| listSize - 1]);
  ds_list_delete(__worldLists, listSize - 1);  
  listSize--;
}
ds_list_destroy(__worldLists);
ds_list_destroy(UniversalJobs);