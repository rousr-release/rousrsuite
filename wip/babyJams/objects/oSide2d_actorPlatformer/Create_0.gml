///@desc oActor Create

// Default actor varaiables
Anims         = array_create(EAnim.Num, undefined);

DefaultFacing = EFacing.Right;
Facing        = EFacing.Right;

var levelTileWidth = game_getTileWidth();
MoveSpeed = levelTileWidth * 2;
VelocityX = 0;
VelocityY = 0;
QueueJump = false;
IsInAir   = false;
MaxJumps   = 1;
MultiJumps = 1;
Jumped     = false;
JumpVelocity = 45