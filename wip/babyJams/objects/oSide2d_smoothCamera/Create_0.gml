///@desc oCamera - Create
GameCam = camera_create();

var vm = matrix_build_lookat(  x,  y, -10,
                               x,  y,   0,
                               0,  1,   0 );

var pm = matrix_build_projection_ortho(1920, 1080, 1, 10000);

camera_set_view_mat(GameCam, vm);
camera_set_proj_mat(GameCam, pm);

camera_destroy(view_camera[0]);
view_camera[0] = GameCam;

Follow = undefined
xTo    = undefined;
yTo    = undefined;