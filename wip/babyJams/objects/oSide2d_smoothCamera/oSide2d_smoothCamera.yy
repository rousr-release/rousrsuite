{
    "id": "4181819a-d1d1-41d8-8cdc-f5e44727b79c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSide2d_smoothCamera",
    "eventList": [
        {
            "id": "590b9e24-b809-439c-965c-8683188375e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4181819a-d1d1-41d8-8cdc-f5e44727b79c"
        },
        {
            "id": "6e68ac72-8c9d-4ce8-be53-0ac7d1677f25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "4181819a-d1d1-41d8-8cdc-f5e44727b79c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}