///@desc oCamera - End Step
if (Follow != undefined) {
	var vieww = camera_get_view_width(GameCam);
	var viewh = camera_get_view_height(GameCam);
	
	var xOffset = (vieww * 0.5) - (game_getTileWidth() * 2);
	var yOffset = (viewh * 0.5) - (game_getTileWidth() * 2);
	
	var l = x - xOffset;
	var r = x + xOffset;
	var t = y - yOffset;
	var b = y + yOffset;

	if (Follow.x < l || Follow.x > r) {
		xTo = Follow.x;
	} else {
		xTo = undefined;
	}

	if (Follow.y < t || Follow.y > b) {
		yTo = Follow.y
	} else {
		yTo = undefined;
	}
} else {
	xTo = undefined;
	yTo = undefined;
}
	
if (xTo != undefined) x += (xTo - x)/15;
if (yTo != undefined) y += (yTo - y)/15;

var vm = matrix_build_lookat(  x,   y, -10, 
                               x,   y,   0, 
                               0,   1,   0);

camera_set_view_mat(GameCam, vm);
