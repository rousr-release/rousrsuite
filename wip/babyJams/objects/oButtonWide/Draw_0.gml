///@desc oButtonWide - Draw
var centerWidth = image_xscale * 2;  // total pixels
centerWidth -= (sprite_get_width(LeftSide) + sprite_get_width(RightSide));
var lx = x;
var ly = y;

draw_sprite(LeftSide, image_index, x, y);
x += sprite_get_width(LeftSide);
draw_sprite_ext(Content, image_index, x, y, centerWidth / sprite_get_width(Content), image_yscale, 0, c_white, 1);
x += centerWidth;
draw_sprite(RightSide, image_index, x, y);

x -= centerWidth;

var labelWidth = string_width(Label);

switch(LabelAlign) {
  case TextAlign.Left:   break;
  case TextAlign.Right:  x += centerWidth - labelWidth; break;
  case TextAlign.Center: x += floor((centerWidth - labelWidth) * 0.5); break;
}

y += 1 + image_index;

draw_set_color(image_index == 0 ? c_white : c_gray);
draw_text(x, y, Label);

x = lx;
y = ly;
