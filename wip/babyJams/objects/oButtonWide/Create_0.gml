///@desc oButtonWide - Create
event_inherited();
clickAction = noone;
//clickCheck = noone;
Label = "Generate";
LabelAlign = TextAlign.Center;

LeftSide = sButtonWide_Left;
Content = sButtonWide_Content;
RightSide = sButtonWide_Right;