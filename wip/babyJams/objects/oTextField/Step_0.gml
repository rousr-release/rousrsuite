/// @description oTextField - Step
if (IsEditing) {
  if (textField_getActive() == noone)
    global.__bjui_textFieldActive = id;
    
  var edit = false;
  if (keyboard_check_pressed(vk_backspace)) {
    if (Text != "") {
      Text = string_delete(Text, string_length(Text), 1);
      edit = true;
    }
  } else if (keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_return)) {
    textField_toggleEdit(true);
  } else if (keyboard_check_pressed(vk_escape)) {
    textField_toggleEdit(false);
  } else if (keyboard_string != "") {
    Text += keyboard_string;
    edit = true;
  }
  
  if (edit && OnEdit != noone)
    script_execute(OnEdit, id);
    
  keyboard_string = ""
}

image_xscale = FieldW * 0.5;
image_yscale = FieldH * 0.5;
