{
    "id": "67c7dce5-6d32-42bb-9fa1-b69185db1144",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTextField",
    "eventList": [
        {
            "id": "18432373-5c10-4f4f-b99d-04354f9d873b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "67c7dce5-6d32-42bb-9fa1-b69185db1144"
        },
        {
            "id": "4c02ca6f-7d28-4288-9e23-5acff558e1ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "67c7dce5-6d32-42bb-9fa1-b69185db1144"
        },
        {
            "id": "8f3f6803-d530-4457-a8e9-829f00c78fab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "67c7dce5-6d32-42bb-9fa1-b69185db1144"
        },
        {
            "id": "a8b6656b-bdda-4f1c-986e-60b23134cd10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "67c7dce5-6d32-42bb-9fa1-b69185db1144"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "869e5be6-f7dc-467c-8dd9-5be8587f42b2",
    "visible": false
}