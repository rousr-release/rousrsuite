/// @description An Entity is a movable 'thing' in the world 

/////
// Movement
SpeedX = 0;
SpeedY = 0;


////
// Stats / Attributes
WalkSpeed = 24;

////
// Animation
DefaultAnim = EEntityAnim.Idle;
__entityAnims = array_create(EEntityAnim.Num, noone);
__entityAnims[EEntityAnim.Idle] = anim_createDefault(sprite_index);

_entity_playAnim(DefaultAnim);
