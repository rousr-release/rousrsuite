///@desc entity draw
var xscale = SpeedX == 0 ? image_xscale : sign(SpeedX) * image_xscale;
draw_sprite_ext(sprite_index, image_index, floor(x), floor(y), xscale, image_yscale, 0, c_white, 1);
