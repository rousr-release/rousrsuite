{
    "id": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButton",
    "eventList": [
        {
            "id": "ad03b93e-801a-43ea-b2f1-448ed83db5ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "d9ee4997-f314-40aa-aced-b17110ca39cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "d45a5465-8066-49df-a2e1-fca16ef12a44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "ec3998b3-874b-413a-aff8-419ee122a8fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "4ef23908-b83b-4d90-9031-59b317a732b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "01bca6d6-852d-483f-89a7-ebc034cbfe1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "visible": true
}