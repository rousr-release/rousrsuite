{
    "id": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "babyjams",
    "eventList": [
        {
            "id": "e1dbe7d0-f4b5-49e7-8085-5329f46dd5de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "a7607b91-d18c-430c-9cbc-b7a5d6e89ec5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "90cdfbe0-98bc-419c-8288-509ce4a9478d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "1b44ed24-fc97-4359-8556-4f5854c4793f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "af28f4fa-c9cb-4bce-8d45-0ab7b2ae3935",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "b76b9b10-547a-434b-85d6-bd39ccd0d6d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "577a918a-765f-4024-9f96-39f5ebce986d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        },
        {
            "id": "c379ab7b-429a-4a63-9e33-bfa560233834",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "6a3e07a5-bcec-485f-9ce9-cd11c0887c33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "091ed4fe-ee28-4624-88d6-aa1a822e9ed1",
    "visible": true
}