/// @description oDatePicker - Create
global._datePicker = id;
enum DatePicker {
  None = -1,
  
  Year = 0,
  Month,
  Day,
  
  FieldCount,
  
};

WeekDayName = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday"
];

MonthName = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
]

Dirty = true;
__date = noone;
Date = noone;

Month = 0;
Day = 0;
Year = 0;

Dim = 0;
Dow = 0;
First = 0;

Font = fnt_game;
Expanded = DatePicker.None;

InputFields = [ ];
for (var inputFieldIndex = 0; inputFieldIndex < DatePicker.FieldCount; ++inputFieldIndex) {
  InputFields[inputFieldIndex] = instance_create_layer(0, 0, layer, oTextField);
  InputFields[inputFieldIndex].visible = false;
  InputFields[inputFieldIndex].HideOnBlur = false;
}

visible = false;
instance_deactivate_object(id);