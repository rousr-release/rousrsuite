{
    "id": "c3bf3975-756c-4ba6-a2d2-ae6d72201a5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDatePicker",
    "eventList": [
        {
            "id": "6fe80cee-89f1-47f5-ba21-8dd303e39ebe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3bf3975-756c-4ba6-a2d2-ae6d72201a5f"
        },
        {
            "id": "6cb84955-27d6-445a-b83b-fab988467e31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3bf3975-756c-4ba6-a2d2-ae6d72201a5f"
        },
        {
            "id": "4f0f0a58-df9c-445a-8bcb-474954b4ef72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c3bf3975-756c-4ba6-a2d2-ae6d72201a5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "95e7d0df-35fd-42db-9fd3-29c388705675",
    "visible": false
}