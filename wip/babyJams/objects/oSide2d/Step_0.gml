///@desc oGame - Step
global.dt = (lastTime - get_timer()) / 1000000;
lastTime = get_timer();

input_step();
audio_step();

// Quit on Escape (Debug?)
if (keyboard_check_pressed(vk_escape)) {
	game_end();
}