///@desc oGame - Create

// Level Defines
enum ELevelLayers {
	Collision,
	Foreground,
	Level,
	Mid,
	Background,
	Fog,
	
	Num
};
	
enum ELevelTileMap {
	Collision,
	Foreground,
	Level,
	Mid,
	Background,
	
	Num
};

// LevelGeneration defines
enum ELevelChunk {
	MarkerLayerId,
	CollisionLayerId,
	ForegroundLayerId,
	LevelLayerId,
	MidLayerId,
	BackgroundLayerId,
	FogLayerId,
	
	TileMap_Markers,
	TileMap_Collision,
	TileMap_Foreground,
	TileMap_Level,
	TileMap_Mid,
	TileMap_Background,
	
	FogInstances,
	
	Cols,
	Rows,
	
	Connectors,
	ConnectorData,
	Removables,
	PlayerStarts,
	Spawners, 
	SpawnerData,
	
	Num,
	
	LayerBegin   = ELevelChunk.MarkerLayerId,
	LayerEnd     = ELevelChunk.FogLayerId,
	
	TileMapBegin = ELevelChunk.TileMap_Markers,
	TileMapEnd   = ELevelChunk.TileMap_Background
};

enum Pt {
	X,Y,
	
	x = Pt.X,	w = Pt.X, W = Pt.w,
	y = Pt.Y, h = Pt.Y, H = Pt.h,
};

enum ELevelMarker {
	None = 0,
	Connector,
	ConnectorSeal,
	Removable,
	BottomRight,
	PlayerStart, 
	SpawnerA,
	SpawnerB,
	
	Num,
	
	SpawnerBegin = ELevelMarker.SpawnerA,
	SpawnerEnd   = ELevelMarker.SpawnerB
};

enum EConnectorData {
	Edge = 0,
	
	Num
};

enum EEdge {
	Left = 0,
	Top,
	Right,
	Bottom,
	
	Num,
	None = EEdge.Num
};

enum EDim {
	X = 0,
	Y,
	W,
	H,
	
	Num, 
	Width = EDim.W,
	Height = EDim.H,
	w = EDim.W,
	h = EDim.H,
	x = EDim.X,
	y = EDim.Y
};

enum EPlayerControl {
	Move_Left,
	Move_Up,
	Move_Right,
	Move_Down,
	
	Jump,
	Attack,
};

enum EFacing {
	Left = 0,
	Right,
	
	Num,
};

enum EAnimation {
	Idle,
	Run,
	Fall,
	
	Num,
}

DebugMode = false;	// Mark false for significantly less output, and various debug controls will also be off.
if (!DebugMode)	gml_release_mode(true); // TODO: Activate this for release builds?

if (DebugMode) {
	Debug_CamControls = true;		// Use WASD to control the camera
} else {
	randomize();
}

global.dt = 0;
lastTime = get_timer() / 1000000;

LevelChunks = [ ];

LevelLayers = [ 
	layer_get_id("Collision"),	
	layer_get_id("Level"),	
];
	
LevelTileMaps = [ 
	layer_tilemap_get_id(LevelLayers[ELevelLayers.Collision]),
	layer_tilemap_get_id(LevelLayers[ELevelLayers.Level]),
];
	
layer_set_visible(LevelLayers[ELevelLayers.Collision], false);

input_init();

levelGeneration_loadChunks();
levelGeneration_generate();

audio_init();