{
    "id": "71261df6-bf7f-456d-bc92-687f6c346a9f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTween",
    "eventList": [
        {
            "id": "f2fe94a9-eb06-429c-ab82-1be6cae5a283",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71261df6-bf7f-456d-bc92-687f6c346a9f"
        },
        {
            "id": "63b403c9-1de7-4ee6-b1bf-bfb6a5b64c75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "71261df6-bf7f-456d-bc92-687f6c346a9f"
        },
        {
            "id": "bbb130ee-143f-4d79-9523-b910b93ef43f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "71261df6-bf7f-456d-bc92-687f6c346a9f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "2e7d478e-90e4-4ad8-87d0-2566b1da2161",
    "visible": false
}