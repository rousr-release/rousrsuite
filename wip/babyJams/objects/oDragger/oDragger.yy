{
    "id": "fe2fa451-d995-4638-a648-52b5ebbaec6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDragger",
    "eventList": [
        {
            "id": "0b2de34d-57d1-47e0-a856-2000f541c2e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe2fa451-d995-4638-a648-52b5ebbaec6e"
        },
        {
            "id": "87fcfaaf-48c8-44b0-9482-599a5643bde0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "fe2fa451-d995-4638-a648-52b5ebbaec6e"
        },
        {
            "id": "bfdb5886-bb15-41bd-8b7d-e6d3feb0a687",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "fe2fa451-d995-4638-a648-52b5ebbaec6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}