if (dragging != noone) {
  with(dragging) {
  	x += mouse_x - __dragger_lastMouseX;
  	y += mouse_y - __dragger_lastMouseY;
  	__dragger_lastMouseX = mouse_x;
  	__dragger_lastMouseY = mouse_y;
  }
}