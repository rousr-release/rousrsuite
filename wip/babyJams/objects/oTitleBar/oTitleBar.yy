{
    "id": "4617493a-9a65-476f-8119-a6740e12df53",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitleBar",
    "eventList": [
        {
            "id": "3c20145d-45dc-42db-9b16-63422a861cde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        },
        {
            "id": "920d0e82-6b9c-4f0c-a860-07572174224c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        },
        {
            "id": "95a9e963-79ac-46c0-85fc-a9ae7e0bd665",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        },
        {
            "id": "a72b7d26-444d-4e5c-ae61-a056189eaa52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        },
        {
            "id": "076a2b64-79e1-4f9a-9ae9-069d08eafd27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4617493a-9a65-476f-8119-a6740e12df53"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "da321195-6230-4870-b017-c16bac16ebc5",
    "visible": true
}