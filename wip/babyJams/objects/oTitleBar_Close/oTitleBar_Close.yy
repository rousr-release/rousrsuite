{
    "id": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitleBar_Close",
    "eventList": [
        {
            "id": "36d99894-ecae-4dd1-9c51-a8dbe8e03967",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 5,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "f4deef58-bfdb-40dd-9336-349f1d325ce9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 10,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "8d28f15a-2a43-4ec4-b78a-86d4a3a827e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "55bd1c84-6f21-4de1-8a59-57727a2bdc81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
    "visible": true
}