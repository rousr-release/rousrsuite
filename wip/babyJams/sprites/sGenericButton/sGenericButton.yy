{
    "id": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGenericButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "033932a5-71af-4a1f-87e7-73569ed1d106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "compositeImage": {
                "id": "93c8d121-ff33-4899-8546-e9aee4083843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033932a5-71af-4a1f-87e7-73569ed1d106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0bbd10d-8236-4f27-9c2b-305e0a98d4e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033932a5-71af-4a1f-87e7-73569ed1d106",
                    "LayerId": "b50efce2-fe53-407a-9ac6-acd9cf1239a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b50efce2-fe53-407a-9ac6-acd9cf1239a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}