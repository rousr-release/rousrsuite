{
    "id": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleBar_Icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eb6ae129-fa78-4b09-9cae-a777246d2fd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
            "compositeImage": {
                "id": "db4291ac-2b4b-46b2-8de8-d834251254c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb6ae129-fa78-4b09-9cae-a777246d2fd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da5d6a70-3d04-4515-b291-800b3429e82d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb6ae129-fa78-4b09-9cae-a777246d2fd5",
                    "LayerId": "147bf968-1547-4173-bbcc-6ca9863884f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "147bf968-1547-4173-bbcc-6ca9863884f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}