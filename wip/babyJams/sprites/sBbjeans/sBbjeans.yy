{
    "id": "091ed4fe-ee28-4624-88d6-aa1a822e9ed1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBbjeans",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f47fc00-3033-43cf-af77-c86e90078e9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "091ed4fe-ee28-4624-88d6-aa1a822e9ed1",
            "compositeImage": {
                "id": "538b202a-2a5b-4b17-958b-b6136d8ceeb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f47fc00-3033-43cf-af77-c86e90078e9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f7714e-9228-4bf4-8887-a40c7096e167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f47fc00-3033-43cf-af77-c86e90078e9d",
                    "LayerId": "962b482f-d078-4010-9262-6269f3a4e790"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "962b482f-d078-4010-9262-6269f3a4e790",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "091ed4fe-ee28-4624-88d6-aa1a822e9ed1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 0,
    "yorig": 0
}