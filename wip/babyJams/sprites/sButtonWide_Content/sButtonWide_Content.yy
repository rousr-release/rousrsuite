{
    "id": "eff29bc9-4f21-4617-b722-bb8a3210a527",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide_Content",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "21f2ee58-3308-4a9a-b769-f00bfdeaf43b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "compositeImage": {
                "id": "2db4b1dc-02aa-4842-a2a7-830a9302ab64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f2ee58-3308-4a9a-b769-f00bfdeaf43b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e7f68c9-01d7-4a37-9687-1474cbe3e283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f2ee58-3308-4a9a-b769-f00bfdeaf43b",
                    "LayerId": "d2a155d6-2904-4c32-86f6-3a38212ebd1f"
                }
            ]
        },
        {
            "id": "7f8ce883-e2fc-42f5-9659-71f7f58207c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "compositeImage": {
                "id": "f71ab6dd-4306-4b4f-b4f7-f751a0d7cde5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8ce883-e2fc-42f5-9659-71f7f58207c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b78d83-06c6-404b-a625-6266772a24c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8ce883-e2fc-42f5-9659-71f7f58207c8",
                    "LayerId": "d2a155d6-2904-4c32-86f6-3a38212ebd1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "d2a155d6-2904-4c32-86f6-3a38212ebd1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}