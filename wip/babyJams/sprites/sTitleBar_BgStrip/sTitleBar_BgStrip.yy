{
    "id": "da321195-6230-4870-b017-c16bac16ebc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleBar_BgStrip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eecd25cf-29d2-4d5a-bcc7-6442df3a6491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da321195-6230-4870-b017-c16bac16ebc5",
            "compositeImage": {
                "id": "ee7c527d-be2a-42bc-b3e2-bed11ce11b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eecd25cf-29d2-4d5a-bcc7-6442df3a6491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5add4f57-9b14-441f-b82f-602d50098cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eecd25cf-29d2-4d5a-bcc7-6442df3a6491",
                    "LayerId": "9bd88d47-b9f8-46a9-99ab-aaf33c54aca9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "9bd88d47-b9f8-46a9-99ab-aaf33c54aca9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da321195-6230-4870-b017-c16bac16ebc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}