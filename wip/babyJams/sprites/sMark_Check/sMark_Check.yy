{
    "id": "38964aab-7bca-45ff-9711-ee3c3e3e9602",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMark_Check",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6433868f-5ec4-4e02-96bb-eec507434482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38964aab-7bca-45ff-9711-ee3c3e3e9602",
            "compositeImage": {
                "id": "1ec837f1-f381-40d2-86ec-1116b5604541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6433868f-5ec4-4e02-96bb-eec507434482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5c528fb-538c-408f-9ae6-e5055749f8fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6433868f-5ec4-4e02-96bb-eec507434482",
                    "LayerId": "9b278b59-1592-4c9a-b3bb-cbfe0b86efcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9b278b59-1592-4c9a-b3bb-cbfe0b86efcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38964aab-7bca-45ff-9711-ee3c3e3e9602",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}