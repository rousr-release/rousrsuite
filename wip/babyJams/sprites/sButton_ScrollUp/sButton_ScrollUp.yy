{
    "id": "9461ec09-42de-47dd-a186-c3f65c5c8fd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton_ScrollUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ab14e8d5-858d-4499-b7b1-1baab8643477",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9461ec09-42de-47dd-a186-c3f65c5c8fd2",
            "compositeImage": {
                "id": "095e2780-219c-47d9-a656-9783d8ad1aaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab14e8d5-858d-4499-b7b1-1baab8643477",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f3e7b6-2d78-4cbc-8c6d-f7f792a7b39e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab14e8d5-858d-4499-b7b1-1baab8643477",
                    "LayerId": "610d2d13-628e-4199-93f0-a6d87a2bc9e7"
                }
            ]
        },
        {
            "id": "4ccba0ba-edf6-437e-9010-9d824c230441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9461ec09-42de-47dd-a186-c3f65c5c8fd2",
            "compositeImage": {
                "id": "5979d291-993d-4e52-b1b2-e536440e3ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ccba0ba-edf6-437e-9010-9d824c230441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bac65ea1-eb17-4737-b82a-84a1d93bbb78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ccba0ba-edf6-437e-9010-9d824c230441",
                    "LayerId": "610d2d13-628e-4199-93f0-a6d87a2bc9e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "610d2d13-628e-4199-93f0-a6d87a2bc9e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9461ec09-42de-47dd-a186-c3f65c5c8fd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}