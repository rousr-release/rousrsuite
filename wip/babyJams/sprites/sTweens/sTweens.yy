{
    "id": "2e7d478e-90e4-4ad8-87d0-2566b1da2161",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTweens",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4231d917-5ab1-4203-ba51-57eaa713d036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e7d478e-90e4-4ad8-87d0-2566b1da2161",
            "compositeImage": {
                "id": "051fb551-0b01-412a-8be9-8c010e6c23c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4231d917-5ab1-4203-ba51-57eaa713d036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d8c5eb2-29ab-4187-b718-82717ec91431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4231d917-5ab1-4203-ba51-57eaa713d036",
                    "LayerId": "37ff9aab-4769-4aa0-99d5-5702625f5a3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "37ff9aab-4769-4aa0-99d5-5702625f5a3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e7d478e-90e4-4ad8-87d0-2566b1da2161",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}