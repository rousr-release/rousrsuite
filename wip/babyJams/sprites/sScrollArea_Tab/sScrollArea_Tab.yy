{
    "id": "1be453cc-1512-4869-9d82-96d44ce20eca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sScrollArea_Tab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d88a03d6-bcaa-45e0-affa-65c615a942c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be453cc-1512-4869-9d82-96d44ce20eca",
            "compositeImage": {
                "id": "0d625ba9-5c48-4f9b-b732-e08728e1895c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88a03d6-bcaa-45e0-affa-65c615a942c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15e89ec8-ebcc-4541-8533-2d53d66f77af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88a03d6-bcaa-45e0-affa-65c615a942c9",
                    "LayerId": "6e65eadd-33d6-49ed-89cf-4001358e5ab9"
                }
            ]
        },
        {
            "id": "531f32c9-96a8-4019-857d-104bb416bdcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be453cc-1512-4869-9d82-96d44ce20eca",
            "compositeImage": {
                "id": "cab6f28c-ba5d-4e76-9b08-1dbbd8307884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "531f32c9-96a8-4019-857d-104bb416bdcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc01f2ab-7875-4151-8ea8-e226ce4cacac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "531f32c9-96a8-4019-857d-104bb416bdcc",
                    "LayerId": "6e65eadd-33d6-49ed-89cf-4001358e5ab9"
                }
            ]
        },
        {
            "id": "b7fe4941-58af-4f4d-a97f-1315e2c5aea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be453cc-1512-4869-9d82-96d44ce20eca",
            "compositeImage": {
                "id": "05a66e63-cdc5-44bd-8ecb-6d2ae8d9aec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7fe4941-58af-4f4d-a97f-1315e2c5aea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37ef89d-636f-4da2-9403-ce8eff9f31ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7fe4941-58af-4f4d-a97f-1315e2c5aea4",
                    "LayerId": "6e65eadd-33d6-49ed-89cf-4001358e5ab9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "6e65eadd-33d6-49ed-89cf-4001358e5ab9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1be453cc-1512-4869-9d82-96d44ce20eca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}