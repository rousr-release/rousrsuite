{
    "id": "94b7c6c5-c20c-4a36-97fb-d1e4da5c716d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCopyright_sm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "dc1b72ca-01f7-429d-bd24-76f1dbb6856f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94b7c6c5-c20c-4a36-97fb-d1e4da5c716d",
            "compositeImage": {
                "id": "2729ff18-39f9-4754-b12c-29cb463ca410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1b72ca-01f7-429d-bd24-76f1dbb6856f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f26757b0-067d-40ac-9203-68805979de38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1b72ca-01f7-429d-bd24-76f1dbb6856f",
                    "LayerId": "6e91b23a-9683-4c9f-a126-e310058d4e7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "6e91b23a-9683-4c9f-a126-e310058d4e7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94b7c6c5-c20c-4a36-97fb-d1e4da5c716d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}