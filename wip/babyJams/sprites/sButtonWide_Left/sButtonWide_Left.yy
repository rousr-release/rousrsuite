{
    "id": "01882bc4-fa43-46ea-ab24-0ae3f0033423",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide_Left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "de0650f2-6053-4eb8-be00-092691579ab1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01882bc4-fa43-46ea-ab24-0ae3f0033423",
            "compositeImage": {
                "id": "60a8936b-6e78-4a6e-bde5-8320d66ec4b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de0650f2-6053-4eb8-be00-092691579ab1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81cbdc6c-9a02-409a-92c9-1d51534f7362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de0650f2-6053-4eb8-be00-092691579ab1",
                    "LayerId": "a56d7a39-ee29-4020-a53c-e4611db8e5cb"
                }
            ]
        },
        {
            "id": "ffd8b168-83ba-4438-b615-e2b60babb9cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01882bc4-fa43-46ea-ab24-0ae3f0033423",
            "compositeImage": {
                "id": "13c2348d-7f9d-4622-b3b0-8b13e87a4ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffd8b168-83ba-4438-b615-e2b60babb9cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452275de-0c8a-4ad9-a0f7-5966f381a3ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffd8b168-83ba-4438-b615-e2b60babb9cc",
                    "LayerId": "a56d7a39-ee29-4020-a53c-e4611db8e5cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "a56d7a39-ee29-4020-a53c-e4611db8e5cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01882bc4-fa43-46ea-ab24-0ae3f0033423",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}