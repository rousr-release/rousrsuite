{
    "id": "869e5be6-f7dc-467c-8dd9-5be8587f42b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWhiteBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2590c49a-f599-4b51-aeef-37a26e34f3a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869e5be6-f7dc-467c-8dd9-5be8587f42b2",
            "compositeImage": {
                "id": "061cc706-77e9-4075-95d9-19c52c5fc403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2590c49a-f599-4b51-aeef-37a26e34f3a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e40c17-02c8-42ab-8afb-6bbde3a5c3bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2590c49a-f599-4b51-aeef-37a26e34f3a4",
                    "LayerId": "25b3e2a7-d8d0-45c5-801b-1bcd7d8b97cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "25b3e2a7-d8d0-45c5-801b-1bcd7d8b97cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "869e5be6-f7dc-467c-8dd9-5be8587f42b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}