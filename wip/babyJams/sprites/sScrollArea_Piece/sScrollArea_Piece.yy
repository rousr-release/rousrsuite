{
    "id": "bb1cb85a-07cd-4753-8e3d-c62186c76419",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sScrollArea_Piece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6d8146de-a614-458b-b5cc-cee0ad8fb9ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb1cb85a-07cd-4753-8e3d-c62186c76419",
            "compositeImage": {
                "id": "8bea8fae-4b3b-405e-8338-d0a824be5738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d8146de-a614-458b-b5cc-cee0ad8fb9ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01aa3943-2b15-4c0a-8df5-50ed51ae3c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d8146de-a614-458b-b5cc-cee0ad8fb9ea",
                    "LayerId": "f92538d1-4565-47a3-9f9e-2776fc6ddbaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "f92538d1-4565-47a3-9f9e-2776fc6ddbaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb1cb85a-07cd-4753-8e3d-c62186c76419",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}