{
    "id": "be957279-1350-4461-b9fa-6096c6130a04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMark_X",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e28aa307-253b-4b72-b4c3-9855547ebc34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be957279-1350-4461-b9fa-6096c6130a04",
            "compositeImage": {
                "id": "af031681-3591-4dd0-ae44-8fae6003c21b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e28aa307-253b-4b72-b4c3-9855547ebc34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4722f05d-e1cc-4e67-8bac-6cd1ea6c8b6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e28aa307-253b-4b72-b4c3-9855547ebc34",
                    "LayerId": "ae831cd9-57a6-4aba-bdc7-b9985164cb8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ae831cd9-57a6-4aba-bdc7-b9985164cb8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be957279-1350-4461-b9fa-6096c6130a04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}