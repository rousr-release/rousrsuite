{
    "id": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleBar_CloseButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9d5d5ca4-68b7-452a-9f2a-68505e584db0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
            "compositeImage": {
                "id": "0d0ab517-c10b-4318-b058-d55a0e17d3fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d5d5ca4-68b7-452a-9f2a-68505e584db0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6ec0048-f711-4cd5-a5ee-1810a7af8df9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d5d5ca4-68b7-452a-9f2a-68505e584db0",
                    "LayerId": "92f7d55b-a4e8-4e7d-bb5a-e6e95b879ed0"
                }
            ]
        },
        {
            "id": "7123ab10-0c3f-4dd5-9015-bf0e972c47c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
            "compositeImage": {
                "id": "83dcf799-01ef-4cd2-a3a2-d95d3b18bdc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7123ab10-0c3f-4dd5-9015-bf0e972c47c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c741357-f0bb-4fe0-8415-4e44b3d74f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7123ab10-0c3f-4dd5-9015-bf0e972c47c4",
                    "LayerId": "92f7d55b-a4e8-4e7d-bb5a-e6e95b879ed0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "92f7d55b-a4e8-4e7d-bb5a-e6e95b879ed0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "109d200d-d79f-451a-be6e-d3d5900b8ca5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}