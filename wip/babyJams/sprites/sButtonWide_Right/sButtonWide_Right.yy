{
    "id": "856df45f-8eb9-4161-b3fb-7e0307098624",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide_Right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2be97b11-0a3f-4712-845c-038832840eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856df45f-8eb9-4161-b3fb-7e0307098624",
            "compositeImage": {
                "id": "4aa8623f-96a4-44c5-bbf2-8a4e8d11420c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2be97b11-0a3f-4712-845c-038832840eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22b9d26b-1fb3-4e71-84f0-3d20bbde153d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2be97b11-0a3f-4712-845c-038832840eed",
                    "LayerId": "b0149dd2-d69d-463b-a769-6b57f227c08e"
                }
            ]
        },
        {
            "id": "a1cb5624-a753-4c56-bb50-8951544e3f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856df45f-8eb9-4161-b3fb-7e0307098624",
            "compositeImage": {
                "id": "6f1695c5-2e13-4e59-8f8c-0ee1a2cbef9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1cb5624-a753-4c56-bb50-8951544e3f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8a82b6-4d6b-4489-81b1-ac221e524c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1cb5624-a753-4c56-bb50-8951544e3f8d",
                    "LayerId": "b0149dd2-d69d-463b-a769-6b57f227c08e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "b0149dd2-d69d-463b-a769-6b57f227c08e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "856df45f-8eb9-4161-b3fb-7e0307098624",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}