{
    "id": "496964d2-502b-47dd-9806-e61a3363450f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton_ScrollDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "12953a14-8ece-404d-a1cc-a2c4cdf40e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496964d2-502b-47dd-9806-e61a3363450f",
            "compositeImage": {
                "id": "9fd98f19-7dbb-41a5-a568-fab5fd0cb05b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12953a14-8ece-404d-a1cc-a2c4cdf40e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4473ffc-090c-41d6-bbe7-4f3f562b485a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12953a14-8ece-404d-a1cc-a2c4cdf40e3c",
                    "LayerId": "fad33d23-8469-48ce-972b-69404e65c45e"
                }
            ]
        },
        {
            "id": "cbc9ab0c-fc3a-462d-acdb-41a3c17ace88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496964d2-502b-47dd-9806-e61a3363450f",
            "compositeImage": {
                "id": "823d7096-40d5-48ef-8634-3225493722cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbc9ab0c-fc3a-462d-acdb-41a3c17ace88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebcb44ef-761b-466c-9178-ab8e9ac1ee26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbc9ab0c-fc3a-462d-acdb-41a3c17ace88",
                    "LayerId": "fad33d23-8469-48ce-972b-69404e65c45e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fad33d23-8469-48ce-972b-69404e65c45e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "496964d2-502b-47dd-9806-e61a3363450f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}