{
    "id": "95e7d0df-35fd-42db-9fd3-29c388705675",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWhitePixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "88a53acb-8ca4-4ded-ad36-6a251cdbaf27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e7d0df-35fd-42db-9fd3-29c388705675",
            "compositeImage": {
                "id": "470fadf4-476e-4728-8939-ed2255bd7920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a53acb-8ca4-4ded-ad36-6a251cdbaf27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "656d541d-99b4-4515-9524-3dcc977b745f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a53acb-8ca4-4ded-ad36-6a251cdbaf27",
                    "LayerId": "051ee14a-3ea8-4112-9c67-0fa6a95c114f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "051ee14a-3ea8-4112-9c67-0fa6a95c114f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95e7d0df-35fd-42db-9fd3-29c388705675",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}