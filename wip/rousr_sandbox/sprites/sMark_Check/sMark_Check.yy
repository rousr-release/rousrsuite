{
    "id": "38964aab-7bca-45ff-9711-ee3c3e3e9602",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMark_Check",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "836803ae-5557-463f-af25-9f057fdb1856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38964aab-7bca-45ff-9711-ee3c3e3e9602",
            "compositeImage": {
                "id": "c7b88aa8-3314-4949-a5c7-670d4d7a57a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "836803ae-5557-463f-af25-9f057fdb1856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "055c7b69-268c-4aec-b015-9aa4fc49fa46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "836803ae-5557-463f-af25-9f057fdb1856",
                    "LayerId": "b59aff86-8292-4d5d-9c29-3d9562bf5bde"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b59aff86-8292-4d5d-9c29-3d9562bf5bde",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38964aab-7bca-45ff-9711-ee3c3e3e9602",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}