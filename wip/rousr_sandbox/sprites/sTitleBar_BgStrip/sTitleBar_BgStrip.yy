{
    "id": "da321195-6230-4870-b017-c16bac16ebc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleBar_BgStrip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b785e67-bfc4-458e-9545-efa5c8d46af3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da321195-6230-4870-b017-c16bac16ebc5",
            "compositeImage": {
                "id": "902df1bf-dc32-4b9c-b783-3365e6f0c685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b785e67-bfc4-458e-9545-efa5c8d46af3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea74d7c-3560-4557-b89c-ad895c55bb93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b785e67-bfc4-458e-9545-efa5c8d46af3",
                    "LayerId": "3dd3b930-c232-4e18-8754-90377a15bab1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "3dd3b930-c232-4e18-8754-90377a15bab1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da321195-6230-4870-b017-c16bac16ebc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}