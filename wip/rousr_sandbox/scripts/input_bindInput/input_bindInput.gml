///@desc input_bindInput - bind an input to an action
///@param _inputType
///@param _inputButton
///@param _action
///@param _event
var _inputType   = argument0;
var _inputButton = argument1;
var _action      = argument2;
var _event       = argument3; 

if (!variable_global_exists("__input") || global.__input == undefined) {
	show_debug_message("input - warning: input system not initialized, input button not bound: " + string(_inputButton));
	return;
}

if (_inputType < 0 || _inputType >= EInputType.Num) {
	show_debug_message("input - warning: can't bind input invalid input type.");
	return;
}

if (_event < 0 || _event >= EInputEvent.Num) {
	show_debug_message("input - warning: can't bind input invalid input event");
	return;
}

var input         = global.__input;
var inputBindMaps = input[@EInput.Binds];
var inputBindMap  = inputBindMaps[_inputType];

var inputBind = inputBindMap[? _inputButton];
if (inputBind == undefined) {
	inputBind = [ ];
	inputBindMap[? _inputButton] = inputBind;
}

var numBindings = array_length_1d(inputBind);
inputBind[@numBindings] = [ _event, _action ];