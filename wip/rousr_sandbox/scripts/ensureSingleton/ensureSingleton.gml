///@desc returns false if this instance is destroyed - be sure to check CleanUp / Destroys
///@param singletonName
///@param (opt)destroyCall
var name = argument[0], destroyCall = argument_count > 1 ? argument[1] : noone;

if (variable_global_exists(name)) {
  var existsId = variable_global_get(name);
  if (existsId != undefined) {
    if (existsId != id) {
      if (destroyCall != noone) 
        script_execute(destroyCall, id);
    
      instance_destroy(id);
    }
    
    return false;
  }
}

variable_global_set(name, id);
return true;