///@desc vfx_fadeRoomInPost
var _id = argument0;
with(_id) {
  // we doing it on a shader?
  return !vfx_isFadeRoom();
}

return true;