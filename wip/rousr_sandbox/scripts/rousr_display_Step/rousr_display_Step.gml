///@desc Display manager step
///@param _babyjams
var _bj = argument0;
with(_bj) {
  __displayDirty |= ScreenWidth != surface_get_width(application_surface) ||
           ScreenHeight != surface_get_height(application_surface) ||
           !view_enabled;
           
           
  if (__displayDirty && !__displayDirtyHooked) {
    babyJams_addBeginStepHook(__display_setResolution_HookBeginStep, id);
    __displayDirtyHooked = true;
  }

}

return false;