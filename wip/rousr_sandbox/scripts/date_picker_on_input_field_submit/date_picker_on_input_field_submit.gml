///@desc Update date based on submitted textfield
///@param field
var _field = argument0;

var fieldIndex = _field.fieldIndex;
var datePicker = _field.datePicker;
var val = string_digits(_field.Text);
if (string_length(val) == 0)
  return;

with (datePicker) {
  val = real(val);
  var validVal = true;
  
  switch(fieldIndex) {
    case DatePicker.Year: {
      if (val >= 2000 && val < 2200) Year = val;
    }  break;
    case DatePicker.Month: {
      if (val >= 1 && val <= 12)     Month = val;
    } break;  
    case DatePicker.Day: {
      if (val < 1) val = 1;
      else {
        var testDate = date_create_datetime(Year, Month, 1, 0,0,0);
        var days = date_days_in_month(testDate);
        val = min(val, days);
      }                              Day = val;
    } break;
    default: break;
  }

  if (validVal) {
    Date = date_create_datetime(Year, Month, Day, 0, 0, 0);
    if (OnEdit != noone)
      script_execute(OnEdit, id, Date);
  }
}