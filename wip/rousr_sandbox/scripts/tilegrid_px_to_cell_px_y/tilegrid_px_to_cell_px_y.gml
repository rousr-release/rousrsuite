///@description return pixel y of a cell containing given pixel position
///@param table
///@param pixelX
///@param pixelY
var _table = argument0;
var _pxX   = argument1;
var _pxY   = argument2;

var mapId = _table.TileMapId;
var th    = _table.TileHeight;
var oy    = _table.TileMapY;

var _y = tilemap_get_cell_y_at_pixel(mapId, _pxX, _pxY);
			
return oy + (_y * th)