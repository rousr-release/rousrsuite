//http://www.algolist.net/Algorithms/Binary_search
/*
* searches for a value in sorted array
*   arr is an array to search in
*   value is searched value
*   left is an index of left boundary
*   right is an index of right boundary
* returns position of searched value, if it presents in the array
* or -1, if it is absent
*/
//int binarySearch(int arr[], int value, int left, int right) {
//      while (left <= right) {
//            int middle = (left + right) / 2;
//            if (arr[middle] == value)
//                  return middle;
//            else if (arr[middle] > value)
//                  right = middle - 1;
//            else
//                  left = middle + 1;
//      }
//      return -1;
//}