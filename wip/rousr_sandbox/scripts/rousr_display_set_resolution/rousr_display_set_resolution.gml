///@desc set up the resoltuion
///@param _width
///@param _height
var _width = argument0;
var _height = argument1;
var _viewWidth  = room_width;
var _viewHeight = room_height;

with(babyjams) {
  var oldWidth = window_get_width();
  var oldHeight = window_get_height();
  var oldX = window_get_x();
  var oldY = window_get_y();
      
  __displayDirty = false;
  if (oldWidth != _width || oldHeight != _height) {
    var windowX = oldX + (oldWidth - _width) * 0.5;
    var windowY = oldY + (oldHeight - _height) * 0.5;

    window_set_position(windowX, windowY);
    window_set_size(_width, _height);
    __displayDirty = true;
  }
  
    
  LastSurfaceW = surface_get_width(application_surface);
  LastSurfaceH = surface_get_height(application_surface);
  
  __displayDirty = (LastSurfaceW != ScreenWidth || LastSurfaceH != ScreenHeight) || 
            (!view_enabled || view_wport[0] != ScreenWidth || view_hport[0] != ScreenHeight) ||
            view_camera[0] < 0;
    
  if (!__displayDirty) {
    var camW = camera_get_view_width(view_camera[0]);
    var camH = camera_get_view_height(view_camera[0]);
    __displayDirty |= !(camW == _viewWidth && camH == _viewH);
  }
  
  __displayDirty |= !(view_wport[0] == _width && view_hport[0] == _height);
  
  
  if (__displayDirty) {
    view_enabled = true;
    for (var i = 0; i < 8; ++i)
      view_visible[i] = false;
      
    view_visible[0] = true;
    
    if (view_camera[0] < 0)
      view_camera[0] = camera_create_view(0, 0, _viewWidth, _viewHeight, 0, 0, 0, 0, 0, 0);
  
    camera_set_view_size(view_camera[0], _viewWidth, _viewHeight);
    view_wport[0] = _width; 
    view_hport[0] = _height;
  }
  
  if (surface_exists(application_surface)) {
    surface_resize(application_surface, _width, _height);
    LastAppSurfaceID = application_surface;
  }
  
  ScreenWidth = _width;
  ScreenHeight = _height;
  ViewWidth = _viewWidth;
  ViewHeight = _viewHeight;
 
  babyJams_addBeginStepHook(__display_setResolution_HookBeginStep, id);
  __displayDirtyHooked = true;
}
