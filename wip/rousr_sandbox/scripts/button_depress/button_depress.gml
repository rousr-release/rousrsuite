///@desc Put a button back into the default state
var _button = argument0;
_button.upIndex      = 0;
_button.downIndex    = 1;
_button.image_index  = _button.upIndex;
_button.clickEnabled = true;