///aabbContainsLine
///courtesy of https://stackoverflow.com/questions/1585525/how-to-find-the-intersection-point-between-a-line-and-a-rectangle
///@desc returns true if an aabb and line intersect
///@param lineStartX
///@param lineStartY
///@param lineEndX
///@param lineEndY
///@param rectMinX
///@param rectMinY
///@param rectMaxX
///@param rectMaxY
var _x1 = argument0;
var _y1 = argument1;
var _x2 = argument2;
var _y2 = argument3;
var _minX = argument4;
var _minY = argument5;
var _maxX = argument6;
var _maxY = argument7;

// Completely outside.
if ((_x1 <= _minX && _x2 <= _minX) || (_y1 <= _minY && _y2 <= _minY) || 
    (_x1 >= _maxX && _x2 >= _maxX) || (_y1 >= _maxY && _y2 >= _maxY))
    return false;

var m = (_y2 - _y1) / (_x2 - _x1);

var yy = m * (_minX - _x1) + _y1;
if (yy > _minY && yy < _maxY) 
  return true;

yy = m * (_maxX - _x1) + _y1;
if (yy > _minY && yy < _maxY) 
  return true;

xx = (_minY - _y1) / m + _x1;
if (xx > _minX && xx < _maxX) 
  return true;

xx = (_maxY - _y1) / m + _x1;
if (xx > _minX && xx < _maxX) 
  return true;

return false;