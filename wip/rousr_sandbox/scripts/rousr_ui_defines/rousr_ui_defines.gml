///@function rousr_ui_defines()
///@desc Definitions / enums for rousr_ui

/// Dragger
#macro DraggerDefaultSlack (5)
enum DragState {
  None,
	Begin,
	Dragging
};

/// TextAlignment
enum TextAlign {
  Left,
  Right,
  Center
};
