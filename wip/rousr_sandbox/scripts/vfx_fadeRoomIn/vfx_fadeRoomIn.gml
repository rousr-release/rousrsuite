///@desc vfx_fadeRoomInPost
var _id = argument0;
with(_id) {
  if (FadeStart == -1)
    FadeStart = global.frameTime;
    
  var t = global.frameTime - FadeStart;
  if (t < FadeHold + FadeDuration) {
    var progress = 1;
    
    if (t >= FadeHold) {
      progress = t - FadeHold;
      progress = progress / FadeDuration;
      progress = script_execute(FadeEase, progress, 0, 1, 1);
      progress = 1 - progress;
    }
    
    var w = surface_get_width(application_surface);
    var h = surface_get_height(application_surface);
    
    surface_set_target(application_surface);
    draw_sprite_ext(sWhitePixel, 0, 0, 0, w, h, 0, c_black, progress);
    surface_reset_target();
  } else {
    FadeDuration = -1;
    return true;
  }
    
  return false;
}

return true;