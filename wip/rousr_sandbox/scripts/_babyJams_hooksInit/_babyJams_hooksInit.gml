///@desc Initialize Hooks System
global.___createHooks    = ds_list_create();
global.___beginStepHooks = ds_list_create();
global.___stepHooks      = ds_list_create();
global.___cleanupHooks   = ds_list_create();
global.___preDrawHooks   = ds_list_create();
global.___postDrawHooks  = ds_list_create();

// utility hooks
global.___windowResizeHooks = ds_list_create();