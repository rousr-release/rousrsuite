///@desc __lm_inListArgArray - actual work to find in list
///@param _argArray
///@param _list
var _argArray = argument0;
var _list = argument1;

var numArgs = array_length_1d(_argArray);
if (numArgs == 0)
	return false;
	
var numItems = array_length_1d(_list);
if (numItems == 0)
	return false;

var found = false;	
for (var itemIndex = 0; itemIndex < numItems && !found; ++itemIndex) {
	var item = _list[itemIndex];
	if (!is_array(item))
		item = [ item ];
	
	var numItemProps = array_length_1d(item);
	if (numArgs > numItemProps)
		continue;
		
	found = true;
	for (var argIndex = 0; argIndex < numArgs && found; ++argIndex)
		found = item[argIndex] == _argArray[argIndex];
}

return found;