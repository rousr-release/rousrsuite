///@desc pop the end off a list and return it
///@param id
var _id = argument0;
var popIndex = ds_list_size(_id) - 1;

if (popIndex >= 0)
  return list_pop(_id, popIndex);

return undefined;