///anchorInstance_stretchTop
///@desc stretch this object acrosst the top
///with(instance)
///@param (opt)padLeftOrSides
///@param (opt)padTop
///@param (opt)padRight
///@param (opt)addToList
var _padLeft   = argument_count > 0 ? argument[0] : 0;
var _padTop    = argument_count > 1 ? argument[1] : 0;
var _padRight  = argument_count > 2 ? argument[2] : _padLeft;
var _addToList = argument_count > 3 ? argument[3] : true;

var sw = babyJams_gameWidth() - (_padLeft + _padRight);

with(id) {
  x = _padLeft;
  y = _padTop;
  
  var scaleW = sw / sprite_get_width(sprite_index);
  image_xscale = scaleW;
  
  if (_addToList) {
    var updateParams = [ _padLeft, _padTop, _padRight, false ];
    anchorInstance_addList(id, anchorInstance_stretchTop, updateParams);
  }
}