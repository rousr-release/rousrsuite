///@desc Add an audio cue to a tween
///Plays when the tween begins
///@param tween
///@param audio 
///@param priority
///@param loops
var _tween = argument0;
_tween[@Tween.AudioCue] = [ argument1, argument2, argument3 ];
return _tween;