///@desc call when releasing left button
if (__dragger_dragState != DragState.None) {
  __dragger_dragState = DragState.None;
  if (iDragger.dragging == id)
    iDragger.dragging = noone;
  
  depth = __dragger_startDepth; // revert just the depth, cause its what we arbitrarily changed
  if (__dragger_onComplete == noone || !script_execute(__dragger_onComplete, true))
    dragger_reset();
}