///tilegrid_cell_to_px_x
///@description return pixel x of cell tl-corner 
///@param table
///@param cellX
///@param cellY
var _table = argument0;
var _cx    = argument1;
var _cy    = argument2;

var mapId = _table.TileMapId;
var tw    = _table.TileWidth;
var ox    = _table.TileMapX;

return ox + (_cx * tw)
