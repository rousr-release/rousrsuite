/// @description oTextField - Create
Alignment = TextAlign.Left;
IsEditing = false;
FieldW = 100;
FieldH = 7;
OnSubmit = noone;
OnEdit = noone;
HideOnBlur = true
SubmitOnBlur = true;
Text = "";

image_xscale = FieldW * 0.5;
image_yscale = FieldH * 0.5;
image_blend = global.fieldBg;

if (HideOnBlur)
  instance_deactivate_object(id);