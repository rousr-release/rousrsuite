{
    "id": "c3bf3975-756c-4ba6-a2d2-ae6d72201a5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DatePicker",
    "eventList": [
        {
            "id": "d1bdbe84-e32d-4686-90d8-7fe1c1127f2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3bf3975-756c-4ba6-a2d2-ae6d72201a5f"
        },
        {
            "id": "cc300bef-1938-4664-b594-61d623dc9341",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3bf3975-756c-4ba6-a2d2-ae6d72201a5f"
        },
        {
            "id": "968e9c98-6be9-47b7-b2fd-a45cdaeab798",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c3bf3975-756c-4ba6-a2d2-ae6d72201a5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "95e7d0df-35fd-42db-9fd3-29c388705675",
    "visible": false
}