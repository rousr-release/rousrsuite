/// @description DatePicker - Step
if (!visible)
  return;
  
if (Date != noone &&
    (Date != __date ||
     Dirty)) {
  Dirty = false;
  
  Year  = date_get_year(Date);
  Month = date_get_month(Date);
  Day   = date_get_day(Date);
  
  First = date_create_datetime(Year, Month, Day, 0, 0, 0);
  Sow   = date_get_weekday(First);
  Sim   = date_days_in_month(Date);
  
  YearS = string(Year);
  MonthS = (Month < 10 ? "0" : "") + string(Month);
  DayS = (Day < 10 ? "0" : "") + string(Day);
  
  draw_set_font(Font);
  YearW  = string_width(YearS);
  MonthW = string_width(MonthS);
  DayW   = string_width(DayS);
  SpacerWidth = string_width("-");
  
  
  var fx = [ ];
  fx[0] =  x + 3;
  fx[1] = fx[0] + YearW + SpacerWidth;
  fx[2] = fx[1] + MonthW + SpacerWidth;
  
  var fw = [ ];
  fw[0] = YearW;
  fw[1] = MonthW;
  fw[2] = DayW;
  
  var ft = [ ];
  ft[0] = YearS;
  ft[1] = MonthS;
  ft[2] = DayS;
  
  for (var fieldIndex = 0; fieldIndex < DatePicker.FieldCount; ++fieldIndex) {
    var field = InputFields[fieldIndex];
    instance_activate_object(field);  field.x = fx[fieldIndex];
    field.visible = true;
    
    field.x = fx[fieldIndex];
    field.y = y;
    field.FieldW = fw[fieldIndex];
    field.Text = ft[fieldIndex];
    field.OnSubmit = date_picker_on_input_field_submit;
    field.fieldIndex = fieldIndex;
    field.datePicker = id;
  }
}
__date = Date;
