/// @description babyjams - step
var windowWidth = window_get_width();
var windowHeight = window_get_height();

// check for window resize
if (LastWindowWidth != windowWidth || LastWindowHeight != windowHeight) {
  //babyJams_window_setSize(windowWidth, windowHeight); // resize the canvas
  _babyJams_doHooks(global.___windowResizeHooks);
  LastWindowWidth = windowWidth;
  LastWindowHeight = windowHeight;
}

// now do our updates
_babyJams_doHooks(babyJams_stepHooks(), true);