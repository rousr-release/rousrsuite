/// @description babyjams - create
/// the main manager for the libraries
if (!ensureSingleton("___babyjams"))
  return;
  
global.___babyjams = id;
global.__bjui_textFieldActive = noone;

global._setFont = noone;
global._setColor = 0;

babyJams_defaults();
var babyjamsDefines = asset_get_index("babyjams_defines");
if (babyjamsDefines != -1) 
  script_execute(babyjamsDefines);

__initialized = false;
// Must be first, most systems depend on hooks
_babyJams_hooksInit();

// init systems
if (EnableAnchors)        _anchorInstance_init();
if (EnableDisplayManager) _displayManager_init();
if (EnableDeltaTime)      _deltaTime_init();

// babyjams init
babyJams_addBeginStepHook(_babyJams_postInit);

ensureFont(fnt_game);