{
    "id": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Button",
    "eventList": [
        {
            "id": "2dd2e465-b5a3-4aba-b2f7-b3c89ee81961",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "ad2cb4e4-a9eb-4a55-b33e-e37d23c3c773",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "da7fcb25-c4d7-4b57-bd54-a2bdd5c5792d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "3a7f3ea9-fe7b-4d3e-bb16-2ae2c5077f59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "48862509-362d-4f5e-b14e-0a0cc89656c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "a9a1bedd-9697-434c-a71d-f45b2ba71163",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "visible": true
}