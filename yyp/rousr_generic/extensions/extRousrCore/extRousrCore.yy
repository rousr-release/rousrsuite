{
    "id": "41aa7af4-8ff0-4c74-b128-a0ebf7e056c1",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "extRousrCore",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 194,
    "date": "2017-33-29 08:12:44",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "32df1491-b20f-4ad6-8ac8-a825b3536e2d",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 194,
            "filename": "extRousrCore.gml",
            "final": "",
            "functions": [
                {
                    "id": "6d1dbfb0-4097-4d1c-ab95-9a02a228c030",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_ensure_singleton",
                    "help": "called `with` object to be singleton - ensure the name passed is a singleton, will call instance destroy on a different id ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_ensure_singleton",
                    "returnType": 2
                },
                {
                    "id": "33ee5338-fcbb-4f23-84bd-77f0e7da3bca",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_ensure_font",
                    "help": "cache the current font ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_ensure_font",
                    "returnType": 2
                },
                {
                    "id": "4b5ebf1e-1cdd-4ee9-b55e-ad28f9535d3d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_ensure_color",
                    "help": "cache the current color ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_ensure_color",
                    "returnType": 2
                },
                {
                    "id": "963bfd16-9349-4c45-a6b6-0fb87ab1f7fa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_color_hex",
                    "help": "[Convert the RGB to BGR][https:\/\/forum.yoyogames.com\/index.php?threads\/why-are-hex-colours-bbggrr-instead-of-rrggbb.16325\/#post-105309] ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_color_hex",
                    "returnType": 2
                },
                {
                    "id": "e0f24b6d-36ec-4b1d-8723-d1ca48fda6e2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_color_glsl",
                    "help": "convert a color to an array float values [0.0 - 1.0] ([_out_array])",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_color_glsl",
                    "returnType": 2
                },
                {
                    "id": "699174c8-1006-4e3f-8f8e-4e378f79c9b5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_next_pot",
                    "help": "return the nearest power of 2 for a given number [integer] ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_next_pot",
                    "returnType": 2
                },
                {
                    "id": "47c2be6f-9735-4b23-8e3e-72bbf051ed1b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_shadow_text",
                    "help": "Draw text with a shadow (_y, _text, _fg, _bg, [_xoff=1], [_yoff=1])",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_shadow_text",
                    "returnType": 2
                },
                {
                    "id": "3d0ff163-8413-4907-af45-4aaefaf55503",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_outline_text",
                    "help": "draw text with an oultine (_y, _text, _fg, _bg)",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_outline_text",
                    "returnType": 2
                },
                {
                    "id": "654b87f8-2c05-492c-9757-65eaaa12503d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_error",
                    "help": "Error wrapper [eventual logging system] (_text)",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_error",
                    "returnType": 2
                },
                {
                    "id": "3a641425-6c9f-4a1f-aa75-2a177ad0ecd9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_log",
                    "help": "Log wrapper [eventual logging system] (_text)",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_log",
                    "returnType": 2
                },
                {
                    "id": "a52b2ee7-4800-4b03-a3a3-e7865b1ed114",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_execute",
                    "help": "(_script_index, _params, [paramCount])",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_execute",
                    "returnType": 2
                },
                {
                    "id": "fef8503c-a262-4e4b-a84b-46577063898e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_aabb_contains_line",
                    "help": "courtesy of https:\/\/stackoverflow.com\/questions\/1585525\/how-to-find-the-intersection-point-between-a-line-and-a-rectangle (_line_y1, _line_x2, _line_y2, _rect_min_x, _rect_min_y, _rect_max_x, _rect_max_y)",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_aabb_contains_line",
                    "returnType": 2
                },
                {
                    "id": "839b3468-f006-495b-9377-d432182a7724",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__extrousrcore_script_index",
                    "help": "Returns the actual runtime script index because YYG doesn't know how to do that apparently (ext_script_index)",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extrousrcore_script_index",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 2,
            "order": [
                "6d1dbfb0-4097-4d1c-ab95-9a02a228c030",
                "33ee5338-fcbb-4f23-84bd-77f0e7da3bca",
                "4b5ebf1e-1cdd-4ee9-b55e-ad28f9535d3d",
                "963bfd16-9349-4c45-a6b6-0fb87ab1f7fa",
                "e0f24b6d-36ec-4b1d-8723-d1ca48fda6e2",
                "699174c8-1006-4e3f-8f8e-4e378f79c9b5",
                "47c2be6f-9735-4b23-8e3e-72bbf051ed1b",
                "3d0ff163-8413-4907-af45-4aaefaf55503",
                "654b87f8-2c05-492c-9757-65eaaa12503d",
                "3a641425-6c9f-4a1f-aa75-2a177ad0ecd9",
                "a52b2ee7-4800-4b03-a3a3-e7865b1ed114",
                "fef8503c-a262-4e4b-a84b-46577063898e",
                "839b3468-f006-495b-9377-d432182a7724"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "0.12.0"
}