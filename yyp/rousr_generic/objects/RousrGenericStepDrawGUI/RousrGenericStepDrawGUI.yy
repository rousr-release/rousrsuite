{
    "id": "d1808178-845d-4ffa-8537-c6b31a8eaee6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrGenericStepDrawGUI",
    "eventList": [
        {
            "id": "49ae092e-2494-4883-b25e-8cdc6380d5f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d1808178-845d-4ffa-8537-c6b31a8eaee6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "79a7d9da-590c-4549-b9b3-b8fc1fb9a005",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}