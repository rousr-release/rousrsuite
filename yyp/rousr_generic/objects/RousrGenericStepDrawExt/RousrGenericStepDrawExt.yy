{
    "id": "c6b2b3c8-bdea-491a-9dbb-063ac14668fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrGenericStepDrawExt",
    "eventList": [
        {
            "id": "bcc0e5a2-4011-4397-b019-690d80dcd57b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c6b2b3c8-bdea-491a-9dbb-063ac14668fd"
        },
        {
            "id": "8048120a-d48a-4f21-982e-e099f2b179b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6b2b3c8-bdea-491a-9dbb-063ac14668fd"
        },
        {
            "id": "a6cdbede-a5af-468a-98f7-c6cdc07da281",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c6b2b3c8-bdea-491a-9dbb-063ac14668fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}