{
    "id": "579ef972-2a67-4057-b184-cfbf2479366a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrGenericStepDraw",
    "eventList": [
        {
            "id": "21dba26e-9ede-4391-905a-a0d4847cca5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "579ef972-2a67-4057-b184-cfbf2479366a"
        },
        {
            "id": "bb8492b5-fbcd-4995-b7e8-729406834e08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "579ef972-2a67-4057-b184-cfbf2479366a"
        },
        {
            "id": "c777c67c-9cc4-47ed-9274-6428185c40ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "579ef972-2a67-4057-b184-cfbf2479366a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "38ede707-f040-4757-9bab-ece1b189fbff",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}