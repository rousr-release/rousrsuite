{
    "id": "79a7d9da-590c-4549-b9b3-b8fc1fb9a005",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrGenericDrawGUI",
    "eventList": [
        {
            "id": "db8e49e0-0fcb-415d-b49f-1d0f95e2c53f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "79a7d9da-590c-4549-b9b3-b8fc1fb9a005"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f1b1d5f8-3386-4e6f-bf29-8febddafcd2c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}