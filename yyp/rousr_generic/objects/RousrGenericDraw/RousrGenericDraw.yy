{
    "id": "86032542-fe0f-427e-8537-e09d4a5cd373",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrGenericDraw",
    "eventList": [
        {
            "id": "39b8bff0-9ed5-4c7a-9d98-6bcecfa16063",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "86032542-fe0f-427e-8537-e09d4a5cd373"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f1b1d5f8-3386-4e6f-bf29-8febddafcd2c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}