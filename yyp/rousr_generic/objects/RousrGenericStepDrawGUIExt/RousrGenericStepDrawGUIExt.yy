{
    "id": "66d08ca0-1b2e-4365-9377-763d44741f48",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrGenericStepDrawGUIExt",
    "eventList": [
        {
            "id": "b0d66efe-e2d0-4202-8990-5b2b1798a002",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "66d08ca0-1b2e-4365-9377-763d44741f48"
        },
        {
            "id": "2cfa42d0-1c54-4c52-ab40-e6eb3b60397c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "66d08ca0-1b2e-4365-9377-763d44741f48"
        },
        {
            "id": "40a55fd8-7a09-4983-bdc5-7c1f2df8d430",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "66d08ca0-1b2e-4365-9377-763d44741f48"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "afe43b15-1c62-40da-845b-629bb89e450e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}