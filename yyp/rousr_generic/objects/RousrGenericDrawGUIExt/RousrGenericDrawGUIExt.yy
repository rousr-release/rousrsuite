{
    "id": "afe43b15-1c62-40da-845b-629bb89e450e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrGenericDrawGUIExt",
    "eventList": [
        {
            "id": "eeb0e8ec-bb7c-4edf-93bb-3718fe303b90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        },
        {
            "id": "2b449512-0be9-4717-8b14-32236b5f34b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        },
        {
            "id": "4eda5085-cde9-4013-ad5c-739df90f0495",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        },
        {
            "id": "262dbd28-6633-4e72-ac75-353bfc27f6ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        },
        {
            "id": "e39b64a5-f028-4343-b6db-9a9edd84b11d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "afe43b15-1c62-40da-845b-629bb89e450e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}