///@desc __generic_object_from_type - return what a thing is
///@arg _type - EGenericType
///@extensionizer { "docs": false }
gml_pragma("forceinline");

var type = RousrGeneric; 
switch (argument0) {
  case EGenericType.Empty:       type = RousrGenericEmpty; break;
	case EGenericType.Normal:      type = RousrGeneric; break;
	case EGenericType.Step:        type = RousrGenericStep;  break;
  
	case EGenericType.Draw:        type = RousrGenericDraw;  break;
	case EGenericType.StepDraw:    type = RousrGenericStepDraw;  break;
  case EGenericType.DrawGUI:     type = RousrGenericDrawGUI;  break;
  case EGenericType.StepDrawGUI: type = RousrGenericStepDrawGUI;  break;

	case EGenericType.StepExt:       	type = RousrGenericStepExt; break;
  case EGenericType.DrawExt:       	type = RousrGenericDrawExt; break;
	case EGenericType.StepDrawExt:   	type = RousrGenericStepDrawExt; break;
                										
  case EGenericType.DrawGUIExt:    	type = RousrGenericDrawGUIExt; break;
  case EGenericType.StepDrawGUIExt: type = RousrGenericStepDrawGUIExt; break;
  
	default: break;
};

return type;