///@function __sr_generic_dummy([_any])
///@desc it does nothing - used for placeholders in callback systems
///@param {*} [_any]   accepts a param optionally so its usable with or without 'em
///@extensionizer { "docs": false }
var _dummy = argument_count > 0 ? argument[0] : false;  // support not syntax erroring on arguments