///@desc __sr_generic_event - helper function to call the event function only if they exist.
///@param_event - event type
///@extensionizer { "docs": false }
gml_pragma("forceinline");

var cb = __Rousr_generic_hooks[argument0];
if (cb != __sr_generic_dummy) script_execute(cb);