///@function sr_generic_draw_self([_id])
///@desc function that just calls draw_self, can be used with generics
///@param {Real} [_id]   if passed, called `with(_id)`
gml_pragma("forceinline");
var _id = argument_count > 0 ? argument[0] : id;
with (_id)
	draw_self();