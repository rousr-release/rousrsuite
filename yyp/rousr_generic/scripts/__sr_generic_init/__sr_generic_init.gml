///@function __sr_generic_init([_id])
///@desc initialize a generic object
///@param {Real} [_id=id]
///@extensionizer { "docs": false }
gml_pragma("forceinline");

var _id = argument_count > 0 ? argument[0] : id;
with (_id)
	__Rousr_generic_hooks = array_create(EGenericHook.Num, __sr_generic_dummy);