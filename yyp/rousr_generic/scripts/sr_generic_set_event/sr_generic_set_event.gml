///@desc sr_generic_event_set - set the callback for a generic event
///@param {Real} _id
///@param {Real:EGenericHook} _hook
///@param {Real}              _event_callback
var _id       = argument0;
var _hook     = argument1;
var _callback = argument2;

with(_id) {
	if (!is_real(_callback) || _callback == noone) 
		_callback = __sr_generic_dummy;
	__Rousr_generic_hooks[_hook] = _callback;
}