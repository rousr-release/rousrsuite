///@function sr_generic_create(_x, _y, _depth, [_create], [_generic_type=EGenericType.Normal])
///@desc create a generic object
///@param {Real} _x
///@param {Real} _y 
///@param {Real} _depth                                            set a layer_id afterwards if you'd prefer layers.
///@param {Real} [_create=noone]                                   called `with(new generic)` **Format:** `function(_generic_id) -> No Return`
///@param {Real:EGenericType} [_generic_type=EGenericType.Normal]   type of generic to make
var _x      = argument[0], 
    _y      = argument[1], 
    _depth  = argument[2];
var _create = argument_count > 3 ? argument[3] : undefined;
var _type   = argument_count > 4 ? argument[4] : EGenericType.Normal;

with (instance_create_depth(_x, _y, _depth, __sr_generic_object_from_type(_type))) {
	if (_create != undefined && _create != noone) 
    script_execute(_create, id);
	return id;
}

return noone;