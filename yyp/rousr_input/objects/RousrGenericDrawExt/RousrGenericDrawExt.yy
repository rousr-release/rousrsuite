{
	"id": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3",
	"modelName": "GMObject",
	"mvc": "1.0",
	"name": "RousrGenericDrawExt",
	"eventList": [
		{
			"id": "4bae590c-06c4-44e9-b84d-ca51e3a2397f",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 72,
			"eventtype": 8,
			"m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
		},
		{
			"id": "a5d3d676-7a0a-4b4a-b973-4670388c2697",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 0,
			"eventtype": 8,
			"m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
		},
		{
			"id": "2c08cbe0-8816-4634-809f-d82a19c4801f",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 73,
			"eventtype": 8,
			"m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
		},
		{
			"id": "24c5d9da-ba65-4b2d-b5bf-20fa1332323a",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 76,
			"eventtype": 8,
			"m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
		},
		{
			"id": "bab0f73a-8b7b-4098-bea4-509cb166ed41",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 77,
			"eventtype": 8,
			"m_owner": "e21e5ada-d590-4ed5-b1b9-ea283068dcf3"
		}
	],
	"maskSpriteId": "00000000-0000-0000-0000-000000000000",
	"overriddenProperties": null,
	"parentObjectId": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827",
	"persistent": false,
	"physicsAngularDamping": 0.1,
	"physicsDensity": 0.5,
	"physicsFriction": 0.2,
	"physicsGroup": 0,
	"physicsKinematic": false,
	"physicsLinearDamping": 0.1,
	"physicsObject": false,
	"physicsRestitution": 0.1,
	"physicsSensor": false,
	"physicsShape": 1,
	"physicsShapePoints": null,
	"physicsStartAwake": true,
	"properties": null,
	"solid": false,
	"spriteId": "00000000-0000-0000-0000-000000000000",
	"visible": true
}