{
	"id": "16b9a859-5919-4978-a493-48f68fe6180c",
	"modelName": "GMObject",
	"mvc": "1.0",
	"name": "RousrGenericStepExt",
	"eventList": [
		{
			"id": "8f60bff6-7b0d-4331-8133-b16ff1d09905",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 0,
			"eventtype": 3,
			"m_owner": "16b9a859-5919-4978-a493-48f68fe6180c"
		},
		{
			"id": "be089f06-ff38-4cd2-a690-f1178a360860",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 1,
			"eventtype": 3,
			"m_owner": "16b9a859-5919-4978-a493-48f68fe6180c"
		},
		{
			"id": "4e037fdf-4fa5-4885-bfe9-d9e308fa0753",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 2,
			"eventtype": 3,
			"m_owner": "16b9a859-5919-4978-a493-48f68fe6180c"
		}
	],
	"maskSpriteId": "00000000-0000-0000-0000-000000000000",
	"overriddenProperties": null,
	"parentObjectId": "5ffbb50e-9ab7-4900-a9d3-0002a1adb827",
	"persistent": false,
	"physicsAngularDamping": 0.1,
	"physicsDensity": 0.5,
	"physicsFriction": 0.2,
	"physicsGroup": 0,
	"physicsKinematic": false,
	"physicsLinearDamping": 0.1,
	"physicsObject": false,
	"physicsRestitution": 0.1,
	"physicsSensor": false,
	"physicsShape": 1,
	"physicsShapePoints": null,
	"physicsStartAwake": true,
	"properties": null,
	"solid": false,
	"spriteId": "00000000-0000-0000-0000-000000000000",
	"visible": true
}