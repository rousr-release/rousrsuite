{
	"id": "38ede707-f040-4757-9bab-ece1b189fbff",
	"modelName": "GMObject",
	"mvc": "1.0",
	"name": "RousrGenericStep",
	"eventList": [
		{
			"id": "3a91ce31-98f5-457e-90bd-95d92e5fcfc8",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 0,
			"eventtype": 3,
			"m_owner": "38ede707-f040-4757-9bab-ece1b189fbff"
		}
	],
	"maskSpriteId": "00000000-0000-0000-0000-000000000000",
	"overriddenProperties": null,
	"parentObjectId": "f1b1d5f8-3386-4e6f-bf29-8febddafcd2c",
	"persistent": false,
	"physicsAngularDamping": 0.1,
	"physicsDensity": 0.5,
	"physicsFriction": 0.2,
	"physicsGroup": 0,
	"physicsKinematic": false,
	"physicsLinearDamping": 0.1,
	"physicsObject": false,
	"physicsRestitution": 0.1,
	"physicsSensor": false,
	"physicsShape": 1,
	"physicsShapePoints": null,
	"physicsStartAwake": true,
	"properties": null,
	"solid": false,
	"spriteId": "00000000-0000-0000-0000-000000000000",
	"visible": true
}