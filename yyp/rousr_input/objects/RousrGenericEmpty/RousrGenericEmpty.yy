{
	"id": "f1b1d5f8-3386-4e6f-bf29-8febddafcd2c",
	"modelName": "GMObject",
	"mvc": "1.0",
	"name": "RousrGenericEmpty",
	"eventList": [
		{
			"id": "448be44c-1290-4278-abc7-fb35b9fdb7d7",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 0,
			"eventtype": 0,
			"m_owner": "f1b1d5f8-3386-4e6f-bf29-8febddafcd2c"
		},
		{
			"id": "839fe115-57a4-444e-a161-f1aa75070ed8",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 0,
			"eventtype": 1,
			"m_owner": "f1b1d5f8-3386-4e6f-bf29-8febddafcd2c"
		},
		{
			"id": "e562fd0f-9924-4ae5-85da-9523d29b385a",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 0,
			"eventtype": 12,
			"m_owner": "f1b1d5f8-3386-4e6f-bf29-8febddafcd2c"
		},
		{
			"id": "b06f8862-87ee-44ce-a709-7ff202ed2c68",
			"modelName": "GMEvent",
			"mvc": "1.0",
			"IsDnD": false,
			"collisionObjectId": "00000000-0000-0000-0000-000000000000",
			"enumb": 0,
			"eventtype": 8,
			"m_owner": "f1b1d5f8-3386-4e6f-bf29-8febddafcd2c"
		}
	],
	"maskSpriteId": "00000000-0000-0000-0000-000000000000",
	"overriddenProperties": null,
	"parentObjectId": "00000000-0000-0000-0000-000000000000",
	"persistent": false,
	"physicsAngularDamping": 0.1,
	"physicsDensity": 0.5,
	"physicsFriction": 0.2,
	"physicsGroup": 0,
	"physicsKinematic": false,
	"physicsLinearDamping": 0.1,
	"physicsObject": false,
	"physicsRestitution": 0.1,
	"physicsSensor": false,
	"physicsShape": 1,
	"physicsShapePoints": null,
	"physicsStartAwake": true,
	"properties": null,
	"solid": false,
	"spriteId": "00000000-0000-0000-0000-000000000000",
	"visible": true
}