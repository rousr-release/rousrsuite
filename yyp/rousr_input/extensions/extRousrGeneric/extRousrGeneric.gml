#define sr_generic_create
///@function sr_generic_create(_x, _y, _depth, [_create], [_generic_type=EGenericType.Normal])
///@desc create a generic object
///@param {Real} _x
///@param {Real} _y 
///@param {Real} _depth                                            set a layer_id afterwards if you'd prefer layers.
///@param {Real} [_create=noone]                                   called `with(new generic)` **Format:** `function(_generic_id) -> No Return`
///@param {Real:EGenericType} [_generic_type=EGenericType.Normal]   type of generic to make
var _x      = argument[0], 
    _y      = argument[1], 
    _depth  = argument[2];
var _create = argument_count > 3 ? argument[3] : undefined;
var _type   = argument_count > 4 ? argument[4] : EGenericType.Normal;

with (instance_create_depth(_x, _y, _depth, __sr_generic_object_from_type(_type))) {
	if (_create != undefined && _create != noone) 
    script_execute(_create, id);
	return id;
}

return noone;

#define sr_generic_draw_self
///@function sr_generic_draw_self([_id])
///@desc function that just calls draw_self, can be used with generics
///@param {Real} [_id]   if passed, called `with(_id)`
gml_pragma("forceinline");
var _id = argument_count > 0 ? argument[0] : id;
with (_id)
	draw_self();

#define sr_generic_set_event
///@desc sr_generic_event_set - set the callback for a generic event
///@param {Real} _id
///@param {Real:EGenericHook} _hook
///@param {Real}              _event_callback
var _id       = argument0;
var _hook     = argument1;
var _callback = argument2;

with(_id) {
	if (!is_real(_callback) || _callback == noone) 
		_callback = __extrousrgeneric_script_index(__sr_generic_dummy);
	__Rousr_generic_hooks[_hook] = _callback;
}

#define __sr_generic_init
///@function __sr_generic_init([_id])
///@desc initialize a generic object
///@param {Real} [_id=id]
gml_pragma("forceinline");

var _id = argument_count > 0 ? argument[0] : id;
with (_id)
	__Rousr_generic_hooks = array_create(EGenericHook.Num, __extrousrgeneric_script_index(__sr_generic_dummy));

#define __sr_generic_event
///@desc __sr_generic_event - helper function to call the event function only if they exist.
///@arg _event - event type
gml_pragma("forceinline");

var cb = __Rousr_generic_hooks[argument0];
if (cb != __extrousrgeneric_script_index(__sr_generic_dummy)) script_execute(cb);

#define __sr_generic_object_from_type
///@desc __generic_object_from_type - return what a thing is
///@arg _type - EGenericType
gml_pragma("forceinline");

var type = RousrGeneric; 
switch (argument0) {
  case EGenericType.Empty:       type = RousrGenericEmpty; break;
	case EGenericType.Normal:      type = RousrGeneric; break;
	case EGenericType.Step:        type = RousrGenericStep;  break;
  
	case EGenericType.Draw:        type = RousrGenericDraw;  break;
	case EGenericType.StepDraw:    type = RousrGenericStepDraw;  break;
  case EGenericType.DrawGUI:     type = RousrGenericDrawGUI;  break;
  case EGenericType.StepDrawGUI: type = RousrGenericStepDrawGUI;  break;

	case EGenericType.StepExt:       	type = RousrGenericStepExt; break;
  case EGenericType.DrawExt:       	type = RousrGenericDrawExt; break;
	case EGenericType.StepDrawExt:   	type = RousrGenericStepDrawExt; break;
                										
  case EGenericType.DrawGUIExt:    	type = RousrGenericDrawGUIExt; break;
  case EGenericType.StepDrawGUIExt: type = RousrGenericStepDrawGUIExt; break;
  
	default: break;
};

return type;

#define __sr_generic_dummy
///@function __sr_generic_dummy([_any])
///@desc it does nothing - used for placeholders in callback systems
///@param {*} [_any]   accepts a param optionally so its usable with or without 'em
var _dummy = argument_count > 0 ? argument[0] : false;  // support not syntax erroring on arguments

#define __extrousrgeneric_script_index
///@function __extrousrgeneric_script_index(ext_script_index)
///@desc Returns the actual runtime script index because YYG doesn't know how to do that apparently
///@param {Real} ext_script_index
///@extensionizer { "docs": false, "export": true} 
///@returns {Real} script_index
gml_pragma("forceinline")
gml_pragma("global", "global.__extrousrgeneric_script_index_lookup = array_create(0);global.__extrousrgeneric_script_index_lookup[@ rousr_generic_definitions] = asset_get_index(\"rousr_generic_definitions\");global.__extrousrgeneric_script_index_lookup[@ sr_generic_create] = asset_get_index(\"sr_generic_create\");global.__extrousrgeneric_script_index_lookup[@ sr_generic_draw_self] = asset_get_index(\"sr_generic_draw_self\");global.__extrousrgeneric_script_index_lookup[@ sr_generic_set_event] = asset_get_index(\"sr_generic_set_event\");global.__extrousrgeneric_script_index_lookup[@ __sr_generic_init] = asset_get_index(\"__sr_generic_init\");global.__extrousrgeneric_script_index_lookup[@ __sr_generic_event] = asset_get_index(\"__sr_generic_event\");global.__extrousrgeneric_script_index_lookup[@ __sr_generic_object_from_type] = asset_get_index(\"__sr_generic_object_from_type\");global.__extrousrgeneric_script_index_lookup[@ __sr_generic_dummy] = asset_get_index(\"__sr_generic_dummy\");");

var _index = argument0;return global.__extrousrgeneric_script_index_lookup[@ _index];
