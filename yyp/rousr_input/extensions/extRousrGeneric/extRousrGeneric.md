#### `sr_generic_create`   
| | | 
| ------------------------------------- | ---------------------------------------------------------------------------------- |   
**_y**                                  | {Real}    
**_depth**                              | {Real} set a layer_id afterwards if you'd prefer layers.   
**[_create=noone]**                     | {Real} called `with(new generic)` **Format:** `function(_generic_id) -> No Return`   
**[_generic_type=EGenericType.Normal]** | {Real:EGenericType} type of generic to make   
**returns**:                           | None   
```
sr_generic_create(_y, _depth, [_create=noone], [_generic_type=EGenericType.Normal])
```   
create a generic object

---

#### `sr_generic_draw_self`   
**returns**: | None   
```
sr_generic_draw_self()
```   
function that just calls draw_self, can be used with generics

---

#### `sr_generic_set_event`   
| | | 
| ----------------- | -------------------- |   
**_hook**           | {Real:EGenericHook}    
**_event_callback** | {Real}    
**returns**:       | None   
```
sr_generic_set_event(_hook, _event_callback)
```   
sr_generic_event_set - set the callback for a generic event

---

#### `__sr_generic_init`   
**returns**: | None   
```
__sr_generic_init()
```   
initialize a generic object

---

#### `__sr_generic_event`   
**returns**: | None   
```
__sr_generic_event()
```   
__sr_generic_event - helper function to call the event function only if they exist.

---

#### `__sr_generic_object_from_type`   
**returns**: | None   
```
__sr_generic_object_from_type()
```   
__generic_object_from_type - return what a thing is

---

#### `__sr_generic_dummy`   
**returns**: | None   
```
__sr_generic_dummy()
```   
it does nothing - used for placeholders in callback systems

---

