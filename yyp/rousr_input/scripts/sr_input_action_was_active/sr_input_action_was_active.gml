///@function sr_input_action_was_active
///@desc return if an action is active
///@param {Real} _action   index of action
var _action = argument0;
var was_active = false;
with (RousrInput) {
  var input_actions = Input_data[ERousrInput.Actions];
  var action_data = input_actions[? _action];
  if (action_data != undefined)
    was_active = action_data[ERousrInputAction.WasActive];
}

return was_active;