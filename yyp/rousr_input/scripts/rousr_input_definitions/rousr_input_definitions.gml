///@function sr_input_definitions()
///@desc enums and macros for sr_input
///@extensionizer { "export": false, "docs": false }
#macro RousrInput (global.__input)

enum ERousrInput {
	Actions = 0,
	Binds,
	
	Num
};

enum ERousrInputType {
	Key,
	GamePad,
	Mouse,
	
	Num
};

enum ERousrInputEvent {
	Down,
	Pressed,
	Released,
	
	Num
};

enum ERousrInputAction {
	WasActive = 0,
  Active,
  Callbacks,
	
	Num
}

enum ERousrInputQueued {
  Action = 0,
  Events,
}

