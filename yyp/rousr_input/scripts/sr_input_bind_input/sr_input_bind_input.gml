///@function sr_input_bind_input(_input_type, _input_button, _action, _event)
///@desc bind an input to an action
///@param _input_type
///@param _input_button
///@param _action
///@param _event
var _input_type   = argument0;
var _input_button = argument1;
var _action      = argument2;
var _event       = argument3; 

if (_input_type < 0 || _input_type >= ERousrInputType.Num) {
	show_debug_message("input - warning: can't bind input invalid input type.");
	return;
}

if (_event < 0 || _event >= ERousrInputEvent.Num) {
	show_debug_message("input - warning: can't bind input invalid input event");
	return;
}

with (RousrInput) {
  var input_bind_maps = Input_data[@ERousrInput.Binds];
  var input_bind_map  = input_bind_maps[_input_type];

  var input_bind = input_bind_map[? _input_button];
  if (input_bind == undefined) {
  	input_bind = [ ];
  	input_bind_map[? _input_button] = input_bind;
  }

  var num_bindings = array_length_1d(input_bind);
  input_bind[@ num_bindings] = [ _event, _action ];
}
