///@function sr_input_clean_up()
///@desc rousr input clean up event
ds_map_destroy(Input_data[ERousrInput.Actions]);
var  binding_maps = Input_data[ERousrInput.Binds];
for (var i = 0; i < ERousrInputType.Num; ++i) 
	ds_map_destroy(binding_maps[i]);
	
Input_data = undefined;
