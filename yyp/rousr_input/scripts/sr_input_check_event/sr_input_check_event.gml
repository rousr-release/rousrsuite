///@function sr_input_check_event(_events, _event_type)
///@desc returns true if `_events` is or cotnains `_event_type`
///@param {Array|Real} _events   array of `ERousrInputEvents` or a single one
///@param {Real} _event_type     `ERousrInputEvent` to find
///@returns {Boolean} true if `_events` is or contains `_event_type`
var _events = argument0;
var _event_type = argument1;

if (_events == _event_type) 
  return true;
  
if (!is_array(_events))
  return false;
  
var num_events = array_length_1d(_events);
var found = false;
for (var i = 0; i < num_events && !found; ++i) {
  found = _events[i] == _event_type; 
}

return found;