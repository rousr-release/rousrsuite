///@function sr_input_system_create()
///@desc input_init - initialize the input data
with (sr_generic_create(0, 0, 0, undefined, EGenericType.Step))
  sr_ensure_singleton("__input");

with (RousrInput) {
  persistent = true;
  
  sr_generic_set_event(id, EGenericHook.Step,    sr_input_step);
  sr_generic_set_event(id, EGenericHook.CleanUp, sr_input_clean_up);
  
  Input_data = array_create(ERousrInput.Num);
  Input_data[@ERousrInput.Actions] = ds_map_create();
  Input_data[@ERousrInput.Binds]   = array_create(ERousrInputType.Num);

  var binding_maps = Input_data[ERousrInput.Binds];
  for (var i = 0; i < ERousrInputType.Num; ++i) 
  	binding_maps[@i] = ds_map_create();
}