///@function sr_input_step()
///@desc step event function
var input_actions = Input_data[ERousrInput.Actions];
var input_binds   = Input_data[ERousrInput.Binds];

// reset the actions
for (var action_index = ds_map_find_first(input_actions); 
     action_index != undefined;
     action_index = ds_map_find_next(input_actions, action_index)) {   
	
  var action_data = input_actions[? action_index];
	if (action_data != undefined) {
		action_data[@ERousrInputAction.WasActive] = action_data[ERousrInputAction.Active];
		action_data[@ERousrInputAction.Active]    = false;
	}
}

// keys first
var key_binds = input_binds[ERousrInputType.Key];
var queued_actions = [ ];
	
var key_check = ds_map_find_first(key_binds);
var num_queued_actions = 0;
while (key_check != undefined) {
	var down     = keyboard_check(key_check);
	var pressed  = keyboard_check_pressed(key_check);
	var released = keyboard_check_released(key_check);
	
  if (down || pressed || released) {
  
  	var binds = key_binds[? key_check];
  	var num_binds = array_length_1d(binds);

  	for (var i = 0; i < num_binds; ++i) {
  		var bind = binds[i];
  		var bind_event  = bind[0];
  		var bind_action = bind[1];
		
      var ignore = true;
      switch (bind_event) {
        case ERousrInputEvent.Down:     ignore = !down; break;
        case ERousrInputEvent.Released: ignore = !released; break;
        case ERousrInputEvent.Pressed:  ignore = !pressed; break;
        default: break;
      }
      
      if (ignore)
        continue;
      
      // see if we already have this action in the queue (pressed/down)
      var queued_action = undefined;
      for (var qi = 0; qi < num_queued_actions && queued_action == undefined; ++qi) {
        var qa = queued_actions[qi];
        if (qa[ERousrInputQueued.Action] == bind_action) {
          queued_action = qa;
        }
      }
    
      if (queued_action == undefined) {
        queued_action = [ bind_action, [ ] ];
        queued_actions[num_queued_actions++] = queued_action;
      }
      
      var queued_events = queued_action[ERousrInputQueued.Events];
      var num_events = array_length_1d(queued_events);
      var found = false;
      for (var ei = 0; ei < num_events && !found; ++ei)
        found = queued_events[ei] == bind_event; 
      
      if (!found)
        queued_events[@ num_events] = bind_event;
    }
  }
  
	key_check = ds_map_find_next(key_binds, key_check);
}

for (var i = 0; i < num_queued_actions; ++i) {
	var queued_action = queued_actions[i];
  
  var action        = queued_action[ERousrInputQueued.Action];
  var input_events  = queued_action[ERousrInputQueued.Events];

  var action_data   = input_actions[? action];
  var callback_list = action_data[ERousrInputAction.Callbacks];
  var num_callbacks = array_length_1d(callback_list);
	
	action_data[@ ERousrInputAction.Active] = true;
	
  for (var callbackIndex = num_callbacks - 1; callbackIndex >= 0; --callbackIndex) {
		var callback_data = callback_list[callbackIndex];
    var callback = callback_data[0];
    var user_data = callback_data[1];
		if (script_execute(callback, action, input_events, user_data))
			break;
	}
}
