///@function sr_input_register_action(_action, _action_function)
///@desc register an action type with the input system
///@param {Real} _action            an real or string that represents the action. this key is bound to.
///@param {Real} _action_function   a script to callback `_action_function(_action, _type)`  `_type` = pressed, released, down, down and pressed come in the same frame.
var _action           = argument[0];
var _action_function  = argument[1];
var _action_user_data = argument_count > 2 ? argument[2] : undefined;

with (RousrInput) {
  var action_map  = Input_data[ERousrInput.Actions];
  var action_data = action_map[? _action];
  if (action_data == undefined) {
  	action_data = [ false, false, [ ] ];
  	action_map[? _action] = action_data;
  }

  var callback_list = action_data[ERousrInputAction.Callbacks];
  var num_callbacks = array_length_1d(callback_list);
  callback_list[@ num_callbacks] = [ _action_function, _action_user_data ];
}
