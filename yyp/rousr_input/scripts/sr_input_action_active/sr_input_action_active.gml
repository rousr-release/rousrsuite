///@function sr_input_action_active
///@desc return if an action is active
///@param {Real} _action   index of action
var _action = argument0;
var active = false;
with (RousrInput) {
  var input_actions = Input_data[ERousrInput.Actions];
  var action_data = input_actions[? _action];
  if (action_data != undefined)
    active = action_data[ERousrInputAction.Active];
}

return active;