{
    "id": "be957279-1350-4461-b9fa-6096c6130a04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMark_X",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "225a7c00-1436-41d5-bf77-21032f42362a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be957279-1350-4461-b9fa-6096c6130a04",
            "compositeImage": {
                "id": "3fe0de21-75ff-4bd8-826e-95167f82c659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "225a7c00-1436-41d5-bf77-21032f42362a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1913c605-719e-4845-b15a-6342413c9d3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "225a7c00-1436-41d5-bf77-21032f42362a",
                    "LayerId": "de31d7d6-b78b-45a7-a1f2-6b447d2715d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "de31d7d6-b78b-45a7-a1f2-6b447d2715d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be957279-1350-4461-b9fa-6096c6130a04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}