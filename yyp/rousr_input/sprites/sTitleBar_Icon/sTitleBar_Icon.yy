{
    "id": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleBar_Icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2821147e-3c6e-41ab-bb1b-948972cf65d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
            "compositeImage": {
                "id": "a1d4fa9f-436f-4ef2-91fe-41e8f5df6ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2821147e-3c6e-41ab-bb1b-948972cf65d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16841d65-83e9-4f3c-90e1-1421e76ab5ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2821147e-3c6e-41ab-bb1b-948972cf65d8",
                    "LayerId": "1f8c820f-ec35-46f6-852e-edf41156fcdf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1f8c820f-ec35-46f6-852e-edf41156fcdf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}