{
    "id": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cf37418-d1d0-41f7-9426-43abedf6559d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "compositeImage": {
                "id": "e1f83446-3ed8-4c25-adf0-e3352ecc4362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cf37418-d1d0-41f7-9426-43abedf6559d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d3e260-a8df-402d-98b2-03aa84b413c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cf37418-d1d0-41f7-9426-43abedf6559d",
                    "LayerId": "ad235da0-abac-486e-8a42-eecc1876436f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ad235da0-abac-486e-8a42-eecc1876436f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}