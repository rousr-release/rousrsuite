///@function sr_skyline_pack_rect(_skyline_bin, _min_x, _min_y, _max_x, _max_y)
///@desc pack a rect into a bin, returns an index for the rectangle in the bin or -1 if unable to add
///@param {Array:ERousrSkyline} _skyline_bin
///@param {Real} _min_x 
///@param {Real} _min_y
///@param {Real} _max_x
///@param {Real} _max_y
///@returns {Real} index of packed rect or -1
var _skyline = argument[0],
    _min_x   = argument[1],
		_min_y   = argument[2],
		_max_x   = argument[3],
		_max_y   = argument[4];
		
var rect_index = -1;



return rect_index;