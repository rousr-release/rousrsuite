///@function sr_skyline_get_packed_rect(_skyline_bin, _rect_id)
///@desc return a packed rect or undefined if not found
///@param {Array:ERousrSkyline} _skyline_bin   bin packer containing the rect
///@param {Real} _rect_id                      id of the packed rect previously added to `_skyline_bin`
///@returns {Real} index of packed rect or -1
var _skyline = argument[0],
    _rect    = argument[1];

var rect_index = -1;



return rect_index;