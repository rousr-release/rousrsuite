///@function sr_skyline_destroy(_skyline_bin)
///@desc destroy a skyline bin packer
///@param {Array:ERousrSkyline} _skyline_bin
///@returns {undefined} 
var _skyline_bin = argument[0];

// nothing to do, really.

return undefined;