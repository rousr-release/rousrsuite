///@function sr_skyline_create(_width, _height, _use_waste_map)
///@desc create a new skyline bin packer
///      ported from: [juj/RectangleBinpack](https://github.com/juj/RectangleBinPack)
///@param {Real} [_width=256]                maximum size to allow skyline to grow
///@param {Real} [_height=256]               maximum size to allow skyline to grow
///@param {Boolean} [_use_waste_map=false]   alternative version, using a waste map to densely pack rects
///@returns {Array:ERousrSkyline} skyline binpacker

enum ERousrSkyline {
	
	Num,
};

var new_packer = array_create(ERousrSkyline.Num);

return new_packer;