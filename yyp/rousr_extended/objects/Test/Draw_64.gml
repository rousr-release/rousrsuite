shader_set(shdr_pretty);
var _res = shader_get_uniform(shdr_pretty, "u_vResolution");
var _time = shader_get_uniform(shdr_pretty, "u_fTime");
shader_set_uniform_f_array(_res, [display_get_gui_width(), display_get_gui_height()]);
shader_set_uniform_f(_time, get_timer()/1000000);
draw_surface_stretched(application_surface, 0, 0, display_get_gui_width(), display_get_gui_height());

shader_reset();