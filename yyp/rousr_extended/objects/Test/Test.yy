{
    "id": "57012ad0-729e-469e-ba6c-9a3b698b70ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Test",
    "eventList": [
        {
            "id": "80827bcb-30a5-4e2a-90fb-941b08ddcde3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "57012ad0-729e-469e-ba6c-9a3b698b70ab"
        },
        {
            "id": "77cfc5e3-eba2-40d5-ac75-5a991355416f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "57012ad0-729e-469e-ba6c-9a3b698b70ab"
        },
        {
            "id": "bda8a943-6f1c-4132-baeb-93a587021337",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57012ad0-729e-469e-ba6c-9a3b698b70ab"
        },
        {
            "id": "349ca5e2-6e5d-4348-9691-6c35ac3dc5b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "57012ad0-729e-469e-ba6c-9a3b698b70ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "e843e9da-7df0-4e73-b674-b368443515ec",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "5.1",
            "varName": "variable_name",
            "varType": 0
        },
        {
            "id": "6569849b-edff-4d7a-a1fe-54c3ae3a73e8",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "variable_name1",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
    "visible": true
}