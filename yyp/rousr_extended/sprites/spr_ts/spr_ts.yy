{
    "id": "5aabbf5f-6ad8-4624-9034-06acf6bd55d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa935464-d961-4376-94a5-52695aca2455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aabbf5f-6ad8-4624-9034-06acf6bd55d2",
            "compositeImage": {
                "id": "13d821e6-77e1-42e2-8d21-d4945dee9080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa935464-d961-4376-94a5-52695aca2455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ce6ed4e-ffe4-4627-8efa-e8c80f619bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa935464-d961-4376-94a5-52695aca2455",
                    "LayerId": "0ac3ec2e-bac6-40c9-987d-e115b5e2f5c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0ac3ec2e-bac6-40c9-987d-e115b5e2f5c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aabbf5f-6ad8-4624-9034-06acf6bd55d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}