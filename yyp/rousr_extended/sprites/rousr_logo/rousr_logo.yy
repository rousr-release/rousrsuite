{
    "id": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rousr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 51,
    "bbox_right": 99,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38e96bbe-b8cd-4a80-b072-a2bf0265c7cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
            "compositeImage": {
                "id": "0c0018a0-0085-4729-abe4-fb3d8dbf7a4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38e96bbe-b8cd-4a80-b072-a2bf0265c7cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1ef4302-e8b4-4792-9d4b-f217678159df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38e96bbe-b8cd-4a80-b072-a2bf0265c7cc",
                    "LayerId": "7a6bc6fa-39c2-40cd-bff4-af05941ffc2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "7a6bc6fa-39c2-40cd-bff4-af05941ffc2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 36,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}