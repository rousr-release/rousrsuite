//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;                  // (x,y,z)
//attribute vec3 in_Normal;                  // (x,y,z)     unused in this shader.
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord0;  // (u,v)
attribute vec2 in_TextureCoord1;  // (u,v)
attribute vec2 in_TextureCoord2;  // (u,v)


varying vec4 v_vColour;
varying vec3 v_vPosition;
varying vec2 v_vQuadPosition;
varying vec2 v_vQuadSize;
varying float v_fBoxRadius;
varying float v_fCircleRadius;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vColour = in_Colour;
	v_vPosition = in_Position;
	v_vQuadPosition = in_TextureCoord0;
	v_vQuadSize = in_TextureCoord1;
	v_fBoxRadius = in_TextureCoord2.x;
	v_fCircleRadius = in_TextureCoord2.y;
}
