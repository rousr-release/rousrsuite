//
// Simple passthrough fragment shader
//
varying vec4 v_vColour;
varying vec3 v_vPosition;
varying vec2 v_vQuadPosition;
varying vec2 v_vQuadSize;
varying float v_fBoxRadius;
varying float v_fCircleRadius;


float udRoundBox( vec2 p, vec2 b, float r )
{
    return length(max(abs(p)-b+r,0.0))-r;
}



void main()
{
    vec2 halfRes = 0.5 * v_vQuadSize.xy;
	vec4 c = v_vColour.rgba;
	
	if(v_fBoxRadius > 0.0){
		// compute box 
		float b = udRoundBox(v_vPosition.xy - halfRes - v_vQuadPosition, halfRes, v_fBoxRadius);
    
		// colorize (red / black )
		c = vec4(mix( c.rgb, vec3(0.0,0.0,0.0), smoothstep(0.0,1.0,b) ), v_vColour.a);
	}
	
	if(v_fCircleRadius > 0.0){
		
		float radius = 0.24 * v_vQuadSize.y;
	
		// Circle
		float d = length(v_vPosition.xy - v_vQuadSize * 0.25 - v_vQuadPosition) - radius;
		float t = clamp(d, 0.0, 1.0);
		vec4 layer2 = vec4(v_vColour.rgb, t);
		c = mix(v_vColour, vec4(0.0, 0.0, 0.0, 0.0), layer2.a);
	}
	gl_FragColor = c;
}
