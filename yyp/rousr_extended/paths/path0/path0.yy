{
    "id": "89643c27-a170-450c-ba65-266bbb0cb9d8",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path0",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "31bd409e-bf54-4b3a-a8bf-832cdf56f65e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 164.6875,
            "y": 267.125,
            "speed": 100
        },
        {
            "id": "4e80823c-548c-4de4-befc-bf5d0f546b22",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 256,
            "speed": 100
        },
        {
            "id": "927fdc20-8fe4-455c-8ad3-4e81c8470ec6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 128,
            "speed": 100
        },
        {
            "id": "7aac0a4e-7c91-4dfc-bf03-c854aa28de59",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 271.6875,
            "y": 73.125,
            "speed": 100
        },
        {
            "id": "627956df-4a3e-4c60-a8d5-881b526e78b0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 80.6875,
            "y": 42.125,
            "speed": 100
        },
        {
            "id": "3013715a-5b42-4597-8dd6-325281e21bd1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 54.6875,
            "y": 164.125,
            "speed": 100
        },
        {
            "id": "40c0917d-726a-4bfd-971f-8cfb2eac9aa0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 70.6875,
            "y": 308.125,
            "speed": 100
        },
        {
            "id": "fb481c8c-1233-482e-b3f3-a68849f3334c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 270.6875,
            "y": 314.125,
            "speed": 100
        },
        {
            "id": "2c8357f6-334c-41fd-94f8-cc20b02ca224",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 421.6875,
            "y": 210.125,
            "speed": 100
        },
        {
            "id": "49298bb5-c538-4601-a007-4830d7cfb311",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 224,
            "speed": 100
        },
        {
            "id": "b42494bc-54ca-4c43-83f6-0a773531417b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 160,
            "speed": 100
        },
        {
            "id": "6c3b023f-25a0-4d28-8037-b8e2f348d067",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 192,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}