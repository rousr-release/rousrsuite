///@function sr_drawlist_push_rectangle(_x1, _y1, _x2, _y2, [_rounding=0], [_bordered=true], [_color=c_white], [_alpha=1])
///@desc Pushes a line to the drawlist
///@param {Real} _x1
///@param {Real} _y1
///@param {Real} _x2
///@param {Real} _y2
///@param {Real} [_rounding=0.0]
///@param {Real} [_bordered=true]
///@param {Real} [_color=c_white]
///@param {Real} [_alpha=1]
__sr_drawlist_ensure();

var _elem = __RousrDrawlistElement;
_elem[@ ERousrDrawlist_Element.X1] = argument[0];
_elem[@ ERousrDrawlist_Element.Y1] = argument[1];
_elem[@ ERousrDrawlist_Element.X2] = argument[2];
_elem[@ ERousrDrawlist_Element.Y2] = argument[3];
_elem[@ ERousrDrawlist_Element.Rounding] = (argument_count > 4) ? argument[4] : 0;
_elem[@ ERousrDrawlist_Element.Bordered] = (argument_count > 5) ? argument[5] : true;
_elem[@ ERousrDrawlist_Element.Colour] = (argument_count > 6) ? argument[6] : c_white;
_elem[@ ERousrDrawlist_Element.Alpha] = (argument_count > 7) ? argument[7] : 1;
_elem[@ ERousrDrawlist_Element.Type] = ERousrDrawlist_Type.Rectangle;
__sr_drawlist_push(ERousrDrawlist_Instruction.AddPrimitive, _elem);

