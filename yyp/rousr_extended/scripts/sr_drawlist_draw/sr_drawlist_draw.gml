///@function sr_drawlist_draw()
///@desc Draws the drawlist. This is handled automatically at the drawlists depth unless you disabled default drawin with `sr_drawlist_draw_enable`
/*
if(!instance_exists(__RousrDrawlist)){
	exit;	
}
*/

var _stack = __RousrDrawlist.stack,
	_buffer = -1,
	_format = __RousrDrawlist.vertex_format,
	_depth_stack = sr_array_create(),
	_depth = depth,
	_draw = false;

/*
if(sr_array_size(_stack) == 0){
	with __RousrDrawlist instance_destroy();	
}
*/

var _i =0;
repeat(sr_array_size(_stack)){
	var _pckg = sr_array_pop_back(_stack),
		_type = _pckg[0],
		_value = _pckg[1];
	
	switch(_type){
		case ERousrDrawlist_Instruction.PushDepth:
			sr_array_push_front(_depth_stack, _depth)
			_depth = _value;
		break;
		case ERousrDrawlist_Instruction.PopDepth:
			repeat(_value){
				_depth = sr_array_pop_front(_depth_stack);
			}
		break;
		case ERousrDrawlist_Instruction.AddPrimitive:
			if(!_draw){
				_draw = true;
				_buffer = vertex_create_buffer();
				vertex_begin(_buffer, _format);
			}
			var _x1 = _value[ERousrDrawlist_Element.X1],
				_y1 = _value[ERousrDrawlist_Element.Y1],
				_x2 = _value[ERousrDrawlist_Element.X2],
				_y2 = _value[ERousrDrawlist_Element.Y2],
				_width = _x2 - _x1,
				_height = _y2 - _y1,
				_length = sqrt(_width * _width + _height * _height),
				_colour = _value[ERousrDrawlist_Element.Colour],
				_alpha = _value[ERousrDrawlist_Element.Alpha],
				_rounding = _value[ERousrDrawlist_Element.Rounding],
				_box_radius = (_rounding != 360) ? (_length/2) * _rounding : 0,
				_circle_radius = (_rounding == 360) ? 1 : 0;
				
			
			switch(_value[ERousrDrawlist_Element.Type]){
				case ERousrDrawlist_Type.Rectangle:
					vertex_position_3d(_buffer, _x1, _y1, _depth);
					vertex_color(_buffer, _colour, _alpha);
					vertex_float2(_buffer, _x1, _y1);
					vertex_float2(_buffer, _width, _height);
					vertex_float2(_buffer, _box_radius, _circle_radius);
					
					vertex_position_3d(_buffer, _x2, _y1, _depth);
					vertex_color(_buffer, _colour, _alpha);
					vertex_float2(_buffer, _x1, _y1);
					vertex_float2(_buffer, _width, _height);
					vertex_float2(_buffer, _box_radius, _circle_radius);
					
					vertex_position_3d(_buffer, _x1, _y2, _depth);
					vertex_color(_buffer, _colour, _alpha); 
					vertex_float2(_buffer, _x1, _y1);
					vertex_float2(_buffer, _width, _height);
					vertex_float2(_buffer, _box_radius, _circle_radius);
					
					vertex_position_3d(_buffer, _x1, _y2, _depth);
					vertex_color(_buffer, _colour, _alpha); 
					vertex_float2(_buffer, _x1, _y1);
					vertex_float2(_buffer, _width, _height);
					vertex_float2(_buffer, _box_radius, _circle_radius);
					
					vertex_position_3d(_buffer, _x2, _y1, _depth);
					vertex_color(_buffer, _colour, _alpha); 
					vertex_float2(_buffer, _x1, _y1);
					vertex_float2(_buffer, _width, _height);
					vertex_float2(_buffer, _box_radius, _circle_radius);
					
					vertex_position_3d(_buffer, _x2, _y2, _depth);
					vertex_color(_buffer, _colour, _alpha); 
					vertex_float2(_buffer, _x1, _y1);
					vertex_float2(_buffer, _width, _height);
					vertex_float2(_buffer, _box_radius, _circle_radius);
				break;
				case ERousrDrawlist_Type.Line:
					var _dx = (_x2 - _x1),
						_dy = (_y2 - _y1),
						_len = sqrt(_dx * _dx + _dy * _dy),
						_thick = _value[ERousrDrawlist_Element.Thickness];

					_dx/=_len;
					_dy/=_len;

					var _c1x = _x1,
						_c1y = _y1,
						_c2x = _x1 + _dx * _len,
						_c2y = _y1 + _dy * _len,
						_c3x = _x1 - _dy * _thick,
						_c3y = _y1 + _dx * _thick,
						_c4x = _x2 - _dy * _thick,
						_c4y = _y2 + _dx * _thick;

					
					vertex_position_3d(_buffer, _c1x, _c1y, _depth);
					vertex_color(_buffer, _colour, _alpha);
					
					vertex_position_3d(_buffer, _c2x, _c2y, _depth);
					vertex_color(_buffer, _colour, _alpha);
					
					vertex_position_3d(_buffer, _c3x, _c3y, _depth);
					vertex_color(_buffer, _colour, _alpha); 
					
					vertex_position_3d(_buffer, _c3x, _c3y, _depth);
					vertex_color(_buffer, _colour, _alpha); 
					
					vertex_position_3d(_buffer, _c2x, _c2y, _depth);
					vertex_color(_buffer, _colour, _alpha); 
					
					vertex_position_3d(_buffer, _c4x, _c4y, _depth);
					vertex_color(_buffer, _colour, _alpha); 
				break;
			}
		break;
	}
		
}

sr_array_clear(_stack);

if(_draw){
	var _res = shader_get_uniform(shdr_round, "u_vResolution");
	var _time = shader_get_uniform(shdr_round, "u_fTime");

	shader_set(shdr_round);
	shader_set_uniform_f_array(_res,[300, 300]);
	shader_set_uniform_f(_time, 300);
	vertex_end(_buffer);
	vertex_submit(_buffer, pr_trianglelist, -1);	
	vertex_delete_buffer(_buffer);
	shader_reset();
}

/*
if(sr_stack_array_empty(_stack)){
	with __RousrDrawlist instance_destroy();	
	exit;
}

while(!sr_stack_array_empty(_stack)){
	var _elem = sr_stack_array_pop(_stack);
	draw_set_colour(_elem[@ ERousrDrawlist_Element.Colour]);
	draw_set_alpha(_elem[@ ERousrDrawlist_Element.Alpha]);
	switch(_elem[ERousrDrawlist_Element.Type]){
		case ERousrDrawlist_Type.Line:
			draw_line(
				_elem[ERousrDrawlist_Element.X1],
				_elem[ERousrDrawlist_Element.Y1],
				_elem[ERousrDrawlist_Element.X2],
				_elem[ERousrDrawlist_Element.Y2]
			);
		break;
		case ERousrDrawlist_Type.Rectangle:
			draw_rectangle(
				_elem[ERousrDrawlist_Element.X1],
				_elem[ERousrDrawlist_Element.Y1],
				_elem[ERousrDrawlist_Element.X2],
				_elem[ERousrDrawlist_Element.Y2],
				_elem[ERousrDrawlist_Element.Bordered]
			);
		break;
		case ERousrDrawlist_Type.Circle:
			draw_circle(
				_elem[ERousrDrawlist_Element.X1],
				_elem[ERousrDrawlist_Element.Y1],
				_elem[ERousrDrawlist_Element.Radius],
				_elem[ERousrDrawlist_Element.Bordered]
			);
		break;
		case ERousrDrawlist_Type.Text:
			draw_set_halign(_elem[ERousrDrawlist_Element.HAlign]);
			draw_set_valign(_elem[ERousrDrawlist_Element.VAlign]);
			if(_elem[ERousrDrawlist_Element.Font] != undefined){
				draw_set_font(_elem[ERousrDrawlist_Element.Font]);
			}
			draw_text(
				_elem[ERousrDrawlist_Element.X1],
				_elem[ERousrDrawlist_Element.Y1],
				_elem[ERousrDrawlist_Element.Text]
			);	
		break;
		case ERousrDrawlist_Type.Sprite:
			draw_sprite_general(
				_elem[ERousrDrawlist_Element.SpriteIndex],
				_elem[ERousrDrawlist_Element.ImageIndex],
				_elem[ERousrDrawlist_Element.Left],
				_elem[ERousrDrawlist_Element.Top],
				_elem[ERousrDrawlist_Element.Width],
				_elem[ERousrDrawlist_Element.Height],
				_elem[ERousrDrawlist_Element.X1],
				_elem[ERousrDrawlist_Element.Y1],
				_elem[ERousrDrawlist_Element.ImageXScale],
				_elem[ERousrDrawlist_Element.ImageYScale],
				_elem[ERousrDrawlist_Element.Rotation],
				_elem[ERousrDrawlist_Element.Colour],
				_elem[ERousrDrawlist_Element.Colour],
				_elem[ERousrDrawlist_Element.Colour],
				_elem[ERousrDrawlist_Element.Colour],
				_elem[ERousrDrawlist_Element.Alpha]
			);
		break;
		case ERousrDrawlist_Type.Grid:
			var _cell_x = _elem[ERousrDrawlist_Element.Width],
				_cell_y = _elem[ERousrDrawlist_Element.Height],
				_width = _elem[ERousrDrawlist_Element.X2] - _elem[ERousrDrawlist_Element.X1],
				_height = _elem[ERousrDrawlist_Element.Y2] - _elem[ERousrDrawlist_Element.Y1],
				_cols = ceil(_width / _cell_x),
				_rows = ceil(_height / _cell_y),
				_x1 = _elem[ERousrDrawlist_Element.X1],
				_y1 = _elem[ERousrDrawlist_Element.Y1],
				_x2 = _x1 + _cols * _cell_x,
				_y2 = _y1 + _rows * _cell_y;
			
			var _yy = _y1;
			repeat(_rows+1){
				draw_line(_x1, _yy, _x2, _yy);
				_yy += _cell_y;
			}
			
			var _xx = _x1;
			repeat(_cols+1){
				draw_line(_xx, _y1, _xx, _y2);
				_xx += _cell_x;
			}
		break;
	}
}

draw_set_colour(c_white);
draw_set_alpha(1);
