///@function sr_drawlist_push_depth(_depth)
///@desc Pushes a depth change to the drawlist 
///@param {Real} _depth
var _depth = argument0;
__sr_drawlist_push(ERousrDrawlist_Instruction.PushDepth, _depth);
