///@function sr_drawlist_pop_depth([_count=1])
///@desc Pops depth `_count` times
///@param {Real} [_count=1]
var _count = (argument_count > 0 ) ? argument[0] : 1;
__sr_drawlist_push(ERousrDrawlist_Instruction.PopDepth, _count);
