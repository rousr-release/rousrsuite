///@function __sr_drawlist_ev_create()
///@extensionizer { "docs" : false } 
__RousrDrawlist = id;
stack = sr_array_create();
draw_enabled = __RousrDrawlistDraw;
depth = __RousrDrawlistDepth;
vertex_format_begin();
vertex_format_add_position_3d();
vertex_format_add_colour();
vertex_format_add_custom(vertex_type_float2, vertex_usage_texcoord);
vertex_format_add_custom(vertex_type_float2, vertex_usage_texcoord);
vertex_format_add_custom(vertex_type_float2, vertex_usage_texcoord);
vertex_format = vertex_format_end();
