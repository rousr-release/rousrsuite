///@function rousr_drawlist_definitions
///@extensionizer { "docs" : false, "export": false } 
gml_pragma("global", "__RousrDrawlistDepth = 0;__RousrDrawlistDraw = true;");
#macro __RousrDrawlist global.__rousr_draw_list
#macro __RousrDrawlistDepth  global.__rousr_drawlist_depth
#macro __RousrDrawlistDraw global.__rousr_drawlist_draw
#macro __RousrDrawlistElement array_create(ERousrDrawlist_Element.Num, undefined)


enum ERousrDrawlist_Type{
	Line,
	Circle,
	Rectangle,
	Text,
	Sprite,
	Grid
}

enum ERousrDrawlist_Instruction{
	AddPrimitive,
	AddSprite,
	PushDepth,
	PopDepth
}

enum ERousrDrawlist_Element{
	Type,
	X1,
	Y1,
	X2,
	Y2,
	Colour,
	Alpha,
	Radius,
	Width,
	Height,
	Bordered,
	HAlign,
	VAlign,
	Font,
	Text,
	ImageXScale,
	ImageYScale,
	Top,
	Left,
	SpriteIndex,
	ImageIndex,
	Rotation,
	Thickness,
	Rounding,
	Num
}


__RousrDrawlistDepth = 0;
