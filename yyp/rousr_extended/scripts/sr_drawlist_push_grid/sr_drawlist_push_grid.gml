///@function sr_drawlist_push_grid(_x1, _y1, _x2, _y2, _cell_size_x, _cell_size_y, [_color=c_white], [_alpha=1])
///@desc Pushes a grid to the drawlist
///@param {Real} _x1
///@param {Real} _y1
///@param {Real} _x2
///@param {Real} _y2
///@param {Real} _cell_size_x
///@param {Real} _cell_size_y
///@param {Real} [_color=c_white]
///@param {Real} [_alpha=1]
__sr_drawlist_ensure();

var _elem = __RousrDrawlistElement;
_elem[@ ERousrDrawlist_Element.X1] = argument[0];
_elem[@ ERousrDrawlist_Element.Y1] = argument[1];
_elem[@ ERousrDrawlist_Element.X2] = argument[2];
_elem[@ ERousrDrawlist_Element.Y2] = argument[3];
_elem[@ ERousrDrawlist_Element.Width] = argument[4];
_elem[@ ERousrDrawlist_Element.Height] = argument[5];
_elem[@ ERousrDrawlist_Element.Colour] = (argument_count > 6) ? argument[6] : c_white;
_elem[@ ERousrDrawlist_Element.Alpha] = (argument_count > 7) ? argument[7] : 1;
_elem[@ ERousrDrawlist_Element.Type] = ERousrDrawlist_Type.Grid;
sr_stack_array_push(__RousrDrawlist.stack, _elem);
