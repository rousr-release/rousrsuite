///@function sr_drawlist_push_circle(_x, _y, _radius, [_bordered=true], [_colour=c_white], [_alpha=1])
///@desc Pushes a circle to the drawlist
///@param {Real} _x
///@param {Real} _y
///@param {Real} _radius
///@param {Real} [_bordered=false]
///@param {Real} [_colour=c_white]
///@param {Real} [_alpha=1]
__sr_drawlist_ensure();
var _x = argument[0],
	_y = argument[1],
	_radius = argument[2],
	_bordered = (argument_count > 3) ? argument[3] : false,
	_colour = (argument_count > 4) ? argument[4] : c_white,
	_alpha = (argument_count > 5) ? argument[5] : 1,
	_x1 = _x,
	_y1 = _y,
	_x2 = _x1 + _radius * 2,
	_y2 = _y1 + _radius * 2;

var _elem = __RousrDrawlistElement;
_elem[@ ERousrDrawlist_Element.X1] = _x1;
_elem[@ ERousrDrawlist_Element.Y1] = _y1;
_elem[@ ERousrDrawlist_Element.X2] = _x2;
_elem[@ ERousrDrawlist_Element.Y2] = _y2;
_elem[@ ERousrDrawlist_Element.Rounding] = 360;
_elem[@ ERousrDrawlist_Element.Bordered] = _bordered;
_elem[@ ERousrDrawlist_Element.Colour] = _colour;
_elem[@ ERousrDrawlist_Element.Alpha] = _alpha;
_elem[@ ERousrDrawlist_Element.Type] = ERousrDrawlist_Type.Rectangle;
__sr_drawlist_push(ERousrDrawlist_Instruction.AddPrimitive, _elem);

/*
var _elem = __RousrDrawlistElement;
_elem[@ ERousrDrawlist_Element.X1] = argument[0];
_elem[@ ERousrDrawlist_Element.Y1] = argument[1];
_elem[@ ERousrDrawlist_Element.Radius] = argument[2];
_elem[@ ERousrDrawlist_Element.Bordered] = (argument_count > 3) ? argument[3] : true;
_elem[@ ERousrDrawlist_Element.Colour] = (argument_count > 4) ? argument[4] : c_white;
_elem[@ ERousrDrawlist_Element.Alpha] = (argument_count > 5) ? argument[5] : 1;
_elem[@ ERousrDrawlist_Element.Type] = ERousrDrawlist_Type.Circle;
sr_stack_array_push(__RousrDrawlist.stack, _elem);
