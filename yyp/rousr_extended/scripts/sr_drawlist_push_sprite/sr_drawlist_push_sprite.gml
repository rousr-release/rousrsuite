///@function sr_drawlist_push_sprite(_x, _y, _sprite_index, _image_index, [_rotation=0], [_colour=c_white], [_alpha=1])
///@desc Pushes a sprite to the drawlist
///@param {Real} _x
///@param {Real} _y
///@param {Real} _sprite_index
///@param {Real} _image_index
///@param {Real} [_rotation=0]
///@param {Real} [_colour=c_white]
///@param {Real} [_alpha=1]
__sr_drawlist_ensure();

var _elem = __RousrDrawlistElement;
_elem[@ ERousrDrawlist_Element.X1] = argument[0];
_elem[@ ERousrDrawlist_Element.Y1] = argument[1];
_elem[@ ERousrDrawlist_Element.SpriteIndex] = argument[2];
_elem[@ ERousrDrawlist_Element.ImageIndex] = argument[3];
_elem[@ ERousrDrawlist_Element.Rotation] = (argument_count > 4) ? argument[4] : 0;
_elem[@ ERousrDrawlist_Element.Colour] = (argument_count > 5) ? argument[5] : c_white;
_elem[@ ERousrDrawlist_Element.Alpha] = (argument_count > 6) ? argument[6] : 1;
_elem[@ ERousrDrawlist_Element.Top] = 0;
_elem[@ ERousrDrawlist_Element.Left] = 0;
_elem[@ ERousrDrawlist_Element.Width] = sprite_get_width(_elem[ERousrDrawlist_Element.SpriteIndex]);
_elem[@ ERousrDrawlist_Element.Height] = sprite_get_height(_elem[ERousrDrawlist_Element.SpriteIndex]);
_elem[@ ERousrDrawlist_Element.ImageXScale] = 1;
_elem[@ ERousrDrawlist_Element.ImageYScale] = 1;
_elem[@ ERousrDrawlist_Element.Type] = ERousrDrawlist_Type.Sprite;
sr_stack_array_push(__RousrDrawlist.stack, _elem);
