///@function sr_drawlist_push_sprite_part(_x, _y, _sprite_index, _image_index, _source_left, _source_top, _source_width, _source_height, [_rotation=0], [_colour=c_white], [_alpha=1])
///@desc Pushes a sprite to the drawlist
///@param {Real} _x
///@param {Real} _y
///@param {Real} _sprite_index
///@param {Real} _image_index
///@param {Real} _source_left
///@param {Real} _source_top
///@param {Real} _source_width
///@param {Real} _source_height
///@param {Real} [_rotation=0]
///@param {Real} [_colour=c_white]
///@param {Real} [_alpha=1]
__sr_drawlist_ensure();

var _elem = __RousrDrawlistElement;
_elem[@ ERousrDrawlist_Element.X1] = argument[0];
_elem[@ ERousrDrawlist_Element.Y1] = argument[1];
_elem[@ ERousrDrawlist_Element.SpriteIndex] = argument[2];
_elem[@ ERousrDrawlist_Element.ImageIndex] = argument[3];
_elem[@ ERousrDrawlist_Element.Left] = argument[4];
_elem[@ ERousrDrawlist_Element.Top] = argument[5];
_elem[@ ERousrDrawlist_Element.Width] = argument[6];
_elem[@ ERousrDrawlist_Element.Height] = argument[7];
_elem[@ ERousrDrawlist_Element.ImageXScale] = 1;
_elem[@ ERousrDrawlist_Element.ImageYScale] = 1;
_elem[@ ERousrDrawlist_Element.Rotation] = (argument_count > 8) ? argument[8] : 0;
_elem[@ ERousrDrawlist_Element.Colour] = (argument_count > 9) ? argument[9] : c_white;
_elem[@ ERousrDrawlist_Element.Alpha] = (argument_count > 10) ? argument[10] : 1;
_elem[@ ERousrDrawlist_Element.Type] = ERousrDrawlist_Type.Sprite;
sr_stack_array_push(__RousrDrawlist.stack, _elem);