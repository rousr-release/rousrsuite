///@function sr_drawlist_push_text(_x, _y, _text, [_font=undefined], [_halign=fa_left], [_valign=fa_top], [_colour=c_white], [_alpha=1])
///@desc Pushes text to the drawlist
///@param {Real} _x
///@param {Real} _y
///@param {String} _text
///@param {Real} [_font=undefined]
///@param {Real} [_halign=undefined]
///@param {Real} [_valign=undefined]
///@param {Real} [_colour=c_white]
///@param {Real} [_alpha=1]
__sr_drawlist_ensure();

var _elem = __RousrDrawlistElement;
_elem[@ ERousrDrawlist_Element.X1] = argument[0];
_elem[@ ERousrDrawlist_Element.Y1] = argument[1];
_elem[@ ERousrDrawlist_Element.Text] = argument[2];
_elem[@ ERousrDrawlist_Element.Font] = (argument_count > 3) ? argument[3] : undefined;
_elem[@ ERousrDrawlist_Element.HAlign] = (argument_count > 4) ? argument[4] : fa_left;
_elem[@ ERousrDrawlist_Element.VAlign] = (argument_count > 5) ? argument[5] : fa_top;
_elem[@ ERousrDrawlist_Element.Colour] = (argument_count > 6) ? argument[6] : c_white;
_elem[@ ERousrDrawlist_Element.Alpha] = (argument_count > 7) ? argument[7] : 1;
_elem[@ ERousrDrawlist_Element.Type] = ERousrDrawlist_Type.Text;
sr_stack_array_push(__RousrDrawlist.stack, _elem);
