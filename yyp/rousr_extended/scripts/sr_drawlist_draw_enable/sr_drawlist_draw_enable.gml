///@function sr_drawlist_draw_enable(enable)
///@desc Enables or disables drawing of the drawlist. Disable drawing and call sr_drawlist_draw to manually draw the drawlist
///@param {Boolean} enable 
__sr_drawlist_ensure();
__RousrDrawlistDraw = argument0;
with(__RousrDrawlist){
	draw_enabled = __RousrDrawlistDraw;
}