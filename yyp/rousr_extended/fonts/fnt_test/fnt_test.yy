{
    "id": "ba5c9364-76a2-4e3a-873c-0a76f7bad0e6",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_test",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3ed220b2-6093-4f4a-8c63-1fcfe94a91ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "49224829-1279-4d39-9e3f-8fa85b8f29e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 166,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "be7e6c6c-1830-4299-a6c4-dfdab7ff991e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "780e2d44-134c-4c91-8a39-98b7211685fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6478cd13-0448-4373-b1cb-8e46d0066ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9b87efff-d668-4cf2-8714-a0299fa4782d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9e22a53c-f8f0-42f3-a385-d90ed4210898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c61480f0-5785-47b8-85cc-de00403d6af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "579c3a6c-4d3c-4904-bffe-c3156ad608d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7bb57923-b997-4ca2-a6e0-32192bda18ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7d70109a-af49-419c-ab63-adbed63d1959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "55f586ec-af45-4e55-a602-d721a4429f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bf319b66-d012-4262-a17e-03e745e229b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 151,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d25ffd4f-8302-46b2-b671-09bb02035b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a830d7b4-0917-43dc-9325-c8ef2860e267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 156,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6a80bda8-1268-4a85-abfd-c2d2fa5cb4df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "890322c9-9b6c-452c-b3e7-1f6f95a50eb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 185,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cc90cc5f-baca-44d6-85c8-caf8b4140301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ebe36aa7-3b6e-4f11-b201-fbe6aca79cd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 196,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d5a06be2-843a-493e-82b4-183cdb7c40a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 207,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "edabe795-68f8-4187-ab70-1dd513376a23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "02c93228-0dba-4e5e-be9a-da63bd962480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4697d921-6a6e-4fe0-9f70-a7a7e05c02dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4584df5a-a3aa-4c58-b3c1-e9a6286c4105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "656c1f51-f126-4078-9651-d0a1537ce33d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bfe84ced-8c9f-41b9-bcff-680fa8ec8101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cfdf205f-6897-405f-9b77-12d24715ed3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4c3e873a-7227-4804-8af7-51bef59ac022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8cc75983-5cd9-4d7d-bfa0-2909346bdf3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7912bc1a-da16-42e8-bb41-667d80247757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6d686a76-e2f0-4df9-81d7-21de83147a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6bfb4ea8-d151-4363-966d-d341ee19d290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 22
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "eb126617-b76c-45a7-9858-3db2ab4e4b39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ae91cce4-2790-408a-81a9-6d25490c47cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8f1a4bf1-661c-494d-a8d3-fa4f88803a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 141,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a8908411-4bf0-481e-9bbb-dd10897ba899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "354a3324-924c-45c1-8642-147fc2f28758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a5751a97-25b7-4dc1-8815-5c5c3faea664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fee644a3-612f-4349-9fd0-b78fdeac2158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 22
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "338d4dc0-9490-4ca8-b7b7-ac18996eff60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0e9017a1-79b4-4acf-8539-4558287c05a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b61633c3-8324-419f-9873-b8d42f72e375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 171,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2c4b0bb3-102e-4466-8f00-11f85e3fd037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8c1cad02-3635-49b3-b826-0c547ccee09c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dff8bf15-dc24-45c2-a109-ad65df1d41db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fee64584-d986-4bb1-85f8-69d088e066bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8c312afc-47d8-455e-bb6d-90881fa2965e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7d31a3b5-0d0b-448a-928a-e1bb8892a3f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c78486f1-4c15-4e96-a305-4671fb1ad019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 152,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b74a150b-4635-4a3a-8cbb-d4b8ee1a0d1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "90a5d534-2b0d-48cb-8f6a-58cf381f0adf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ba14c387-9050-4caf-9369-165a08c798b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "700a20f5-a492-4852-a200-b0ab262831da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3b9acbed-8e16-4ceb-b5c0-caf6d21ae8de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4e0c95d1-fb32-4ac0-8028-74a804a6cdcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2c7fcb12-3b75-4668-95db-3df7c9ec6a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "57df4077-6488-4492-992f-cc0548c28572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5cd44417-0682-4452-9928-b20e53a2705c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "504c2101-b903-4fe6-8045-b7d6bfb6ceeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d53ed0fa-096d-4f7b-8b64-bad8cafb656d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 145,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bb444cb6-affc-4492-bcdf-f37e39462fa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5669b110-4e8d-477a-8895-3120d0ddb19b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 127,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "39aa03e0-3300-4d5b-87a0-e40b4ef0900c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 131,
                "y": 42
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "aa0a547a-a7ca-4dfe-8f4f-f86a3f8da098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e62cd285-8db7-4f56-ab2a-4d49e9c04c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 133,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9f06dd5b-a91d-4f8a-9e30-cf5cfd2be38a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "522bdeda-a952-440c-8997-17d33f49bc2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0166fb18-64fe-441b-a914-e1cdf061818c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 151,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ddf272c4-801a-49be-96f6-fca97e25fc9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 42
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a8142e32-6a7d-4858-8154-e1953e25f0d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 108,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "590618bf-a235-4678-9840-f4f857197764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c343399c-bd48-4d94-8438-37816ab6cdfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 161,
                "y": 42
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "aac98ec5-5e4e-4811-81bf-6934bd02d4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "012b21fc-cc49-4691-9819-544c6e2675a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 194,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1f8b1db9-956b-4f58-b886-764254663a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 139,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "34957f07-089c-4eb3-9d22-d7834f198e72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7a1803cb-d2ec-4082-8081-c5242fe5da31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "48a11b14-abc1-43a1-9667-b3ebcdc45c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d6e23476-63e2-4694-a8d0-45d9b7957c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0888823f-915e-4c21-9100-7ecede807d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0fc6aca9-e6a7-48bb-a8a8-79abe6c55203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 141,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5a991420-cc8b-4545-81e8-a1e56bc37214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 111,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "39ce3e41-bf85-4e46-83e1-1dbe4e6ad243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0c8676c6-57ad-4f43-83a7-565eb6c7459c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b6162428-e421-4516-bcac-e6326cceb729",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3a24d69f-25fe-4001-b250-702b2ca58645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "dc2384f1-2969-4f2c-86cc-350a9cd1b8cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 181,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "416d314c-623d-42c0-a213-de7de45a4f7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7c67a1c9-10d4-49db-ad5e-5ff00cdd4bc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7931395f-9840-4d02-9575-9ac7d1145847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "88208f8b-1c97-4c15-a0e6-05064ac497f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "37a9c286-1251-4804-adb4-c3beeb2ae7f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7af0552d-8319-4c9b-a194-00aa659d728b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 190,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0b2bbfcc-712d-4d4d-98fe-1ba70183cd4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5aa62ba2-8e74-48b6-a22c-7ee290596645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 163,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b003c1eb-188c-4cf4-9cd6-5b76cb7e020f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "97f5d27d-bbba-488c-aef3-67b5ca5209ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "dd530c48-aee5-4f28-8349-d0fce6b6a9d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "d5849183-aa29-460c-869e-1b564b85020c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "fe302687-46d2-4dd6-b0ac-a6f7f8c087d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "c780ccef-b0a5-4ab6-aed7-9c249fd4b7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "9627d41f-f5a7-44cc-803e-826d54554497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "9cb164a1-df33-4133-a56d-3487e28339da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "70b187d5-5e41-4168-b44b-f4ebc8e05198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "d520933e-60af-41fd-8f40-b2ebe9d13690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "ad321964-547e-4987-b5a6-ba5f02c331d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "d18cfb9f-edeb-4756-ba5f-9f010a767e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "a70ed059-4fa8-4a4a-8cfd-18834c45a2dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "bbf43527-3e2e-47a1-a7fd-c11cdf48f128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "963a8dc7-3a6c-4ad5-b5de-1ce130bcbe5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "39c5e43c-b889-4aee-8101-c17a5c6484a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "e9a09d96-7012-497c-9970-1f1230091e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "7164279f-93a6-42dc-8b5e-5987c0f12bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "9c5a3632-4f17-4ca7-ae39-49b36d102d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "38ac12e8-171d-4b9d-bd46-21f30e59190c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "13402b99-4d06-4fdf-89ff-52721db83664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "f5489e01-aa5c-49be-8fc8-408437516614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "3f253146-a28c-48f1-87a2-25be1fcb9c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "33913c5f-3be7-4438-86c7-b9bab33cab75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "6783cb8b-addf-4896-9ff5-1d3c4386f850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "48c92fa4-f335-4479-83ba-68d41b50baad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "d669e81f-efeb-4518-a089-e522a6c1f6f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "08ce8a1a-b94a-403f-a198-8947dd4b136e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "82ba332e-71e5-487d-b7ff-d10354690c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "d32e693f-b5c8-4c48-95c3-62486c03af41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "f7c66591-3ca8-4f5d-a5c9-9bf6b17fd9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "9c0b1f94-9a0d-4ca9-82db-0475ea05ee87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "e7583405-f12f-483c-ae2a-efe7fa84fcdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "3f032096-4a15-4789-8c61-9dc54174b2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "d527af06-16bf-4b1f-8635-0350f6cf2350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "b83de542-da29-40a9-9c30-9fea876ddbd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "25528c7c-06ef-4c7c-90ec-f8d0b88dff4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "8520ffe4-fc3b-4ab7-95cd-f50c8e2e6731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "82427bb8-126d-4ddc-a318-34a02ec6347a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "076b9521-469f-413d-b177-17faa7b3a7d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "7d509a3a-55ed-47ad-84dd-19e2033091c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "60dbd15f-9b97-4561-954d-d16ada1b7e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "2f7f4ff7-eb61-4e24-9a1c-e984b67c1154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "ea74a35e-39d8-492b-932b-50e3b6ef3021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "db53bfe0-69a5-46aa-98d2-6a434ce0f49d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "b8e666b7-4a83-46f2-94ef-581aa388e895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "81abbe0f-d705-4193-91df-6d0669cfc665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "e6fbed79-ae5c-4865-9dc0-e7cbc644d308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "2226865d-081f-4a9d-88da-2e4723ee66ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "11a30c65-f408-4e6e-92cc-d1589822e283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "af30e50a-5135-447d-bb15-def6a991e7fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "0b1da95a-0d9d-45d1-bc6b-be84e29cc33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "921a8002-77f4-4f6d-8214-14ce84733361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "5fdfb6b6-448d-4cc3-a4ff-b10206ba2405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "63a140dc-01a4-4c92-bd1d-5128fe3a2360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "434f8404-dcac-4cef-a72e-7509fda95cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "38bc2305-f7ec-489a-af24-053acab6390c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "9bc23878-0920-4398-88ef-90053b1801a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "1d8b4dfd-b0e6-44dc-8afa-210907ab31f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "dd2d95f9-62c5-49f9-bff2-7def3bed7fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "ab02487d-5bdc-420d-ba1c-ac695820b987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "7c5f93e7-343e-40fd-ad88-66bfe1407a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "c7aa523e-de35-4ff9-a91f-c23b647b868f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "9ee768c9-0201-4a0f-a4ce-5395d6b19eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "8cc03eeb-ec74-4ea1-b1d3-e17bfb8aaa3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "a3880f39-ef88-4da7-b787-9e5abc0dea24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "f7453a3e-ee1f-4009-a445-4dcec9a56ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "1dbc5771-411f-4a3a-91d7-641012e77bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "32b3996a-b0f4-40ba-8c34-1047676ba768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "81902443-9d22-43a3-b478-091a069a15e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "79a42a01-0f8a-4eb5-9d79-504b4b0bb579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}