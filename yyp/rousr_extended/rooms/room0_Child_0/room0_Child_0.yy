
{
    "name": "room0_Child_0",
    "id": "dcea94ff-243c-4901-8426-cee700594630",
    "creationCodeFile": "",
    "inheritCode": true,
    "inheritCreationOrder": true,
    "inheritLayers": true,
    "instanceCreationOrderIDs": [
        "6d2cbfcd-0df9-44b0-b853-20e54a9d6d93"
    ],
    "IsDnD": false,
    "layers": [
        {
            "name": "Folder_1",
            "id": "00edcbc3-ce94-4d06-932f-48a3fce8c486",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [
                {
                    "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "Instances",
                    "id": "68a0cd78-c6c2-4528-9f44-34004f678f34",
                    "depth": 100,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": true,
                    "inheritLayerSettings": true,
                    "inheritSubLayers": true,
                    "inheritVisibility": true,
                    "instances": [
{"name": "inst_650A21B6","id": "6d2cbfcd-0df9-44b0-b853-20e54a9d6d93","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": ".gml","ignore": false,"inheritCode": true,"inheritItemSettings": true,"IsDnD": false,"m_originalParentID": "eb8422c0-065f-4a87-a88a-8f3a6b6f5103","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_650A21B6","objId": "57012ad0-729e-469e-ba6c-9a3b698b70ab","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 192,"y": 96}
                    ],
                    "layers": [

                    ],
                    "m_parentID": "2351966e-7be4-4d9a-a3a8-30aa207eb54a",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRInstanceLayer",
                    "mvc": "1.0",
                    "userdefined_depth": false,
                    "visible": true
                },
                {
                    "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "Assets_2",
                    "id": "d9785fc3-3516-4681-9865-66cccf217254",
                    "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_4D7781B2","id": "0f796160-a1ce-4b6d-9b0c-d0f3f00c5f4e","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": true,"m_originalParentID": "4d4cafd2-0c72-456f-b712-8650171c8a07","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa","userdefined_animFPS": false,"x": 448,"y": 192},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_568C2EDC","id": "2c4c47ba-043a-425e-994f-f004501140ff","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": true,"m_originalParentID": "03aaaeee-44d2-455b-8688-53129310a3a2","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa","userdefined_animFPS": false,"x": 448,"y": -32},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_39D2AC2A","id": "0665e3fe-1a92-41a2-8714-9c148db29bdb","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": true,"m_originalParentID": "3cd3757b-20de-4bcf-99c2-edaa5fe8387e","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa","userdefined_animFPS": false,"x": 96,"y": 0},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_43AFB35E","id": "c6c6aba1-ef5e-46c3-bfb7-2d43faa1382b","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": true,"m_originalParentID": "b0e796af-ba8c-48e4-884f-6557917405e8","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa","userdefined_animFPS": false,"x": 128,"y": 224}
                    ],
                    "depth": 200,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": true,
                    "inheritLayerSettings": true,
                    "inheritSubLayers": true,
                    "inheritVisibility": true,
                    "layers": [

                    ],
                    "m_parentID": "8da0e53b-9b30-4f5f-b1c5-bc4d77c8c2d1",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRAssetLayer",
                    "mvc": "1.0",
                    "userdefined_depth": false,
                    "visible": true
                }
            ],
            "m_parentID": "0c91c079-9d5c-495b-999c-cdf791cc0c92",
            "m_serialiseFrozen": false,
            "modelName": "GMRLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Assets_1",
            "id": "5184bd93-4089-40b8-9277-5db30ac15587",
            "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_183700F","id": "bb979092-760b-407f-a03e-0808186bd9e5","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": true,"m_originalParentID": "1bf329db-b41c-4ddc-9cbb-92f5d19622a8","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa","userdefined_animFPS": false,"x": 512,"y": 64},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_77C35591","id": "7b4eb93c-d9d6-4320-afe2-5d58a451fbae","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": true,"m_originalParentID": "bcb4efcb-0a14-44c3-bc6b-7a72ac2292a4","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa","userdefined_animFPS": false,"x": 256,"y": 192},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_15B326F4","id": "a72e542a-bcae-4e4e-ab05-0292e2bc6377","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": true,"m_originalParentID": "9eb63977-c704-4541-8020-873eaf7130eb","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa","userdefined_animFPS": false,"x": 0,"y": 32}
            ],
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "047441de-4c43-45c9-9278-547c4593c964",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRPathLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Path_1",
            "id": "58f2829c-ba13-409c-b614-44d3a8667a0a",
            "colour": { "Value": 4278190335 },
            "depth": 400,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": false,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "3d5016c8-6501-456a-bb31-00c44c8d86ba",
            "m_serialiseFrozen": false,
            "modelName": "GMRPathLayer",
            "pathId": "89643c27-a170-450c-ba65-266bbb0cb9d8",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles_1",
            "id": "a9a0748a-5d82-4fd5-876b-1d8f9bfd0703",
            "depth": 500,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [
                {
                    "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "Instances_1",
                    "id": "cf92fefc-2ea1-47f9-86b5-8252da77b91f",
                    "depth": 600,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": true,
                    "inheritLayerSettings": true,
                    "inheritSubLayers": true,
                    "inheritVisibility": true,
                    "instances": [

                    ],
                    "layers": [

                    ],
                    "m_parentID": "37f99ec4-7e92-41cf-8613-a61377ead870",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRInstanceLayer",
                    "mvc": "1.0",
                    "userdefined_depth": false,
                    "visible": true
                }
            ],
            "m_parentID": "06e3ba35-84af-4b9c-bcba-6fb1feb460ec",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 16,
            "prev_tilewidth": 16,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 23,
                "SerialiseWidth": 40,
                "TileSerialiseData": [
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483649,2147483649,2147483649,2147483649,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483649,2147483649,2147483649,2147483648,2147483649,2147483649,2147483649,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483649,2147483649,2147483648,2147483648,2147483649,2147483648,2147483649,2147483649,2147483649,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483649,2147483649,2147483649,2147483649,2147483649,2147483649,2147483648,2147483649,2147483649,2147483648,2147483648,2147483648,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483649,2147483649,2147483648,2147483649,2147483648,2147483648,2147483649,2147483649,2147483648,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483649,2147483649,2147483649,2147483649,2147483649,2147483649,2147483649,2147483649,2147483649,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483650,2147483650,2147483648,2147483650,2147483650,2147483650,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483650,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648
                ]
            },
            "tilesetId": "699e9af4-42d9-45ba-b893-bb1895b58b41",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Backgrounds_1",
            "id": "256bbb1e-50b5-4a2a-81f2-1ba845672f71",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294530815 },
            "depth": 700,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": true,
            "inheritLayerSettings": true,
            "inheritSubLayers": true,
            "inheritVisibility": true,
            "layers": [

            ],
            "m_parentID": "c6051e17-943f-4728-868d-a71438b96b60",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "e0b198f0-9400-497c-a518-3dbb22b35cf0",
    "physicsSettings":     {
        "id": "c1d9c1c1-cd08-4056-ba5a-02112abd0178",
        "inheritPhysicsSettings": true,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "189cb50f-ade0-41ca-aded-97d85bd3773f",
        "Height": 360,
        "inheritRoomSettings": true,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 640
    },
    "mvc": "1.0",
    "views": [
{"id": "2b5b57bc-8d84-4d62-a8bc-ee087012fd8b","hborder": 32,"hport": 720,"hspeed": -1,"hview": 360,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1280,"wview": 640,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ea9b3e47-32e6-477c-bb1c-c027ce2f4b40","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "9852d9b7-4b17-43b2-a928-73b0a37ac8ba","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "393a2ae6-295d-40a0-b32f-a6b5f14cc002","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "85f9f03e-c77d-4845-b2a9-b9eae741a18c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "bed2228d-6a6e-468e-8771-4eec00bc8ff3","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "2e8f2348-18fa-49d6-bdbe-617791ba5c85","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "da58a469-a3f7-40b1-bd75-4ea15ea256a4","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": true,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "9bb445e5-e87c-458d-b1c6-92fca585fb66",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": true,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}