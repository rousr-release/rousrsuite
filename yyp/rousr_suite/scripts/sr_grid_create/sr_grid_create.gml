///@function sr_grid_create(_cols, _rows, [_default_value=undefined])
///@desc Creates a new `sr_grid`
///@param {Real} _cols
///@param {Real} _rows
///@param {Real} [_default_value=undefined]
///@returns {Real}
var _cols = argument[0],
	_rows = argument[1],
	_default = (argument_count > 2 ) ? argument[2] : undefined,
	_value = _sr_ds_register(ERousrDS.Grid, ds_grid_create(_cols, _rows));

if(_default != undefined){
	ds_grid_set_region(_value, 0, 0, _cols-1, _rows-1, _default);
}
              
return _value;