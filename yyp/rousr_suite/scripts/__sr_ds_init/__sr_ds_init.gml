///@function __sr_ds_init()
///@desc create internal data structures

//Internal DS
_RousrDSContainer = sr_array_create();
_RousrDSFree = sr_stack_array_create();
_RousrDSCount = array_create(ERousrDS.Num);

//Call this script
gml_pragma("global", "__sr_ds_init()");