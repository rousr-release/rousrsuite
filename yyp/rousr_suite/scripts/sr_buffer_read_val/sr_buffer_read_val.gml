///@function sr_buffer_read_val(_buffer)
///@description Reads a value from a packed buffer, first reading data type to expect.
///@param {Real} _buffer - id of buffer we're reading the _next_ value from
///@returns {Real|String} return of the buffer read
var _buffer = argument0;

var dataType = buffer_read(_buffer, buffer_s8);
var bdt = sr_buffer_rousr_type_to_buffer_type(dataType);
if (bdt == undefined)
  return undefined;
  
return buffer_read(_buffer, bdt);
