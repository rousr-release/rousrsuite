///@function sr_shadow_text(_x, _y, _text, _fg, _bg, [_xoff=1], [_yoff=1])
///@desc Draw text with a shadow
///@param {Real}   _x          x position to draw text
///@param {Real}   _y          y position to draw text
///@param {String} _text       text to draw
///@param {Real}   _fg         foreground color
///@param {Real}   _bg         background (shadow) color
///@param {Real}   [_xoff=1]   x offset for the shadow
///@param {Real}   [_yoff=1]   y offset for the shadow
var _x       = argument[0];
var _y       = argument[1];
var _text    = argument[2];
var _fg      = argument[3];
var _bg      = argument[4];
var _xoff    = argument_count > 5 ? argument[5] : 1;
var _yoff    = argument_count > 6 ? argument[6] : 1;

sr_ensure_color(_bg);
draw_text(_x + _xoff, _y + _yoff, _text);

sr_ensure_color(_fg);
draw_text(_x, _y, _text);