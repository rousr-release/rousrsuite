///@function sr_stack_array_pop(_rousr_stack_array)
///@description pop the last `val`  from a `_rousr_stack_array`
///@param {Array} _rousr_stack_array - `rousr_stack_array` array object to pop
///@returns {*} the end of the stack, or undefined if empty
gml_pragma("forceinline");
var _rousr_stack_array = argument[0];

var top   = _rousr_stack_array[ERousrStackArray.Top];
var stack = _rousr_stack_array[ERousrStackArray.Stack];

if (top < 0) 
  return undefined;
  
var _val = stack[top--];
_rousr_stack_array[@ ERousrStackArray.Top] = top;

//todo: garbage collection pass
// flush the array if we've cleaned the stack out
//if (top < 0) _rousr_stack_array[@ ERousrStackArray.Stack] = [ ];

return _val;