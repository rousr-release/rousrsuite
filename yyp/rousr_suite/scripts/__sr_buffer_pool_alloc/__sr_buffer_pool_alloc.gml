///@function _rousr_pool_buffer_create(_rousr_pool)
///@description create a new buffer
///@param {Array} _rousr_pool - `rousr_pool` that owns the created buffer
///@returns {Real} buffer id for created buffer
var _rousr_pool = argument0;
var user_data = _rousr_pool[@ ERousrPool.UserData];

var size      = user_data[ERousrPool_Buffer_UserData.Size];
var type      = user_data[ERousrPool_Buffer_UserData.Type];
var alignment = user_data[ERousrPool_Buffer_UserData.Alignment];

return buffer_create(size, type, alignment);