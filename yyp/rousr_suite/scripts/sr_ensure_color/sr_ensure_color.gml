///function rousr_ensure_color(_color)
///@desc cache the current color
///@param {Real} _color   color value to set
gml_pragma("global", "global.__rousr_set_color = undefined;");
gml_pragma("forceinline");
var _color = argument0;

if (_color != global.__rousr_set_color) {
  draw_set_color(_color);
  global.__rousr_set_color = _color;
}