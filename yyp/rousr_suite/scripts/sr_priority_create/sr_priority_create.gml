///@function sr_priority_create()
///@desc Creates a new `sr_priority`
///@returns {Real}
return _sr_ds_register(ERousrDS.Priority, ds_priority_create());