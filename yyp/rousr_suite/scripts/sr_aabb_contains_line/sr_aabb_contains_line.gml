///@function sr_aabb_contains_line(_line_x1, _line_y1, line_x2, line_y2, _rect_min_x, rect_min_y, _rect_max_x, _rect_max_y)
///@desc courtesy of https://stackoverflow.com/questions/1585525/how-to-find-the-intersection-point-between-a-line-and-a-rectangle
///@param {Real} _line_x1
///@param {Real} _line_y1
///@param {Real} _line_x2
///@param {Real} _line_y2
///@param {Real} _rect_min_x
///@param {Real} _rect_min_y
///@param {Real} _rect_max_x
///@param {Real} _rect_max_y
///@returns {Boolean} true if an aabb and line intersect
var _x1   = argument0,
    _y1   = argument1,
    _x2   = argument2,
    _y2   = argument3,
    _minX = argument4,
    _minY = argument5,
    _maxX = argument6,
    _maxY = argument7;

// Completely outside.
if ((_x1 <= _minX && _x2 <= _minX) || (_y1 <= _minY && _y2 <= _minY) || 
    (_x1 >= _maxX && _x2 >= _maxX) || (_y1 >= _maxY && _y2 >= _maxY))
    return false;

var m = (_y2 - _y1) / (_x2 - _x1);

var yy = m * (_minX - _x1) + _y1;
if (yy > _minY && yy < _maxY) 
  return true;

yy = m * (_maxX - _x1) + _y1;
if (yy > _minY && yy < _maxY) 
  return true;

xx = (_minY - _y1) / m + _x1;
if (xx > _minX && xx < _maxX) 
  return true;

xx = (_maxY - _y1) / m + _x1;
if (xx > _minX && xx < _maxX) 
  return true;

return false;