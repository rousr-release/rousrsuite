///@function sr_array_find(_rousr_array, _val, [_startIndex=0])
///@description returns the first occurence of _val beginning at _startIndex
///@param {Array:ERourArray} _rousr_array   rousr_array to search
///@param {*} _val   value to search for
///@param {Real} [_startIndex=0]   index to start searching from
///@returns {Real} index or -1 if not found
gml_pragma("forceinline");

var _rousr_array =  argument[0],
            _val =  argument[1],
     _startIndex = (argument_count > 2 ? argument[2] : 0);

var _array = _rousr_array[ERousrArray.Array],
     _size = _rousr_array[ERousrArray.Size];

for (var i = _startIndex; i < _size; ++i)
  if (_array[i] == _val) return i;

return -1;