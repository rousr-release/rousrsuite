///@function sr_log([_system], _error_text)
///@desc Log wrapper (eventual logging system)
///@param {String} [_system]   system name for logging
///@param {String} _text       log message
var _sys = argument_count > 1 ? argument[0] : undefined;
var _txt = argument_count > 1 ? argument[1] : argument[0];

if (_sys != undefined)
  _txt = "[LOG] " + _sys + " :: " + _txt;
else 
  _txt = "[LOG] :: " + _txt;

show_debug_message(_txt);