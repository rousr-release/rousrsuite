///@function sr_ds_get_count(_sr_ds_type)
///@param {Real:ERousrDS} _sr_ds_type
///@desc returns the total count of this ds type currently in the game 
return _RousrDSCount[argument0];