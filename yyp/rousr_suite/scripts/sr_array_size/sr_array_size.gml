///@function sr_array_size(_rousr_array, [_new_size])
///@description return the size of the `rousr_array`
///@param _rousr_array       - `rousr_array` to get the size of
///@param {Real} [_new_size] - if passed, sets the size of the array to this new size
///@returns {Real} size of `rousr_array` (before it was set new, if `_new_size` is passed)
gml_pragma("forceinline");
var _rousr_array = argument[0];
var size = _rousr_array[ERousrArray.Size];

if (argument_count > 1) _rousr_array[@ ERousrArray.Size] = argument[1];

return size;