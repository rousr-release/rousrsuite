///@function sr_array_data(_rousr_array)
///@description return the data of the `rousr_array`
///@param {Array:ERousrArray} _rousr_array   `rousr_array` to get the data from
///@returns {Array} data in the `rousr_array`
gml_pragma("forceinline");
var _rousr_array = argument0;
return _rousr_array[ERousrArray.Data]; 