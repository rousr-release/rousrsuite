///@function sr_stack_array_push(_rousr_stack_array, _val)
///@description push a `_val` onto the stack of a `_rousr_stack_array`
///@param {Array} _rousr_stack_array - `rousr_stack_array` to push `_val` on to
///@param {*} _val                   - value to push on stack
///@returns {Real} new top index
var _rousr_stack_array = argument[0],
                  _val = argument[1];

var top   = _rousr_stack_array[ERousrStackArray.Top];
var stack = _rousr_stack_array[ERousrStackArray.Stack];

top++;
stack[@ top] = _val;
_rousr_stack_array[@ ERousrStackArray.Top] = top;

return top;