///@function sr_callback_create(_script_index, [_argument0], [_argument1], [...)]
///@param {Real} _script
///@param {Any} [_argument0] 
///@param {Any} [_argument1]
///@param {Any} [...] 
///@returns {Array:Callback}
var _callback = array_create(argument_count-1),
	_i = 1;
_callback[@ 0] = argument[0];

repeat(argument_count-1){
	_callback[@ _i] = argument[_i];
	++_i;
};

return _callback;

