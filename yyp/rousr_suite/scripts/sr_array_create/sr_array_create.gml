///@function sr_array_create([_array], [_arraySize])
///@description create a `rousr style` array, with the size as the first element, and the actual array as second
///@param {Array}    [_array]   array to wrap
///@param {Real} [_arraySize]   if `_array`s size is cached, pass it.
var _array     = argument_count > 0 ? argument[0] : [ ];
var _arraySize = argument_count > 1 ? argument[1] : array_length_1d(_array);
return [ 
  _arraySize, // ERousrArray.Count
  _array      // ERousrArray.Array
];