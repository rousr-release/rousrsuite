///@function sr_array_push_back(_rousr_array, _val)
///@description push a value on the end of a `rousr_array`
///@param {Array:ERourArray} _rousr_array   `_rousr_array` to push data to
///@param {*}                        _val   value to push onto the array
///@returns {Real} index pushed or -1 on error
gml_pragma("forceinline");

var _rousr_array = argument[0],
            _val = argument[1]; 
var       _array = _rousr_array[ERousrArray.Array],
           _size = _rousr_array[ERousrArray.Size];

var  index = _size;

_array[@ index]                  = _val;
_rousr_array[@ ERousrArray.Size] = index + 1;

return index;
