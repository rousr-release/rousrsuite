///@function _sr_ds_register(_type, _value)
///@param {Real} _type
///@param {*} _value 
///@returns {Real} original_id.sr_id
///@extensionizer { "docs": false } 
var _type = argument0,
	_value = argument1,
	_sr_id = sr_stack_array_empty(_RousrDSFree) ? sr_array_size(_RousrDSContainer) : sr_stack_array_pop(_RousrDSFree),
	_id = _value + _sr_id / 100,
	_data = array_create(ERousrDSData.Num);

_data[@ ERousrDSData.Id] = _id;
_data[@ ERousrDSData.Type] = _type;
_data[@ ERousrDSData.Nested] = sr_array_create();
sr_array(_RousrDSContainer, _sr_id, _data);

_RousrDSCount[@ _type] = _RousrDSCount[_type] + 1;

return _id;
