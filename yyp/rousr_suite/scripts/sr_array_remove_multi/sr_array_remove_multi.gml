///@function sr_array_remove_multi(_array, _index_array, [_index_array_size])
///@description generate a new array with _index removed from the `_rousr_array`
///@param {Array}       _rousr_array  - rousr array to remove element at `_index` from
///@param {Real}        _index_array  - index to remove from _array (note: normal array)
///@param {Real}  [_index_array_size] - size of `_index_array` if you've already cached it.
///@returns {Array} removed values or undefined if error
gml_pragma("forceinline");
var _rousr_array = argument[0],
    _indices     = argument[1];
var _indexCount  = argument_count > 3 ? argument[3] : array_length_1d(_indices);
var  _size = _rousr_array[ERousrArray.Size],
    _array = _rousr_array[ERousrArray.Array];


if (_indexCount == 0 || _size == 0)
  return undefined;
  
var newSize = _size - _indexCount;
var removed = array_create(_indexCount);
var newArray = array_create(newSize);
var dstIndex = 0,
    srcIndex = 0;

for (var i = 0; i < _indexCount; ++i) {
  var index = _indices[i];
  removed[i] = _array[index];
  
  if (index != 0) {
    var cpySize = (index - srcIndex); 
    array_copy(newArray, dstIndex, _array, srcIndex, cpySize);
    dstIndex += cpySize;
  } 

  srcIndex = index + 1;
}

if (srcIndex < _size)
  array_copy(newArray, dstIndex, _array, srcIndex, _size - srcIndex);

_rousr_array[@ ERousrArray.Size] = newSize;
_rousr_array[@ ERousrArray.Data] = newArray;

return removed;