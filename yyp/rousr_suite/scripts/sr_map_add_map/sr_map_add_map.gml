///@function sr_map_add_list(_sr_map, _key, _child_map)
///@desc Sets the child map to a key in the map and creates an association
///@param {Real} _sr_map
///@param {*} _key
///@param {Real} _child
var _map = argument0,
	_key = argument1,
	_child = argument2;
_map[? _key] = _child;

sr_map_mark_as(_map, _key, ERousrDS.Map);