///@function sr_array_pop_back(_rousr_array)
///@description pop the back of a rousr array and return it
///@param {Array:ERourArray} _rousr_array   rousr array to pop last value of
///@returns {*} back, or undefined on failure
gml_pragma("forceinline");

var _rousr_array = argument[0];
var _array = _rousr_array[ERousrArray.Array],
     _size = _rousr_array[ERousrArray.Size];

if (_size == 0)
  return undefined;

return sr_array_remove(_rousr_array, _size - 1);