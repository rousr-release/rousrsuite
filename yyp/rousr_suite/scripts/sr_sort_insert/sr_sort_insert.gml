///@description an insertion sort with a predicate
///ported from http://www.algolist.net/Algorithms/Sorting/Insertion_sort
///cause why not?
///@param list
///@param predicate
var _list = argument0, _compare = argument1;

var len = ds_list_size(_list);
for (var i = 1;  i < len; ++i) {
  var j = i;
  while (j > 0 && script_execute(_compare, _list[| j - 1], _list[| j]) > 0) {
    var tmp = _list[| j];
    _list[| j] = _list[|j - 1];
    _list[| j - 1] = tmp;
    j--;
  }
}