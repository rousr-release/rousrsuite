///@function sr_outline_text(_x, _y, _text, _fg, _bg)
///@desc draw text with an oultine
///@param {Real} _x        x position for text
///@param {Real} _y        y position for text
///@param {String} _text   text to draw
///@param {Real} _fg       foreground color
///@param {Real} _bg       background color
var _x    = argument0,
    _y    = argument1,
    _text = argument2,
    _fg   = argument3,
    _bg   = argument4;

sr_ensure_color(_bg);
for (var i = -1; i <= 1; i++)
 for (var j = -1; j <=1; j++)
	draw_text(_x+i, _y+j, _text);

sr_ensure_color(_fg);
draw_text(_x, _y, _text);
