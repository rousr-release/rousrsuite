///@function sr_stack_create()
///@desc Creates a new `sr_stack`
///@returns {Real}
return _sr_ds_register(ERousrDS.Stack, ds_stack_create());