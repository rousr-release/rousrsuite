///@function sr_stack_array_create()
///@description create a `rousr_stack_array` - a stack purely made of arrays
///@returns {Array} rousr_stack_array
///@see ERousrStackArray
var rousr_stack_array = array_create(ERousrStackArray.Num);
rousr_stack_array[@ ERousrStackArray.Stack] = [ ];
rousr_stack_array[@ ERousrStackArray.Top]   = -1;

return rousr_stack_array;