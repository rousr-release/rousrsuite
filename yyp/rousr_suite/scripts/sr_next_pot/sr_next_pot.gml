///@function sr_next_pot(_val)
///@desc return the nearest power of 2 for a given number (integer)
///@param {Real} _va   note: if a float is given, its rounded up to 'fit' the float into the POT
///@returns {Real} nearest power of 2 to given integer
var _val = argument0;

var npot = ceil(log2(ceil(_val)));
npot = power(2, npot);

return npot;