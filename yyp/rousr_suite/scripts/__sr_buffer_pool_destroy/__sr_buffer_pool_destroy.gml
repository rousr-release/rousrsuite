///@function _rousr_pool_buffer_destroy(_rousr_pool, _buffer)
///@description destroy a pooled buffer
///@param {Array} _rousr_pool - `rousr_pool` that contains the buffer
///@param {Real}  _buffer - id of a buffer
var _rousr_pool = argument0;
var _buffer = argument1;

buffer_delete(_buffer);