///@function sr_pool_buffer(_buffer_size, _buffer_type, _buffer_alignment)
///@description helper function to create a `rousr_pool` of buffers
///@param {Real} _buffer_size      - size of the buffers to generate
///@param {Real} _buffer_type      - buffer type to pass through to `buffer_create` i.e., `buffer_u8`
///@param {Real} _buffer_alignment - alignment, in bytes, of buffer
///@returns {Array} `rousr_pool` setup to manage bufferses
gml_pragma("forceinline");
var buffer_args = array_create(ERousrPool_Buffer_UserData.Num);

buffer_args[@ ERousrPool_Buffer_UserData.Size]      = argument0;
buffer_args[@ ERousrPool_Buffer_UserData.Type]      = argument1;
buffer_args[@ ERousrPool_Buffer_UserData.Alignment] = argument2;

var buffer_pool = sr_pool_create_pool(__sr_buffer_pool_alloc, __sr_buffer_pool_destroy, __sr_buffer_pool_reset);
buffer_pool[@ ERousrPool.UserData] = buffer_args;

return buffer_pool;