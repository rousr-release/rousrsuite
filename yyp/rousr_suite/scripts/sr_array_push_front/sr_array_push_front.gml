///@function sr_array_push_front(_rousr_array, _val)
///@description push a value on the front of a rousr array
///@param {Array} _array      - rousr_array to push data to
///@param {*}       _val      - value to push onto the array
///@returns {Real} index pushed or -1 on error
gml_pragma("forceinline");

var _rousr_array = argument[0],
            _val = argument[1];
var _array = _rousr_array[ERousrArray.Array],
     _size = _rousr_array[ERousrArray.Size];

var index = 0;
var tmpArray = array_create(_size + 1);
tmpArray[@ index] = _val;
array_copy(tmpArray, 1, _array, 0, _size);

_rousr_array[@ ERousrArray.Array] = tmpArray;
_rousr_array[@ ERousrArray.Size] = _size + 1;

return index;