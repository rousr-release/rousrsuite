///@function sr_color_glsl(_color, [_out_array])
///@desc convert a color to an array float values [0.0 - 1.0]
///@param {Real} _color          color to convert
///@param {Array} [_out_array]   optional out array to not allocate a new array
///@returns {Array} array of colors in float range
var _color     = argument[0],
    _out_array = argument_count > 1 ? argument[1] : array_create(3);

_out_array[@ 0] = color_get_red(  _color)/255;
_out_array[@ 1] = color_get_green(_color)/255;
_out_array[@ 2] = color_get_blue( _color)/255;

return _out_array;
