///@function sr_array_remove(_rousr_array, _index)
///@description generate a new array with _index removed from the `_rousr_array`
///@param {Array} _rousr_array - rousr_array to remove element at `_index` from
///@param {Real}        _index - index to remove from _array
///@returns {*} element at removed index, or undefined
gml_pragma("forceinline");
var _rousr_array = argument0,
          _index = argument1;
var  _size = _rousr_array[ERousrArray.Size],
    _array = _rousr_array[ERousrArray.Array];

if (_size == 0 || _index >= _size)
  return undefined;
  
var newSize = _size - 1;
var newArray = array_create(newSize);
var dstIndex = 0,
    srcIndex = 0;

if (_index != 0) {
  var cpySize = (_index - srcIndex); 
  array_copy(newArray, dstIndex, _array, srcIndex, cpySize);
  dstIndex += cpySize;
} 

srcIndex = _index + 1;
if (srcIndex < _size)
  array_copy(newArray, dstIndex, _array, srcIndex, _size - srcIndex);

_rousr_array[@ ERousrArray.Array] = newArray;
_rousr_array[@ ERousrArray.Size]  = newSize;

return _array[_index];