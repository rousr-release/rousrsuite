///@function sr_json_encode(_sr_serializable, [_pretty_print]
///@param {Real} _sr_serializable
///@param {Boolean} [_pretty_print=false]
///@param TODO: Pretty print
var _value = argument[0],
	_type = sr_ds_get_type(_value),
	_pretty_print = (argument_count > 1) ? argument[1] : false;

if(_type != ERousrDS.List && _type != ERousrDS.Map && _type != ERousrDS.Grid){
	show_error("Invalid ERousrDS type ` " + string(_type) + " ` given. This type is not serializable", true);
	exit;
}

return __sr_json_encode_type(_value, _type, _pretty_print, 0); 