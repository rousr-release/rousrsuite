///@function sr_list_to_array(_list)
///@desc Converts a `sr_list` to an array
///@param {Real} _list
///@returns {Array}
var _list = argument0,
	_array = array_create(ds_list_size(_list)),
	_i=0;
repeat(array_length_1d(_array)){
	_array[@ _i] = _list[| _i];
	++_i;
}

return _array;
