///@function sr_array_push_unique(_sr_array, _val, [_returnIndexOnExists=false])
///@desc Push a value on the end of a rousr array, but only if value isn't in the array already
///@param {Array} _sr_array   rousr_array to push data to
///@param {*} _val   value to push onto the array
///@param {Boolean} [_returnIndexOnExists=false] - if true returns index if it already exists, otherwise returns undefined (to signal failure)
///@returns {Real} index pushed or -1 if it's not unique
gml_pragma("forceinline");

var _sr_array = argument[0],
            _val = argument[1];
var _array = _sr_array[ERousrArray.Array],
     _size = _sr_array[ERousrArray.Size];
var _returnIndexOnExists = argument_count > 3 ? argument[3] : false;

var index = sr_array_find(_array, _val, 0);
if (index >= 0)
  return (_returnIndexOnExists ? index : -1);

index = _size;    

_array[@ index]                  = _val;
_sr_array[@ ERousrArray.Size] = _size + 1;

return index;
