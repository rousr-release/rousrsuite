///@function sr_list_add_list(_sr_list, _child_list)
///@desc Adds a new list to the list and creates an association 
///@param {Real} _sr_list
///@param {Real} _child_list
var _list = argument0,
	_child = argument1;

ds_list_add(_list, _child);
sr_list_mark_as(_list, ds_list_size(_list)-1, ERousrDS.List);