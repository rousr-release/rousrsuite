///@function sr_ds_destroy(_sr_ds)
///@desc destroys a ds that has been created using a sr_*_create ds function.
///@param {Real} _sr_ds 
var _id       = argument0,
    _rousr_id = frac(argument0)*100,
    _data     = sr_array(_RousrDSContainer, _rousr_id);

if(!is_array(_data)) {
	show_debug_message("Warning! Cannot destroy sr_ds. " + string(_id) + " does not exist");	
	exit;
}

var _type = _data[0];
switch(_type){
	case ERousrDS.List:     
		var _nested = _data[ERousrDSData.Nested],
			  _i = 0;
		repeat (sr_array_size(_nested)) {
			var _nested_data = sr_array(_nested, _i++);
			sr_ds_destroy(_id[| _nested_data[0]]);	
		}
		ds_list_destroy(_id);     
	break;
	
	case ERousrDS.Map:      
		var _nested = _data[ERousrDSData.Nested],
			  _i = 0;
		repeat (sr_array_size(_nested)) {
			var _nested_data = sr_array(_nested, _i++);
			sr_ds_destroy(_id[? _nested_data[0]]);	
		}
		ds_map_destroy(_id);       
	break;
	
	case ERousrDS.Grid:     
		var _nested = _data[ERousrDSData.Nested],
			  _i = 0;
		
		repeat (sr_array_size(_nested)) {
			var _nested_data = sr_array(_nested, _i++);
			sr_ds_destroy(_id[# _nested_data[0], _nested_data[1]]);	
		}
		ds_grid_destroy(_id);     
	break;
	
	//Does not contain associations
	case ERousrDS.Queue:    ds_queue_destroy(_id);    break;
	case ERousrDS.Priority: ds_priority_destroy(_id); break;
	case ERousrDS.Stack:    ds_stack_destroy(_id);    break;
}

_RousrDSCount[@ _type] = _RousrDSCount[_type] - 1;
sr_array(_RousrDSContainer, _rousr_id, undefined);
sr_stack_array_push(_RousrDSFree, _rousr_id);