///@function sr__pool_destroy_pool(_rousr_pool)
///@description destroy a `rousr_pool` and free its memory use (data structures)
///@param {Array} _rousr_pool - `rousr_array_pool` to destroy
gml_pragma("forceinline");
var _rousr_pool = argument0;
if (!is_array(_rousr_pool))
  return;
 
var array   = _rousr_pool[@ ERousrPool.All];
var destroy = _rousr_pool[@ ERousrPool.Destroy];

var data = sr_array_data(array);
var size = sr_array_size(array);

for (var i = 0; i < size && destroy != noone; ++i)
  script_execute(destroy, _rousr_pool, array[i]);  

sr_array_destroy(array); 
sr_stack_array_destroy(_rousr_pool[ERousrPool.Available]);

_rousr_pool[ERousrPool.All]         = noone;
_rousr_pool[ERousrPool.Destroy]     = noone;
_rousr_pool[ERousrPool.Reset]       = noone;
_rousr_pool[ERousrPool.Constructor] = noone; 