///@function sr_grid_mark_as(_sr_grid, _x, _y, _type)
///@desc Creates a nested association at given x/y coord
///@param {Real} _sr_grid
///@param {Real} _x
///@param {Real} _y
///@param {Real:ERousrDS} _type
var _list = argument0,
	_x = argument1,
	_y = argument2,
	_type = argument3,
	_data = sr_array(_RousrDSContainer, frac(_list)*100);

sr_array_push_back(_data[ERousrDSData.Nested], [_x, _y, _type]); 