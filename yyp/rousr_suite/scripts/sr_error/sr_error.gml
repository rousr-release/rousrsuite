///@function sr_error([_system], _error_text)
///@desc Error wrapper (eventual logging system)
///@param {String} [_system]   system name for logging
///@param {String} _text       error message
var _sys = argument_count > 1 ? argument[0] : undefined;
var _txt = argument_count > 1 ? argument[1] : argument[0];

if (_sys != undefined)
  _txt = "[ERROR] " + _sys + " :: " + _txt;
else 
  _txt = "[ERROR] :: " + _txt;

// Potentially show error dialog
show_debug_message(_txt);