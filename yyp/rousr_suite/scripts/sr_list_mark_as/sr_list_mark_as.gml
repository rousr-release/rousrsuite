///@function sr_list_mark_as(_sr_list, _index, _type)
///@desc Creates a nested association at given index 
///@param {Real} _sr_list
///@param {Real} _index
///@param {ERousrDS} _type
var _list = argument0,
	_index = argument1,
	_type = argument2,
	_data = sr_array(_RousrDSContainer, frac(_list)*100);
	
sr_array_push_back(_data[ERousrDSData.Nested], [_index, _type]); 