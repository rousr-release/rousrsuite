///@function sr_queue_create()
///@desc Creates a new `sr_queue`
///@returns {Real}
return _sr_ds_register(ERousrDS.Queue, ds_queue_create());