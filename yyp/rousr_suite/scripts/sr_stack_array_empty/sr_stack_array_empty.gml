///@function sr_stack_array_empty(_rousr_stack_array)
///@description check if a stack is empty
///@param {Array} _rousr_stack_array - `rousr_stack_array` to check
///@returns {Boolean} true if `_rousr_stack_array` is empty
gml_pragma("forceinline");
var top = sr_stack_array_top(argument0);
return top < 0;