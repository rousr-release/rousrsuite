///@function sr_stack_array_destroy(_rousr_stack_array)
///@description destroy a `rousr_stack_array` - since they're made up of arrays, this is unnecessary to call, but provided future API use or re-using stack arrays.
///@param {Array} _rousr_stack_array - `rousr_stack_array` to destroy
///@see ERousrStackArray
gml_pragma("forceinline");
var _rousr_stack_array = argument0;

_rousr_stack_array[@ ERousrStackArray.Stack] = [ ];
_rousr_stack_array[@ ERousrStackArray.Top]   = -1;