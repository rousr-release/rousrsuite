///@function sr_stack_array_top(_rousr_stack_array)
///@description return the top index of a `rousr_stack_array`
///@param {Array} _rousr_stack_array - `rousr_stack_array` to get top index of
///@returns {Real} index of top, or -1 if empty
gml_pragma("forceinline");

var _rousr_stack_array = argument0;
var top = _rousr_stack_array[ERousrStackArray.Top];

return top;	