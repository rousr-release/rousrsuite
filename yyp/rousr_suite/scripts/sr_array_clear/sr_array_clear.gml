///@functiontion sr_array_clear(_rousr_array, [_flush_mem=false])
///@description emtpy a rousr_array
///@param {Real}     _rousr_array        array to clear, sets size=0
///@param {Boolean} [_flush_mem=false]   if true, we will replace the data array with a new, empty array.
///@returns {Array} _ra
var _ra    = argument[0],
    _flush = argument_count > 1 ? argument[1] : false;
		
_ra[@ ERousrArray.Size] = 0;
if (_flush)
	_ra[@ ERousrArray.Data] = [ ];
	
return _ra;