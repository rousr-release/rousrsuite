///@function sr_buffer_rousr_type_to_buffer_type(_type)
///@description return a buffer type from a luarousr data type
///@param {Real} _type - The ERousrData to get buffer_xx of
///@returns {Real} buffer_xx to use for this type when writing to a buffer
var _type = argument0;
var bdt = buffer_f64;
switch (_type) {
  case ERousrData.Bool:   bdt = buffer_s8; break;
  case ERousrData.Byte:   bdt = buffer_s8; break;
                                   
  case ERousrData.Int8:   bdt = buffer_s8; break;
  case ERousrData.Int16:  bdt = buffer_s16; break;
  case ERousrData.Int32:  bdt = buffer_s32; break;
  //case ERousrData.Int64:  bdt = buffer_s64; break; // Unsupported
                                   
  case ERousrData.Uint8:  bdt = buffer_u8; break;
  case ERousrData.Uint16: bdt = buffer_u16; break;
  case ERousrData.Uint32: bdt = buffer_u32; break;
  case ERousrData.Uint64: bdt = buffer_u64; break;
       
  //case __ELuaRousrDataTypeFloat16: bdt = buffer_f16; break; // Also unsupported (though buffer_f16 exists)
  case ERousrData.Float:  bdt = buffer_f32; break;
  case ERousrData.Double: bdt = buffer_f64; break;
  
  case ERousrData.String: bdt = buffer_string; break;

  default: bdt = undefined;
}

return bdt;
