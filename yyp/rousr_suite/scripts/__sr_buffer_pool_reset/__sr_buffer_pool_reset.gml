///@function _rousr_pool_buffer_reset(_rousr_pool, _buffer)
///@description reset a buffer, after being returned to the pool
///@param {Array} _rousr_pool - pool being returned to
///@param {Real} _buffer      - buffer id of buffer being returned
var _rousr_pool = argument0;
var _buffer = argument1;

buffer_seek(_buffer, buffer_seek_start, 0);