///@function sr_array(_rousr_array, _index, [_val]) 
///@description return an index, or set an index in a `_rousr_array`
///@param {Array:ERourArray} _rousr_array   rousr_array to read / write
///@param {Real}        _index   index to read or write to
///@param {*}           [_val]   if passed, value to set element to, same as insert.
var _rousr_array = argument[0],
          _index = argument[1];
          
#region Set

if (argument_count > 2) {
  var _set = argument[2];
  
  var _size = sr_array_size(_rousr_array);
  if (_index < _size ) {
    var _data = sr_array_data(_rousr_array);
    _data[@ _index] = _set;
  } else
    sr_array_insert(_rousr_array, _index, _set);
  return;
} 

#endregion
#region Get

var size = sr_array_size(_rousr_array);
if (_index < size) {
  var data = sr_array_data(_rousr_array);
  return data[_index];
}

#endregion