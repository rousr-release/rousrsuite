///@function sr_buffer_write_val(_buffer, _val, [_type=ERousrData.Invald])
///@description writes a value to the buffer, but first writes the type
///@param {Real}                               _buffer - buffer id to write to
///@param {Real|String}                        _val    - val to write to the buffer
///@param {Real|String} [type=ERousrData.Invalid] - the forced type to use, can be string, or ERousrData
///@returns {Boolean} true on success
var _buffer = argument[0];
var _val    = argument[1];
var _type   = argument_count > 2 ? argument[2] : ERousrData.Invalid;

if (_type == undefined || _type == ERousrData.Invalid) {
  if (is_real(_val))        _type = ERousrData.Double;
  else if (is_string(_val)) _type = ERousrData.String;
  else if (is_bool(_val))   _type = ERousrData.Bool;
  else if (is_int32(_val) || is_int64(_val))  _type = ERousrData.Int32;
//else if (is_int64(_val))  _type = ERousrData.Int64; // int64 is unsupported
  else return false; // invalid type
} else if (!is_real(_val) && !is_bool(_val) && !is_string(_val) && !is_int32(_val) && !is_int64(_val)) {
  return false; // invalid type
}

if (_type != ERousrData.String && is_string(_val)) _type = ERousrData.String;
if (_type == ERousrData.Bool) _val = _val ? 1 : 0;

var bdt = sr_buffer_rousr_type_to_buffer_type(_type);
if (bdt == undefined) 
  return false;

buffer_write(_buffer, buffer_s8, _type);
buffer_write(_buffer, bdt, _val);

// cap with a -1
var bend = buffer_tell(_buffer);
buffer_write(_buffer, buffer_s8, -1);
buffer_seek(_buffer, buffer_seek_start, bend);

return true;
