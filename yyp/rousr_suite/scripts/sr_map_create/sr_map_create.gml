///@function sr_map_create()
///@desc Creates a new `sr_map`
///@returns {Real}
return _sr_ds_register(ERousrDS.Map, ds_map_create());