///@function sr_callback_execute(_callback)
///@param {Callback} _callback
///@returns {Unknown} return value of the script
if !is_array(argument0) exit;

var _callback = argument0,
	_func = _callback[0],
	_params = _callback,
	_paramCount = array_length_1d(_callback)-1;

var ret = undefined;
switch (_paramCount) {
	case 0:  ret = script_execute(_func); break;
	case 1:  ret = script_execute(_func, _params[0+1]); break;
	case 2:  ret = script_execute(_func, _params[0+1], _params[1+1]); break;
	case 3:  ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1]); break;
	case 4:  ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1]); break;
	case 5:  ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1]); break;
	case 6:  ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1]); break;
	case 7:  ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1]); break;
	case 8:  ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1]); break;
	case 9:  ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1]); break;
	case 10: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1]); break;
	case 11: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1]); break;
	case 12: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1]); break;
	case 13: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1]); break;
	case 14: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1]); break;
	case 15: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1]); break;
    case 16: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1]); break;
    case 17: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1]); break;
    case 18: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1]); break;
    case 19: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1]); break;
    case 20: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1]); break;
    case 21: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1]); break;
    case 22: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1]); break;
    case 23: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1]); break;
    case 24: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1]); break;
    case 25: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1], _params[24+1]); break;
    case 26: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1], _params[24+1], _params[25+1]); break;
    case 27: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1], _params[24+1], _params[25+1], _params[26+1]); break;
    case 28: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1], _params[24+1], _params[25+1], _params[26+1], _params[27+1]); break;
    case 29: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1], _params[24+1], _params[25+1], _params[26+1], _params[27+1], _params[28+1]); break;
    case 30: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1], _params[24+1], _params[25+1], _params[26+1], _params[27+1], _params[28+1], _params[29+1]); break;
    case 31: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1], _params[24+1], _params[25+1], _params[26+1], _params[27+1], _params[28+1], _params[29+1], _params[30+1]); break;
    case 32: ret = script_execute(_func, _params[0+1], _params[1+1], _params[2+1], _params[3+1], _params[4+1], _params[5+1], _params[6+1], _params[7+1], _params[8+1], _params[9+1], _params[10+1], _params[11+1], _params[12+1], _params[13+1], _params[14+1], _params[15+1], _params[16+1], _params[17+1], _params[18+1], _params[19+1], _params[20+1], _params[21+1], _params[22+1], _params[23+1], _params[24+1], _params[25+1], _params[26+1], _params[27+1], _params[28+1], _params[29+1], _params[30+1], _params[31+1]); break;
};

return ret;

