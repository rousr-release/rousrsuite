///@function sr_array_to_list(_array)
///@desc converts an array to a `sr_list`
///@param {Array} _array
///@returns {Array}
var _array = argument0,
	_list = sr_list_create(),
	_i=0;
repeat(array_length_1d(_array)){
	ds_list_add(_list, _array[_i++]);
}

return _list;
