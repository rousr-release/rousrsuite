///@function sr_ensure_singleton(_singleton_name)
///@desc called `with` object to be singleton - ensure the name passed is a singleton, will call instance destroy on a different id
///@param {String} _singleton_name   global name to use for the singleton
///@returns {Boolean} false if an existing instance is destroyed - be sure to check CleanUp / Destroys
gml_pragma("forceinline");

var _singleton_name = argument[0];
var existed = false;
if (variable_global_exists(_singleton_name)) {
  var existsId = variable_global_get(_singleton_name);
  if (existsId != undefined && existsId != id && instance_exists(existsId)) {
    instance_destroy(existsId);
    existed = true;
  }
}

variable_global_set(_singleton_name, id);
return !existed;