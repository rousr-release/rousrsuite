///@function sr_array_destroy(_array)
///@description destroy a `rousr style` array - currently does nothing, but future proofing by including.
///@param {Array:ERourArray} _array  array to destroy
gml_pragma("forceinline");
var _array = argument0;
_array[@ ERousrArray.Size]  = 0;
_array[@ ERousrArray.Array] = [ ];