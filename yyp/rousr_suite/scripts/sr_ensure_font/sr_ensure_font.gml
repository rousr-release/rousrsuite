///@functino rousr_ensure_font(_font_index)
///@desc cache the current font
///@param _font_index   font resource index
gml_pragma("global", "global.__rousr_set_font = undefined;");
gml_pragma("forceinline");

var _font_index = argument0;
if (_font_index != global.__rousr_set_font) {
  draw_set_font(_font_index);
  global.__rousr_set_font = _font_index;
}