///@function sr_point_distance_squared(_x1, _y1, _x2, _y2)
///@desc Compare distances while avoiding square roots! If you must compare
///      the result with a non squared value... simply square it! 
///      if(point_distance_squared() > max * max){ 
///               
///      }
///@param {Real} _x1
///@param {Real} _y1 
///@param {Real} _x2
///@param {Real} _y2 
///@returns {Real} 

var _x1 = argument0,
	_y1 = argument1,
	_x2 = argument2,
	_y2 = argument3,
	_dx = (_x2 - _x1),
	_dy = (_y2 - _y1);

return _dx * _dx + _dy * _dy;