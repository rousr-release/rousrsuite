///@function sr_list_add_map(_sr_list, _sr_map)
///@desc Adds a new map to the list and creates an association 
///@param {Real} _sr_list
///@param {Real} _sr_map
var _list = argument0,
	_map = argument1;

ds_list_add(_list, _map);
sr_list_mark_as(_list, ds_list_size(_list)-1, ERousrDS.Map);