///@function sr_array_insert(_rousr_array, _index, _val)
///@description insert a value into a rousr array
///@param {Array} _rousr_array  rousr array to insert value into
///@param {Real}        _index  index to insert value at
///@param {*}             _val  value to insert
///@returns {Boolean} true on success
gml_pragma("forceinline");

var _rousr_array = argument[0],
          _index = argument[1],
            _val = argument[2];

var _array = _rousr_array[ERousrArray.Array],
     _size = _rousr_array[ERousrArray.Size];

if (_index == 0) {
  return sr_array_push_front(_rousr_array, _val) >= 0;
} else if (_index >= _size) {
  _array[@ _index] = _val;
  _rousr_array[@ ERousrArray.Size] = _index + 1;
  return true;
} else {
  var tmpArray = array_create(_size + 1);
  array_copy(tmpArray, 0, _array, 0, _index);
  array_copy(tmpArray, _index + 1, _array, _index, _size - _index)
  
  tmpArray[@ _index] = _val;
  
  _rousr_array[@ ERousrArray.Array] = tmpArray;
  _rousr_array[@ ERousrArray.Size]  = _size + 1;
  
  return true;
}

return false;
