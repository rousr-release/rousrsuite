///@function sr_pool_create(_rousr_pool, [_constructor])
///@description get a fresh item, optionally call the passed constructor on the object. 
///@param {Array} _rousr_pool   - the `rousr_pool` object to allocate new element from
///@param {Real} [_countructor] - called on the new object. if a constructor was defined for the pool, this *OVERRIDES* it. **Format:** `function(_rousr_pool, _elem) -> No Return`
///@returns {*} a new element from the `rousr_pool`
var _rousr_pool  = argument[0],
    _constructor = argument_count > 1 ? argument[1] : _rousr_pool[ERousrPool.Constructor];

var avail = _rousr_pool[@ERousrPool.Available];
var elem  = undefined;

if (sr_stack_array_empty(avail)) {
  var alloc = _rousr_pool[ERousrPool.Alloc];
  var array = _rousr_pool[ERousrPool.All];
  
  elem = script_execute(alloc, _rousr_pool);
  sr_array_push_back(array, elem); // track it to free memory with pool
} else {
  elem = sr_stack_array_pop(avail);
}

if (elem != undefined && _constructor != noone)
  script_execute(_constructor, _rousr_pool, elem);

return elem; 