///@function sr_pool_create_pool(_alloc_script, [_destroy_script], [_reset_script], [_constructor_script])
///@description create a `rousr_pool`, using the `_alloc_script` to create them. use `_destroy_script` to remove datastructures that are pooled. `rousr_pools` allow you to create generic pools of similiar objects for pooling resources, rather than allocating brand new ones at run-time.
///@param {Real} _alloc_script     - script to call for a new element of this `rousr_pool`  **Format:** `function(_rousr_pool) -> returns {*} newly created object` (the way you'd create a new one without a pool)
///@param {Real} [_destroy_script] - script to call to destroy / free the element **Format:** `function(_rousr_pool, _elem) -> No Return`
///@param {Real} [_reset_script]   - script to call to reset an element after its returned to the pool **Format:** ``function(_rousr_pool, _elem) -> No Return`
///@param {Real} [_constructor]    - script to call on returning a new element from the pool **Format:** `function(_rousr_pool, _elem) -> No Return`
///@returns {Array} undefined or new `rousr_pool`
gml_pragma("forceinline");

var _alloc       = argument[0];
var _destroy     = argument_count > 1 ? argument[1] : noone;
var _reset       = argument_count > 2 ? argument[2] : noone;
var _constructor = argument_count > 3 ? argument[3] : noone;

var pool = array_create(ERousrPool.Num);

pool[@ ERousrPool.All]       = sr_array_create();
pool[@ ERousrPool.Available] = sr_stack_array_create();

pool[@ ERousrPool.Alloc]       = _alloc;
pool[@ ERousrPool.Destroy]     = _destroy;
pool[@ ERousrPool.Reset]       = _reset;
pool[@ ERousrPool.Constructor] = _constructor;
  
return pool; 