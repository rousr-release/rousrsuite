///@function sr_pool_release(_rousr_pool, _element)
///@description returns element to the pool to be stored and reused
///@param {Array} _rousr_pool - `rousr_pool` to put `_element` in.
///@param {*} _element        - an `_element` compatible with `rousr_pool`
var _rousr_pool = argument0;
var _element = argument1;

var avail = _rousr_pool[ERousrPool.Available];
var reset = _rousr_pool[ERousrPool.Reset];

if (reset != noone)
  script_execute(reset, _rousr_pool, _element);

sr_stack_array_push(avail, _element);