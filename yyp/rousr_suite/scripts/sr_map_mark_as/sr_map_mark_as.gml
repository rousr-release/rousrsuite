///@function sr_map_mark_as(_sr_map, _key, _type)
///@desc Creates a nested association at given key
///@param {Real} _sr_map
///@param {Real} _key
///@param {ERousrDS} _type
var _list = argument0,
	_key = argument1,
	_type = argument2,
	_data = sr_array(_RousrDSContainer, frac(_list)*100);



sr_array_push_back(_data[ERousrDSData.Nested], [_key, _type]); 