///@function sr_map_add_list(_sr_map, _key, _sr_list)
///@desc Sets the list to a key in the map and creates an association
///@param {Real} _sr_map
///@param {*} _key
///@param {Real} _sr_list
var _map = argument0,
	_key = argument1,
	_list = argument2;
_map[? _key] = _list;

sr_map_mark_as(_map, _key, ERousrDS.List);