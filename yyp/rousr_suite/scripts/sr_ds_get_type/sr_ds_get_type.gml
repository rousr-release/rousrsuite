///@function sr_ds_get_type(_sr_ds)
///@desc Returns the sr_ds type
///@param {Real} _sr_ds
///@returns {Real:ERousrDS} 
var _id = argument0,
	_rousr_id = frac(argument0)*100,
	_data = sr_array(_RousrDSContainer, _rousr_id);

return _data[0];