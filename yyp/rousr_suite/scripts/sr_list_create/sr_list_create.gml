///@function sr_list_create()
///@desc Creates a new `sr_list`
///@returns {Real}
return _sr_ds_register(ERousrDS.List, ds_list_create());