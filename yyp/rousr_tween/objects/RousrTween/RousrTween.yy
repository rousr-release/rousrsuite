{
    "id": "71261df6-bf7f-456d-bc92-687f6c346a9f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrTween",
    "eventList": [
        {
            "id": "364595c1-91b7-4937-b3a1-db2d5644feb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71261df6-bf7f-456d-bc92-687f6c346a9f"
        },
        {
            "id": "5b0286af-78a8-452d-9273-c3969c1572a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "71261df6-bf7f-456d-bc92-687f6c346a9f"
        },
        {
            "id": "b13af6a4-b8d1-4fde-a415-60000ff932e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "71261df6-bf7f-456d-bc92-687f6c346a9f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2e7d478e-90e4-4ad8-87d0-2566b1da2161",
    "visible": false
}