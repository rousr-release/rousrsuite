/// @desc tween settings and constants here
gml_pragma("global", "global.__rousr_tweener = noone");
// tween / tweener config
Default_destroy_on_end = true;
Default_easing = easeExpoIn;

///////////////// 
Tweener_pool			  = [ ]; 
Tween_free_indices  = ds_stack_create();
tweenerList			    = ds_list_create();

tween_pool          = [ ];
tween_freeIndices   = ds_stack_create();

RousrTweener = id;
//global._tweenController = id;
//global.tweener = sr_tweener_add();