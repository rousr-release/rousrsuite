/// @desc Create a new tweener and return it
var tc = global._tweenController;
var newTweener = noone;
with (tc) {
	var newIndex = 0;
	if (ds_stack_empty(Tween_free_indices)) {
		// create a new tweener array
		newIndex = array_length_1d(Tweener_pool);
 	  Tweener_pool[@newIndex] = array_create(Tweener.Size);
		newTweener = Tweener_pool[newIndex];
		newTweener[@Tweener.TweenList] = ds_list_create();
	} else {
		newIndex = ds_stack_pop(Tween_free_indices);
		newTweener = Tweener_pool[newIndex];
	}
	
	
	newTweener[@Tweener.Id] = newIndex;
}

return newTweener;