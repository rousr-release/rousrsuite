/// @desc tween alpha
/// @param alphaValue
/// @param duration
/// @param (opt)easing=tweenController.Default_easing
/// @param (opt)destroyOnEnd=true
// todo:
// @param (opt)completeCallback=noone
//
// completCallback is called when the tween finishes completely.

var _alpha    = argument[0];
var _duration = argument[1];

var tc = global._tweenController;
var tween = _tween_add(id, _duration, argument_count > 3 ? argument[3] : tc.Default_destroy_on_end);

tween[@Tween.Tweening]   = _tweeningAlpha;
tween[@Tween.start_alpha] = image_alpha;
tween[@Tween.end_alpha]   = _alpha
tween[@Tween.Easing]  = argument_count > 2 ? argument[2] : tc.Default_easing;

return tween;