///@function sr_tweener_play(_tweener, _tween)
/// @desc Play a tween with the current tweener
/// @param tweener
/// @param tween
var tweener = argument0;
var tween = argument1;

with (RousrTweener) {
	if (!is_array(tweener)) tweener = Tweener_pool[tweener];
	if (!is_array(tween))   tween   = Tween_pool[tween];
}

tween[@Tween.StartTime] = gameTimer;
tween[@Tween.Status] = TweenStatus.Playing;

var snd = tween[@Tween.AudioCue];
if (is_array(snd)) {
  audio_play_sound(snd[0], snd[1], snd[2]);
}

// Add our tweener to the update list if this is the first tween on it.
var tweenList = tweener[Tweener.TweenList];
ds_list_add(tweenList, tween);
if (ds_list_size(tweenList) == 1) {
	var tweenerId = tweener[@Tweener.Id];
	ds_list_add(tc.tweenerList, tweenerId);
}

return tween;