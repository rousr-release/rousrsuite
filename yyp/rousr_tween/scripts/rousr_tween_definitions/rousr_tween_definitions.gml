///@function rousr_tween_definitions()
///@desc enums / defines for the tween system
#macro RousrTweener (global.__rousr_tweener)
enum Tweener {
  Id,
	TweenList,
	Scratch,
	
	Size,
};

enum Tween {
	Id,
	Tweening,
	
	TargetId,
	Status,
	Duration,
	Easing,
	Easing2,
	DestroyOnEnd,
	AudioCue,
  
	Callback,
	CallbackParams,
	CallbackParamCount,
	Prev,
	Next,
	
	// from the data start index, the array stores data specific to the tweener
	StartTime,
	Tweeners,
	
	TweeningData,
	
	// tween_move, tween_scale
	start_x = Tween.TweeningData,
	start_y,
	end_x,
	end_y,
	
	// tween_rotate, tween_image_rotate
	start_rot = Tween.TweeningData,
	end_rot,
	
  start_alpha = Tween.TweeningData,
  end_alpha,
  
	data0 = Tween.TweeningData,
	data1,
	data2,
	data3,
	data4,
	data5,
	data6,
	maxData, 
	
	// a rough estimate of how many maximum data spots we'll need so we can pre-allocate
	// increase at your le-sh-ore
	Size = Tween.TweeningData + 16,	
	defaultCallbackParamCount = 10,
}

enum TweenStatus {
	Idle,
	Playing,
	Complete,
	Cancelled,
}
