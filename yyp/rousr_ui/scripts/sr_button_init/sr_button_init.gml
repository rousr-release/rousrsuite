///@function sr_button_init([_click_action], [_click_check], [_user_data])
///@desc helper function for children to init common button stuff (all optional) **Called `with(instance)`**
///@param {Real} _click_action   script to call when clicked **Format:** `function(_button, _user_data) -> No Return`
///@param {Real} _can_click      script to check if a button can be clicked **Format:** `function(_button, _user_data) -> {Boolean} _can_click`
///@param {*} [_user_data]   passed along on click
Click_action = argument_count > 0 ? argument[0] : Click_action;
Click_check  = argument_count > 1 ? argument[1] : Click_check;
User_data    = argument_count > 2 ? argument[2] : User_data;