///@function sr_anchor_top_right([_pad_right=0], [_pad_top=0], [_add_list=true])
///@desc snap an instance to the bottom left of the screen. **Called `with(instance)`**
///@param {Real} [_pad_right=0]        pixels to pad on right
///@param {Real} [_pad_top=0]          pixels to pad on top
///@param {Boolean} [_add_list=true]   add it to the list to update on resize
var _pad_right = argument_count > 0 ? argument[0] : 0;
var _pad_top = argument_count > 1 ? argument[1] : 0;
var _add_list = argument_count > 2 ? argument[2] : true;

x = sr_anchor_display_width() - (sr_anchor_get_width(id) + _pad_right);
y = _pad_top;

if (_add_list)
  __sr_anchor_add_list(id, sr_anchor_top_right, [ _pad_right, _pad_top, false ]);