///@function sr_dragger_sensitivity(_dragger, [_new_sensitivity=RousrDraggerDefaultSlack])
///@desc Make it a sensitive dragger
///@param {Real} [_dragger]   dragger instance id
///@param {Real} [_new_sensitivity=RousrDraggerDefaultSlack]
with (argument[0]) {
	__rousr_dragger_slack = argument[1] ? 0 : RousrDraggerDefaultSlack;
	__rousr_dragger_has_moved = false;
}