///@function sr_button_create(_label, _click_action, [_depth], [_user_data], [_object_index=RousrButton])
///@desc create a new button
///@param {String} _label             label to use with button
///@param {Real} _click_action        script to call when clicked **Format:** `function(_button_instance, id, user_data) -> No Return`
///@param {Real} [_can_click=noone]   script to check if this button can be clicked, besides normal button rules. **Format:** `function(_button, _user_data) -> {Boolean} _can_click`
///@param {Real} [_depth=depth]       depth to create at (just set layer afterwards if layer desired)
///@param {*} [_user_data]            passed along to click fucnction (use an id to use one button callback for multiple buttons, etc)
///@param {Real} [_object_index=RousrButton] button object
///@returns {Real} id of button or noone if not created
var _label     = argument[0],
    _click     = argument[1],
		_can_click = argument_count > 2 ? argument[2] : noone,
		_depth     = argument_count > 3 ? argument[3] : depth,
		_user_data = argument_count > 4 ? argument[4] : undefined,
		_obj       = argument_count > 5 ? argument[5] : RousrButton;
		
var button = instance_create_depth(x, y, _depth, _obj);
with (button) sr_button_init(_click, _can_click, _user_data);	

return button;