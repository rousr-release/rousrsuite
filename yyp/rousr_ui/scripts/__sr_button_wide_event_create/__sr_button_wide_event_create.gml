///@function __sr_button_wide_event_create()
///@desc Create Event for RousrButtonWide object
///@extensionizer { "docs": false }
event_inherited();
Click_action = noone;

Label       = "Press Me!";
Label_align = TextAlign.Center;
Font        = fnt_game;

Left_side   = sButtonWide;
Content     = sButtonWide;
Right_side  = sButtonWide;

Left_frame    = 0;
Content_frame = 0;
Right_frame   = 0;