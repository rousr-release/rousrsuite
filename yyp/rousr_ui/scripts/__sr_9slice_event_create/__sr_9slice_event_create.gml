///@function __sr_9slice_event_create()
///@desc Create Event for Rousr9Slice Object
///@extensionizer { "docs": false }
Frame_sprite  = sprite_index;
Frame_surface = -1;

// Using image_xscale as a way to determine the intentional width/height lets us use the room editor
Frame_width   = image_xscale;
Frame_height  = image_yscale;

__Center_width  = 0;
__Center_height = 0;

__Frame_dirty       = true;
__Last_width        = Frame_width;
__Last_height       = Frame_height;
__Last_sprite       = noone;
__Last_offset       = -1;
__Surface_width     = -1;
__Surface_height    = -1;

Frame_sprite_width  = sprite_get_width(Frame_sprite);
Frame_sprite_height = sprite_get_height(Frame_sprite);

Frame_surface_pad_left   = 0;
Frame_surface_pad_top    = 0;
Frame_surface_pad_right  = 0;
Frame_surface_pad_bottom = 0;

Draw_gui = false;

sr_9slice_init();