///@function __sr_9silce_event_step()
///@desc Step Event for Rousr9Slice
///@extensionizer { "docs": false }
if (!visible)
  return;
  
var size_dirty = false, sprite_dirty = false;

// mark dirty if the frame size changed
if (image_xscale != Frame_width || image_yscale != Frame_height ||
    Frame_width != __Last_width || Frame_height != __Last_height ) {
  
  if (Frame_width  == __Last_width)  Frame_width = image_xscale;
  else image_xscale = Frame_width;
  
  if (Frame_height == __Last_height) Frame_height = image_yscale
  else image_yscale = Frame_height;
  
  size_dirty = true;
}

// mark dirty if the sprite changed
if (Frame_sprite != sprite_index || Frame_sprite != __Last_sprite) {
  if (Frame_sprite == __Last_sprite) Frame_sprite = sprite_index;
  else sprite_index = Frame_sprite;
  
  Frame_sprite_width  = sprite_get_width(Frame_sprite);
  Frame_sprite_height = sprite_get_height(Frame_sprite);
  
  sprite_dirty = true;
}

if (size_dirty || sprite_dirty) {
  Frame_width  = max((Frame_sprite_width  * 2) + 1, Frame_width);
  Frame_height = max((Frame_sprite_height * 2) + 1, Frame_height);
  
  __Center_width  = (Frame_width  - (2 * Frame_sprite_width)) / Frame_sprite_width;
  __Center_height = (Frame_height - (2 * Frame_sprite_height)) / Frame_sprite_height;  
}

__Frame_dirty |= size_dirty || sprite_dirty;
__Last_width  = Frame_width;
__Last_height = Frame_height;
__Last_sprite = Frame_sprite;