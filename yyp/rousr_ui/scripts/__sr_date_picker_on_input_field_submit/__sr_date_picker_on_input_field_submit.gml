///@functgion __sr_date_picker_on_input_field_submit(_field)
///@desc Update date based on submitted textfield
///@param {Array:ERousrTextField} _field
///@extensionizer { "docs": false }

var _field = argument0;

var field_index = _field.Field_index;
var date_picker = _field.Date_picker;
var val = string_digits(_field.Text);
if (string_length(val) == 0)
  return;

with (date_picker) {
  val = real(val);
  var validVal = true;
  
  switch(field_index) {
    case EDatePicker.Year: {
      if (val >= 2000 && val < 2200) Year = val;
    }  break;
    case EDatePicker.Month: {
      if (val >= 1 && val <= 12)     Month = val;
    } break;  
    case EDatePicker.Day: {
      if (val < 1) val = 1;
      else {
        var testDate = date_create_datetime(Year, Month, 1, 0,0,0);
        var days = date_days_in_month(testDate);
        val = min(val, days);
      }                              Day = val;
    } break;
    default: break;
  }

  if (validVal) {
    Date = date_create_datetime(Year, Month, Day, 0, 0, 0);
    if (On_edit != noone)
      script_execute(On_edit, id, Date);
  }
}