///@function sr_dragger_is_dragging(_id)
///@desc check if a draggable is dragging
///@param {Real} _id   _id of dragable
///@returns {Boolean} true if we're dragging
with (argument[0])
	if (__rousr_dragger_drag_state >= EDragState.Dragging)
		with (RousrDragger) return Dragging == other.id;

return false;