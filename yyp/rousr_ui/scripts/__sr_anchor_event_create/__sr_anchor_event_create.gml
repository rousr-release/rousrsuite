///@function __sr_anchor_event_create()
///@desc Create Event for RousrAnchorSystem Object
///@extensionizer { "docs": false }
persistent = true;
  
Anchor_list    = ds_list_create();
Display_width  = 0;
Display_height = 0;
Size_type      = 0;

sr_anchor_system_init();