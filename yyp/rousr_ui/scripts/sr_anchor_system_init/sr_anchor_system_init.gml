///@function sr_anchor_system_init([ERousrAnchorSize = ApplicationSurface], [_width], [_height])
///@desc initialize the RousrAnchor system
///@param {Real} [_ERousrAnchorSize]   Type to keep anchor position matching to - application surface by default
///@param {Real} [_width]                  ignored unless type is Manual, width to use
///@param {Real} [_height]                 ignored unless type is Manual, height to use
if (!instance_exists(RousrAnchor))
	instance_create_depth(0, 0, 0, RousrAnchorSystem)

with (RousrAnchor) {
	var _display_width  = argument_count > 2 ? argument[1] : 0;
	var _display_height = argument_count > 2 ? argument[2] : 0;
	var _anchor_sizing  = argument_count > 0 ? argument[0] : ERousrAnchorSize.ApplicationSurface;

	Display_width  = _display_width;
	Display_height = _display_height;
	Size_type      = _anchor_sizing;
  
	switch (Size_type) {
	case ERousrAnchorSize.ApplicationSurface:
		Display_width  = surface_get_width(application_surface);
		Display_height = surface_get_height(application_surface);
	break;
  
	case ERousrAnchorSize.Display:
		Display_width  = display_get_width();
		Display_height = display_get_height();
	break;
	default: break;
	}
}
