///@function sr_anchor_display_height()
///@desc return the anchor instance system's display height
///@returns {Real} display height
gml_pragma("forceinline");
var display_height = undefined;
with (RousrAnchor)
  display_height = Display_height;
  
return display_height != undefined ? display_height : window_get_height();