///@function __sr_title_draw()
///@desc Actually draw the title bar [called from Draw or DrawGUI]
///@extensionizer { "docs": false }
draw_self();
draw_sprite(Icon_sprite, 0, Scale, Scale);

Title_font = Draw_gui ? fnt_game_3x : fnt_game;

sr_ensure_font(Title_font);
sr_shadow_text(Title_x, Title_y, Title, Title_color_fg, Title_color_bg, 0, Scale);