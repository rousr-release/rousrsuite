///@function __sr_9slice_draw_surface
///@desc Simply draw the frame surface with a surface frame.
///       called `with(frameObject)`
///       only needs to manually called if optionally disabled in frame_draw
///@param __opt_x
///@param __opt_y
///@extensionizer { "docs": false }
gml_pragma("forceinline");
if (Frame_surface == undefined)
  return;

var _x = argument_count > 1 ? argument[0] : x;
var _y = argument_count > 1 ? argument[1] : y; 
draw_surface(Frame_surface, _x, _y);