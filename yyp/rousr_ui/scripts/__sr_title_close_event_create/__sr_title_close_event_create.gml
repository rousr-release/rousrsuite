///@function __sr_title_close_event_create()
///@desc Create Event for RousrTitleBar Object
///@extensionizer { "docs": false }
if (!sr_ensure_singleton("___rousr_title_bar_close"))
  return;

RousrTitleBarClose = id;
event_inherited();
Click_action = sr_game_quit;

sr_anchor_top_right();
Was_in_rect = false;

with (RousrTitleBar)
  other.depth = depth - 1;