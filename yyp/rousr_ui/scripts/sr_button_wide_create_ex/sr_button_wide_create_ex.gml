///@function sr_button_wide_create(_label, _click_action, [_depth], [_user_data])
///@desc create a new button
///@param {String} _label                         label to use with button
///@param {Real} _click_action                    script to call when clicked **Format:** `function(_button_instance, id, user_data) -> No Return`
///@param {Real} _center_sprite                   index of center sprite, or left+right if those aren't included
///@param {Real} [_left_sprite=_center_sprite]    index of left sprite, or left+right if those aren't included
///@param {Real} [_right_sprite=_center_sprite]   index of right sprite, or left+right if those aren't included
///@param {Real} [_center_frame=0]                `image_index` to use for center of button (expects the 'down' version to be the next `image_index`)
///@param {Real} [_left_frame=0]			            `image_index` to use for left of button	(expects the 'down' version to be the next `image_index`)
///@param {Real} [_right_frame=0]			            `image_index` to use for right of button (expects the 'down' version to be the next `image_index`)
///@param {Real} [_can_click=noone]               script to check if this button can be clicked, besides normal button rules. **Format:** `function(_button, _user_data) -> {Boolean} _can_click`
///@param {Real} [_depth=depth]                   depth to create at (just set layer afterwards if layer desired)
///@param {*} [_user_data]                        passed along to click fucnction (use an id to use one button callback for multiple buttons, etc)
///@returns {Real} id of button or noone if not created
var _label        = argument[0],
    _click        = argument[1],
		_center       = argument[2],
		_left         = argument_count > 3  ? argument[3]  : _center,
		_right        = argument_count > 4  ? argument[4]  : _center,
		_center_frame = argument_count > 5  ? argument[5]  : 0,
		_left_frame   = argument_count > 6  ? argument[6]  : 0,
		_right_frame  = argument_count > 7  ? argument[7]  : 0,
		_can_click    = argument_count > 8  ? argument[8]  : noone,
		_depth        = argument_count > 9  ? argument[9]  : depth,
		_user_data    = argument_count > 10 ? argument[10] : undefined;
		
return sr_button_wide_set_sprites(sr_button_create(_label, _click, _can_click, _depth, _user_data), _center, _left, _right, _center_frame, _left_frame, _right_frame);