///@function __sr_dragger_reset(_id)
///@desc reset the dragger to where it was before dragging
///@param {Real} _id    dragger to reset
///@extensionizer { "docs": false }
with (argument[0]) {
	x     = __rousr_dragger_start_x;
	y     = __rousr_dragger_start_y;
	depth = __rousr_dragger_start_depth;

	if (__rousr_dragger_drag_state != EDragState.None)
	  script_execute(__rousr_dragger_on_complete, false);

	__rousr_dragger_drag_state = EDragState.None;
}