///@function __sr_button_event_create()
///@desc Create Event for RousrButton object
///@extensionizer { "docs": false }
Click_action  = noone;
Click_check    = noone;
Up_index      = 0;
Down_index    = 1;
Click_enabled = true;
Can_click     = true;
Was_clicked   = false;
Was_over      = false;
image_index   = Up_index;
User_data     = undefined;
Draw_gui      = false;