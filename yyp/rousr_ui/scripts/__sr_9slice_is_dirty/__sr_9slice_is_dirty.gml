///@function __sr_9slice_is_dirty([_id=id])
///@desc helper to return frame dirty
///@param {Real} [_id=id]   optional, id of 9slice (or `id`)
///@returns {Boolean} if `_id` is dirty
///@extensionizer { "docs": false }
gml_pragma("forceinline");
var _id = argument_count > 0 ? argument[0] : id;
with (_id)
	return __Frame_dirty;
	