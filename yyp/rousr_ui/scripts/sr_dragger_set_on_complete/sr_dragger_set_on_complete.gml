///@function sr_dragger_set_on_complete(_id, _on_complete)
///@desc set the on complete callback after the fact
///@param {Real} _id            dragger id
///@param {Real} _on_complete   **Format:** `function({Boolean}_button_released)->No Return`
with (argument[0])
	__rousr_dragger_on_complete = argument[1];
