///@desc Put a button back into the default state
var _button = argument0;
_button.Up_index      = 0;
_button.Down_index    = 1;
_button.image_index  = _button.Up_index;
_button.Click_enabled = true;