///@function __sr_anchor_event_step()
///@desc Step Event for RousrAnchorSystem Object
///@extensionizer { "docs": false }

// check if the size has changed and update the anchored instances - expected to be Step event of RousrAnchor object
var new_width = Display_width;
var new_height = Display_height;
switch (Size_type) {
  case ERousrAnchorSize.ApplicationSurface: {
    
  } break;
  
  case ERousrAnchorSize.Display: {
    
  } break;
  
  case ERousrAnchorSize.Manual: break;
}

if (new_width != Display_width || new_height != Display_height) {
  __sr_anchor_refresh_display(new_width, new_height);
}