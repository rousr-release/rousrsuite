///@function __sr_button_wide_draw()
///@desc draw the button **Called `with(instance)`**
///@extensionizer { "docs": false }
var center_width = image_xscale;  // total pixels
center_width -= (sprite_get_width(Left_side) + sprite_get_width(Right_side));
var lx = x;
var ly = y;

sr_ensure_font(Font);

draw_sprite(Left_side, image_index, x, y);
x += sprite_get_width(Left_side);
draw_sprite_ext(Content, image_index, x, y, center_width / sprite_get_width(Content), image_yscale, 0, c_white, 1);
x += center_width;
draw_sprite(Right_side, image_index, x, y);
x -= center_width;

var label_width = string_width(Label);

switch(Label_align) {
  case TextAlign.Left:   break;
  case TextAlign.Right:  x += center_width - label_width; break;
  case TextAlign.Center: x += floor((center_width - label_width) * 0.5); break;
}

y += 1 + image_index;

sr_ensure_color(image_index == 0 ? c_white : c_gray);
draw_text(x, y, Label);

x = lx;
y = ly;
