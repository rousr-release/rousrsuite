///@function __sr_title_bar_event_draw()
///@desc Draw Event for Title Bar object
///@extensionizer { "docs": false }

// do nothing (don't draw anything in Draw event)
if (!Draw_gui)
	__sr_title_draw();
