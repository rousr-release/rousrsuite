///@function __sr_datepicker_event_step()
///@desc Step Event for RousrDatepicker Object
///@extensionizer { "docs": false }
if (!visible)
  return;
  
if (Date != noone && (Date != Last_date || Dirty)) {
  Dirty = false;
  
  Year  = date_get_year(Date);
  Month = date_get_month(Date);
  Day   = date_get_day(Date);
  
  First = date_create_datetime(Year, Month, Day, 0, 0, 0);
  Sow   = date_get_weekday(First);
  Sim   = date_days_in_month(Date);
  
  Year_string  = string(Year);
  Month_string = string(Month);
  Day_string   = string(Day);
  
	if (Month < 10) Month_string = "0" + Month_string;
	if (Day < 10)   Day_string   = "0" + Day_string;
	
  sr_ensure_font(Font);
  Year_width   = string_width(Year_string);
  Month_width  = string_width(Month_string);
  Day_width    = string_width(Day_string);
  Spacer_width = string_width("-");
  
  var fx = [ ];
  fx[0] =  x + 3;
  fx[1] = fx[0] + Year_width + Spacer_width;
  fx[2] = fx[1] + Month_width + Spacer_width;
  
  var fw = [ ];
  fw[0] = Year_width;
  fw[1] = Month_width;
  fw[2] = Day_width;
  
  var ft = [ ];
  ft[0] = Year_string;
  ft[1] = Month_string;
  ft[2] = Day_string;
  
  for (var field_index = 0; field_index < EDatePicker.FieldCount; ++field_index) {
    var field = Input_fields[field_index];
    instance_activate_object(field);  
		
		with (field) {
			visible = true;
    
	    x = fx[field_index];
	    y = other.y;
	    Field_width = fw[field_index];
	    Text        = ft[field_index];
	    On_submit   = __sr_date_picker_on_input_field_submit;
	    Field_index = field_index;
	    Date_picker = other.id;
		}
  }
}

Last_date = Date;