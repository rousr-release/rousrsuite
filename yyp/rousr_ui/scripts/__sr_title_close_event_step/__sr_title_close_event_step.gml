///@function __sr_title_close_event_step()
///@desc Step Event for RousrTitleBarClose Object
///@extensionizer { "docs": false }
var esc_key    = undefined;
var esc_action = __sr_ui_dummy;
var esc_closes = false;

with (RousrTitleBar) {
	esc_key      = Esc_key;
	esc_action   = On_close;
	esc_closes   = Esc_closes;
	click_action = On_close;
	other.Draw_gui     = Draw_gui;
	other.image_xscale = Scale;
}

if (esc_closes) {
	var esc_pressed  = keyboard_check_pressed(esc_key);
	var esc_down     = keyboard_check(esc_key);
	var esc_released = keyboard_check_released(esc_key);

	if (esc_down) 
		image_index = Down_index;
		
	if (esc_pressed) {
		if (image_index == Down_index) {
			script_execute(esc_action);	
		}
	} else if (esc_released) {
		image_index = Up_index;
	}
}

event_inherited();