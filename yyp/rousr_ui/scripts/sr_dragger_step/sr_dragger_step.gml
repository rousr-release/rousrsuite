///@function sr_dragger_step()
///@desc call each step on a dragger
var left_down = mouse_check_button(mb_left);
var left_released = mouse_check_button_released(mb_left);

if (!left_down && !left_released)
	return;
	
if (left_down) {
	var is_over = position_meeting(mouse_x, mouse_y, id);
  __sr_dragger_left_down();
} else if (left_released) {
	__sr_dragger_left_released();
}