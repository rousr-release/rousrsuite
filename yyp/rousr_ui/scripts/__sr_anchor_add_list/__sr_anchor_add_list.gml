///@function __sr_anchor_add_list(_id, _script, _params)
///@desc Add an instance that has been anchored to the list to track for resizes
///@param {Real} _id    		instance id to anchor
///@param {Real} _script		anchor script to use
///@param {Array} _params   params to pass to anchor script
///@returns {Real} index added at, or -1
///@extensionizer { "docs": false }
var _id     = argument0;
var _scr    = argument1;
var _params = argument2;

with (RousrAnchor) {
	var anchorList = Anchor_list
	ds_list_add(anchorList, [ _id, _scr, _params] );
	return ds_list_size(anchorList) - 1;
}
 
return -1;