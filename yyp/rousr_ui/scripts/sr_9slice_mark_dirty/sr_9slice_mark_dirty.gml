///@function sr_9slice_mark_dirty([_id])
///@desc just a helper function so we dont need to know internals
///@param {Real} [_id]   optional, id of 9slice (or this)
gml_pragma("forceinline");
var _id = argument_count > 0 ? argument[0] : id;
with (_id)
	__Frame_dirty = true;