///@function __sr_title_close_event_draw()
///@desc Draw Event for RousrTitleBarClose Object
///@extensionizer { "docs": false }
var draw_gui = true;
with (RousrTitleBar)
	draw_gui = Draw_gui;
	
if (!draw_gui)
	__sr_title_close_draw();