///@function sr_anchor_bottom_left([_pad_left=0], [_pad_bottom=0], [_add_list=true])
///@desc snap an instance to the bottom left of the screen **Called `with(instance)`**
///@param {Real} [_pad_left=0]         pixels to pad on left
///@param {Real} [_pad_bottom=0]			 pixels to pad on bottom
///@param {Boolean} [_add_list=true]   add it to the list to update on resize
var _pad_left = argument_count > 0 ? argument[0] : 0;
var _pad_bottom = argument_count > 1 ? argument[1] : 0;
var _add_list = argument_count > 2 ? argument[2] : true;

x = _pad_left;
y = sr_anchor_display_height()- (sr_anchor_get_height(id) + _pad_bottom);

if (_add_list )
  __sr_anchor_add_list(id, sr_anchor_bottom_left, [ _pad_left, _pad_bottom, false ]);