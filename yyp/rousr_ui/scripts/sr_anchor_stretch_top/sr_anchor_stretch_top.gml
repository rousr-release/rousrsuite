///@function sr_anchor_stretch_top([_pad_left_or_sides=0], [_pad_top=0], [_pad_right=0], [_add_list=true])
///@desc stretch this object acrosst the top. **Called `with(instance)`**
///@param {Real} [_pad_left_or_sides=0]   pixels to pad on left siide, or both sides if `_padRight` is undefined
///@param {Real} [_pad_top=0]             pixels to pad on top
///@param {Real} [_pad_right=0]           pixels to pad on the right
///@param {Boolean} [_add_list=true]      add it to the list to update on reisze
var _pad_left   = argument_count > 0 ? argument[0] : 0;
var _pad_top    = argument_count > 1 ? argument[1] : 0;
var _pad_right  = argument_count > 2 ? argument[2] : _pad_left;
var _add_list   = argument_count > 3 ? argument[3] : true;

x = _pad_left;
y = _pad_top;
  
var sw = sr_anchor_display_width() - (_pad_left + _pad_right);
image_xscale = sw / sprite_get_width(sprite_index);
  
if (_add_list) 
	__sr_anchor_add_list(id, sr_anchor_stretch_top, [ _pad_left, _pad_top, _pad_right, false ]);
