///@function sr_textfield_toggle_edit(_submit_if_done)
///@desc toggle edit on a textfield (memberfunction)
///@param {Boolean} _submit_if_done   call `on_submit` if we're disabled edit
var _submit = argument0;

var ta = sr_textfield_active();
if (ta != id)
  with(ta) sr_textfield_toggle_edit(Submit_on_blur); 

Is_editing = !Is_editing;

if (Is_editing) {
  keyboard_string = "";
  sr_textfield_active(id);
} else {
  sr_textfield_active(noone);
  if (_submit && On_submit != noone)
    script_execute(On_submit, id);
    
  if (Hide_on_blur)
    instance_deactivate_object(id);

	keyboard_string = "";
}