///@function __sr_textfield_event_draw()
///@desc Draw Event for RousrTextfield Object
///@extensionizer { "docs": false }
draw_self();
sr_ensure_color(Field_fg);

var tx = x;
switch(Alignment) {
  case TextAlign.Right:
    tx += (Field_width - string_width(Text)) - 1;
    break;
    
  case TextAlign.Left:
  default: break;
}
  
draw_text(tx, y + 1, Text);