///@function sr_anchor_display_width()
///@desc return the anchor instance system's display width
///@returns {Real} display width
gml_pragma("forceinline");
var display_width = undefined;
with (RousrAnchor)
  display_width = Display_width;
  
return display_width != undefined ? display_width : window_get_width();