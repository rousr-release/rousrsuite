///@functino __sr_drag_manager_event_begin_step()
///@desc Begin Step Event for RousrDragManager
///@extensionizer { "docs": false }
if (Dragging != noone) {
  with(Dragging) {
  	x += mouse_x - __rousr_dragger_last_mouse_x;
  	y += mouse_y - __rousr_dragger_last_mouse_y;
  	__rousr_dragger_last_mouse_x = mouse_x;
  	__rousr_dragger_last_mouse_y = mouse_y;
  }
}