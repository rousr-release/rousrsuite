///@function sr_title_init(_title, [_fg_color=0x639bff], [_bg_color=0x5b6ee1], [_title_font=fnt_game_3x], [_icon=sTitleBar_Icon], [_bg_sprite=sTitleBar_BgStrip], [_close_action=sr_game_quit], [_esc_closes=true], [_draw_gui=true], [_scale=3]
///@desc called after a title bar is created (Instance Create, for instance) **Called `with(instance)`**
///@param {String} _title                           title on titlebar
///@param {Real} [_fg_color=0x639bff]	              title foreground color
///@param {Real} [_bg_color=0x5b6ee1]	              title shadow color
///@param {Real} [_title_font=fnt_game_3x]					font to use for title
///@param {Real} [_icon=sTitleBar_Icon]             icon to show in the corner
///@param {Real} [_bg_sprite=sTitleBar_BgStrip]     sprite to use for background (must be a 1px strip)
///@param {Real} [_close_action=sr_game_quit],			close action, if `undefined`, then no close button
///@param {Real} [_esc_closes=true],								does escape also do the close action?
///@param {Boolean} [_draw_gui=true],								draw on the gui layer
///@param {Real} [_scale=3]													scale the title bar this much (and close button)
var _title        = argument[0], 
    _fg_color     = argument_count > 1 ? argument[1] : sr_color_hex($639bff), 
		_bg_color     = argument_count > 2 ? argument[2] : sr_color_hex($5b6ee1), 
		_title_font   = argument_count > 3 ? argument[3] : fnt_game_3x, 
		_icon         = argument_count > 4 ? argument[4] : sTitleBar_Icon, 
		_bg_sprite    = argument_count > 5 ? argument[5] : sTitleBar_BgStrip, 
		_close_action = argument_count > 6 ? argument[6] : sr_game_quit, 
		_esc_closes   = argument_count > 7 ? argument[7] : true, 
		_draw_gui     = argument_count > 8 ? argument[8] : true,
		_scale        = argument_count > 9 ? argument[9] : 3;

Scale    = _scale;
Draw_gui = _draw_gui;
__last_draw_gui = _draw_gui;

Icon_sprite       = _icon;
Background_sprite = _bg_sprite;

Title          = _title;
Title_color_fg = _fg_color;
Title_color_bg = _bg_color;
Title_font     = _title_font;

On_close = _close_action; 
On_close = On_close == undefined || !is_real(On_close) ? noone : On_close;
Esc_closes = On_close != noone && _esc_closes;

sprite_index = Background_sprite;
__sr_title_resize();

if (On_close == noone) {
	with (RousrTitleBarClose) instance_destroy();
} else {
	if (!instance_exists(RousrTitleClose))
		instance_create_depth(0, 0, depth-1, RousrTitleClose);
	
	with (RousrTitleBarClose)
		depth = other.depth-1;
}