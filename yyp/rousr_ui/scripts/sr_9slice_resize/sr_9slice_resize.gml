///@function sr_9slice_resize(_id, _width, _height)
///@desc helper functino to resize a frame
///@param {Real} _id
///@param {Real} _width
///@param {Real} _height
var _id = argument0, 
    _w  = argument1, 
		_h  = argument2;

with (_id) {
	Frame_width = _w;
	Frame_height = _h;
	__Frame_dirty = true;
}