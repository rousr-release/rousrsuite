///@function __sr_textfield_event_create()
///@desc Create Event for RousrTextfield Object
///@extensionizer { "docs": false }
Alignment      = TextAlign.Left;
Is_editing     = false;
Field_width    = 100;
Field_height   = 7;
On_submit      = noone;
On_edit        = noone;
Hide_on_blur   = true
Submit_on_blur = true;
Text           = "";
Field_fg       = c_gray;
Field_bg       = c_dkgray;

image_xscale = Field_width;
image_yscale = Field_height;
image_blend  = Field_bg;

if (Hide_on_blur)
  instance_deactivate_object(id);