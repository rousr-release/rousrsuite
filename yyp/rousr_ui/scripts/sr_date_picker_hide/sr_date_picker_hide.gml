///@function sr_date_picker_hide()
///@desc Hide datePicker and its fields
var _datePicker = global.__rousr_active_date_picker;

with(_datePicker) {
  for(var i = 0; i < EDatePicker.FieldCount; ++i)
    instance_deactivate_object(Input_fields[i]);

  visible = false;
  instance_deactivate_object(id);
}