///@function __sr_dragger_left_released()
///@desc call when releasing left button
///@extensionizer { "docs": false }
if (__rousr_dragger_drag_state != EDragState.None) {
  __rousr_dragger_drag_state = EDragState.None;
  with (RousrDragger) {
		if (Dragging == other.id)
			Dragging = noone;
	}
	
  depth = __rousr_dragger_start_depth; // revert just the depth, cause its what we arbitrarily changed
  if (__rousr_dragger_on_complete == noone || !script_execute(__rousr_dragger_on_complete, true))
    __sr_dragger_reset();
}