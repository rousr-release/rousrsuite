///@function rousr_ui_definitions()
///@desc Definitions / enums for rousr_ui
///@extensionizer { "export": false, "docs": false }
gml_pragma("global", "rousr_ui_definitions();");
gml_pragma("global", "global.___rousr_title_bar       = noone;");
gml_pragma("global", "global.___rousr_title_bar_close = noone;");
gml_pragma("global", "global.___rousr_anchor          = noone;");
gml_pragma("global", "global.___rousr_drag_manager    = noone;");

#macro RousrTitleBar        (global.___rousr_title_bar)
#macro RousrTitleBarClose   (global.___rousr_title_bar_close)
#macro RousrAnchor          (global.___rousr_anchor)
#macro RousrDragger         (global.___rousr_drag_manager)

#region RousrAnchor

/// rousr_anchor
/// definitions, constants, enums

enum ERousrAnchor {
  BottomLeft = 0,
  Bottom,
  BottomRight,
  Left,
  Center,
  Right,
  TopLeft,
  Top,
  TopRight,
	
	Num
}

enum ERousrAnchorSize {
  ApplicationSurface = 0,
  Display,
  Manual,
  
  Num,
};

#endregion
#region Dragger

/// rousr_dragger
/// definitions, constants, enums

#macro RousrDraggerDefaultSlack (5)

enum EDragState {
  None = 0,
	Begin,
	Dragging,
	
	Num
};

/// TextAlignment
enum TextAlign {
  Left = 0,
  Right,
  Center,
	
	Num
};

#endregion
#region 9Slice

enum EFrameParts {
	TL = 0, T,	TR,
	L,      C,  R,
	BL,     B,  BR,
	
	Num
}

#endregion
#region Datepicker
gml_pragma("global", "global.__rousr_active_date_picker = noone;");
enum EDatePicker {
  None = -1,
  
  Year = 0,
  Month,
  Day,
  
  FieldCount,
  
};

global.Week_day_name = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday"
];

global.Month_name = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

#endregion
#region TextFIeld
gml_pragma("global", "global.___rousr_active_textfield = noone");
#endregion