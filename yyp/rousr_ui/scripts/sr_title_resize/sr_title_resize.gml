///@function sr_title_resize(_draw_gui, _scale)
///@desc resize the title bar for draw gui / draw mode, and update the scale.
///@param {Boolean} _draw_gui
///@param {Real} _scale
var _draw_gui = argument0,
    _scale = argument1;
		
with (RousrTitleBar) {
	Draw_gui = _draw_gui;
	Scale    = _scale;	
	__last_draw_gui = Draw_gui;
	
	__sr_title_resize();
	
	with (RousrTitleBarClose)	{
		Draw_gui = _draw_gui;
		Scale    = _scale;
		sr_anchor_top_right();
		__last_draw_gui = Draw_gui;
	}
}