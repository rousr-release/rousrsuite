///@function __sr_drag_manager_event_step()
///@desc Step Event for RousrDragManager Object
///@extensionizer { "docs": false }

// reset dragger on button release
if (mouse_check_button_released(mb_left)) {
	__sr_dragger_reset(Dragging);
  Dragging = noone;
}