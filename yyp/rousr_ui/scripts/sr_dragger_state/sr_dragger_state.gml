///@function sr_dragger_state(_id)
///@desc Conveinence function for state
///@param {Real} _id   instance id for dragger
///@returns {Boolean} drag state for `_id`
var _id = argument[0];
var drag_state = EDragState.None;
with (_id) drag_state = __rousr_dragger_drag_state;
return drag_state;