///@function sr_date_picker_show(_show, [_x], [_y], [_date_time])
///@desc show the date picker at the given position
///      position adjusts to screen edges
///@param x
///@param y
///@param(opt) dateTime
var _show = argument[0];

if (_show) {
	var _x = argument[0];
	var _y = argument[1];
	var _date = argument_count > 2 ? argument[2] : date_current_datetime();
	var _onEdit = argument_count >3 ? argument[3] : noone;

	var datePicker = global.__rousr_active_date_picker;
	instance_activate_object(datePicker);
	datePicker.visible = true;
	datePicker.Date = _date;
	datePicker.Dirty = true;
	datePicker.Expanded = EDatePicker.None;
	datePicker.x = _x;
	datePicker.y = _y;
	datePicker.On_edit = _onEdit;
}