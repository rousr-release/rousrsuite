///@desc call *with* an object you'd like to make draggable
///@param (opt=noone)onDragComplete(bool)
///@param (opt=noone)onDragCheck
///@param (opt=noone)onDragBegin
///@param (opt=Default)draggerSlack
__rousr_dragger_drag_state =  EDragState.None;
__rousr_dragger_start_x      = -1;
__rousr_dragger_start_y      = -1;
__rousr_dragger_start_depth  = depth;

__rousr_dragger_drag_start_x  = -1;
__rousr_dragger_drag_start_y  = -1;
__rousr_dragger_drag_depth   = undefined;
__dragger_depthOffset = -5;

__rousr_dragger_on_complete  = argument_count > 0 ? argument[0] : noone;
__rousr_dragger_on_check     = argument_count > 1 ? argument[1] : noone;
__rousr_dragger_on_begin     = argument_count > 2 ? argument[2] : noone;
__rousr_dragger_slack        = argument_count > 3 ? argument[3] : RousrDraggerDefaultSlack;