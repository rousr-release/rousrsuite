///@function __sr_title_bar_event_create()
///@desc Create Event for the Title Bar Object
///@extensionizer { "docs": false }
if (!sr_ensure_singleton("___rousr_title_bar"))
  return;

Drag_state     = EDragState.None;
Drag_start_x   = 0;
Drag_start_y   = 0;

Scale    = 1;
Draw_gui = true;

Icon_sprite       = noone;
Background_sprite = noone;

Title          = "";
Title_color_fg = c_black;
Title_color_bg = c_gray;
Title_font     = fnt_game;

Title_x = 1;	
Title_y = Scale;

On_close   = sr_game_quit;
Esc_closes = true;
Esc_key    = vk_escape;

sr_anchor_stretch_top();

sr_title_init("");
with (RousrTitleBarClose)
  depth = other.depth - 1;	