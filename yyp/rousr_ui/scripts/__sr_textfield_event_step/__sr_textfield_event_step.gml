///@function __sr_textfield_event_step
///@desc Step Event for RousrTextfield Object
///@extensionizer { "docs": false }
if (mouse_check_button_pressed(mb_left)) {
	var is_over = position_meeting(mouse_x, mouse_y, id);
	if (is_over)
		sr_textfield_toggle_edit(true);
}

if (Is_editing) {
  sr_textfield_active(id);
    
  var edit = false;
  if (keyboard_check_pressed(vk_backspace)) {
    if (Text != "") {
      Text = string_delete(Text, string_length(Text), 1);
      edit = true;
    }
  } else if (keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_return) || keyboard_check_pressed(vk_escape)) {
		sr_textfield_toggle_edit(!keyboard_check_pressed(vk_escape));
	}
	
	if (keyboard_string != "") {
    Text += keyboard_string;
    edit = true;
  }
  
  if (edit && On_edit != noone)
    script_execute(On_edit, id);
    
  keyboard_string = ""
}

image_xscale = Field_width;
image_yscale = Field_height;