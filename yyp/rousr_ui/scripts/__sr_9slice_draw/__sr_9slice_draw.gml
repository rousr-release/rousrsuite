///@function __sr_9slice_draw()
///@desc draw a 9slice **Called `with(9slice)`**
///@desc just draw it if no surface param is passed
///@param _sub_image_offset
///@param _optTrue_SurfaceDraw
///@extensionizer { "docs": false }
var _sub_image_offset = argument_count > 0 ? argument[0] : 0;
var _draw_surface = argument_count > 1 ? argument[1] : true;
var surface_updated = false;

__Frame_dirty |= _sub_image_offset != __Last_offset;

if (Frame_surface != undefined) {
  var pad_width = Frame_surface_pad_left + Frame_surface_pad_right;
  var pad_height = Frame_surface_pad_top + Frame_surface_pad_bottom;
  if (!surface_exists(Frame_surface)) {
    Frame_surface = surface_create(sr_next_pot(Frame_width + pad_width), sr_next_pot(Frame_height + pad_height));
    __Frame_dirty = true;  
  
    __frameSurfaceW = Frame_width + pad_width;
    __frameSurfaceH = Frame_height + pad_height;
  }
  
  if (__Frame_dirty) {
    if (__frameSurfaceW != Frame_width + pad_width ||
        __frameSurfaceH != Frame_height + pad_height) {
      surface_resize(Frame_surface, sr_next_pot(Frame_width + pad_width), sr_next_pot(Frame_height + pad_height));
      __frameSurfaceW = Frame_width  + pad_width;
      __frameSurfaceH = Frame_height + pad_height;
    }
    surface_set_target(Frame_surface);
    draw_clear_alpha(c_black, 0);
  }
}

if (__Frame_dirty || Frame_surface == undefined) {
  var xx           = Frame_surface == undefined ? x : Frame_surface_pad_left;
  var yy           = Frame_surface == undefined ? y : Frame_surface_pad_top;
  var sw           = Frame_sprite_width;
  var sh           = Frame_sprite_height;
  var cx_scale     = __Center_width;
  var cy_scale     = __Center_height;
  var cw           = cx_scale * sw;
  var ch           = cy_scale * sh;
  
  draw_sprite_ext(Frame_sprite, EFrameParts.TL + _sub_image_offset, xx,           yy,           1,            1,            0, c_white, 1);
  draw_sprite_ext(Frame_sprite, EFrameParts.T  + _sub_image_offset, xx + sw,      yy,           cx_scale, 1,            0, c_white, 1);
  draw_sprite_ext(Frame_sprite, EFrameParts.TR + _sub_image_offset, xx + sw + cw, yy,           1,            1,            0, c_white, 1);
  draw_sprite_ext(Frame_sprite, EFrameParts.L  + _sub_image_offset, xx,           yy + sh,      1,            cy_scale, 0, c_white, 1);
  draw_sprite_ext(Frame_sprite, EFrameParts.C  + _sub_image_offset, xx + sw,      yy + sh,      cx_scale, cy_scale, 0, c_white, 1);
  draw_sprite_ext(Frame_sprite, EFrameParts.R  + _sub_image_offset, xx + sw + cw, yy + sh,      1,            cy_scale, 0, c_white, 1);
  draw_sprite_ext(Frame_sprite, EFrameParts.BL + _sub_image_offset, xx,           yy + sh + ch, 1,            1,            0, c_white, 1);
  draw_sprite_ext(Frame_sprite, EFrameParts.B  + _sub_image_offset, xx + sw,      yy + sh + ch, cx_scale, 1,            0, c_white, 1);
  draw_sprite_ext(Frame_sprite, EFrameParts.BR + _sub_image_offset, xx + sw + cw, yy + sh + ch, 1,            1,            0, c_white, 1);

  if (Frame_surface != undefined) {
    surface_updated = true;
    surface_reset_target();
  }
}

if (_draw_surface) __sr_9slice_draw_surface();

__Frame_dirty = false;
__Last_offset = _sub_image_offset;

return surface_updated;