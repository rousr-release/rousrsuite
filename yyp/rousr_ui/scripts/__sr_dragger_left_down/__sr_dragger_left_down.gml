///@function __sr_dragger_left_down()
///@desc react to mouse left down, doing the heavy lifting of the drag
///@extensionizer { "docs": false }
if (!visible) {
  if (__rousr_dragger_drag_state != EDragState.None) 
		__sr_dragger_reset(id);
	return;
}

var current_drag = noone;
with (RousrDragger)
	current_drag = Dragging;
    
switch (__rousr_dragger_drag_state) {
	case EDragState.None: 
		if (current_drag != noone || !mouse_check_button_pressed(mb_left)|| (__rousr_dragger_on_check != noone && !script_execute(__rousr_dragger_on_check)))
			return;
		
		__rousr_dragger_start_x     = x;
		__rousr_dragger_start_y     = y;
		__rousr_dragger_start_depth = depth;
		
    __rousr_dragger_has_moved = false;
		__rousr_dragger_drag_start_x = mouse_x;
		__rousr_dragger_drag_start_y = mouse_y;
		
		__rousr_dragger_last_mouse_x = mouse_x;
		__rousr_dragger_last_mouse_y = mouse_y;
      
    __rousr_dragger_drag_state = EDragState.Begin;
		break;
	
	case EDragState.Begin:
    if (__rousr_dragger_last_mouse_x != mouse_x || __rousr_dragger_last_mouse_y != mouse_y)
      __rousr_dragger_has_moved = true;
      
    if (current_drag != noone) {
      if (current_drag != id) 
				__rousr_dragger_drag_state = EDragState.None;
    } else if (point_distance(__rousr_dragger_start_x, __rousr_dragger_start_y, mouse_x, mouse_y) > __rousr_dragger_slack && __rousr_dragger_has_moved) {
			__rousr_dragger_drag_state = EDragState.Dragging;
			
			with (RousrDragger)
				Dragging = other.id;
		
      depth = !is_undefined(__rousr_dragger_drag_depth) ? __rousr_dragger_drag_depth : (depth + __dragger_depthOffset);
			
			__rousr_dragger_last_mouse_x = __rousr_dragger_drag_start_x;
			__rousr_dragger_last_mouse_y = __rousr_dragger_drag_start_y;
      
      if (__rousr_dragger_on_begin != noone) script_execute(__rousr_dragger_on_begin);
		}
		break;
		
	default: break;
}