///@function sr_dragger_set_on_check(_id, _on_complete)
///@desc set the on check callback after the fact
///@param {Real} _id         dragger id
///@param {Real} _on_check   **Format:** `function()->No Return`
with (argument[0])
	__rousr_dragger_on_check = argument[1];
