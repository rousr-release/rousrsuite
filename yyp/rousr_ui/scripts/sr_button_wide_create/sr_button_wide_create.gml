///@function sr_button_wide_create(_label, _click_action, [_depth], [_user_data])
///@desc create a new button
///@param {String} _label                         label to use with button
///@param {Real} _click_action                    script to call when clicked **Format:** `function(_button_instance, id, user_data) -> No Return`
///@param {Real} _center_sprite                   index of center sprite, or left+right if those aren't included
///@param {Real} [_left_sprite=_center_sprite]    index of left sprite, or left+right if those aren't included
///@param {Real} [_right_sprite=_center_sprite]   index of right sprite, or left+right if those aren't included
///@param {Real} [_can_click=noone]               script to check if this button can be clicked, besides normal button rules. **Format:** `function(_button, _user_data) -> {Boolean} _can_click`
///@param {Real} [_depth=depth]                   depth to create at (just set layer afterwards if layer desired)
///@param {*} [_user_data]                        passed along to click fucnction (use an id to use one button callback for multiple buttons, etc)
///@returns {Real} id of button or noone if not created
var _label     = argument[0],
    _click     = argument[1],
		_center    = argument[2],
		_left      = argument_count > 3 ? argument[3] : _center,
		_right     = argument_count > 4 ? argument[4] : _center,
		_can_click = argument_count > 5 ? argument[5] : noone,
		_depth     = argument_count > 6 ? argument[6] : depth,
		_user_data = argument_count > 7 ? argument[7] : undefined;
		
var button = sr_button_create(_label, _click, _can_click, _depth, _user_data);
with (button) {
	Content   = _center;
	Left_side = _left;
	Right_side = _right;
}

return button;