///@function sr_title_create(_title, [_fg_color=0x639bff], [_bg_color=0x5b6ee1], [_close_action=sr_game_quit], [_esc_closes=true]);
///@desc create a new rousr title
///@param {String} _title                       title string to show
///@param {Real} [_fg_color=0x639bff]	          color for title text
///@param {Real} [_bg_color=0x5b6ee1]           color for title text shadow
///@param {Real} [_close_action=sr_game_quit]   action to call on close button, or `undefined` for no close button.
///@param {Boolean} [_esc_closes=true]          escape key also calls close action
if (!instance_exists(RousrTitleBar))
	instance_create_depth(0, 0, -5000, RousrTitle);
	
with (RousrTitleBar) {
	sr_title_init(argument[0], 
		            argument_count > 1 ? argument[1] : sr_color_hex($639bff), 
								argument_count > 2 ? argument[2] : sr_color_hex($5b6ee1), 
								Title_font,
								Icon_sprite,
								Background_sprite,
								argument_count > 3 ? argument[3] : sr_game_quit,
								argument_count > 4 ? argument[4] : true,
								true,
								3);
}
						  