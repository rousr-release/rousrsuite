///@function __sr_anchor_refresh_display(_display_width, _display_height)
///@desc refresh the display, readjusting all the instances registered
///@param {Real} _display_width   display width to use
///@param {Real} _display_height  display height to use
///@extensionizer { "docs": false }
var _display_width = argument0;
var _display_height = argument1;

with (RousrAnchor) {
  Display_width = _display_width;
  Display_height = _display_height;
  
  var num_anchors = ds_list_size(Anchor_list);
  for (var i = 0; i < num_anchors; ++i) {
    var anchor_inst = Anchor_list[|i];
    var inst = anchor_inst[0];
    var scr = anchor_inst[1];
    var params = anchor_inst[2];
    with (inst) {
      sr_execute(scr, params);
    }
  }
}