///@function __sr_button_event_step()
///@desc Step Event for RousrButton Object
///@extensionizer { "docs": false }
var left_down     = mouse_check_button(mb_left);
var left_released = mouse_check_button_released(mb_left);
var is_over       = position_meeting(mouse_x, mouse_y, id);
				
if (Draw_gui) {
	var mx = device_mouse_x_to_gui(0);
	var my = device_mouse_y_to_gui(0);
	
	is_over = point_in_rectangle(mx, my, x, y, x+sprite_width, y+sprite_height);
}


if (sr_button_can_click()) {
	if (is_over) {
		if (left_down) {
			image_index = Down_index;
		} else if (left_released) {
			if (image_index == Down_index && Click_action != noone)
				script_execute(Click_action, id, User_data);
			image_index = Up_index;
		}
	}
}

if (left_released) {
	if (image_index == Up_index) 
		Was_clicked = false;
}

if (is_over) {
	if (!Was_over)
		Can_click = Click_enabled && (Was_clicked || !left_down);
} else if (Was_over) {
	Was_clicked = image_index == Down_index;
	Can_click   = false;
	image_index = Up_index;
}

Was_over = is_over;