///@desc used by Button, returns whether button click is legal for button
///intended to be called *with* Button instance
if (!Can_click || !Click_enabled)
  return false;
 
if (!visible)
  return false;
 
if (Click_check != noone && !script_execute(Click_check, id, User_data))
  return false;
  
return true;