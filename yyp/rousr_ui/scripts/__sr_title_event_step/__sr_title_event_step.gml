///@function __sr_title_event_step()
///@desc Step Event for RousrTitleBar Object
///@extensionizer { "docs": false }
var left_down     = mouse_check_button(mb_left);
var left_released = mouse_check_button_released(mb_left);
var is_over       = position_meeting(mouse_x, mouse_y, id);
var is_over_close = instance_position(mouse_x, mouse_y, RousrTitleBarClose) != noone;

if (Draw_gui != __last_draw_gui) {
	__sr_title_resize();
	__last_draw_gui = Draw_gui;
}


if (Draw_gui) {
	var mx = device_mouse_x_to_gui(0);
	var my = device_mouse_y_to_gui(0);
	
	is_over = point_in_rectangle(mx, my, x, y, x+sprite_width, y+sprite_height);
}

with (RousrTitleBarClose) {
	if (Draw_gui)  {
		var mx = device_mouse_x_to_gui(0);
		var my = device_mouse_y_to_gui(0);

		is_over_close = point_in_rectangle(mx, my, x , y, x+sprite_width, y+sprite_height);
	}
}

if (left_down) {
	if (Drag_state == EDragState.Dragging) {
		var mx = window_mouse_get_x();
		var my = window_mouse_get_y();
		var win_x = window_get_x();
		var win_y = window_get_y();
		var new_x = win_x + (mx - Drag_start_x);
		var new_y = win_y + (my - Drag_start_y);
		window_set_position(new_x, new_y);
			
		Drag_start_x = mx - (new_x - win_x);
		Drag_start_y = my - (new_y - win_y);
	}
	
	if (is_over) {
		/// @desc Manually drag window
		switch (Drag_state) {
			case EDragState.None: 
				if (!is_over_close) {
					Drag_start_x = window_mouse_get_x();
					Drag_start_y = window_mouse_get_y();
					Drag_state = EDragState.Begin;
				}	
				break;
	
			case EDragState.Begin:
				var mx = window_mouse_get_x(); var my = window_mouse_get_y();
			  if (mx < 0 || my < 0 || mx > window_get_width() || my > window_get_height())
			    Drag_state = EDragState.None;
      
				if (point_distance(Drag_start_x, Drag_start_y, mx, my) > 5)
					Drag_state = EDragState.Dragging;
				break;
		
			default: break;
		}
	}
}

if (left_released)
	Drag_state = EDragState.None;