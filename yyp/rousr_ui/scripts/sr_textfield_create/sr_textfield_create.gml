///@function sr_textfield_create([_text=""], [_depth=depth], [_obj=RousrTextfield])
///@desc create a new textfield object
///@param {String} [_text=""]            text for textfield to default to
///@param {Real} [_depth=depth           depth to create at, or the calling object's depth
///@param {Real} [_obj=RousrTextfield]   not really for you, man.
///@returns {Real} _textfield_d
var _text  = argument_count > 0 ? argument[0] : "",
    _depth = argument_count > 1 ? argument[1] : depth,
		_obj   = argument_count > 2 ? argument[2] : RousrTextfield;

return instance_create_depth(x, y, _depth, _obj);