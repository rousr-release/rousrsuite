///@function sr_9slice_init([_sprite_index=sprite_index], [_use_surface=true], [_width=image_xscale], [_height=image_yscale])
///@desc initialize an instance with the appropriate fields
///@param [_sprite_index=sprite_index]   new `sprite_index`
///@param [_use_surface=true]            use surface to draw
///@param [_width=image_xscale]	         width of the entire frame
///@param [_height=image_yscale]         height of the entire frame
Frame_sprite  = argument_count > 0  ? argument[0] : sprite_index;
Frame_surface = argument_count < 1 || argument[1] ? -1 : undefined;

// Using image_xscale as a way to determine the intentional width/height lets us use the room editor
Frame_width  = argument_count > 2 ? argument[2] : image_xscale;
Frame_height = argument_count > 3 ? argument[3] : image_yscale;

Frame_sprite_width  = sprite_get_width(Frame_sprite);
Frame_sprite_height = sprite_get_height(Frame_sprite);

image_xscale = Frame_width;
image_yscale = Frame_height;
sprite_index = Frame_sprite;

// trigger redraw
__Surface_width  = -1;
__Surface_height = -1;
__Frame_dirty    = true;