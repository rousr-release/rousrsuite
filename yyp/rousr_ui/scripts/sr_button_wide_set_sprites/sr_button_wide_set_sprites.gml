///@function sr_button_wide_set_sprites(_button_wide, _center, [_left=_center], [_right=_center], [_center_frame=0], [_left_frame=0], [_right_frame=0])
///@desc set the sprites and frame indices for a wide button
///@param {Real} _button_wide         instance id of button wide
///@param {Real} _center              `sprite_index` to use for center of button
///@param {Real} [_left=_center]      `sprite_index` to use for left side of button
///@param {Real} [_right=_center]			`sprite_index` to use for right side of button
///@param {Real} [_center_frame=0]    `image_index` to use for center of button (expects the 'down' version to be the next `image_index`)
///@param {Real} [_left_frame=0]			`image_index` to use for left of button	(expects the 'down' version to be the next `image_index`)
///@param {Real} [_right_frame=0]			`image_index` to use for right of button (expects the 'down' version to be the next `image_index`)
///@return {Real} _button_wide
var _button_wide = argument[0];

with (_button_wide) {
	var _center = argument[1],
	    _left   = argument_count > 2 ? argument[2] : _center,
			_right  = argument_count > 3 ? argument[3] : _center;
			
	var _center_frame = argument_count > 4 ? argument[4] : 0,
	    _left_frame   = argument_count > 5 ? argument[5] : 0,
			_right_frame  = argument_count > 6 ? argument[6] : 0;
			
	Content = _center;   Content_frame = _center_frame;
	Left_side = _left;   Left_frame    = _left_frame;
	Right_side = _right; Right_frame   = _right_frame;
}

return _button_wide;