///@function sr_anchor_get_height(_instance)
///@desc helper fucntion to best guess height
///@param {Real} _instance
///@returns {Real} height of the instance
var _id = argument0;
var height = 0;

if (variable_instance_exists(_id, "height")) {
  height = variable_instance_get(_id, "height");
} else {
  with(_id) {
    if (mask_index == -1) 
      height = sprite_height;
    else
      height = bbox_bottom - bbox_top;
  }
}

return height;