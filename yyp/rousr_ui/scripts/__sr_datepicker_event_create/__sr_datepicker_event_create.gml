///@function __sr_datepicker_event_create()
///@desc Create Event for RousrDatepicker Object
///@extensionizer { "docs": false }
Draw_gui  = false;
Dirty     = true;
Last_date = noone;
Date      = noone;

Month = 0;
Day   = 0;
Year  = 0;

Dim   = 0;
Dow   = 0;
First = 0;

Font     = fnt_game;
Expanded = EDatePicker.None;

On_input_field_submit = noone;

Input_fields = [ ];
for (var field_index = 0; field_index < EDatePicker.FieldCount; ++field_index) {
  Input_fields[@ field_index] = sr_textfield_create();
  Input_fields[@ field_index].visible = false;
  Input_fields[@ field_index].Hide_on_blur = false;
}

global.__rousr_active_date_picker = id;