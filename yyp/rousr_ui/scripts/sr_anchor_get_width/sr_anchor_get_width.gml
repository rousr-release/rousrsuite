///@function sr_anchor_get_width(_instance)
///@desc helper fucntion to best guess width
///@param {Real} _instance
///@returns {Real} anchored instance's width
var _id = argument0;
var width = 0;

if (variable_instance_exists(_id, "width")) {
  width = variable_instance_get(_id, "width");
} else {
  with(_id) {
    if (mask_index == -1) 
      width = sprite_width;
    else
      width = bbox_right - bbox_left;
  }
}

return width;