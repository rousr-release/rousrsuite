///@function sr_textfield_active([_new_active])
///@desc Return the active textfield or set a new one
///@param {Real} [_new_active]   if passed, sets the new textfield
///@returns {Real} current active textfield
if (argument_count > 0)
	global.___rousr_active_textfield = argument[0];

return global.___rousr_active_textfield;