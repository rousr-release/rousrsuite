{
    "id": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "defd9d29-eeba-4f8d-8b86-a9e5918e8bfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "compositeImage": {
                "id": "48136512-de22-4b90-adfe-25aec34b336f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "defd9d29-eeba-4f8d-8b86-a9e5918e8bfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1b3f9c-ad5d-4bbd-9283-c44e1ac37b5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "defd9d29-eeba-4f8d-8b86-a9e5918e8bfc",
                    "LayerId": "26a6014b-ea55-4a45-a5b2-12e817605ef4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "26a6014b-ea55-4a45-a5b2-12e817605ef4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}