{
	"id": "da321195-6230-4870-b017-c16bac16ebc5",
	"modelName": "GMSprite",
	"mvc": "1.12",
	"name": "sTitleBar_BgStrip",
	"For3D": false,
	"HTile": false,
	"VTile": false,
	"bbox_bottom": 32,
	"bbox_left": 0,
	"bbox_right": 1,
	"bbox_top": 0,
	"bboxmode": 0,
	"colkind": 1,
	"coltolerance": 0,
	"edgeFiltering": false,
	"frames": [
		{
			"id": "de0da314-c3a3-41e6-b388-24cd53ede39f",
			"modelName": "GMSpriteFrame",
			"mvc": "1.0",
			"SpriteId": "da321195-6230-4870-b017-c16bac16ebc5",
			"compositeImage": {
				"id": "57d48245-876d-44da-8041-e9135d62a435",
				"modelName": "GMSpriteImage",
				"mvc": "1.0",
				"FrameId": "de0da314-c3a3-41e6-b388-24cd53ede39f",
				"LayerId": "00000000-0000-0000-0000-000000000000"
			},
			"images": [
				{
					"id": "22bcf123-3316-485f-a166-59f3fb4daabe",
					"modelName": "GMSpriteImage",
					"mvc": "1.0",
					"FrameId": "de0da314-c3a3-41e6-b388-24cd53ede39f",
					"LayerId": "26f0e9ae-a0e6-4024-b075-7373cd7dc70b"
				}
			]
		}
	],
	"gridX": 0,
	"gridY": 0,
	"height": 33,
	"layers": [
		{
			"id": "26f0e9ae-a0e6-4024-b075-7373cd7dc70b",
			"modelName": "GMImageLayer",
			"mvc": "1.0",
			"SpriteId": "da321195-6230-4870-b017-c16bac16ebc5",
			"blendMode": 0,
			"isLocked": false,
			"name": "default",
			"opacity": 100,
			"visible": true
		}
	],
	"origin": 0,
	"originLocked": false,
	"playbackSpeed": 15,
	"playbackSpeedType": 0,
	"premultiplyAlpha": false,
	"sepmasks": false,
	"swatchColours": null,
	"swfPrecision": 0,
	"textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
	"type": 0,
	"width": 2,
	"xorig": 0,
	"yorig": 0
}