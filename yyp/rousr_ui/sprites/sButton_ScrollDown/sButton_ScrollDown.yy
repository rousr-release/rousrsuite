{
    "id": "496964d2-502b-47dd-9806-e61a3363450f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton_ScrollDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "979f0566-fec0-4e25-b44a-be94c2509af3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496964d2-502b-47dd-9806-e61a3363450f",
            "compositeImage": {
                "id": "228d3f85-fdce-4872-b225-2e502dad3196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "979f0566-fec0-4e25-b44a-be94c2509af3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58099442-cd82-4995-a1df-39fcd63eb33a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "979f0566-fec0-4e25-b44a-be94c2509af3",
                    "LayerId": "1e0c1dd3-a73a-407c-9d82-cb6c8cb150f2"
                }
            ]
        },
        {
            "id": "14be12bc-e962-4990-a48b-bcb4909bb428",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "496964d2-502b-47dd-9806-e61a3363450f",
            "compositeImage": {
                "id": "7eca0d41-2c7c-45c1-b394-ce4e10829160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14be12bc-e962-4990-a48b-bcb4909bb428",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a08d213f-a698-40fd-b914-7d41040f67eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14be12bc-e962-4990-a48b-bcb4909bb428",
                    "LayerId": "1e0c1dd3-a73a-407c-9d82-cb6c8cb150f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1e0c1dd3-a73a-407c-9d82-cb6c8cb150f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "496964d2-502b-47dd-9806-e61a3363450f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}