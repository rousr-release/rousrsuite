{
    "id": "bb1cb85a-07cd-4753-8e3d-c62186c76419",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sScrollArea_Piece",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5abd1cc7-5a9e-4264-8180-93a7d1c420ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb1cb85a-07cd-4753-8e3d-c62186c76419",
            "compositeImage": {
                "id": "cad794b1-29da-48a8-895a-6d5ba57d79cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5abd1cc7-5a9e-4264-8180-93a7d1c420ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45ccf5a3-9515-4f0e-a807-67c765073e1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5abd1cc7-5a9e-4264-8180-93a7d1c420ad",
                    "LayerId": "55e5f31a-6328-4945-b4c2-6297433509c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "55e5f31a-6328-4945-b4c2-6297433509c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb1cb85a-07cd-4753-8e3d-c62186c76419",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}