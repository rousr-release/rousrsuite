{
    "id": "eff29bc9-4f21-4617-b722-bb8a3210a527",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bd22035-b745-47ab-93a0-312150054d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "compositeImage": {
                "id": "a254356b-58c7-4021-bdf7-93857fe31a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd22035-b745-47ab-93a0-312150054d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7564062-c542-4ac9-ba34-a9c5740eabf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd22035-b745-47ab-93a0-312150054d56",
                    "LayerId": "567db933-0941-489c-9a7d-22b0ba464a9a"
                }
            ]
        },
        {
            "id": "8c66de8c-65fb-45f2-ba35-1e35c5ec301c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "compositeImage": {
                "id": "92c6642b-8227-4bf6-adc0-ee63581d3ec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c66de8c-65fb-45f2-ba35-1e35c5ec301c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e24719cf-5c5f-4bfe-8d2d-71c60824d108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c66de8c-65fb-45f2-ba35-1e35c5ec301c",
                    "LayerId": "567db933-0941-489c-9a7d-22b0ba464a9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "567db933-0941-489c-9a7d-22b0ba464a9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}