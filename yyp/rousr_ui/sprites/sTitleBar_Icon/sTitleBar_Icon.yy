{
	"id": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
	"modelName": "GMSprite",
	"mvc": "1.12",
	"name": "sTitleBar_Icon",
	"For3D": false,
	"HTile": false,
	"VTile": false,
	"bbox_bottom": 23,
	"bbox_left": 0,
	"bbox_right": 26,
	"bbox_top": 0,
	"bboxmode": 0,
	"colkind": 1,
	"coltolerance": 0,
	"edgeFiltering": false,
	"frames": [
		{
			"id": "b6ce6e3a-8e23-4335-b13b-ed7b7c60ce2e",
			"modelName": "GMSpriteFrame",
			"mvc": "1.0",
			"SpriteId": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
			"compositeImage": {
				"id": "02d60fd7-01c2-4023-aa85-e4b9d09da994",
				"modelName": "GMSpriteImage",
				"mvc": "1.0",
				"FrameId": "b6ce6e3a-8e23-4335-b13b-ed7b7c60ce2e",
				"LayerId": "00000000-0000-0000-0000-000000000000"
			},
			"images": [
				{
					"id": "e5647128-5442-4d52-b18f-01e26604383f",
					"modelName": "GMSpriteImage",
					"mvc": "1.0",
					"FrameId": "b6ce6e3a-8e23-4335-b13b-ed7b7c60ce2e",
					"LayerId": "02a508b5-9271-4433-92e6-0d30b3e6d4e7"
				}
			]
		}
	],
	"gridX": 0,
	"gridY": 0,
	"height": 24,
	"layers": [
		{
			"id": "02a508b5-9271-4433-92e6-0d30b3e6d4e7",
			"modelName": "GMImageLayer",
			"mvc": "1.0",
			"SpriteId": "4e34842b-5fe0-43c0-856c-fc38cba8d8aa",
			"blendMode": 0,
			"isLocked": false,
			"name": "default",
			"opacity": 100,
			"visible": true
		}
	],
	"origin": 0,
	"originLocked": false,
	"playbackSpeed": 15,
	"playbackSpeedType": 0,
	"premultiplyAlpha": false,
	"sepmasks": false,
	"swatchColours": null,
	"swfPrecision": 0,
	"textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
	"type": 0,
	"width": 27,
	"xorig": 0,
	"yorig": 0
}