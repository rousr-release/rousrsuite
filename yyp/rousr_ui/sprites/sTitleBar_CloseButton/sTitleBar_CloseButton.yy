{
	"id": "5c177cd7-3073-446d-9c3a-9b57da5eb5d6",
	"modelName": "GMSprite",
	"mvc": "1.12",
	"name": "sTitleBar_CloseButton",
	"For3D": false,
	"HTile": false,
	"VTile": false,
	"bbox_bottom": 29,
	"bbox_left": 0,
	"bbox_right": 35,
	"bbox_top": 0,
	"bboxmode": 0,
	"colkind": 1,
	"coltolerance": 0,
	"edgeFiltering": false,
	"frames": [
		{
			"id": "be0da435-0224-4cc3-ad69-eda441d59048",
			"modelName": "GMSpriteFrame",
			"mvc": "1.0",
			"SpriteId": "5c177cd7-3073-446d-9c3a-9b57da5eb5d6",
			"compositeImage": {
				"id": "16ccc3cc-f122-4adf-9e34-9f0a4c057df5",
				"modelName": "GMSpriteImage",
				"mvc": "1.0",
				"FrameId": "be0da435-0224-4cc3-ad69-eda441d59048",
				"LayerId": "00000000-0000-0000-0000-000000000000"
			},
			"images": [
				{
					"id": "40dd956e-180f-45f3-a233-cb827defd828",
					"modelName": "GMSpriteImage",
					"mvc": "1.0",
					"FrameId": "be0da435-0224-4cc3-ad69-eda441d59048",
					"LayerId": "90080c62-6f2a-4edd-b411-7405a3127024"
				}
			]
		},
		{
			"id": "dc419f06-061a-4efe-a51e-9a1d61becca5",
			"modelName": "GMSpriteFrame",
			"mvc": "1.0",
			"SpriteId": "5c177cd7-3073-446d-9c3a-9b57da5eb5d6",
			"compositeImage": {
				"id": "c484f28b-1a7f-480e-8776-180b6432a3af",
				"modelName": "GMSpriteImage",
				"mvc": "1.0",
				"FrameId": "dc419f06-061a-4efe-a51e-9a1d61becca5",
				"LayerId": "00000000-0000-0000-0000-000000000000"
			},
			"images": [
				{
					"id": "2aa494a4-c6f7-40d5-8780-3bb64bf7f82e",
					"modelName": "GMSpriteImage",
					"mvc": "1.0",
					"FrameId": "dc419f06-061a-4efe-a51e-9a1d61becca5",
					"LayerId": "90080c62-6f2a-4edd-b411-7405a3127024"
				}
			]
		}
	],
	"gridX": 0,
	"gridY": 0,
	"height": 30,
	"layers": [
		{
			"id": "90080c62-6f2a-4edd-b411-7405a3127024",
			"modelName": "GMImageLayer",
			"mvc": "1.0",
			"SpriteId": "5c177cd7-3073-446d-9c3a-9b57da5eb5d6",
			"blendMode": 0,
			"isLocked": false,
			"name": "default",
			"opacity": 100,
			"visible": true
		}
	],
	"origin": 0,
	"originLocked": false,
	"playbackSpeed": 0,
	"playbackSpeedType": 0,
	"premultiplyAlpha": false,
	"sepmasks": false,
	"swatchColours": null,
	"swfPrecision": 0,
	"textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
	"type": 0,
	"width": 36,
	"xorig": 0,
	"yorig": 0
}