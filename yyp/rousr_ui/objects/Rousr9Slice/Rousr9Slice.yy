{
    "id": "4edb1bcc-b168-4196-8207-f405754dfa4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Rousr9Slice",
    "eventList": [
        {
            "id": "0d0a4682-6bac-42d3-97ed-7130b483c0e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4edb1bcc-b168-4196-8207-f405754dfa4b"
        },
        {
            "id": "d26af952-1612-4848-a5cb-a4899787671f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4edb1bcc-b168-4196-8207-f405754dfa4b"
        },
        {
            "id": "fc95aa56-5fa2-4043-82d7-f6a9ffc66ae5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4edb1bcc-b168-4196-8207-f405754dfa4b"
        },
        {
            "id": "fe1b4f59-efe9-4297-af7b-67f17f4a499f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4edb1bcc-b168-4196-8207-f405754dfa4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "95e7d0df-35fd-42db-9fd3-29c388705675",
    "visible": true
}