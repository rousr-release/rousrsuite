{
    "id": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrTitleClose",
    "eventList": [
        {
            "id": "b4a0a4b8-7a04-4e45-a4ea-a41617cc69b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "7ccffd7a-a7f2-41cf-af0e-11a7c84c953b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "a15a700a-691c-457d-86fe-ccd550f800a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        },
        {
            "id": "24c222b1-f274-4a54-8ee6-fe2ee02f712b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1d65db7a-75d9-47ac-b4b5-64e8dbbca440"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5c177cd7-3073-446d-9c3a-9b57da5eb5d6",
    "visible": true
}