{
    "id": "c02cc007-a8a7-4a22-be3d-ace39b64c6b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrAnchorSystem",
    "eventList": [
        {
            "id": "21fe54f9-3f18-49a9-a559-388cdbbbec5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c02cc007-a8a7-4a22-be3d-ace39b64c6b4"
        },
        {
            "id": "c5341cf6-c202-4be9-8fee-9d4c84b1eafd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "c02cc007-a8a7-4a22-be3d-ace39b64c6b4"
        },
        {
            "id": "f23195a2-c709-4812-9d10-30774616aad5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c02cc007-a8a7-4a22-be3d-ace39b64c6b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}