///@desc 
var start_width = (string_width("START") + 15) * 2;
var xx = floor((room_width - start_width) * 0.5);
var yy = 50;

var quit_width = (string_width("QUIT") + 15) * 2;
xx = floor((room_width - quit_width) * 0.5);
Quit_game = instance_create_layer(xx, yy, "Instances", RousrButtonWide);
Quit_game.image_xscale = quit_width / 2;
Quit_game.Label = "QUIT";
Quit_game.Click_action = sr_game_quit;
	