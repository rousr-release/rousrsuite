#### `sr_ensure_singleton`   
**returns**: | {Boolean} false if an existing instance is destroyed - be sure to check CleanUp / Destroys   
```
sr_ensure_singleton()
```   
called `with` object to be singleton - ensure the name passed is a singleton, will call instance destroy on a different id

---

#### `sr_ensure_font`   
**returns**: | None   
```
sr_ensure_font()
```   
cache the current font

---

#### `sr_ensure_color`   
**returns**: | None   
```
sr_ensure_color()
```   
cache the current color

---

#### `sr_color_hex`   
**returns**: | {Real} color formatted in BBGGRR for GML   
```
sr_color_hex()
```   
[Convert the RGB to BGR](https://forum.yoyogames.com/index.php?threads/why-are-hex-colours-bbggrr-instead-of-rrggbb.16325/#post-105309)

---

#### `sr_color_glsl`   
| | | 
| -------------- | ------------------------------------------------------ |   
**[_out_array]** | {Array} optional out array to not allocate a new array   
**returns**:    | None   
```
sr_color_glsl([_out_array])
```   
convert a color to an array float values [0.0 - 1.0]

---

#### `sr_next_pot`   
**returns**: | {Real} nearest power of 2 to given integer   
```
sr_next_pot()
```   
return the nearest power of 2 for a given number (integer)

---

#### `sr_shadow_text`   
| | | 
| ----------- | -------------------------------- |   
**_y**        | {Real} y position to draw text   
**_text**     | {String} text to draw   
**_fg**       | {Real} foreground color   
**_bg**       | {Real} background (shadow) color   
**[_xoff=1]** | {Real} x offset for the shadow   
**[_yoff=1]** | {Real} y offset for the shadow   
**returns**: | None   
```
sr_shadow_text(_y, _text, _fg, _bg, [_xoff=1], [_yoff=1])
```   
Draw text with a shadow

---

#### `sr_outline_text`   
| | | 
| ---------- | -------------------------- |   
**_y**       | {Real} y position for text   
**_text**    | {String} text to draw   
**_fg**      | {Real} foreground color   
**_bg**      | {Real} background color   
**returns**: | None   
```
sr_outline_text(_y, _text, _fg, _bg)
```   
draw text with an oultine

---

#### `sr_error`   
| | | 
| ---------- | ---------------------- |   
**_text**    | {String} error message   
**returns**: | None   
```
sr_error(_text)
```   
Error wrapper (eventual logging system)

---

#### `sr_log`   
| | | 
| ---------- | -------------------- |   
**_text**    | {String} log message   
**returns**: | None   
```
sr_log(_text)
```   
Log wrapper (eventual logging system)

---

#### `sr_execute`   
| | | 
| --------------- | ------------------------------------------------------------------------------- |   
**_script_index** | {Real} - script to execute   
**_params**       | {Array} - parameters to call, in an array   
**[paramCount]**  | {Real} - if you've already got the count ready, pass it, or else it's computed.   
**returns**:     | {*} return value from script call   
```
sr_execute(_script_index, _params, [paramCount])
```   
false

---

#### `sr_aabb_contains_line`   
| | | 
| ------------- | ------- |   
**_line_y1**    | {Real}    
**_line_x2**    | {Real}    
**_line_y2**    | {Real}    
**_rect_min_x** | {Real}    
**_rect_min_y** | {Real}    
**_rect_max_x** | {Real}    
**_rect_max_y** | {Real}    
**returns**:   | {Boolean} true if an aabb and line intersect   
```
sr_aabb_contains_line(_line_y1, _line_x2, _line_y2, _rect_min_x, _rect_min_y, _rect_max_x, _rect_max_y)
```   
courtesy of https://stackoverflow.com/questions/1585525/how-to-find-the-intersection-point-between-a-line-and-a-rectangle

---

