{
	"id": "4b38b324-83b1-4553-84cb-5ec8ccc0d3d1",
	"modelName": "GMFont",
	"mvc": "1.0",
	"name": "fnt_game_3x",
	"AntiAlias": 0,
	"TTFName": null,
	"bold": false,
	"charset": 0,
	"first": 0,
	"fontName": "04b_24",
	"glyphs": [
		{
			"Key": 32,
			"Value": {
				"id": "3667bad2-54a5-4cf8-aa3f-225ed334cfd4",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 32,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 6,
				"x": 112,
				"y": 80
			}
		},
		{
			"Key": 33,
			"Value": {
				"id": "ffb8fe97-0bf7-4d7b-b0f9-99e690c38306",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 33,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 3,
				"x": 209,
				"y": 80
			}
		},
		{
			"Key": 34,
			"Value": {
				"id": "cf94f18e-4130-4bd5-a13e-820ef43595b8",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 34,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 101,
				"y": 54
			}
		},
		{
			"Key": 35,
			"Value": {
				"id": "a166df4d-83d1-4b48-bbc1-19c98ec17904",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 35,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 90,
				"y": 54
			}
		},
		{
			"Key": 36,
			"Value": {
				"id": "554a176e-f0b5-40df-b36b-80a938c60ae0",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 36,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 79,
				"y": 54
			}
		},
		{
			"Key": 37,
			"Value": {
				"id": "76722746-afdb-4211-a6f5-b6a89f1ff8d2",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 37,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 68,
				"y": 54
			}
		},
		{
			"Key": 38,
			"Value": {
				"id": "4a9eb2b1-5a82-478c-b6d6-b1103fe02da1",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 38,
				"h": 24,
				"offset": 0,
				"shift": 15,
				"w": 12,
				"x": 19,
				"y": 2
			}
		},
		{
			"Key": 39,
			"Value": {
				"id": "c84c2890-824e-48c2-a183-78c7d97866cd",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 39,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 3,
				"x": 204,
				"y": 80
			}
		},
		{
			"Key": 40,
			"Value": {
				"id": "25434573-ebba-47df-958b-0d9bafd824ac",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 40,
				"h": 24,
				"offset": 0,
				"shift": 9,
				"w": 6,
				"x": 168,
				"y": 80
			}
		},
		{
			"Key": 41,
			"Value": {
				"id": "fdb8cb6e-738b-4d13-a051-dfa807517ca9",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 41,
				"h": 24,
				"offset": 0,
				"shift": 9,
				"w": 6,
				"x": 160,
				"y": 80
			}
		},
		{
			"Key": 42,
			"Value": {
				"id": "92e6a94d-806d-4f77-bc03-13e7389cdcdd",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 42,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 24,
				"y": 54
			}
		},
		{
			"Key": 43,
			"Value": {
				"id": "72e44c42-85be-4619-bc4a-8e234d4bc006",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 43,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 13,
				"y": 54
			}
		},
		{
			"Key": 44,
			"Value": {
				"id": "214a2e75-d7a5-456e-bfc4-f599e6e4dc60",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 44,
				"h": 24,
				"offset": 0,
				"shift": 9,
				"w": 6,
				"x": 152,
				"y": 80
			}
		},
		{
			"Key": 45,
			"Value": {
				"id": "a8b109a0-ef9c-4aac-bd81-c46f6ff43b86",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 45,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 244,
				"y": 28
			}
		},
		{
			"Key": 46,
			"Value": {
				"id": "276f0c9b-0479-4bfd-b14f-600f57c1f9a6",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 46,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 3,
				"x": 199,
				"y": 80
			}
		},
		{
			"Key": 47,
			"Value": {
				"id": "7b66ae54-ade5-4bc2-8538-d4a95501f6a3",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 47,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 222,
				"y": 28
			}
		},
		{
			"Key": 48,
			"Value": {
				"id": "dc754b69-c1f3-4588-a530-6f10934da0dc",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 48,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 211,
				"y": 28
			}
		},
		{
			"Key": 49,
			"Value": {
				"id": "4edcb752-e15f-4a86-98e0-55cd8b7548eb",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 49,
				"h": 24,
				"offset": 0,
				"shift": 9,
				"w": 6,
				"x": 144,
				"y": 80
			}
		},
		{
			"Key": 50,
			"Value": {
				"id": "5bc4ed36-dd33-4498-9052-7991760b0e54",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 50,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 134,
				"y": 54
			}
		},
		{
			"Key": 51,
			"Value": {
				"id": "c556ce76-6d70-4264-aaf1-34ccacf0ebda",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 51,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 145,
				"y": 54
			}
		},
		{
			"Key": 52,
			"Value": {
				"id": "b380090f-9c01-40d1-8d8c-50870f50e806",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 52,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 156,
				"y": 54
			}
		},
		{
			"Key": 53,
			"Value": {
				"id": "3151b821-c97f-4ff1-9531-bd2440667b5b",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 53,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 79,
				"y": 80
			}
		},
		{
			"Key": 54,
			"Value": {
				"id": "96c7055a-5b58-487e-b68f-958af3677cac",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 54,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 68,
				"y": 80
			}
		},
		{
			"Key": 55,
			"Value": {
				"id": "4613eb46-a181-48ba-a8b0-1c938e163574",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 55,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 57,
				"y": 80
			}
		},
		{
			"Key": 56,
			"Value": {
				"id": "605168ca-8cfc-4c70-af91-fc1b71ca8220",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 56,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 46,
				"y": 80
			}
		},
		{
			"Key": 57,
			"Value": {
				"id": "a8c6ca46-4ab7-4626-9d43-ec6a1c22e831",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 57,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 35,
				"y": 80
			}
		},
		{
			"Key": 58,
			"Value": {
				"id": "379c3f61-2816-4dd0-867f-1e126bff401e",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 58,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 3,
				"x": 194,
				"y": 80
			}
		},
		{
			"Key": 59,
			"Value": {
				"id": "40862cbc-015f-42bc-9b78-7401a801fa43",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 59,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 3,
				"x": 189,
				"y": 80
			}
		},
		{
			"Key": 60,
			"Value": {
				"id": "656244fe-f95c-432a-b18e-1653a7e28433",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 60,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 90,
				"y": 80
			}
		},
		{
			"Key": 61,
			"Value": {
				"id": "22227186-5f1f-49cf-82ca-ce6ee17166b7",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 61,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 2,
				"y": 80
			}
		},
		{
			"Key": 62,
			"Value": {
				"id": "b7eca7c7-91a9-4dda-a6c0-82d6a3384df3",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 62,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 233,
				"y": 54
			}
		},
		{
			"Key": 63,
			"Value": {
				"id": "b3088536-4e15-45f7-9f28-09da1caabdc0",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 63,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 222,
				"y": 54
			}
		},
		{
			"Key": 64,
			"Value": {
				"id": "a339b39f-60df-4beb-a7c1-a680c4959860",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 64,
				"h": 24,
				"offset": 0,
				"shift": 18,
				"w": 15,
				"x": 2,
				"y": 2
			}
		},
		{
			"Key": 65,
			"Value": {
				"id": "6126fc92-9d31-4ab6-ab8c-b32194cac610",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 65,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 211,
				"y": 54
			}
		},
		{
			"Key": 66,
			"Value": {
				"id": "8997fb3a-8ede-4dbb-81b1-a744413977d3",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 66,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 200,
				"y": 54
			}
		},
		{
			"Key": 67,
			"Value": {
				"id": "8e41e657-e58b-4012-8233-7f684689470d",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 67,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 189,
				"y": 54
			}
		},
		{
			"Key": 68,
			"Value": {
				"id": "42d7af8b-c197-4d04-8506-4666bde21d91",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 68,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 178,
				"y": 54
			}
		},
		{
			"Key": 69,
			"Value": {
				"id": "35e4b09b-8d7c-43fd-b717-9ef3e05e159d",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 69,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 167,
				"y": 54
			}
		},
		{
			"Key": 70,
			"Value": {
				"id": "2634fb23-542b-4712-bb2a-dcc2e7b119ab",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 70,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 189,
				"y": 28
			}
		},
		{
			"Key": 71,
			"Value": {
				"id": "c2539d38-2ce7-46e0-b518-82bd5a141fd2",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 71,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 244,
				"y": 54
			}
		},
		{
			"Key": 72,
			"Value": {
				"id": "d7d7e6b2-3ae5-4208-a156-b374084138b0",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 72,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 178,
				"y": 28
			}
		},
		{
			"Key": 73,
			"Value": {
				"id": "bcb15099-0bab-4e1e-afb7-3e8799e1aefe",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 73,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 3,
				"x": 184,
				"y": 80
			}
		},
		{
			"Key": 74,
			"Value": {
				"id": "24f24f5b-ea00-4b57-b233-0c960e23c3b6",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 74,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 187,
				"y": 2
			}
		},
		{
			"Key": 75,
			"Value": {
				"id": "a9e90f52-6784-4d7f-b792-050c7404466e",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 75,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 176,
				"y": 2
			}
		},
		{
			"Key": 76,
			"Value": {
				"id": "a3bf4223-df2d-4dab-a0fe-cced16b9df71",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 76,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 165,
				"y": 2
			}
		},
		{
			"Key": 77,
			"Value": {
				"id": "dab1176a-02e1-4698-a4fc-af591cf819da",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 77,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 154,
				"y": 2
			}
		},
		{
			"Key": 78,
			"Value": {
				"id": "67b3c682-c8c4-40ed-9aff-b44ddb75eb25",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 78,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 143,
				"y": 2
			}
		},
		{
			"Key": 79,
			"Value": {
				"id": "e1a104ee-9f48-4d32-8ed4-0092aa87d0ca",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 79,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 33,
				"y": 2
			}
		},
		{
			"Key": 80,
			"Value": {
				"id": "d36d7b6f-b855-4ee6-bdeb-f3d3dfeea895",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 80,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 121,
				"y": 2
			}
		},
		{
			"Key": 81,
			"Value": {
				"id": "73ef8131-db54-49b5-a0d7-844f497e729e",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 81,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 110,
				"y": 2
			}
		},
		{
			"Key": 82,
			"Value": {
				"id": "9006a53e-005e-4279-9f4e-5717820aca1c",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 82,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 99,
				"y": 2
			}
		},
		{
			"Key": 83,
			"Value": {
				"id": "e6bd3488-07ad-4122-9b4d-ad324c6193fa",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 83,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 88,
				"y": 2
			}
		},
		{
			"Key": 84,
			"Value": {
				"id": "03504483-5ece-4ffd-996e-b636aaf9282c",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 84,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 77,
				"y": 2
			}
		},
		{
			"Key": 85,
			"Value": {
				"id": "90521bee-79d2-42c8-8175-1220276e137f",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 85,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 66,
				"y": 2
			}
		},
		{
			"Key": 86,
			"Value": {
				"id": "19c71e5d-2ea3-4f31-8c80-c960d688d314",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 86,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 55,
				"y": 2
			}
		},
		{
			"Key": 87,
			"Value": {
				"id": "012c0aea-42bf-46ad-b56e-328c5638df2f",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 87,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 44,
				"y": 2
			}
		},
		{
			"Key": 88,
			"Value": {
				"id": "20ba62bd-d728-44f5-90e4-ad9cb8e1b269",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 88,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 198,
				"y": 2
			}
		},
		{
			"Key": 89,
			"Value": {
				"id": "4f873a6a-e61f-4065-a363-e01d6b564f6a",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 89,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 209,
				"y": 2
			}
		},
		{
			"Key": 90,
			"Value": {
				"id": "f7be4601-f287-42a3-9b61-7ce785f4d3d9",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 90,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 220,
				"y": 2
			}
		},
		{
			"Key": 91,
			"Value": {
				"id": "cdfd1195-8eee-4439-b7c6-3336834ce0e8",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 91,
				"h": 24,
				"offset": 0,
				"shift": 9,
				"w": 6,
				"x": 136,
				"y": 80
			}
		},
		{
			"Key": 92,
			"Value": {
				"id": "baf6b25f-8f5b-4d43-9330-4ad3f03516b5",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 92,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 156,
				"y": 28
			}
		},
		{
			"Key": 93,
			"Value": {
				"id": "fd8a45e0-a176-4802-b4f6-40fa185e37b6",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 93,
				"h": 24,
				"offset": 0,
				"shift": 9,
				"w": 6,
				"x": 128,
				"y": 80
			}
		},
		{
			"Key": 94,
			"Value": {
				"id": "6991677b-bd31-4a3e-9fba-6a65c3e05649",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 94,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 134,
				"y": 28
			}
		},
		{
			"Key": 95,
			"Value": {
				"id": "6769ae64-0136-48c3-b777-819462843f2c",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 95,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 123,
				"y": 28
			}
		},
		{
			"Key": 96,
			"Value": {
				"id": "5622f96b-6116-43f8-8893-436414e48ac1",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 96,
				"h": 24,
				"offset": 0,
				"shift": 9,
				"w": 6,
				"x": 120,
				"y": 80
			}
		},
		{
			"Key": 97,
			"Value": {
				"id": "2cc295df-f997-4232-bef1-88e95f283830",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 97,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 101,
				"y": 28
			}
		},
		{
			"Key": 98,
			"Value": {
				"id": "26c77aef-893c-4781-811a-2df7e4b3fc68",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 98,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 90,
				"y": 28
			}
		},
		{
			"Key": 99,
			"Value": {
				"id": "a48ba2a9-33c8-4037-a968-0b84397c30d2",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 99,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 167,
				"y": 28
			}
		},
		{
			"Key": 100,
			"Value": {
				"id": "23f6a9e3-d3ba-4e7d-b18f-d40b3aa0d71c",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 100,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 79,
				"y": 28
			}
		},
		{
			"Key": 101,
			"Value": {
				"id": "feea1f56-65b5-47e8-aa6f-8f5cc7853dd4",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 101,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 57,
				"y": 28
			}
		},
		{
			"Key": 102,
			"Value": {
				"id": "4d5481f1-83f0-403c-b77f-626ec7f7cbae",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 102,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 46,
				"y": 28
			}
		},
		{
			"Key": 103,
			"Value": {
				"id": "35ee0357-95de-468f-824a-8392e5b61b21",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 103,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 35,
				"y": 28
			}
		},
		{
			"Key": 104,
			"Value": {
				"id": "837c13d2-63a9-485a-ae17-9ebf3cf164b9",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 104,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 24,
				"y": 28
			}
		},
		{
			"Key": 105,
			"Value": {
				"id": "b049367e-ec79-420b-9f6b-9aeb7a29c5d8",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 105,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 3,
				"x": 219,
				"y": 80
			}
		},
		{
			"Key": 106,
			"Value": {
				"id": "ffec479e-f6e8-4337-8e99-f13ece11b72f",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 106,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 2,
				"y": 28
			}
		},
		{
			"Key": 107,
			"Value": {
				"id": "11deffb5-5d35-47a4-b5c0-12b1cc7b9adf",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 107,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 242,
				"y": 2
			}
		},
		{
			"Key": 108,
			"Value": {
				"id": "7f40ff16-f537-4eb4-b577-ba40e39d4ff2",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 108,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 200,
				"y": 28
			}
		},
		{
			"Key": 109,
			"Value": {
				"id": "babeeb85-488d-43f4-83f2-734fd9d5ba00",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 109,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 101,
				"y": 80
			}
		},
		{
			"Key": 110,
			"Value": {
				"id": "152cd5d6-364f-4190-933a-2f63321c1639",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 110,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 13,
				"y": 28
			}
		},
		{
			"Key": 111,
			"Value": {
				"id": "253c971a-7d64-40fb-8927-e01d956ddd4f",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 111,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 112,
				"y": 28
			}
		},
		{
			"Key": 112,
			"Value": {
				"id": "6612ca4d-af2d-4467-b9c2-591097221086",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 112,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 145,
				"y": 28
			}
		},
		{
			"Key": 113,
			"Value": {
				"id": "dfc53385-e8cf-4963-a6f7-65a05f67389b",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 113,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 231,
				"y": 2
			}
		},
		{
			"Key": 114,
			"Value": {
				"id": "49b794a1-0d90-4f90-b5f9-c94dc9cba659",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 114,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 68,
				"y": 28
			}
		},
		{
			"Key": 115,
			"Value": {
				"id": "f1d7996d-454f-48b2-99b6-f0769410fbb7",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 115,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 13,
				"y": 80
			}
		},
		{
			"Key": 116,
			"Value": {
				"id": "2068c86a-250e-4791-9164-6cd49a60698f",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 116,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 24,
				"y": 80
			}
		},
		{
			"Key": 117,
			"Value": {
				"id": "923f5183-89bc-4157-9c0d-c6ad73926d46",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 117,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 123,
				"y": 54
			}
		},
		{
			"Key": 118,
			"Value": {
				"id": "fb335065-35ee-49a5-9214-f35f21fe3939",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 118,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 233,
				"y": 28
			}
		},
		{
			"Key": 119,
			"Value": {
				"id": "74fb0c5f-539b-4011-b025-f9934660b731",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 119,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 2,
				"y": 54
			}
		},
		{
			"Key": 120,
			"Value": {
				"id": "08f072a4-2ceb-4385-9829-36c2ff634bd9",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 120,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 35,
				"y": 54
			}
		},
		{
			"Key": 121,
			"Value": {
				"id": "e61d5509-0e3e-4950-b677-e640937add1a",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 121,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 46,
				"y": 54
			}
		},
		{
			"Key": 122,
			"Value": {
				"id": "9515b47a-0b62-4cab-b0c4-5f1a6199577e",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 122,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 57,
				"y": 54
			}
		},
		{
			"Key": 123,
			"Value": {
				"id": "1f12b156-acf4-4f9f-b618-fb216cc530de",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 123,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 112,
				"y": 54
			}
		},
		{
			"Key": 124,
			"Value": {
				"id": "1db127ee-4224-465f-940f-5e99103889b5",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 124,
				"h": 24,
				"offset": 0,
				"shift": 6,
				"w": 3,
				"x": 214,
				"y": 80
			}
		},
		{
			"Key": 125,
			"Value": {
				"id": "6c162d98-de3c-4c2f-be96-aa882653857a",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 125,
				"h": 24,
				"offset": 0,
				"shift": 12,
				"w": 9,
				"x": 132,
				"y": 2
			}
		},
		{
			"Key": 126,
			"Value": {
				"id": "6d1c80d4-5202-4c7c-9f3c-7710716c3101",
				"modelName": "GMGlyph",
				"mvc": "1.0",
				"character": 126,
				"h": 24,
				"offset": 0,
				"shift": 9,
				"w": 6,
				"x": 176,
				"y": 80
			}
		}
	],
	"image": null,
	"includeTTF": true,
	"italic": false,
	"kerningPairs": [],
	"last": 0,
	"ranges": [
		{
			"x": 32,
			"y": 127
		}
	],
	"sampleText": "Testing",
	"size": 18,
	"styleName": "Regular",
	"textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}