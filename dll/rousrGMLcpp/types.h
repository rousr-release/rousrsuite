#pragma once

#include "dllapi.h"

////
// C++
#include <stdio.h>

////
// Platform
#ifdef WIN32
#include <Windows.h>
#endif 

////
// STL
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <set>
#include <unordered_set>

#include <tuple>
#include <functional>
#include <thread>
#include <mutex>

#include <atomic>