// buildImGuiGML.js
//  babyjeans
///
let scryyptPath = "../scryypt/";

const fs            = require("fs-extra");
const child_process = require('child_process');
const YYP           = require(scryyptPath + "script/gms2/yyp.js");

const MarkDownIt       = require('markdown-it');
const MarkDownItAnchor = require('./submission/scripts/markdown-it-anchor.js');

var scryyptExec     = "node " + scryyptPath + "scryypt.js ";
var projects        = [ "rousr_suite", "rousr_extended" ];
var selectProjects  = [ "rousr_suite", "rousr_extended" ];
var justExtenionize = true;

fs.removeSync("extensionized/");
fs.ensureDirSync("extensionized/");

if (!justExtenionize) {
    console.log ("Flushing rousr_suite.yyp");
    fs.removeSync('./submission/yymp');
    child_process.execSync(scryyptExec + "--new submission/yymp.yyp");
    child_process.execSync("mv submission/yymp/yymp.yyp submission/yymp/rousr_suite.yyp");
}

let readme = "<link rel = \"stylesheet\" type = \"text/css\" href = \"retro.css\" />  \n\n";

projects.forEach( project => {
    if (selectProjects.length != 0 && selectProjects.indexOf(project) == -1)
        return;

    let yypPath       = "yyp/" + project;
    let yypFile       = yypPath + "/" + project + ".yyp";
    let extensionPath = "extensionized/" + project;
    let manifest      = project + ".json";

    let extensionFilename = project;
    extensionFilename = project[0].toUpperCase() + project.slice(1);
    let index = extensionFilename.indexOf("_");
    while (index > 0 && index + 1 < extensionFilename.length) {
        let str = "_" + extensionFilename[index + 1];
        extensionFilename = extensionFilename.replace(str, extensionFilename[index + 1].toUpperCase());
        index = extensionFilename.indexOf("_");
    }

    extensionFilename = "ext" + extensionFilename;

    console.log("Removing old extension: " + project);
    child_process.execSync('rm -rf ' + extensionPath);
    console.log("Copying yyp to : " + extensionPath);
    child_process.execSync('cp -R ' + yypPath + ' ' + extensionPath);

    var extensionFilepath = extensionPath + "/extensions/" + extensionFilename + "/";
    let extensionizedGML = extensionFilepath + extensionFilename + ".gml";
    console.log("Building gml and yy for " + extensionFilename + " (" + extensionizedGML + "): ...");
    
    var cmd = "node " + scryyptPath + "scryypt.js --extensionize ";
    cmd += manifest + " " + yypFile + " ";
    cmd += extensionizedGML;

    child_process.execSync(cmd, {stdio:[0,1,2]});

    if (!justExtenionize) {
        let extensionYYP = extensionPath + "/" + project + ".yyp";
        console.log("Building extension cryypt package");
        cmd = "node " + scryyptPath + "scryypt.js --build " +
            manifest + " " + extensionYYP + " " + project + "_ext.zip";
        child_process.execSync(cmd);

        console.log("Adding to rousr_suite...");
        cmd = "node " + scryyptPath + "scryypt.js --install " +
            project + "_ext.zip" + " " + "submission/yymp/rousr_suite.yyp";
        child_process.execSync(cmd, {stdio:[0,1,2]});

        fs.removeSync(project + "_ext.zip");

        readme += "## " + extensionFilename + "  \n\n";
        readme += fs.readFileSync(extensionFilepath + extensionFilename + ".md");
        readme += "\n\n"
        readme += "---   \n\n";
    }
});

if (!justExtenionize) {
    console.log("Updating http://suite.rou.sr/")
    let md = MarkDownIt({html:true}).use(MarkDownItAnchor, { });

    let indexhtml = md.render(readme);
    fs.writeFileSync("index.html", indexhtml);
    child_process.execSync("scp \"index.html\" grogshack:/home/webhost/suite.rou.sr/index.html", {stdio:[0,1,2]});
    fs.removeSync("extensionized/");
}